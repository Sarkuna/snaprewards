import React, { useState } from "react";
import { StyleSheet } from "react-native";
import * as Font from "expo-font";
import { AppLoading } from "expo";
import { createStore, combineReducers, applyMiddleware } from "redux";
import { Provider } from "react-redux";
import ReduxThunk from "redux-thunk";

//import { createStore} from "redux";
//import { combineReducers } from "redux";
//import { applyMiddleware } from "redux";

import authReducer from "./store/reducers/auth";
import myPageReducer from "./store/reducers/categories";
import pendingReducer from "./store/reducers/receipt";
import addressReducer from "./store/reducers/categories";
import bankReducer from "./store/reducers/categories";
import companyReducer from "./store/reducers/categories";
import AppNavigator from "./navigation/AppNavigator";
import { SafeAreaProvider } from "react-native-safe-area-context";

const rootReducer = combineReducers({
  //allProducts: productReducer,
  auth: authReducer,
  myPage: myPageReducer,
  pending: pendingReducer,
  address: addressReducer,
  bank: bankReducer,
  company: companyReducer,
});

const store = createStore(rootReducer, applyMiddleware(ReduxThunk));

const fetchFonts = () => {
  Font.loadAsync({
    "open-sans": require("./assets/fonts/OpenSans-Regular.ttf"),
    "open-sans-bold": require("./assets/fonts/OpenSans-Bold.ttf"),
    "roboto-medium": require("./assets/fonts/Roboto-Medium.ttf"),
    // "nexa-bold": require("./assets/fonts/Nexa-Bold.otf"),
    // "nexa-light": require("./assets/fonts/nexa-Light.otf"),
  });
};

export default function App() {
  const [fontLoaded, setFontLoaded] = useState(false);

  /*if (!fontLoaded) {
    return (
      <AppLoading
        startAsync={fetchFonts}
        onFinish={() => setFontLoaded(true)}
      />
    );
  }*/

  return (
    <SafeAreaProvider>
      <Provider store={store}>
        <AppNavigator />
      </Provider>
    </SafeAreaProvider>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "center",
  },
});
