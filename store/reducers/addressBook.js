import { SET_ADDRESS } from "../actions/addressBook";

const initialState = {
  availableCategories: [],
};

export default (state = initialState, action) => {
  switch (action.type) {
    case SET_ADDRESS:
      return {
        availableCategories: action.categories,
      };
  }
  // console.log(availableCategories)
  return state;
};
