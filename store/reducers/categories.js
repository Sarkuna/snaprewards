import {
  SET_PRODUCTS,
  SET_PENDING,
  SET_APPROVED,
  SET_PROCESSING,
  SET_ADDRESS,
  SET_BANK,
  SET_COMPANYINFO,
} from "../actions/categories";

const initialState = {
  availableCategories: [],
  availablePending: [],
  availableProcessing: [],
  availableApproved: [],
  totalItems: [],
  availableAddress: [],
  availableBanks: [],
  availableCompany: [],
};

export default (state = initialState, action) => {
  switch (action.type) {
    case SET_PRODUCTS:
      return {
        availableCategories: action.categories,
      };
    case SET_PENDING:
      return {
        availablePending: action.pendingAll,
      };
    case SET_APPROVED:
      return {
        availableApproved: action.approvedAll,
        totalItems: action.aprovedTotalItems,
      };
    case SET_PROCESSING:
      return {
        availableProcessing: action.processingAll,
      };
    case SET_ADDRESS:
      return {
        availableAddress: action.addressAll,
      };
    case SET_BANK:
      return {
        availableBanks: action.bankAll,
      };
    case SET_COMPANYINFO:
      return {
        availableCompany: action.companyAll,
      };
  }
  return state;
};
