import { SET_PRODUCTS } from "../actions/receipt";

const initialState = {
  availableCategories: [],
};

export default (state = initialState, action) => {
  switch (action.type) {
    case SET_PRODUCTS:
      return {
        availableCategories: action.categories,
      };
      
  }
 // console.log(availableCategories)
  return state;
};
