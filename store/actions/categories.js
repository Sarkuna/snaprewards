export const SET_PRODUCTS = "SET_PRODUCTS";
export const SET_PENDING = "SET_PENDING";
export const SET_APPROVED = "SET_APPROVED";
export const SET_PROCESSING = "SET_PROCESSING";
export const SET_ADDRESS = "SET_ADDRESS";
export const SET_BANK = "SET_BANK";
export const SET_COMPANYINFO = "SET_BANK";
import ENV from "./../../apiEnv";
import Category from "../../models/category";
import Pending from "../../models/receipt";
import Approved from "../../models/approvedReceipt";
import Address from "../../models/addressBook";
import Bank from "../../models/bankDetails";
import Company from "../../models/companyInfo";
import Approvedpoints from "../../models/receipt";
import PendingDetail from "../../components/PendingDetail";

export const fetchProducts = (myToken) => {
  // console.log(myToken);
  return async (dispatch) => {
    // any async code you want
    const response = await fetch(
      `${ENV.ApiKey}pages/menu?access-token=${myToken}`,
      {
        // default is get so dont need the method: 'GET',
      }
    );

    const resData = await response.json();
    const loadedCategories = [];
    //console.log(resData);
    for (const key in resData) {
      loadedCategories.push(
        new Category(
          resData[key].id,
          resData[key].title,
          resData[key].link,
          resData[key].target,
          resData[key].parent,
          resData[key].sort,
          resData[key].created_at,
          resData[key].updated_at,
          resData[key].icon_font,
          resData[key].menu_id,
          resData[key].related_id,
          resData[key].type,
          resData[key].depth
        )
      );
    }
    // console.log(loadedCategories);
    dispatch({ type: SET_PRODUCTS, categories: loadedCategories });
  };
};

export const addressDetails = (myToken) => {
  let id = 0;
  //console.log(myToken);
  return async (dispatch) => {
    // any async code you want
    const response = await fetch(
      `http://api.businessboosters.com.my/address-book/index?access-token=${myToken}`,
      {
        // default is get so dont need the method: 'GET',
      }
    );

    const resData = await response.json();
    const loadedCategories = [];

    const resData1 = resData.address_lists;
    console.log(resData1);
    for (const key in resData1) {
      loadedCategories.push(
        new Address(
          resData1[key].address,
          resData1[key].country,
          resData1[key].default_address,
          resData1[key].id,
          resData1[key].name,
          resData1[key].postcode,
          resData1[key].stateid,
          resData1[key].statename
        )
      );
    }
    // console.log(pendingCategorioes);
    dispatch({ type: SET_ADDRESS, addressAll: loadedCategories });
  };
};

export const addressEditDetails = (myToken) => {
  let id = 0;
  //console.log(myToken);
  return async (dispatch) => {
    // any async code you want
    const response = await fetch(
      `http://api.businessboosters.com.my/address-book/index?access-token=${myToken}`,
      {
        // default is get so dont need the method: 'GET',
      }
    );

    const resData = await response.json();
    const loadedCategories = [];
    console.log(resData);
    const resData1 = resData.address_lists;
    console.log(resData1);
    for (const key in resData1) {
      loadedCategories.push(
        new Address(
          resData1[key].address,
          resData1[key].country,
          resData1[key].default_address,
          resData1[key].id,
          resData1[key].name,
          resData1[key].postcode
        )
      );
    }
    // console.log(pendingCategorioes);
    dispatch({ type: SET_ADDRESS, addressAll: loadedCategories });
  };
};

export const bankDetails = (myToken) => {
  let id = 0;
  //console.log(myToken);
  return async (dispatch) => {
    // any async code you want
    const response = await fetch(
      `http://api.businessboosters.com.my/banks/index?access-token=${myToken}`,
      {
        // default is get so dont need the method: 'GET',
      }
    );

    const resData = await response.json();
    const loadedBankDetails = [];
    //console.log(resData);
    const resData1 = resData.bank_lists;
    //console.log(resData1);
    for (const key in resData1) {
      loadedBankDetails.push(
        new Bank(
          resData1[key].id,
          resData1[key].bank_name,
          resData1[key].account_name,
          resData1[key].account_number,
          resData1[key].nric_passport,
          resData1[key].verify
        )
      );
    }
    // console.log(pendingCategorioes);
    dispatch({ type: SET_BANK, bankAll: loadedBankDetails });
  };
};

export const companyDetails = (myToken) => {
  let id = 0;
  //console.log(myToken);
  return async (dispatch) => {
    // any async code you want
    const response = await fetch(
      `http://api.businessboosters.com.my/profile/company?access-token=${myToken}`,
      {
        // default is get so dont need the method: 'GET',
      }
    );

    const resData = await response.json();
    const loadedCategories = [];
    // console.log(resData);
    const resData1 = resData.data;
    //console.log(resData1);
    for (const key in resData) {
      loadedCategories.push(
        new Company(
          resData[key].company_name,
          resData[key].contact_no,
          resData[key].contact_person,
          resData[key].fax,
          resData[key].mailing_address,
          resData[key].registration_number,
          resData[key].registration_type,
          resData[key].tel
        )
      );
    }
    console.log(loadedCategories);
    dispatch({ type: SET_COMPANYINFO, companyAll: loadedCategories });
  };
};

export const pendingDetails = (myToken) => {
  let id = 0;
  //console.log(myToken);
  return async (dispatch) => {
    // any async code you want
    const response = await fetch(
      `http://api.businessboosters.com.my/receipt/receipt-list-pending?access-token=${myToken}`,
      {
        // default is get so dont need the method: 'GET',
      }
    );

    const resData = await response.json();
    const pendingCategorioes = [];
    //console.log(resData.receipts);
    const resData1 = resData.receipts;
    for (const key in resData1) {
      pendingCategorioes.push(
        new Pending(
          resData1[key].vip_receipt_id,
          resData1[key].created_datetime,
          resData1[key].date_of_receipt,
          resData1[key].invoice_no,
          resData1[key].reseller_name
        )
      );
    }
    // console.log(pendingCategorioes);
    dispatch({ type: SET_PENDING, pendingAll: pendingCategorioes });
  };
};

export const processingDetails = (myToken) => {
  let id = 0;
  //console.log(myToken);
  return async (dispatch) => {
    // any async code you want
    const response = await fetch(
      `http://api.businessboosters.com.my/receipt/receipt-list-processing?access-token=${myToken}`,
      {
        // default is get so dont need the method: 'GET',
      }
    );

    const resData = await response.json();
    const processingCategorioes = [];
    //console.log(resData.receipts);
    const resData1 = resData.receipts;
    for (const key in resData1) {
      processingCategorioes.push(
        new Pending(
          resData1[key].vip_receipt_id,
          resData1[key].created_datetime,
          resData1[key].date_of_receipt,
          resData1[key].invoice_no,
          resData1[key].reseller_name
        )
      );
    }
    // console.log(pendingCategorioes);
    dispatch({ type: SET_PROCESSING, processingAll: processingCategorioes });
  };
};

export const ApprovedDetails = (myToken) => {
  let id = 0;
  console.log(myToken);
  return async (dispatch) => {
    // any async code you want
    const response = await fetch(
      `http://api.businessboosters.com.my/receipt/receipt-list-approves?access-token=${myToken}`,
      {
        // default is get so dont need the method: 'GET',
      }
    );

    const resData = await response.json();
    const approvedCategorioes = [];
    //console.log(resData.receipts);
    let resData1 = resData.receipts;
    let resData2 = resData.totalitem;
    let resData3 = resData.totalpoint;
    let resData4 = resData.resellername;
    let i;
    let length = resData1.length;
    var combinedArray1 = [];
    var cat1;
    for (i = 0; i < length; i++) {
      cat1 = Object.assign(
        {},
        resData1[i],
        { totalItem: resData2[i] },
        { totalpoint: resData3[i] },
        { resellername: resData4[i] }
      );
      combinedArray1 = combinedArray1.concat(cat1);
    }
    console.log(resData1);
    console.log(combinedArray1);
    for (const key in combinedArray1) {
      approvedCategorioes.push(
        new Approved(
          combinedArray1[key].vip_receipt_id,
          combinedArray1[key].created_datetime,
          combinedArray1[key].date_of_receipt,
          combinedArray1[key].invoice_no,
          combinedArray1[key].reseller_name,
          combinedArray1[key].totalItem,
          combinedArray1[key].totalpoint,
          combinedArray1[key].resellername
        )
      );
    }
    console.log(approvedCategorioes);
    dispatch({
      type: SET_APPROVED,
      approvedAll: approvedCategorioes,
      aprovedTotalItems: resData2,
    });
  };
};
