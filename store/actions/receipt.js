export const SET_PRODUCTS = "SET_PRODUCTS";

import ENV from "./../../apiEnv";
import Category from "../../models/receipt";

export const fetchProducts = (myToken) => {
  let id = 0
  console.log(myToken)
  return async (dispatch) => {
    // any async code you want
    const response = await fetch(`http://api.businessboosters.com.my/receipt/receipt-list-pending?access-token=${myToken}`, {
      // default is get so dont need the method: 'GET',
    });

    const resData = await response.receipts.json();
    const loadedCategories = []
   // console.log(resData);
    
     for (const key in resData) {
       loadedCategories.push(
        new Category(
          resData[key].created_datetime,
          resData[key].date_of_receipt,
          resData[key].invoice_no,
          resData[key].reseller_name,
        )
      );
    }
  // console.log(loadedCategories);
    dispatch({ type: SET_PRODUCTS, categories: loadedCategories });
  };
};
