import { AsyncStorage } from "react-native";
import { Alert } from "react-native";
// export const SIGNUP = "SIGNUP";
// export const LOGIN = "LOGIN";
export const AUTHENTICATE = "AUTHENTICATE";
export const LOGOUT = "LOGOUT";
export const SUBMITRECEIPT = "SUBMITRECEIPT";

let timer;

export const authenticate = (Type, access_token, userinfo) => {
  return (dispatch) => {
    dispatch({
      type: AUTHENTICATE,
      Type: Type,
      access_token: access_token,
      userinfo: userinfo,
    });
  };
};

export const signup = (
  clientID,
  salutation,
  name,
  company,
  email,
  mobile,
  pass1
) => {
  return async (dispatch) => {
    const response = await fetch(
      "http://api.businessboosters.com.my/login/signup",
      {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify({
          clientID: clientID,
          salutation: salutation,
          full_name: name,
          company_name: company,
          email: email,
          mobile_no: mobile,
          password: pass1,
          returnSecureToken: true,
        }),
      }
    );

    if (!response.ok) {
      throw new Error("Something went wrong");
    }

    const resData = await response.json();
    console.log(resData);
    dispatch(authenticate(resData.Type, resData.access_token));
  };
};

export const login = (email, password) => {
  console.log(email);
  return async (dispatch) => {
    const response = await fetch("http://biz.businessboosters.com.my/api/auth/login", { // change to http://biz.businessboosters.com.my/api/auth/login
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        "Accept": "application/json",
      },
      body: JSON.stringify({
        email: email,
        password: password,
        clientID: 10,
      }),
    });

    if (!response.ok) {
      throw new Error("Something went wrong");
    }
    const resData = await response.json();
    if (resData.message === false) {
      throw new Error(
        "Something went wrong, Please check your mail and password"
      );
    } else {
      console.log(resData.token);
      dispatch(
        authenticate(resData.Type, resData.access_token, resData.userinfo)
      );
    }
  };
};

export const submitReceipt = (data) => {
  //console.log(receiptNumber)
  // console.log(selectedDate)
  // const invoice_no = receiptNumber
  // const  date_of_receipt =  selectedDate
  console.log(data);
  return async (dispatch) => {
    const response = await fetch(
      "http://api.businessboosters.com.my/receipt/upload-receipt",
      {
        method: "POST",

        body: data,
      }
    );
    // if (!response.ok) {
    //     throw new Error("Something went wrong");
    // }
    const resData = await response.json();
    console.log(resData);
    //dispatch(resData.invoice_no, resData.date_of_receipt);
  };
};

export const logout = () => {
  clearLogoutTimer();
  AsyncStorage.removeItem("userData");
  return { type: LOGOUT };
};

const clearLogoutTimer = () => {
  if (timer) {
    clearTimeout(timer);
  }
};

const setLogoutTimer = (expirationTime) => {
  return (dispatch) => {
    timer = setTimeout(() => {
      dispatch(logout());
    }, expirationTime / 1000);
  };
};

const saveDataToStorage = (token, userId, expirationDate) => {
  AsyncStorage.setItem(
    "userData",
    JSON.stringify({
      token: token,
      userId: userId,
      expiryDate: expirationDate.toISOString(),
    })
  ); // save data to phone storage
};

// let localUri = pickedImage.uri;
// let filename = localUri.split('/').pop();
// // console.log(filename)
// let match = /\.(\w+)$/.exec(filename);
// // console.log(match)
// let type = match ? `image/${match[1]}` : `image`;
// //    console.log(type)
// let formData = new FormData();
// formData.append('file', upfile);
// formData.append('upfile[0]', { uri: localUri, name: filename, type });
