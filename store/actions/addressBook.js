export const SET_ADDRESS = "SET_ADDRESS";

import ENV from "../../apiEnv";
import Category from "../../models/addressBook";

export const fetchProducts = (myToken) => {
  let id = 0;
  console.log(myToken);
  return async (dispatch) => {
    // any async code you want
    const response = await fetch(
      `http://api.businessboosters.com.my/address-book/index?access-token=${myToken}`,
      {
        // default is get so dont need the method: 'GET',
      }
    );

    const resData = await response.data.address_lists.json();
    const loadedCategories = [];
    console.log(resData);

    for (const key in resData) {
      loadedCategories.push(
        new Category(
          resData[key].address,
          resData[key].country,
          resData[key].defaultAddress,
          resData[key].id,
          resData[key].name,
          resData[key].postcode
        )
      );
    }
    // console.log(loadedCategories);
    dispatch({ type: SET_ADDRESS, categories: loadedCategories });
  };
};
