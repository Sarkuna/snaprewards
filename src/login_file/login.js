import React, { useState, useEffect } from "react";
import gS from "../globalStyle";

import { Checkbox, HelperText, TextInput } from "react-native-paper";
import {
  StyleSheet,
  Text,
  View,
  Image,
  SafeAreaView,
  TouchableOpacity,
  Keyboard,
  ToastAndroid,
  Alert,
} from "react-native";
import { useDispatch } from "react-redux";
import * as authActions from "../../store/actions/auth";

const login = (props) => {
  //Declaration

  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [checked, setChecked] = useState(false);
  const [titleIsValid, setTitleIsValid] = useState(false);

  const [passwordIsValid, setPasswordIsValid] = useState(false);
  const [errorEmailPass, setErrorEmailPass] = useState(false);
  const [blankEmailPass, setBlankEmailPass] = useState(false);
  const [emailError, setEmailError] = useState(false);
  const [passError, setPassError] = useState(false);

  const [error, setError] = useState();
  const dispatch = useDispatch();

  const emailHandler = (text) => {
    setEmail(text);
    if (email == "") setEmailError(true);
    else setEmailError(false);
  };

  const titleChangeHandler = (text) => {
    if (text.trim().length === 0) {
      setTitleIsValid(false);
    } else {
      setTitleIsValid(true);
    }
    setEmail(text);
  };

  const passwordChangeHandler = (text) => {
    if (text.trim().length === 0) {
      setPasswordIsValid(false);
    } else {
      setPasswordIsValid(true);
    }
    setPassword(text);
  };

  const passwordHandler = (text) => {
    setPassword(text);
    if (password == "") setPassError(true);
    else setPassError(false);
  };

  useEffect(() => {
    if (error) {
      // Alert.alert("Oops!", error, [{ text: "Okay" }]);
      setErrorEmailPass(false);
    } else {
      setErrorEmailPass(true);
    }
  }, [error]);

  const authHandler = async () => {
    if (!passwordIsValid || !titleIsValid) {
      setErrorEmailPass(false);
    } else {
      let action;
      action = authActions.login(email, password);

      setError(null);
      //setIsLoading(true);
      try {
        await dispatch(action);
        props.navigation.navigate("Home");
        setEmail("");
        setPassword("");
      } catch (err) {
        console.log(err);
        setError(err.message);
      }
      //setIsLoading(false);
    }
  };

  return (
    <SafeAreaView style={gS.container}>
      <Image
        source={require("C:/React_Native/snaprewards/assets/mv_logo.png")}
        style={gS.logo}
      />

      <View style={styles.field}>
        <View>
          <TextInput
            label="Email"
            autoCompleteType="email"
            mode="outlined"
            value={email}
            keyboardType="email-address"
            onChangeText={titleChangeHandler}
          />
          {!errorEmailPass && !titleIsValid && (
            <HelperText type="error">Email cannot be blank.</HelperText>
          )}
        </View>
        <TextInput
          label="Password"
          secureTextEntry
          mode="outlined"
          value={password}
          onChangeText={passwordChangeHandler}
        />
        {!errorEmailPass && !passwordIsValid && (
          <HelperText type="error">Password cannot be blank.</HelperText>
        )}
        {passwordIsValid === true ? (
          !errorEmailPass && (
            <HelperText type="error">Incorrect email or password</HelperText>
          )
        ) : (
          <HelperText type="error"></HelperText>
        )}
      </View>

      <View style={gS.hori}>
        <View style={{ margin: 10 }}>
          <Checkbox
            status={checked ? "checked" : "unchecked"}
            onPress={() => {
              setChecked(!checked);
            }}
            color="#0168B7"
          />
        </View>
        <Text style={gS.wordBlackSmall}>Remember me</Text>
        <Text
          style={gS.wordBlueSmall}
          onPress={() => props.navigation.navigate("ResetPassword")}
        >
          Forgot password?
        </Text>
      </View>
      <View style={{ justifyContent: "center", alignItems: "stretch" }}>
        <TouchableOpacity onPress={authHandler}>
          <View style={gS.blueBtn}>
            <Text style={gS.blueBtnWord}> Login </Text>
          </View>
        </TouchableOpacity>
        <View style={gS.hori}>
          <Text style={gS.wordBlackSmall}>Don't have an account?</Text>
          <Text
            style={gS.wordBlueSmall}
            onPress={() => props.navigation.navigate("Register")}
          >
            Sign up here!
          </Text>
        </View>
      </View>
      <View style={{ marginTop: 50 }}>
        <Text style={styles.poweredText}>Powered by Moms Village Asia</Text>
        <Text style={styles.poweredText}>
          Email : support@momsvillageasia.com
        </Text>
        <Text style={styles.poweredText}>
          Phone : +6011-18889597 Mobile : +6011-18889597
        </Text>
        <Text style={{ textAlign: "center", color: "#ADACAC", fontSize: 10 }}>
          V 3.17
        </Text>
        <View>
          <Text></Text>
        </View>
      </View>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  btn: {
    width: 300,
    height: 70,
    color: "#0168B7",
    marginVertical: 5,
  },
  field: {
    width: "70%",
    fontSize: 16,
  },
  poweredText: {
    textAlign: "center",
    color: "#ADD8E6",
    fontSize: 11.5,
  },
});

export default login;
