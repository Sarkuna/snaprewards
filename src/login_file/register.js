import React, { useState, useEffect } from "react";
import axios from "axios";
import gS from "../globalStyle";
import { TouchableWithoutFeedback } from "react-native-gesture-handler";
import {
  Alert,
  Text,
  View,
  TextInput,
  SafeAreaView,
  ScrollView,
  Switch,
  StyleSheet,
  Picker,
} from "react-native";
import * as SecureStore from "expo-secure-store";
import { HelperText } from "react-native-paper";

import { useSelector } from "react-redux";
import Colors from "../../constants/Colors";
import { TouchableOpacity } from "react-native";
import DateTimePickerModal from "react-native-modal-datetime-picker";
import moment from "moment";

const editCompany = (props) => {
  const myToken = useSelector((state) => state.auth.access_token);

  const [saluationIsValid, setSaluationIsValid] = useState(false);
  const [fullnameIsValid, setFullnameIsValid] = useState(false);
  const [companyNameIsValid, setCompanyNameIsValid] = useState(false);
  const [ssmNumberIsValid, setSsmNumberIsValid] = useState(false);
  const [incomeTaxNumberIsValid, setIncomeTaxNumberIsValid] = useState(false);
  const [companyAddressIsValid, setCompanyAddressIsValid] = useState(false);
  const [emailIsValid, setEmailIsValid] = useState(false);
  const [passwordIsValid, setPasswordIsValid] = useState(false);
  const [rePasswordIsValid, setRePasswordIsValid] = useState(false);
  const [mobileNumberIsValid, setMobileNumberIsValid] = useState(false);
  const [passwordMatch, setPasswordMatch] = useState(false);
  const [submitValidation, setSubmitValidation] = useState(false);
  const [error, setError] = useState();

  const submitUrl = global.url + "/login/signup";

  useEffect(() => {
    if (error) {
      // Alert.alert("Oops!", error, [{ text: "Okay" }]);
      setSubmitValidation(false);
    } else {
      setSubmitValidation(true);
    }
  }, [error]);

  const [frmfullName, setfrmFullName] = useState("");
  const [frmcompanyName, setfrmCompanyName] = useState("");
  const [frmSsmNumber, setFrmSsmNumber] = useState("");
  const [frmIncomeTaxNumber, setFrmIncomeTaxNumber] = useState("");
  const [frmCompanyAddress, setFrmCompanyAddress] = useState("");
  const [frmPassword, setFrmPassword] = useState("");
  const [frmRePassword, setFrmRePassword] = useState("");
  const [regType, setRegType] = useState("");
  const [frmEmail, SetFrmEmail] = useState("");
  const [frmMobileNum, setFrmMobileNum] = useState("");

  console.log(
    !saluationIsValid,
    !fullnameIsValid,
    !emailIsValid,
    !companyNameIsValid,
    !passwordIsValid,
    !rePasswordIsValid,
    passwordMatch
  );
  function handler() {
    if (
      !saluationIsValid ||
      !fullnameIsValid ||
      !emailIsValid ||
      // !mobileNumberIsValid ||
      !companyNameIsValid ||
      // !ssmNumberIsValid ||
      // !incomeTaxNumberIsValid ||
      // !companyAddressIsValid ||
      !passwordIsValid ||
      !rePasswordIsValid
    ) {
      setSubmitValidation(false);
      setPasswordMatch(false);
      console.log("Not Submit");
    } else {
      props.navigation.goBack(),
        axios
          .post(submitUrl, {
            // vip_customer_id: vip_customer_id,
            // salutation: regType,
            // full_name: frmfullName,
            // date_of_birth: date_of_Birth,
            // mobile_no: frmMobileNum,
            // nric: frmNricPass,
            // telephone_no: frmTellNum,
            // Race: frmRace,
            // country_id: frmCountry,
            // Region: frmRegion,
            // nationality_id: frmNationality,
            // gender: frmGender,
            clientID: 10,
            salutation: regType,
            full_name: frmfullName,
            company_name: frmcompanyName,
            email: frmEmail,
            mobile_no: frmMobileNum,
            password: frmPassword,
          })
          .then(function (response) {
            console.log(response.data);
            if (response.data.message === true) {
              alert("Succesfully created.");
            } else if (response.data.message === false) {
              alert("Somthing went wrong.");
            } else {
              Alert.alert(
                "Signup Failed",
                "Your email or phone number is already taken.",
                [{ text: "Okay" }]
              );
            }
          })
          .catch(function (error) {
            console.log(error);
          });
    }
  }

  // const handler = () => {
  //   Alert.alert(
  //     "Confirm to edit ?",
  //     "",
  //     [
  //       {
  //         text: "Cancel",
  //         onPress: () => console.log("Cancel Pressed"),
  //         style: "cancel",
  //       },
  //       {
  //         text: "Confirm",
  //         onPress: () => {
  //           props.navigation.goBack(),
  //             axios
  //               .post(submitUrl, {
  //                 vip_customer_id: vip_customer_id,
  //                 salutation: regType,
  //                 full_name: frmfullName,
  //                 date_of_birth: date_of_Birth,
  //                 mobile_no: frmMobileNum,
  //                 nric: frmNricPass,
  //                 telephone_no: frmTellNum,
  //                 Race: frmRace,
  //                 country_id: frmCountry,
  //                 Region: frmRegion,
  //                 nationality_id: frmNationality,
  //                 gender: frmGender,
  //               })
  //               .then(function (response) {
  //                 console.log(response.data);
  //                 if (response.data.message === "Success") {
  //                   alert("Succesfully edited");
  //                 } else {
  //                   alert("Somthing went wrong");
  //                 }
  //               })
  //               .catch(function (error) {
  //                 console.log(error);
  //               });
  //         },
  //       },
  //     ],
  //     { cancelable: false }
  //   );
  // };

  const changeSaluation = (itemValue) => {
    if (regType === 0 || regType === "0") {
      setSaluationIsValid(false);
    } else {
      setSaluationIsValid(true);
    }
    setRegType(itemValue);
  };

  const changeFullname = (text) => {
    if (text.trim().length === 0) {
      setFullnameIsValid(false);
    } else {
      setFullnameIsValid(true);
    }
    setfrmFullName(text);
  };

  const changeCompanyName = (text) => {
    if (text.trim().length === 0) {
      setCompanyNameIsValid(false);
    } else {
      setCompanyNameIsValid(true);
    }
    setfrmCompanyName(text);
  };

  const changeSsmNumber = (text) => {
    if (text.trim().length === 0) {
      setSsmNumberIsValid(false);
    } else {
      setSsmNumberIsValid(true);
    }
    setFrmSsmNumber(text);
  };

  const changeIncomeTaxNumber = (text) => {
    if (text.trim().length === 0) {
      setIncomeTaxNumberIsValid(false);
    } else {
      setIncomeTaxNumberIsValid(true);
    }
    setFrmIncomeTaxNumber(text);
  };

  const changeCompanyAddress = (text) => {
    if (text.trim().length === 0) {
      setCompanyAddressIsValid(false);
    } else {
      setCompanyAddressIsValid(true);
    }
    setFrmCompanyAddress(text);
  };

  const changeEmail = (text) => {
    if (text.trim().length === 0) {
      setEmailIsValid(false);
    } else {
      setEmailIsValid(true);
    }
    SetFrmEmail(text);
  };

  const changeMobileNum = (text) => {
    if (text.trim().length === 0) {
      setMobileNumberIsValid(false);
    } else {
      setMobileNumberIsValid(true);
    }
    setFrmMobileNum(text);
  };

  const changePassword = (text) => {
    if (text.trim().length === 0) {
      setPasswordIsValid(false);
    } else {
      setPasswordIsValid(true);
    }
    setFrmPassword(text);
  };

  const changeRePassword = (text) => {
    if (text.trim().length === 0) {
      setRePasswordIsValid(false);
    } else {
      setRePasswordIsValid(true);
    }
    setFrmRePassword(text);
  };

  return (
    <SafeAreaView style={gS.container}>
      <ScrollView style={gS.scroll}>
        <View style={gS.section2}>
          <View style={styles.containerForm}>
            <View style={gS.section2}>
              <View style={styles.formControl}>
                <View style={styles.form}>
                <View style={{ marginTop: 30 }}>
                  <Text style={styles.label}>Fullname (as per IC)</Text>
                  <TextInput
                    style={styles.input}
                    id="title"
                    errorText="Please enter a valid name"
                    placeholder={"John Doe"}
                    keyboardType="default"
                    autoCapitalize="sentences"
                    autoCorrect
                    returnKeyType="next"
                    onChangeText={changeFullname}
                    required
                    value={frmfullName}
                  />
                  {!submitValidation &&
                    !fullnameIsValid &&
                    frmfullName === "" && (
                      <HelperText type="error">
                        Fullname cannot be blank.
                      </HelperText>
                    )}
                    </View>
                  {/* <Text style={styles.label}>Fullname</Text>
                  <TextInput
                    style={styles.input}
                    id="title"
                    errorText="Please enter a valid title"
                    keyboardType="default"
                    autoCapitalize="sentences"
                    autoCorrect
                    returnKeyType="next"
                    onChangeText={changeFullname}
                    required
                    value={frmfullName}
                  />
                  {!submitValidation &&
                    !fullnameIsValid &&
                    frmfullName === "" && (
                      <HelperText type="error">
                        Fullname cannot be blank.
                      </HelperText>
                    )} */}
                 {/* <Text style={styles.label}>Company name</Text>
                  <TextInput
                    style={styles.input}
                    id="title"
                    errorText="Please enter a valid title"
                    keyboardType="default"
                    autoCapitalize="sentences"
                    autoCorrect
                    returnKeyType="next"
                    onChangeText={changeCompanyName}
                    required
                    value={frmcompanyName}
                  />*/}
                  {/*!submitValidation &&
                    !companyNameIsValid &&
                    frmMobileNum === "" && (
                      <HelperText type="error">
                        Company Name cannot be blank.
                      </HelperText>
                    )*/}
                  {/* <Text style={styles.label}>SSM Number</Text>
                  <TextInput
                    style={styles.input}
                    id="title"
                    errorText="Please enter a valid title"
                    keyboardType="default"
                    autoCapitalize="sentences"
                    autoCorrect
                    returnKeyType="next"
                    onChangeText={changeSsmNumber}
                    required
                    value={frmSsmNumber}
                  /> */}
                  {/* {!submitValidation &&
                    !ssmNumberIsValid &&
                    frmSsmNumber === "" && (
                      <HelperText type="error">
                        SSM number cannot be blank.
                      </HelperText>
                    )} */}
                  {/* <Text style={styles.label}>Income Tax No</Text>
                  <TextInput
                    style={styles.input}
                    id="title"
                    errorText="Please enter a valid title"
                    keyboardType="default"
                    autoCapitalize="sentences"
                    autoCorrect
                    returnKeyType="next"
                    onChangeText={changeIncomeTaxNumber}
                    required
                    value={frmIncomeTaxNumber}
                  /> */}
                  {/* {!submitValidation &&
                    !incomeTaxNumberIsValid &&
                    frmIncomeTaxNumber === "" && (
                      <HelperText type="error">
                        Income tax number cannot be blank.
                      </HelperText>
                    )} */}
                  {/* <Text style={styles.label}>Company address</Text>
                  <TextInput
                    style={styles.input}
                    id="title"
                    errorText="Please enter a valid title"
                    keyboardType="default"
                    autoCapitalize="sentences"
                    autoCorrect
                    returnKeyType="next"
                    onChangeText={changeCompanyAddress}
                    required
                    value={frmCompanyAddress}
                  /> */}
                  {/* {!submitValidation &&
                    !companyAddressIsValid &&
                    frmCompanyAddress === "" && (
                      <HelperText type="error">
                        Company Address cannot be blank.
                      </HelperText>
                    )} */}
                    <Text style={styles.label}>Mobile Number</Text>
                  <TextInput
                    style={styles.input}
                    id="title"
                    placeholder={"eg. 60XX-XXXXXXXX"}
                    errorText="Please enter a valid title"
                    keyboardType="default"
                    autoCapitalize="sentences"
                    autoCorrect
                    returnKeyType="next"
                    onChangeText={changeMobileNum}
                    required
                    value={frmMobileNum}
                  />
                  {!submitValidation &&
                    !mobileNumberIsValid &&
                    frmMobileNum === "" && (
                      <HelperText type="error">
                        Mobile Number cannot be blank.
                      </HelperText>
                    )}
                  <Text style={styles.label}>Email</Text>
                  <TextInput
                    style={styles.input}
                    id="title"
                    placeholder={"eg. abc123@momsvillageasia.com"}
                    errorText="Please enter a valid title"
                    keyboardType="default"
                    autoCapitalize="sentences"
                    autoCorrect
                    returnKeyType="next"
                    onChangeText={changeEmail}
                    required
                    value={frmEmail}
                  />
                  {!submitValidation && !emailIsValid && frmEmail === "" && (
                    <HelperText type="error">Email cannot be blank.</HelperText>
                  )}
                  
                    <Text style={styles.label}>Member Name</Text>
                    <View
                      style={{
                        color: "#F00",
                        // borderBottomWidt: 10,
                        borderColor: "#ccc",
                        borderBottomWidth: 1,
                      }}
                    >
                      <Picker
                        selectedValue={regType}
                        onValueChange={changeSaluation}
                      >
                        <Picker.Item
                          style={{ color: "#F00", borderBottomWidt: 10 }}
                          label="- Member Name -"
                          value="0"
                        />
                        <Picker.Item label="ABC Pvt Ltd" value={1}></Picker.Item>
                        <Picker.Item label="Business Booster Sdn Bhd" value={2}></Picker.Item>
                        <Picker.Item label="Maxis Sdn Bhd" value={3}></Picker.Item>                       
                      </Picker>
                    </View>
                    {!submitValidation && !saluationIsValid && (
                      <HelperText type="error">
                        Member cannot be blank.
                      </HelperText>
                    )}                  
                  <Text style={styles.label}>Password</Text>
                  <TextInput
                    style={styles.input}
                    placeholder={"*******"}
                    //id="title"
                    //errorText="Please enter a valid title"
                    //keyboardType="default"
                    //autoCapitalize="sentences"
                    //autoCorrect
                    secureTextEntry={true}
                    returnKeyType="next"
                    onChangeText={changePassword}
                    //required
                    value={frmPassword}
                  />
                  {!submitValidation &&
                    !passwordIsValid &&
                    frmPassword === "" && (
                      <HelperText type="error">
                        Password cannot be blank.
                      </HelperText>
                    )}
                  <Text style={styles.label}>Re-Password</Text>
                  <TextInput
                    style={styles.input}
                    placeholder={"*******"}
                    //id="title"
                    //errorText="Please enter a valid title"
                    //keyboardType="default"
                    //autoCapitalize="sentences"
                    //autoCorrect
                    secureTextEntry={true}
                    returnKeyType="next"
                    onChangeText={changeRePassword}
                    //required
                    value={frmRePassword}
                  />
                  {!submitValidation &&
                    !rePasswordIsValid &&
                    frmRePassword === "" && (
                      <HelperText type="error">
                        Re-Password cannot be blank.
                      </HelperText>
                    )}
                  {!passwordMatch && frmRePassword != frmPassword && (
                    <HelperText type="error">Passwords dont match.</HelperText>
                  )}
                </View>
              </View>
            </View>
          </View>
          {/* <View style={styles.containerFormDefault}>
            <FiltersSwitch
              label="Set as default address"
              state={isVeganFree}
              onChange={(newValue) => setIsVeganFree(newValue)}
            />
          </View> */}

          {/* <View style={styles.containerFormDefault2}>
            <TouchableOpacity onPress={() => handlerAddressDelete()}>
              <Text style={styles.DeleteAddText}>Delete</Text>
            </TouchableOpacity>
          </View> */}

          <TouchableWithoutFeedback onPress={handler}>
            <View style={gS.blueBtn}>
              <Text style={gS.blueBtnWord}>Register</Text>
            </View>
          </TouchableWithoutFeedback>
        </View>
      </ScrollView>
    </SafeAreaView>
  );
};

export default editCompany;

const styles = StyleSheet.create({
  containerForm: {
    shadowColor: "#f9f5f5",
    shadowOpacity: 0.26,
    shadowOffset: { width: 0, height: 2 },
    shadowRadius: 8,
    elevation: 1,
    borderRadius: 5,
    borderLeftColor: "#f9f5f5",
    borderStartColor: "#f9f5f5",
    borderColor: "#f9f5f5",
    // height: "90%",
    width: "100%",
    // margin: 5,
    backgroundColor: "#FFFFFF",
  },
  containerFormDefault: {
    marginTop: 10,
    shadowColor: "#f9f5f5",
    shadowOpacity: 0.26,
    shadowOffset: { width: 0, height: 2 },
    shadowRadius: 8,
    elevation: 1,
    borderRadius: 5,
    borderLeftColor: "#f9f5f5",
    borderStartColor: "#f9f5f5",
    borderColor: "#f9f5f5",
    height: "15%",
    width: "100%",

    backgroundColor: "#FFFFFF",
  },
  containerFormDefault2: {
    marginTop: 10,

    shadowColor: "#f9f5f5",
    shadowOpacity: 0.26,
    shadowOffset: { width: 0, height: 2 },
    shadowRadius: 8,
    elevation: 1,
    borderRadius: 5,
    // borderLeftColor: "#f9f5f5",
    // borderStartColor: "#f9f5f5",
    borderColor: "#f9f5f5",
    height: "15%",
    width: "100%",
    // margin: 5,
    backgroundColor: "#FFFFFF",
  },
  filterContainer: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    marginVertical: 10,
    width: "80%",
  },
  title: {
    fontFamily: "open-sans-bold",
    fontSize: 17,
    margin: 20,
    textAlign: "center",
    justifyContent: "center",
  },
  DeleteAddText: {
    fontFamily: "open-sans-bold",
    fontSize: 17,
    margin: 20,
    color: "#a90308",
    // textAlign: "center",
  },
  form: {
    margin: 10,
  },
  formControl: {
    width: "100%",
  },
  label: {
    fontFamily: "open-sans-bold",
    marginVertical: 8,
  },
  input: {
    paddingHorizontal: 2,
    paddingVertical: 2,
    borderBottomColor: "#ccc",
    borderBottomWidth: 1,
    fontSize: 18,
  },
  input2True: {
    // paddingHorizontal: 2,
    // paddingVertical: 5,
    // borderBottomColor: "#ccc",
    // borderBottomWidth: 1,
    color: "#000000",
    //marginLeft: -2,
    //paddingLeft: 0,
  },
});
