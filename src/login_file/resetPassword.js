import React, { useState } from "react";
import gS from "../globalStyle";
import axios from "axios";
import {
  Alert,
  StyleSheet,
  Text,
  View,
  Image,
  TextInput,
  SafeAreaView,
  Keyboard,
} from "react-native";
import { TouchableOpacity } from "react-native-gesture-handler";

export default function App({ navigation }) {
  console.log("Reset Password Page");

  const [email, setEmail] = useState("");

  const emailHandler = (text) => {
    setEmail(text);
  };

  const onPressHandler = () => {
    Keyboard.dismiss();

    axios
      .post(global.url + "/login/forgot-password-email", {
        email: email,
        clientID: "10",
      })
      .then(function (response) {
        //console.log(response.data);
        if (response.data.message === true) {
          Alert.alert(
            "Password reset instruction sent.",
            "Please check your spam folder.",
            [
              {
                text: "Ok",
                onPress: () => {
                  console.log("Ok Pressed"), navigation.goBack();
                },
              },
            ],
            { cancelable: false }
          );
        } else {
          Alert.alert(
            "Password reset instruction not sent.",
            "Email does not exist. Please try again.",
            [{ text: "Ok", onPress: () => console.log("Ok Pressed") }],
            { cancelable: false }
          );
        }
      })
      .catch(function (error) {
        console.log(error);
      });
  };

  return (
    <SafeAreaView style={gS.container}>
      <View>
        <Image
          source={require("C:/React_Native/snaprewards/assets/mv_logo.png")}
          style={styles.logo}
        />
      </View>

      <Text style={styles.wordExplain}>
        Enter your registered email address and we will send you password reset
        instruction. You may need to check your spam folder.
      </Text>

      <TextInput
        style={styles.form}
        placeholder={"eg. abc123@momsvillageasia.com"}
        keyboardType="email-address"
        textContentType="emailAddress"
        onChangeText={emailHandler}
      />
      <View style={{ justifyContent: "center", alignItems: "stretch" }}>
        <TouchableOpacity style={gS.blueBtn} onPress={() => onPressHandler()}>
          <Text style={gS.blueBtnWord}> Reset Password </Text>
        </TouchableOpacity>
      </View>
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  logo: {
    height: 80,
    marginVertical: 20,
  },
  wordForgetPassword: {
    fontSize: 30,
    color: "#0168B7",
    marginVertical: 20,
  },
  wordExplain: {
    fontSize: 18,
    color: "black",
    textAlign: "center",
    margin: 35,
    opacity: 0.7,
  },

  form: {
    textAlign: "center",
    alignSelf: "stretch",
    fontSize: 20,
    margin: 30,
    borderBottomWidth: 1,
    borderColor: "#0168B7",
  },
});
