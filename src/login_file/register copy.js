import React, { useState, useEffect } from "react";

import {
  View,
  StyleSheet,
  ScrollView,
  Text,
  Picker,
  Alert,
  TouchableOpacity,
} from "react-native";
import { TextInput, Button, Checkbox, HelperText } from "react-native-paper";
import { SafeAreaView } from "react-navigation";
import { useDispatch } from "react-redux";

import gS from "../globalStyle";
import * as authActions from "../../store/actions/auth";

const register = (props) => {
  //Declaration
  const clientID = 10;
  const [salutation, setSalutation] = useState("1");
  const [name, setName] = React.useState("");
  const [email, setEmail] = React.useState("");
  const [mobile, setMobile] = React.useState("");
  const [company, setCompnay] = React.useState("");
  const [pass1, setPass1] = React.useState("");
  const [pass2, setPass2] = React.useState("");
  const [checked, setChecked] = React.useState(false);

  //Error Section
  const [nameE, setNameE] = useState(false);
  const [emailE, setEmailE] = useState(false);
  const [saluE, setSaluE] = useState(false);
  const [mobileE, setMobileE] = useState(false);
  const [companyE, setCompanyE] = useState(false);
  const [pass1E, setPass1E] = useState(false);
  const [pass2E, setPass2E] = useState(false);
  const [tncE, setTncE] = useState(false);
  const [errorE, setErrorE] = useState(false);
  const [error, setError] = useState("");

  const dispatch = useDispatch();

  const url = "http://api.businessboosters.com.my/login/signup";

  useEffect(() => {
    if (error) {
      Alert.alert("An Error Occured", error, [{ text: "Okay" }]);
    }
  }, [error]);

  const authHandler = async () => {
    console.log(clientID, salutation, name, company, email, mobile, pass1);
    let action;
    action = authActions.signup(
      clientID,
      salutation,
      name,
      company,
      email,
      mobile,
      pass1
    );

    setError(null);
    //setIsLoading(true);
    try {
      await dispatch(action);
      props.navigation.navigate("Login");
    } catch (err) {
      setError(err.message);
    }
    //setIsLoading(false);
  };

  return (
    <SafeAreaView style={styles.containerStyle}>
      <ScrollView showsVerticalScrollIndicator={false}>
        <View style={{ backgroundColor: "white", padding: 40 }}>
          <View style={gS.form}>
            <Picker
              selectedValue={salutation}
              onValueChange={(itemValue, itemIndex) => setSalutation(itemValue)}
            >
              <Picker.Item label="Male" value="1"></Picker.Item>
              <Picker.Item label="Female" value="2"></Picker.Item>
            </Picker>
          </View>
          <HelperText type="error" visible={saluE}>
            Salutation is required!
          </HelperText>

          <TextInput
            label="Full Name"
            mode="outlined"
            value={name}
            onChangeText={(text) => setName(text)}
          />
          <HelperText type="error" visible={nameE}>
            Name is required!
          </HelperText>

          <TextInput
            label="Email"
            value={email}
            mode="outlined"
            onChangeText={(text) => setEmail(text)}
          />
          <HelperText type="error" visible={emailE}>
            Email is required!
          </HelperText>

          <TextInput
            label="Mobile No."
            value={mobile}
            keyboardType="name-phone-pad"
            mode="outlined"
            onChangeText={(text) => setMobile(text)}
          />
          <HelperText type="error" visible={mobileE}>
            Mobile Number is required!
          </HelperText>

          <TextInput
            label="Company Name"
            value={company}
            mode="outlined"
            onChangeText={(text) => setCompnay(text)}
          />
          <HelperText type="error" visible={companyE}>
            Company Name is required!
          </HelperText>

          <TextInput
            label="Password"
            mode="outlined"
            value={pass1}
            onChangeText={(text) => setPass1(text)}
            secureTextEntry
          />
          <HelperText type="error" visible={pass1E}>
            Password is required!
          </HelperText>

          <TextInput
            label="Retype Password"
            mode="outlined"
            value={pass2}
            onChangeText={(text) => setPass2(text)}
            secureTextEntry
          />
          <HelperText type="error" visible={pass2E}>
            Password Error!
          </HelperText>

          <View
            style={{
              flexDirection: "row",
              justifyContent: "center",
              alignItems: "center",
              marginBottom: 20,
            }}
          >
            <Checkbox
              status={checked ? "checked" : "unchecked"}
              onPress={() => {
                setChecked(!checked);
              }}
              color="#80F"
            />
            <Text>I agreed to the </Text>
            <Text style={{ color: "blue" }}>Terms and Conditions.</Text>
          </View>

          <View style={{ justifyContent: "center", alignItems: "stretch" }}>
            {/* <Button mode="contained" onPress={authHandler}>
              Register
            </Button> */}
            <TouchableOpacity onPress={authHandler}>
              <View style={gS.blueBtn}>
                <Text style={gS.blueBtnWord}> Register </Text>
              </View>
            </TouchableOpacity>
            <HelperText type="error" visible={tncE}>
              You must agreed to Terms and Consition to continue.
            </HelperText>
            <HelperText type="error" visible={errorE}>
              {error}
            </HelperText>
          </View>
        </View>
      </ScrollView>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  containerStyle: {
    flex: 1,
  },
});

export default register;
