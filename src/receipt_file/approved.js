import "react-native-gesture-handler";

import React, { useState, useEffect } from "react";
import gS from "../globalStyle";
import {
  SafeAreaView,
  Text,
  StyleSheet,
  View,
  Dimensions,
  Clipboard,
  ToastAndroid,
  Pressable,
} from "react-native";
import * as Device from "expo-device";
import {
  FlatList,
  ScrollView,
  TouchableOpacity,
} from "react-native-gesture-handler";
import Constants from "expo-constants";
import { useSelector } from "react-redux";
import PendingDetail from "../../components/PendingDetail";
import { useDispatch } from "react-redux";
import * as productsActions from "../../store/actions/categories";
import OrderItemApproved from "../../components/shop/OrderItemApproved";
import moment from "moment";

const ApprovedScreen = (props) => {
  const myToken = useSelector((state) => state.auth.access_token);
  console.log(myToken);
  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(productsActions.ApprovedDetails(myToken));
    //dispatch(productsActions.pendingDetails(myToken));
  }, [dispatch]);
  //console.log("GenePending Detail Page");

  //const myToken = useSelector(state => state.auth.access_token)
  const Categories = useSelector((state) => state.myPage.availableApproved);
  // const totalItems = useSelector((state) => state.myPage.totalItems);
  // // const allCategories = Categories;
  // // const allTotalItems = totalItems;
  console.log(Categories);
  // var i;
  // //const combined;
  // // for (i = 0; i < Categories.length; i++) {
  // //   const x = Object.assign({}, Categories[i], { totalItem: totalItems[i] });
  // //   // combined = [].concat(...x, x);
  // // }
  // const x = Object.assign({}, allCategories[0], {
  //   totalItem: allTotalItems[0],
  // });

  // console.log(x);
  //const combined1 = [].concat(Categories, x);
  //  console.log(combined);
  return (
    <SafeAreaView style={gS.container}>
      <ScrollView style={gS.scroll} showsVerticalScrollIndicator={false}>
        {Categories === undefined || Categories.length === 0 ? (
          <Text style={styles.textmsg}>
            Please wait, Currently there are is no approved receipts
          </Text>
        ) : (
          <FlatList
            // horizontal={false}
            // numColumns={1}
            // showsHorizontalScrollIndicator={false}
            keyExtractor={(item) => item.vip_receipt_id.toString()}
            data={Categories}
            renderItem={(itemData) => (
              <OrderItemApproved
                // {moment(itemData.item.date_of_receipt).format(
                //   "MMMM Do YYYY"
                // )}
                created_datetime={moment(itemData.item.created_datetime).format(
                  "MMMM Do YYYY"
                )}
                items={itemData.item.invoice_no}
                date_of_receipt={moment(itemData.item.date_of_receipt).format(
                  "MMMM Do YYYY"
                )}
                totalItem={itemData.item.totalItem}
                totalPoint={itemData.item.totalpoint}
                resellername={itemData.item.resellername}
              />
            )}
          />
        )}
      </ScrollView>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  word: {
    color: "red",
    fontSize: 16,
  },
  box: {
    flexDirection: "row",
    padding: 15,
    borderRadius: 15,
    marginBottom: 15,
  },
  mainTitle: {
    fontFamily: "open-sans-bold",
    fontSize: 25,
    fontWeight: "900",
    textAlign: "auto",
  },
  textmsg: {
    fontFamily: "open-sans-bold",
    fontSize: 15,
  },
});

export default ApprovedScreen;
