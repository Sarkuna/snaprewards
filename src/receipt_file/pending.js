import "react-native-gesture-handler";

import React, { useState, useEffect } from "react";
import gS from "../globalStyle";
import {
  SafeAreaView,
  Text,
  StyleSheet,
  View,
  Dimensions,
  Clipboard,
  ToastAndroid,
  Pressable,
} from "react-native";
import * as Device from "expo-device";
import {
  FlatList,
  ScrollView,
  TouchableOpacity,
} from "react-native-gesture-handler";
import Constants from "expo-constants";
import { useSelector } from "react-redux";
import PendingDetail from "../../components/PendingDetail";
import { useDispatch } from "react-redux";
import * as productsActions from "../../store/actions/categories";
import OrderItemPending from "../../components/shop/OrderItemPending";
import moment from "moment";
const PendingScreen = (props) => {
  const myToken = useSelector((state) => state.auth.access_token);
  console.log(myToken);
  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(productsActions.pendingDetails(myToken));
    //dispatch(productsActions.pendingDetails(myToken));
  }, [dispatch]);
  //console.log("GenePending Detail Page");

  //const myToken = useSelector(state => state.auth.access_token)
  const Categories = useSelector((state) => state.myPage.availablePending);
  console.log(Categories);
  // console.log(Categories);

  const renderGridItem = (itemData) => {
    //console.log(itemData.item.created_datetime)
    return (
      //moment(created_datetime).format("YYYY-MM-DD");
      <View>
        <View>
          <PendingDetail // categories Flatlist
            created_datetime={itemData.item.created_datetime}
            date_of_receipt={itemData.item.date_of_receipt}
            invoice_no={itemData.item.invoice_no}
            reseller_name={itemData.item.reseller_name}
          />
        </View>
      </View>
    );
  };

  return (
    <SafeAreaView style={gS.container}>
      <ScrollView style={gS.scroll} showsVerticalScrollIndicator={false}>
        {Categories === undefined || Categories.length === 0 ? (
          <Text style={styles.textmsg}>
            Currently dont have pending receipts
          </Text>
        ) : (
          <FlatList
            keyExtractor={(item) => item.vip_receipt_id.toString()}
            data={Categories}
            renderItem={(itemData) => (
              <OrderItemPending
                //moment(created_datetime).format("YYYY-MM-DD")

                created_datetime={moment(itemData.item.created_datetime).format(
                  "MMMM Do YYYY"
                )}
                items={itemData.item.invoice_no}
                date_of_receipt={moment(itemData.item.date_of_receipt).format(
                  "MMMM Do YYYY"
                )}
                //  reseller_name={itemData.item.reseller_name}
              />
            )}
          />
        )}
      </ScrollView>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  word: {
    color: "red",
    fontSize: 16,
  },
  box: {
    flexDirection: "row",
    padding: 15,
    borderRadius: 15,
    marginBottom: 15,
  },
  mainTitle: {
    fontFamily: "open-sans-bold",
    fontSize: 25,
    fontWeight: "900",
    textAlign: "auto",
  },
  textmsg: {
    fontFamily: "open-sans-bold",
    fontSize: 15,
  },
});

export default PendingScreen;
