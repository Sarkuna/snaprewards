import React, { useState, useEffect } from "react";
import axios from "axios";
import gS from "../globalStyle";
import { TouchableWithoutFeedback } from "react-native-gesture-handler";
import {
  Alert,
  Text,
  View,
  TextInput,
  SafeAreaView,
  ScrollView,
  Switch,
  StyleSheet,
  Picker,
} from "react-native";
import * as SecureStore from "expo-secure-store";
import { HelperText } from "react-native-paper";

import { useSelector } from "react-redux";
import Colors from "../../constants/Colors";
import { TouchableOpacity } from "react-native";
import DateTimePickerModal from "react-native-modal-datetime-picker";
import moment from "moment";

const editCompany = (props) => {
  const myToken = useSelector((state) => state.auth.access_token);

  const [saluationIsValid, setSaluationIsValid] = useState(false);
  const [fullnameIsValid, setFullnameIsValid] = useState(false);
  const [emailIsValid, setEmailIsValid] = useState(false);
  const [mobileNumberIsValid, setMobileNumberIsValid] = useState(false);
  const [submitValidation, setSubmitValidation] = useState(false);
  const [error, setError] = useState();

  const frmAddress = props.route.params.frmAddress;
  const frmdateOfBirth = props.route.params.frmdateOfBirth;
  console.log(frmdateOfBirth);
  const vip_customer_id = useSelector(
    (state) => state.auth.userinfo.vip_customer_id
  );
  const userID = useSelector((state) => state.auth.userinfo.userID);
  console.log("editCompanyInfo");
  console.log(vip_customer_id);
  // const companyId = props.route.params.comId;
  // const comName = props.route.params.comName;
  // const frmComAddress = props.route.params.frmComAddress;
  // const frmTelNum = props.route.params.frmTelNum;
  // const frmfax = props.route.params.frmfax;
  // const frmRegNum = props.route.params.frmRegNum;
  // const frmRegType = props.route.params.frmRegNum;
  // console.log(companyId);
  const submitUrl = global.url + "/profile/update?access-token=" + myToken;
  const profileUrl =
    global.url + `profile?access-token=${myToken}&userID=${userID}`;
  console.log(profileUrl);
  axios
    .get(profileUrl)
    .then(function (response) {
      // console.log(response);
    })
    .catch(function (error) {
      console.log(error);
    });

  let frmSaluationId;
  if (props.route.params.frmsalutation === "Mr") {
    frmSaluationId = 1;
  } else if (props.route.params.frmsalutation === "Mrs") {
    frmSaluationId = 2;
  } else if (props.route.params.frmsalutation === "Miss") {
    frmSaluationId = 3;
  } else if (props.route.params.frmsalutation === "Ms") {
    frmSaluationId = 4;
  } else if (props.route.params.frmsalutation === "Dr") {
    frmSaluationId = 5;
  } else if (props.route.params.frmsalutation === "EN") {
    frmSaluationId = 6;
  } else {
    frmSaluationId = "";
  }

  let frmGenderId;
  if (props.route.params.frmGender === "Male") {
    frmGenderId = "M";
  } else {
    frmGenderId = "F";
  }

  let frmRaceId;
  if (props.route.params.frmRace === "Malay") {
    frmRaceId = "1";
  } else if (props.route.params.frmRace === "Chinese") {
    frmRaceId = "2";
  } else if (props.route.params.frmRace === "Indian") {
    frmRaceId = "3";
  } else if (props.route.params.frmRace === "Others") {
    frmRaceId = "4";
  } else {
    frmRaceId = "";
  }

  let frmRegionId;
  if (props.route.params.frmRegion === "Peninsular") {
    frmRegionId = "1";
  } else if (props.route.params.frmRegion === "Sabah") {
    frmRegionId = "2";
  } else if (props.route.params.frmRegion === "Sarawak") {
    frmRegionId = "3";
  } else {
    frmRegionId = "";
  }

  let frmNationalityId;
  if (props.route.params.frmNationality === "Malaysia") {
    frmNationalityId = 129;
  } else {
    frmNationalityId = "";
  }

  let frmCountryId;
  if (props.route.params.frmCountry === "Malaysia") {
    frmCountryId = 129;
  } else {
    frmCountryId = "";
  }

  useEffect(() => {
    if (error) {
      // Alert.alert("Oops!", error, [{ text: "Okay" }]);
      setSubmitValidation(false);
    } else {
      setSubmitValidation(true);
    }
  }, [error]);

  const [frmfullName, setfrmFullName] = useState(
    props.route.params.frmfullName
  );
  const [frmRace, setFrmRace] = useState(frmRaceId);
  const [frmTellNum, setFrmTelNum] = useState(props.route.params.frmTelephone);
  const [frmRegion, setFrmRegion] = useState(frmRegionId);
  const [frmNricPass, setFrmNricPass] = useState(
    props.route.params.frmNricPassport
  );
  const [frmNationality, setFrmNationality] = useState(frmNationalityId);
  const [frmCountry, setFrmCountry] = useState(frmCountryId);
  const [regType, setRegType] = useState(frmSaluationId);
  const [frmEmail, SetFrmEmail] = useState(props.route.params.frmEmail);
  const [frmGender, setFrmGender] = useState(frmGenderId);
  const [frmMobileNum, setFrmMobileNum] = useState(
    props.route.params.frmMobileNumber
  );
  const [date_of_receipt, setDate_of_receipt] = useState(frmdateOfBirth);
  const [isDatePickerVisible, setDatePickerVisibility] = useState(false);
  console.log(date_of_receipt);
  var date_of_Birth = moment(date_of_receipt).format("MM-DD-YYYY");
  console.log(date_of_Birth);

  console.log(!saluationIsValid);
  console.log(!fullnameIsValid);
  console.log(!emailIsValid);
  console.log(!mobileNumberIsValid);

  function handler() {
    if (
      // !saluationIsValid ||
      // !fullnameIsValid ||
      // !emailIsValid ||
      // !mobileNumberIsValid ||
      regType === "" ||
      frmfullName === "" ||
      frmEmail === "" ||
      frmMobileNum === ""
    ) {
      setSubmitValidation(false);
      console.log("Not Submit");
    } else {
      console.log("Submit WORK");
      props.navigation.navigate("Home"),
        axios
          .post(submitUrl, {
            vip_customer_id: vip_customer_id,
            salutation: regType,
            full_name: frmfullName,
            date_of_birth: date_of_Birth,
            mobile_no: frmMobileNum,
            nric: frmNricPass,
            telephone_no: frmTellNum,
            Race: frmRace,
            country_id: frmCountry,
            Region: frmRegion,
            nationality_id: frmNationality,
            gender: frmGender,
          })
          .then(function (response) {
            console.log(response.data);
            if (response.data.message === "Success") {
              alert("Succesfully edited");
            } else {
              alert("Somthing went wrong");
            }
          })
          .catch(function (error) {
            console.log(error);
          });
    }
  }

  // const handler = () => {
  //   Alert.alert(
  //     "Confirm to edit ?",
  //     "",
  //     [
  //       {
  //         text: "Cancel",
  //         onPress: () => console.log("Cancel Pressed"),
  //         style: "cancel",
  //       },
  //       {
  //         text: "Confirm",
  //         onPress: () => {
  //           props.navigation.goBack(),
  //             axios
  //               .post(submitUrl, {
  //                 vip_customer_id: vip_customer_id,
  //                 salutation: regType,
  //                 full_name: frmfullName,
  //                 date_of_birth: date_of_Birth,
  //                 mobile_no: frmMobileNum,
  //                 nric: frmNricPass,
  //                 telephone_no: frmTellNum,
  //                 Race: frmRace,
  //                 country_id: frmCountry,
  //                 Region: frmRegion,
  //                 nationality_id: frmNationality,
  //                 gender: frmGender,
  //               })
  //               .then(function (response) {
  //                 console.log(response.data);
  //                 if (response.data.message === "Success") {
  //                   alert("Succesfully edited");
  //                 } else {
  //                   alert("Somthing went wrong");
  //                 }
  //               })
  //               .catch(function (error) {
  //                 console.log(error);
  //               });
  //         },
  //       },
  //     ],
  //     { cancelable: false }
  //   );
  // };
  // const changeEmail = (text) => {
  //   SetFrmEmail(text);
  // };
  // const changeFullname = (text) => {
  //   setfrmFullName(text);
  // };
  // const changeMobileNum = (text) => {
  //   setFrmMobileNum(text);
  // };
  const changeTelNum = (text) => {
    setFrmTelNum(text);
  };
  const changeNricPass = (text) => {
    setFrmNricPass(text);
  };
  const showDatePicker = () => {
    setDatePickerVisibility(true);
  };
  const hideDatePicker = () => {
    setDatePickerVisibility(false);
  };
  const handleConfirm = (date) => {
    setDate_of_receipt(date);
    console.log("A date has been picked: ", date_of_receipt);
    hideDatePicker();
  };

  const changeSaluation = (itemValue) => {
    if (regType === 0 || regType === "0") {
      setSaluationIsValid(false);
    } else {
      setSaluationIsValid(true);
    }
    setRegType(itemValue);
  };

  const changeFullname = (text) => {
    if (text.trim().length === 0) {
      setFullnameIsValid(false);
    } else {
      setFullnameIsValid(true);
    }
    setfrmFullName(text);
  };

  const changeEmail = (text) => {
    if (text.trim().length === 0) {
      setEmailIsValid(false);
    } else {
      setEmailIsValid(true);
    }
    SetFrmEmail(text);
  };

  const changeMobileNum = (text) => {
    if (text.trim().length === 0) {
      setMobileNumberIsValid(false);
    } else {
      setMobileNumberIsValid(true);
    }
    setFrmMobileNum(text);
  };

  console.log(vip_customer_id);
  console.log(regType);
  console.log(frmfullName);
  console.log(date_of_Birth);
  console.log(frmMobileNum);
  console.log(frmNricPass);
  console.log(frmTellNum);
  console.log(frmRace);
  console.log(frmCountry);
  console.log(frmRegion);
  console.log(frmNationality);
  console.log(frmGender);

  // vip_customer_id: vip_customer_id,
  // salutation: regType,
  // full_name: frmfullName,
  // date_of_birth: date_of_Birth,
  // mobile_no: frmMobileNum,
  // nric: frmNricPass,
  // telephone_no: frmTellNum,
  // Race: frmRace,
  // country_id: frmCountry,
  // Region: frmRegion,
  // nationality_id: frmNationality,
  // gender: frmGender,

  return (
    <SafeAreaView style={gS.container}>
      <ScrollView style={gS.scroll}>
        <View style={gS.section2}>
          <View style={styles.containerForm}>
            <View style={gS.section2}>
              <View style={styles.formControl}>
                <View style={styles.form}>
                  <View style={{ marginTop: 30 }}>
                    <Text style={styles.label}>Saluation</Text>
                    <View
                      style={{
                        color: "#F00",
                        // borderBottomWidt: 10,
                        borderColor: "#ccc",
                        borderBottomWidth: 1,
                      }}
                    >
                      <Picker
                        selectedValue={regType}
                        onValueChange={changeSaluation}
                      >
                        {/* <Picker.Item
                          style={{ color: "#F00", borderBottomWidt: 10 }}
                          label={frmsalutation}
                          value="0"
                        /> */}
                        <Picker.Item label="Mr" value={1}></Picker.Item>
                        <Picker.Item label="Mrs" value={2}></Picker.Item>
                        <Picker.Item label="Miss" value={3}></Picker.Item>
                        <Picker.Item label="Ms" value={4}></Picker.Item>
                        <Picker.Item label="Dr" value={5}></Picker.Item>
                        <Picker.Item label="EN" value={6}></Picker.Item>
                        <Picker.Item label="N/A" value={7}></Picker.Item>
                      </Picker>
                    </View>
                    {!submitValidation &&
                      !saluationIsValid &&
                      frmSaluationId === "" && (
                        <HelperText type="error">
                          Saluation cannot be blank.
                        </HelperText>
                      )}
                  </View>
                  <Text style={styles.label}>Fullname</Text>
                  <TextInput
                    style={styles.input}
                    id="title"
                    errorText="Please enter a valid title"
                    keyboardType="default"
                    autoCapitalize="sentences"
                    autoCorrect
                    returnKeyType="next"
                    onChangeText={changeFullname}
                    required
                    value={frmfullName}
                  />
                  {!submitValidation &&
                    !fullnameIsValid &&
                    frmfullName === "" && (
                      <HelperText type="error">
                        Fullname cannot be blank.
                      </HelperText>
                    )}
                  <Text style={styles.label}>Date of Birth</Text>
                  <View
                    style={{
                      color: "#F00",
                      // borderBottomWidt: 10,
                      borderColor: "#ccc",
                      borderBottomWidth: 1,
                    }}
                  >
                    <View style={gS.sectionScan}>
                      <TouchableWithoutFeedback onPress={showDatePicker}>
                        {date_of_receipt === frmdateOfBirth ? (
                          <Text
                          // style={{
                          //   color: "#ccc",
                          // }}
                          >
                            {date_of_receipt.toString()}
                          </Text>
                        ) : (
                          <Text
                            // style={{
                            //   color: "#999",
                            // }}
                            style={styles.input2True}
                          >
                            {date_of_receipt.toString().substring(4, 15)}
                          </Text>
                        )}

                        <DateTimePickerModal
                          isVisible={isDatePickerVisible}
                          mode="date"
                          onConfirm={handleConfirm}
                          onCancel={hideDatePicker}
                        />
                      </TouchableWithoutFeedback>
                    </View>
                  </View>
                  <Text style={styles.label}>Email</Text>
                  <TextInput
                    style={styles.input}
                    id="title"
                    errorText="Please enter a valid title"
                    keyboardType="default"
                    autoCapitalize="sentences"
                    autoCorrect
                    returnKeyType="next"
                    onChangeText={changeEmail}
                    required
                    value={frmEmail}
                  />
                  {!submitValidation && !emailIsValid && frmEmail === "" && (
                    <HelperText type="error">Email cannot be blank.</HelperText>
                  )}
                  <View style={{ marginTop: 30 }}>
                    <Text style={styles.label}>Gender</Text>
                    <View
                      style={{
                        color: "#F00",
                        // borderBottomWidt: 10,
                        borderColor: "#ccc",
                        borderBottomWidth: 1,
                      }}
                    >
                      <Picker
                        selectedValue={frmGender}
                        onValueChange={(itemValue, itemIndex) =>
                          setFrmGender(itemValue)
                        }
                      >
                        {/* <Picker.Item
                          style={{ color: "#F00", borderBottomWidt: 10 }}
                          label={"--"}
                          value="0"
                        /> */}
                        <Picker.Item label="Male" value={"M"}></Picker.Item>
                        <Picker.Item label="Female" value={"F"}></Picker.Item>
                      </Picker>
                    </View>
                  </View>
                  <Text style={styles.label}>NRIC/Passport No</Text>
                  <TextInput
                    style={styles.input}
                    id="title"
                    errorText="Please enter a valid title"
                    keyboardType="default"
                    autoCapitalize="sentences"
                    autoCorrect
                    returnKeyType="next"
                    onChangeText={changeNricPass}
                    required
                    value={frmNricPass}
                  />
                  <Text style={styles.label}>Mobile Number</Text>
                  <TextInput
                    style={styles.input}
                    id="title"
                    errorText="Please enter a valid title"
                    keyboardType="default"
                    autoCapitalize="sentences"
                    autoCorrect
                    returnKeyType="next"
                    onChangeText={changeMobileNum}
                    required
                    value={frmMobileNum}
                  />
                  {!submitValidation &&
                    !mobileNumberIsValid &&
                    frmMobileNum === "" && (
                      <HelperText type="error">
                        Mobile Number cannot be blank.
                      </HelperText>
                    )}
                  <Text style={styles.label}>Telephone</Text>
                  <TextInput
                    style={styles.input}
                    id="title"
                    errorText="Please enter a valid title"
                    keyboardType="default"
                    autoCapitalize="sentences"
                    autoCorrect
                    returnKeyType="next"
                    onChangeText={changeTelNum}
                    required
                    value={frmTellNum}
                  />
                  <View style={{ marginTop: 30 }}>
                    <Text style={styles.label}>Race</Text>
                    <View
                      style={{
                        color: "#F00",
                        // borderBottomWidt: 10,
                        borderColor: "#ccc",
                        borderBottomWidth: 1,
                      }}
                    >
                      <Picker
                        selectedValue={frmRace}
                        onValueChange={(itemValue, itemIndex) =>
                          setFrmRace(itemValue)
                        }
                      >
                        <Picker.Item
                          style={{ color: "#F00", borderBottomWidt: 10 }}
                          label={"--"}
                          value=""
                        />
                        <Picker.Item label="Malay" value={"1"}></Picker.Item>
                        <Picker.Item label="Chinese" value={"2"}></Picker.Item>
                        <Picker.Item label="Indian" value={"3"}></Picker.Item>
                        <Picker.Item label="Others" value={"4"}></Picker.Item>
                      </Picker>
                    </View>
                  </View>
                  <View style={{ marginTop: 30 }}>
                    <Text style={styles.label}>Region</Text>
                    <View
                      style={{
                        color: "#F00",
                        // borderBottomWidt: 10,
                        borderColor: "#ccc",
                        borderBottomWidth: 1,
                      }}
                    >
                      <Picker
                        selectedValue={frmRegion}
                        onValueChange={(itemValue, itemIndex) =>
                          setFrmRegion(itemValue)
                        }
                      >
                        <Picker.Item
                          style={{ color: "#F00", borderBottomWidt: 10 }}
                          label={"--"}
                          value=""
                        />
                        <Picker.Item
                          label="Peninsular"
                          value={"1"}
                        ></Picker.Item>
                        <Picker.Item label="Sabah" value={"2"}></Picker.Item>
                        <Picker.Item label="Sarawak" value={"3"}></Picker.Item>
                      </Picker>
                    </View>
                  </View>
                  <View style={{ marginTop: 30 }}>
                    <Text style={styles.label}>Nationality</Text>
                    <View
                      style={{
                        color: "#F00",
                        // borderBottomWidt: 10,
                        borderColor: "#ccc",
                        borderBottomWidth: 1,
                      }}
                    >
                      <Picker
                        selectedValue={frmNationality}
                        onValueChange={(itemValue, itemIndex) =>
                          setFrmNationality(itemValue)
                        }
                      >
                        <Picker.Item
                          style={{ color: "#F00", borderBottomWidt: 10 }}
                          label={"--"}
                          value=""
                        />
                        <Picker.Item label="Malaysia" value={129}></Picker.Item>
                      </Picker>
                    </View>
                  </View>
                  <View style={{ marginTop: 30 }}>
                    <Text style={styles.label}>Country</Text>
                    <View
                      style={{
                        color: "#F00",
                        // borderBottomWidt: 10,
                        borderColor: "#ccc",
                        borderBottomWidth: 1,
                      }}
                    >
                      <Picker
                        selectedValue={frmCountry}
                        onValueChange={(itemValue, itemIndex) =>
                          setFrmCountry(itemValue)
                        }
                      >
                        <Picker.Item
                          style={{ color: "#F00", borderBottomWidt: 10 }}
                          label={"--"}
                          value=""
                        />
                        <Picker.Item label="Malaysia" value={129}></Picker.Item>
                      </Picker>
                    </View>
                  </View>
                </View>
              </View>
            </View>
          </View>
          {/* <View style={styles.containerFormDefault}>
            <FiltersSwitch
              label="Set as default address"
              state={isVeganFree}
              onChange={(newValue) => setIsVeganFree(newValue)}
            />
          </View> */}

          {/* <View style={styles.containerFormDefault2}>
            <TouchableOpacity onPress={() => handlerAddressDelete()}>
              <Text style={styles.DeleteAddText}>Delete</Text>
            </TouchableOpacity>
          </View> */}

          <TouchableWithoutFeedback onPress={handler}>
            <View style={gS.blueBtn}>
              <Text style={gS.blueBtnWord}>Submit</Text>
            </View>
          </TouchableWithoutFeedback>
        </View>
      </ScrollView>
    </SafeAreaView>
  );
};

export default editCompany;

const styles = StyleSheet.create({
  containerForm: {
    shadowColor: "#f9f5f5",
    shadowOpacity: 0.26,
    shadowOffset: { width: 0, height: 2 },
    shadowRadius: 8,
    elevation: 1,
    borderRadius: 5,
    borderLeftColor: "#f9f5f5",
    borderStartColor: "#f9f5f5",
    borderColor: "#f9f5f5",
    // height: "90%",
    width: "100%",
    // margin: 5,
    backgroundColor: "#FFFFFF",
  },
  containerFormDefault: {
    marginTop: 10,
    shadowColor: "#f9f5f5",
    shadowOpacity: 0.26,
    shadowOffset: { width: 0, height: 2 },
    shadowRadius: 8,
    elevation: 1,
    borderRadius: 5,
    borderLeftColor: "#f9f5f5",
    borderStartColor: "#f9f5f5",
    borderColor: "#f9f5f5",
    height: "15%",
    width: "100%",

    backgroundColor: "#FFFFFF",
  },
  containerFormDefault2: {
    marginTop: 10,

    shadowColor: "#f9f5f5",
    shadowOpacity: 0.26,
    shadowOffset: { width: 0, height: 2 },
    shadowRadius: 8,
    elevation: 1,
    borderRadius: 5,
    // borderLeftColor: "#f9f5f5",
    // borderStartColor: "#f9f5f5",
    borderColor: "#f9f5f5",
    height: "15%",
    width: "100%",
    // margin: 5,
    backgroundColor: "#FFFFFF",
  },
  filterContainer: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    marginVertical: 10,
    width: "80%",
  },
  title: {
    fontFamily: "open-sans-bold",
    fontSize: 17,
    margin: 20,
    textAlign: "center",
    justifyContent: "center",
  },
  DeleteAddText: {
    fontFamily: "open-sans-bold",
    fontSize: 17,
    margin: 20,
    color: "#a90308",
    // textAlign: "center",
  },
  form: {
    margin: 10,
  },
  formControl: {
    width: "100%",
  },
  label: {
    fontFamily: "open-sans-bold",
    marginVertical: 8,
  },
  input: {
    paddingHorizontal: 2,
    paddingVertical: 2,
    borderBottomColor: "#ccc",
    borderBottomWidth: 1,
    fontSize: 18,
  },
  input2True: {
    // paddingHorizontal: 2,
    // paddingVertical: 5,
    // borderBottomColor: "#ccc",
    // borderBottomWidth: 1,
    color: "#000000",
    //marginLeft: -2,
    //paddingLeft: 0,
  },
});
