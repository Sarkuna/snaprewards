import "react-native-gesture-handler";

import React, { useState } from "react";
import axios from "axios";
import gS from "../globalStyle";
import { Icon } from "react-native-elements";
import { Alert, Linking } from "react-native";
import { TouchableOpacity } from "react-native-gesture-handler";

import { StyleSheet, Text, View, SafeAreaView, ScrollView } from "react-native";
import { useSelector } from "react-redux";

export default function App({ navigation }) {
  console.log("Setting Launched");
  console.log(global.kansai);
  const url2 = global.kansai + "/order-history";
  const url3 = global.kansai + "/my-transactions";

  const [version, setVersion] = useState("V 3.17 Build 21");
  const [username, setUsername] = useState("John Lim");
  const [status, setStatus] = useState("Completed");

  const myToken = useSelector((state) => state.auth.access_token);
  const myName = useSelector((state) => 'test');
  const userID = useSelector((state) => 'state.auth.userinfo.userID');
  console.log(myName);
  function chgStat() {
    status === "Completed" ? setStatus("Incomplete") : setStatus("Completed");
  }

  const profileUrl =
    global.url + `/profile?access-token=${myToken}&userID=${userID}`;
  console.log(profileUrl);
  let address;
  let fullName;
  let dateOfBirth;
  let email;
  let gender;
  let nricPassport;
  let mobileNumber;
  let telephone;
  let race;
  let region;
  let nationality;
  let country;
  let salutation;
  axios
    .get(profileUrl)
    .then(function (response) {
      console.log(response.data);
      address = response.data.address;
      fullName = response.data.full_name;
      dateOfBirth = response.data.date_of_birth;
      email = response.data.email;
      gender = response.data.gender;
      nricPassport = response.data.nric_passport_no;
      mobileNumber = response.data.mobile_no;
      telephone = response.data.telephone_no;
      race = response.data.race;
      region = response.data.address;
      nationality = response.data.nationality_id;
      country = response.data.country_id;
      salutation = response.data.salutation;
    })
    .catch(function (error) {
      console.log(error);
    });

  const logoutCheck = () =>
    Alert.alert(
      "Confirm to Logout?",
      "",
      [
        {
          text: "No",
          onPress: () => console.log("Cancel Pressed"),
          style: "cancel",
        },
        { text: "Yes", onPress: () => navigation.navigate("Login") },
      ],
      { cancelable: false }
    );

  return (
    <SafeAreaView style={gS.container}>
      <ScrollView style={gS.scroll}>
        <View style={gS.crossleft}>
          <Icon
            name="angle-left"
            type="font-awesome"
            color="gray"
            onPress={() => navigation.goBack()}
          />
        </View>
        <View style={gS.center}>
          <View style={{ marginTop: 50 }}>
            <Icon
              reverse
              name="user-circle-o"
              type="font-awesome"
              color="#66F"
              size={40}
              onPress={() => navigation.goBack()}
            />
          </View>

          <Text style={styles.name}>{myName}</Text>
        </View>
        <View style={gS.center}>
          <View style={gS.hori}>
            <View style={gS.verti}>
              <View style={gS.center}>
                <Icon
                  raised
                  name="envelope-o"
                  type="font-awesome"
                  color="#66F"
                  onPress={() => console.log("Btn-Inbox")}
                />
                <Text style={gS.wordBlueSmall}>Inbox</Text>
              </View>
            </View>
            <View style={gS.verti}>
              <View style={gS.center}>
                <Icon
                  raised
                  name="pencil"
                  type="font-awesome"
                  color="#66F"
                  onPress={() =>
                    navigation.navigate({
                      name: "Profile",
                      params: {
                        // id: itemData.item.id,
                        // frmFullName: itemData.item.name,
                        // address: itemData.item.address,
                        // postcode: itemData.item.postcode,
                        // country: itemData.item.country,
                        // statename: itemData.item.statename,
                        // stateid: itemData.item.stateid,
                        frmAddress: address,
                        frmfullName: fullName,
                        frmsalutation: salutation,
                        frmdateOfBirth: dateOfBirth,
                        frmEmail: email,
                        frmGender: gender,
                        frmNricPassport: nricPassport,
                        frmMobileNumber: mobileNumber,
                        frmTelephone: telephone,
                        frmRace: race,
                        frmRegion: region,
                        frmNationality: nationality,
                        frmCountry: country,
                        //pageName: "Redeem Gallery",
                      },
                    })
                  }
                />
                <Text style={gS.wordBlueSmall}>Edit Profile</Text>
              </View>
            </View>
          </View>
        </View>
        {/* <TouchableOpacity>
          <View style={styles.settingSection}>
            <View style={styles.iconA}>
              <Icon name="university" type="font-awesome" color="#000" />
            </View>
            <View style={styles.iconB}>
              <Icon name="angle-right" type="font-awesome" color="#66F" />
            </View>
            <Text style={styles.wordA}>Merchant Dashboard</Text>
          </View>a
        </TouchableOpacity> */}

        <View style={styles.line}></View>

        <TouchableOpacity onPress={() => navigation.navigate("Company")}>
          <View style={styles.settingSection}>
            <View style={styles.iconA}>
              <Icon name="bank" type="font-awesome" color="#000" />
            </View>
            {/* <View style={styles.iconB}>
              <Icon name="angle-right" type="font-awesome" color="#66F" />
            </View> */}
            <Text style={styles.wordA}>Company Info</Text>
          </View>
        </TouchableOpacity>

        <View style={styles.line}></View>

        <TouchableOpacity onPress={() => navigation.navigate("AddressBook")}>
          <View style={styles.settingSection}>
            <View style={styles.iconA}>
              <Icon name="address-card" type="font-awesome" color="#000" />
            </View>
            {/* <View style={styles.iconB}>
              <Icon name="angle-right" type="font-awesome" color="#66F" />
            </View> */}
            <Text style={styles.wordA}>Address Book</Text>
          </View>
        </TouchableOpacity>

        <View style={styles.line}></View>
        {/* BAK INFO
        <TouchableOpacity onPress={() => navigation.navigate("Bank")}>
          <View style={styles.settingSection}>
            <View style={styles.iconA}>
              <Icon name="credit-card" type="font-awesome" color="#000" />
            </View>
             <View style={styles.iconB}>
              <Icon name="angle-right" type="font-awesome" color="#66F" />
            </View> 
            <Text style={styles.wordA}>My Bank Info</Text>
          </View>
        </TouchableOpacity>

        <View style={styles.line}></View>*/}


        {/*MY APPLICATION AND MY PAGES INFO
        <TouchableOpacity onPress={() => navigation.navigate("MyApplication")}>
          <View style={styles.settingSection}>
            <View style={styles.iconA}>
              <Icon name="cubes" type="font-awesome" color="#000" />
            </View>
            <View style={styles.iconB}>
              <Icon name="angle-right" type="font-awesome" color="#66F" />
            </View>
            <Text style={styles.wordA}>My Applications</Text>
          </View>
        </TouchableOpacity>

        <View style={styles.line}></View>

        <TouchableOpacity onPress={() => navigation.navigate("MyPages")}>
          <View style={styles.settingSection}>
            <View style={styles.iconA}>
              <Icon name="book" type="font-awesome" color="#000" />
            </View>
            <View style={styles.iconB}>
              <Icon name="angle-right" type="font-awesome" color="#66F" />
            </View>
            <Text style={styles.wordA}>My Pages</Text>
          </View>
        </TouchableOpacity>

        <View style={styles.line}></View>*/}

        {/* <View style={styles.line}></View>

        <TouchableOpacity onPress={() => chgStat()}>
          <View style={styles.settingSection}>
            <View style={styles.iconA}>
              <Icon name="user-o" type="font-awesome" color="#000" />
            </View>
            <View style={styles.iconB}>
              <Icon name="angle-right" type="font-awesome" color="#66F" />
            </View>
            <Text style={styles.wordA}>Account Verifivation</Text>
            <Text style={styles.wordB}>{status}</Text>
          </View>
        </TouchableOpacity> */}
        <View style={styles.line}></View>
        <TouchableOpacity onPress={() => navigation.navigate("SettingMain")}>
          <View style={styles.settingSection}>
            <View style={styles.iconA}>
              <Icon name="sliders" type="font-awesome" color="#000" />
            </View>
            <View style={styles.iconB}>
              <Icon name="angle-right" type="font-awesome" color="#66F" />
            </View>
            <Text style={styles.wordA}>Settings</Text>
          </View>
        </TouchableOpacity>
        {/* <View style={styles.line}></View>

        <TouchableOpacity>
          <View style={styles.settingSection}>
            <View style={styles.iconA}>
              <Icon name="question" type="font-awesome" color="#000" />
            </View>
            <View style={styles.iconB}>
              <Icon name="angle-right" type="font-awesome" color="#66F" />
            </View>
            <Text style={styles.wordA}>Help Center</Text>
          </View>
        </TouchableOpacity> */}

        <View style={styles.line}></View>
        {/* <TouchableOpacity onPress={() => logoutCheck()}>
          <View style={styles.logout}>
            <Text style={{ color: "red", fontSize: 20 }}>Log out</Text>
          </View>
        </TouchableOpacity> */}
        <View style={{ justifyContent: "center", alignItems: "stretch" }}>
          <TouchableOpacity
            style={gS.blueBtnLogout}
            onPress={() => logoutCheck()}
          >
            <Text style={gS.blueBtnWord}> Logout </Text>
          </TouchableOpacity>
        </View>
        <Text
          style={styles.version}
          onPress={() => navigation.navigate("General")}
        >
          {version}
        </Text>
      </ScrollView>
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  settingSection: {
    padding: 10,
    backgroundColor: "white",
    flexDirection: "row",
    alignItems: "center",
  },
  wordA: {
    color: "black",
    fontSize: 16,
    position: "absolute",
    left: "18%",
  },
  wordB: {
    fontSize: 12,
    color: "gray",
    position: "absolute",
    right: "15%",
  },
  iconA: {
    margin: 10,
  },
  iconB: {
    position: "absolute",
    right: 15,
    margin: 10,
  },
  line: {
    width: "100%",
    height: 1,
    backgroundColor: "lightgray",
    marginLeft: "15%",
  },
  logout: {
    marginHorizontal: 30,
    marginVertical: 20,
    borderWidth: 2,
    borderColor: "red",
    borderRadius: 50,
    padding: 10,
    alignItems: "center",
  },
  version: {
    color: "blue",
    fontSize: 12,
    textAlign: "center",
    // position: "absolute",
    // margin: -10,
    bottom: 0,
  },
});
