import "react-native-gesture-handler";

import React, { useState, useEffect } from "react";
import { Icon, Badge } from "react-native-elements";
import gS from "../globalStyle";
import {
  StyleSheet,
  Text,
  View,
  ImageBackground,
  SafeAreaView,
  ScrollView,
  Linking,
  ToastAndroid,
  Alert,
  FlatList,
  BackHandler,
} from "react-native";
import {
  TouchableOpacity,
  TouchableWithoutFeedback,
} from "react-native-gesture-handler";
import { useRoute, useFocusEffect } from "@react-navigation/native";
import { useSelector } from "react-redux";
import axios from "axios";
import { useDispatch } from "react-redux";
import CategoryGridTile from "../../components/CategoryGridTile";
import * as productsActions from "../../store/actions/categories";
import * as productsActions2 from "../../store/actions/receipt";

const HomeScreen = (props) => {
  const myToken = useSelector((state) => state.auth.access_token);
  const Type = useSelector((state) => state.auth.Type);
  const myName = useSelector((state) => state.auth.userinfo.full_name);
  const pendingValue = useSelector((state) => state.auth.userinfo.updated_by);
  const processingValue = useSelector(
    (state) => state.auth.userinfo.vip_customer_id
  );
  console.log(myToken);
  const acc1Points =
    global.url + "/profile/total-available-point?access-token=" + myToken;
  const acc2Points =
    global.url + "/profile/account-two?access-token=" + myToken;
  const expiringacc1 =
    global.url + "/profile/point-expiring?access-token=" + myToken;
  const expiringacc2 =
    global.url + "/profile/point-expiring-account-two?access-token=" + myToken;

  const dispatch = useDispatch();
  // console.log(Categories);

  const directToRewardGallery =
    global.kansai + "/site/app-login?token=" + myToken;
  const directToOrdersSummary = global.kansai + "/order-history";
  const url1 = global.kansai + "/my-reward-points";
  const url2 = global.kansai + "/order-history";
  const url3 = global.kansai + "/my-transactions";
  const url4 = global.kansai + "/receipts-history";
  const url5 = global.kansai + "/flash-deal-history";
  const url6 = global.kansai + "/point-system";
  const url7 = global.kansai + "/product-info";
  const url8 = global.kansai + "/latest-promotion";
  console.log(global.kansai);

  const Categories = useSelector((state) => state.myPage.availableCategories);
  // console.log(Categories)
  //console.log(global.url.concat("/" + "images/menu_icon_png.png"))

  const renderGridItem = (itemData) => {
    return (
      <View>
        <View>
          <CategoryGridTile // categories Flatlist
            title={itemData.item.title}
            image={global.kansai.concat("/" + itemData.item.icon_font)}
            // image={ENV.webRoot.concat(
            //   "/upload/business-categories/",
            //   itemData.item.categorie_image
            // )} // concatenate two strings

            onSelect={
              itemData.item.type == "custom-link"
                ? async () => await Linking.openURL(itemData.item.link)
                : () => {
                    props.navigation.navigate({
                      name: "MyPage",
                      params: {
                        pageId: itemData.item.id,
                        pageName: itemData.item.title,
                        related_id: itemData.item.related_id,
                      },
                    });
                  }
            }
          />
        </View>
      </View>
    );
  };

  React.useEffect(() => {
    const unsubscribe = props.navigation.addListener("focus", () => {
      // Alert.alert('Refreshed');

      dispatch(productsActions.fetchProducts(myToken));
      //dispatch(productsActions.pendingDetails(myToken));
    });
    return unsubscribe;
  }, [props.navigation, dispatch]);

  return (
    <SafeAreaView style={gS.container}>
      <ScrollView style={gS.scroll}>
        {/* <View style={gS.section}> */}
        {/* <Text style={gS.sectionTitle}>My Pages</Text> */}
        <FlatList
          // horizontal={true}
          // numColumns={1}
          showsHorizontalScrollIndicator={false}
          keyExtractor={(item, index) => item.id.toString()}
          data={Categories}
          renderItem={renderGridItem}
          // sliderWidth={200}
          // itemWidth={200}
        />
        {/* </View> */}
      </ScrollView>
    </SafeAreaView>
  );
};

export const screenOptions = (navData) => {
  const pageName = navData.route.params.pageName;
  //console.log(pageName)
  return {
    headerTitle: pageName,
    headerStyle: { backgroundColor: "#7367F0" },
    headerTintColor: "white",
  };
};

export default HomeScreen;

const styles = StyleSheet.create({
  headerbackground1: {
    position: "absolute",
    top: 0,
    width: "100%",
    height: 350,
  },
  headerbackground2: {
    position: "absolute",
    top: 0,
    width: "100%",
    height: 300,
  },
  barCard: {
    alignSelf: "stretch",
    alignItems: "center",
    height: 100,
    flex: 1,
    flexDirection: "row",
    justifyContent: "space-around",
    backgroundColor: "#F9F9F9",
    borderWidth: 0,
    borderRadius: 20,
    shadowColor: "#000",
    shadowOffset: { width: 0, height: 5 },
    shadowOpacity: 0.34,
    shadowRadius: 6.27,
    elevation: 10,
  },
  barCard2: {
    //alignSelf: "stretch",
    //alignItems: "center",
    height: 50,
    flex: 1,
    //flexDirection :"row",
    //justifyContent: "space-around",
    // backgroundColor: "#F9F9F9",
    //borderWidth: 0,
    // borderRadius: 20,
    //shadowColor: "#000",
    // shadowOffset: { width: 0, height: 5,},
    // shadowOpacity: 0.34,
    // shadowRadius: 6.27,
    // elevation: 10,
  },
  barIcon: {
    flex: 1,
    alignSelf: "auto",
    height: 80,
    alignItems: "center",
  },
  myInfoIcon: {
    width: 80,
    height: 80,
    alignItems: "center",
  },
  verti: {
    flexDirection: "column",
    justifyContent: "center",
  },
  card2: {
    width: 200,
    padding: 15,
    height: 80,
    flexDirection: "row",
    borderRadius: 15,
    alignItems: "center",
    margin: 10,

    shadowColor: "#000",
    shadowOffset: { width: 0, height: 5 },
    shadowOpacity: 0.34,
    shadowRadius: 6.27,
    elevation: 5,
  },
});
