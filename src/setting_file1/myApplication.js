import "react-native-gesture-handler";

import React, { useState, useEffect } from "react";
import { Icon, Badge } from "react-native-elements";
import gS from "../globalStyle";
import {
  StyleSheet,
  Text,
  View,
  ImageBackground,
  SafeAreaView,
  ScrollView,
  Linking,
  ToastAndroid,
  Alert,
  FlatList,
  BackHandler,
} from "react-native";
import {
  TouchableOpacity,
  TouchableWithoutFeedback,
} from "react-native-gesture-handler";
import { useRoute, useFocusEffect } from "@react-navigation/native";
import { useSelector } from "react-redux";
import axios from "axios";
import { useDispatch } from "react-redux";
//import CategoryGridTile from "../../components/CategoryGridTile";
import * as productsActions from "../../store/actions/categories";
import * as productsActions2 from "../../store/actions/receipt";

const HomeScreen = (props) => {
  const myToken = useSelector((state) => state.auth.access_token);
  const Type = useSelector((state) => state.auth.Type);
  const myName = useSelector((state) => state.auth.userinfo.full_name);
  const pendingValue = useSelector((state) => state.auth.userinfo.updated_by);
  const processingValue = useSelector(
    (state) => state.auth.userinfo.vip_customer_id
  );
  console.log(myToken);
  const acc1Points =
    global.url + "/profile/total-available-point?access-token=" + myToken;
  const acc2Points =
    global.url + "/profile/account-two?access-token=" + myToken;
  const expiringacc1 =
    global.url + "/profile/point-expiring?access-token=" + myToken;
  const expiringacc2 =
    global.url + "/profile/point-expiring-account-two?access-token=" + myToken;

  console.log(global.url);
  const [pts, setPts] = useState("");
  const [pts2, setPts2] = useState("");
  const [points, setPoints] = useState(pts);
  const [exp1, setExp1] = useState("");
  const [exp2, setExp2] = useState("");
  const [expPoints, setExpPoints] = useState("");
  const [acc, setAcc] = useState("1");
  const dispatch = useDispatch();
  // console.log(Categories);

  const directToRewardGallery =
    global.kansai + "/site/app-login?token=" + myToken;
  const directToOrdersSummary = global.kansai + "/order-history";
  const url1 = global.kansai + "/my-reward-points";
  const url2 = global.kansai + "/order-history";
  const url3 = global.kansai + "/my-transactions";
  const url4 = global.kansai + "/receipts-history";
  const url5 = global.kansai + "/flash-deal-history";
  const url6 = global.kansai + "/point-system";
  const url7 = global.kansai + "/product-info";
  const url8 = global.kansai + "/latest-promotion";
  console.log(global.kansai);
  axios
    .get(acc1Points)
    .then(function (response) {
      // console.log(response.data);
      setPts(response.data.account1point + "");
      //console.log(pts)
    })
    .catch(function (error) {
      console.log(error);
    });

  axios
    .get(acc2Points)
    .then(function (response) {
      // console.log(response.data);
      setPts2(response.data.account2point + "");
    })
    .catch(function (error) {
      console.log(error);
    });

  axios
    .get(expiringacc1)
    .then(function (response) {
      //console.log(response.data);
      setExp1(
        response.data.message + ": " + response.data.total_expiry_account_one
      );
    })
    .catch(function (error) {
      console.log(error);
    });

  axios
    .get(expiringacc2)
    .then(function (response) {
      //console.log(response.data);
      setExp2(
        response.data.message + ": " + response.data.total_expiry_account_two
      );
    })
    .catch(function (error) {
      console.log(error);
    });

  function switchAcc() {
    if (Type === 1) {
      if (acc === "1") {
        setPoints(pts);
        setExpPoints(exp1);
        setAcc("2");
        ToastAndroid.show("Account 2", ToastAndroid.SHORT);
      } else {
        setPoints(pts2);
        setExpPoints(exp2);
        setAcc("1");
        ToastAndroid.show("Account 1", ToastAndroid.SHORT);
      }
    }
  }

  return (
    <SafeAreaView style={gS.container}>
      <ScrollView style={gS.scroll}>
        {/* <Text style={gS.sectionTitle}>My Applications</Text> */}

        {Type == 1 ? (
          <View>
            <TouchableOpacity
              onPress={() => {
                props.navigation.navigate({
                  name: "DetailsScreen",
                  params: {
                    pageId: 3,
                    pageName: "Rewards Point",
                  },
                });
              }}
            >
              <View style={styles.settingSection}>
                <View style={styles.iconA}>
                  <Icon name="gift" type="font-awesome" color="#000" />
                </View>
                <View style={styles.iconB}>
                  <Icon name="angle-right" type="font-awesome" color="#66F" />
                </View>
                <Text style={styles.wordA}>Rewards Point</Text>
              </View>
            </TouchableOpacity>

            <View style={styles.line}></View>
            <TouchableOpacity
              onPress={() => {
                props.navigation.navigate({
                  name: "DetailsScreen",
                  params: {
                    pageId: 4,
                    pageName: "Order History",
                  },
                });
              }}
            >
              <View style={styles.settingSection}>
                <View style={styles.iconA}>
                  <Icon name="history" type="font-awesome" color="#000" />
                </View>
                {/* <View style={styles.iconB}>
                <Icon name="angle-right" type="font-awesome" color="#66F" />
              </View> */}
                <Text style={styles.wordA}>Order History</Text>
              </View>
            </TouchableOpacity>

            <View style={styles.line}></View>
            <TouchableOpacity onPress={async () => await Linking.openURL(url3)}>
              <View style={styles.settingSection}>
                <View style={styles.iconA}>
                  <Icon name="user-o" type="font-awesome" color="#000" />
                </View>
                {/* <View style={styles.iconB}>
                <Icon name="angle-right" type="font-awesome" color="#66F" />
              </View> */}
                <Text style={styles.wordA}>Account 2</Text>
              </View>
            </TouchableOpacity>

            <View style={styles.line}></View>
            <TouchableOpacity
              onPress={() => {
                props.navigation.navigate({
                  name: "DetailsScreen",
                  params: {
                    pageId: 5,
                    pageName: "Receipt History",
                  },
                });
              }}
            >
              <View style={styles.settingSection}>
                <View style={styles.iconA}>
                  <Icon
                    name="pencil-square-o"
                    type="font-awesome"
                    color="#000"
                  />
                </View>
                {/* <View style={styles.iconB}>
                <Icon name="angle-right" type="font-awesome" color="#66F" />
              </View> */}
                <Text style={styles.wordA}>Receipt History</Text>
              </View>
            </TouchableOpacity>

            <View style={styles.line}></View>
            <TouchableOpacity
              onPress={() => {
                props.navigation.navigate({
                  name: "DetailsScreen",
                  params: {
                    pageId: 6,
                    pageName: "Flash Deals",
                  },
                });
              }}
            >
              <View style={styles.settingSection}>
                <View style={styles.iconA}>
                  <Icon name="ticket" type="font-awesome" color="#000" />
                </View>
                {/* <View style={styles.iconB}>
                <Icon name="angle-right" type="font-awesome" color="#66F" />
              </View> */}
                <Text style={styles.wordA}>Flash Deals</Text>
              </View>
            </TouchableOpacity>

            <View style={styles.line}></View>
            <TouchableOpacity
              onPress={() => {
                props.navigation.navigate({
                  name: "DetailsScreen",
                  params: {
                    pageId: 7,
                    pageName: "Latest Promotions",
                  },
                });
              }}
            >
              <View style={styles.settingSection}>
                <View style={styles.iconA}>
                  <Icon name="magic" type="font-awesome" color="#000" />
                </View>
                {/* <View style={styles.iconB}>
                <Icon name="angle-right" type="font-awesome" color="#66F" />
              </View> */}
                <Text style={styles.wordA}>Latest Promotions</Text>
              </View>
            </TouchableOpacity>

            <View style={styles.line}></View>
            <TouchableOpacity
              onPress={() => {
                props.navigation.navigate({
                  name: "DetailsScreen",
                  params: {
                    pageId: 8,
                    pageName: "Product Info",
                  },
                });
              }}
            >
              <View style={styles.settingSection}>
                <View style={styles.iconA}>
                  <Icon name="info-circle" type="font-awesome" color="#000" />
                </View>
                {/* <View style={styles.iconB}>
                <Icon name="angle-right" type="font-awesome" color="#66F" />
              </View> */}
                <Text style={styles.wordA}>Product Info</Text>
              </View>
            </TouchableOpacity>

            <View style={styles.line}></View>
            <TouchableOpacity
              onPress={() => {
                props.navigation.navigate({
                  name: "DetailsScreen",
                  params: {
                    pageId: 9,
                    pageName: "Point System",
                  },
                });
              }}
            >
              <View style={styles.settingSection}>
                <View style={styles.iconA}>
                  <Icon name="shopping-bag" type="font-awesome" color="#000" />
                </View>
                {/* <View style={styles.iconB}>
                <Icon name="angle-right" type="font-awesome" color="#66F" />
              </View> */}
                <Text style={styles.wordA}>Point System</Text>
              </View>
            </TouchableOpacity>
          </View>
        ) : (
          <View>
            <View style={styles.line}></View>
            <TouchableOpacity
              onPress={() => {
                props.navigation.navigate({
                  name: "DetailsScreen",
                  params: {
                    pageId: 3,
                    pageName: "Rewards Point",
                  },
                });
              }}
            >
              <View style={styles.settingSection}>
                <View style={styles.iconA}>
                  <Icon name="gift" type="font-awesome" color="#000" />
                </View>
                {/* <View style={styles.iconB}>
                <Icon name="angle-right" type="font-awesome" color="#66F" />
              </View> */}
                <Text style={styles.wordA}>Rewards Point</Text>
              </View>
            </TouchableOpacity>

            <View style={styles.line}></View>
            <TouchableOpacity
              onPress={() => {
                props.navigation.navigate({
                  name: "DetailsScreen",
                  params: {
                    pageId: 4,
                    pageName: "Order History",
                  },
                });
              }}
            >
              <View style={styles.settingSection}>
                <View style={styles.iconA}>
                  <Icon name="history" type="font-awesome" color="#000" />
                </View>
                {/* <View style={styles.iconB}>
                <Icon name="angle-right" type="font-awesome" color="#66F" />
              </View> */}
                <Text style={styles.wordA}>Order History</Text>
              </View>
            </TouchableOpacity>

            <View style={styles.line}></View>
            <TouchableOpacity
              onPress={() => {
                props.navigation.navigate({
                  name: "DetailsScreen",
                  params: {
                    pageId: 5,
                    pageName: "Receipt History",
                  },
                });
              }}
            >
              <View style={styles.settingSection}>
                <View style={styles.iconA}>
                  <Icon
                    name="pencil-square-o"
                    type="font-awesome"
                    color="#000"
                  />
                </View>
                {/* <View style={styles.iconB}>
                <Icon name="angle-right" type="font-awesome" color="#66F" />
              </View> */}
                <Text style={styles.wordA}>Receipt History</Text>
              </View>
            </TouchableOpacity>

            <View style={styles.line}></View>
            <TouchableOpacity
              onPress={() => {
                props.navigation.navigate({
                  name: "DetailsScreen",
                  params: {
                    pageId: 6,
                    pageName: "Flash Deals",
                  },
                });
              }}
            >
              <View style={styles.settingSection}>
                <View style={styles.iconA}>
                  <Icon name="ticket" type="font-awesome" color="#000" />
                </View>
                {/* <View style={styles.iconB}>
                <Icon name="angle-right" type="font-awesome" color="#66F" />
              </View> */}
                <Text style={styles.wordA}>Flash Deals</Text>
              </View>
            </TouchableOpacity>

            <View style={styles.line}></View>
            <TouchableOpacity
              onPress={() => {
                props.navigation.navigate({
                  name: "DetailsScreen",
                  params: {
                    pageId: 7,
                    pageName: "Latest Promotions",
                  },
                });
              }}
            >
              <View style={styles.settingSection}>
                <View style={styles.iconA}>
                  <Icon name="magic" type="font-awesome" color="#000" />
                </View>
                {/* <View style={styles.iconB}>
                <Icon name="angle-right" type="font-awesome" color="#66F" />
              </View> */}
                <Text style={styles.wordA}>Latest Promotions</Text>
              </View>
            </TouchableOpacity>

            <View style={styles.line}></View>
            <TouchableOpacity
              onPress={() => {
                props.navigation.navigate({
                  name: "DetailsScreen",
                  params: {
                    pageId: 8,
                    pageName: "Product Info",
                  },
                });
              }}
            >
              <View style={styles.settingSection}>
                <View style={styles.iconA}>
                  <Icon name="info-circle" type="font-awesome" color="#000" />
                </View>
                {/* <View style={styles.iconB}>
                <Icon name="angle-right" type="font-awesome" color="#66F" />
              </View> */}
                <Text style={styles.wordA}>Product Info</Text>
              </View>
            </TouchableOpacity>

            <View style={styles.line}></View>
            <TouchableOpacity
              onPress={() => {
                props.navigation.navigate({
                  name: "DetailsScreen",
                  params: {
                    pageId: 9,
                    pageName: "Point System",
                  },
                });
              }}
            >
              <View style={styles.settingSection}>
                <View style={styles.iconA}>
                  <Icon name="shopping-bag" type="font-awesome" color="#000" />
                </View>
                {/* <View style={styles.iconB}>
                <Icon name="angle-right" type="font-awesome" color="#66F" />
              </View> */}
                <Text style={styles.wordA}>Point System</Text>
              </View>
            </TouchableOpacity>
          </View>
        )}
      </ScrollView>
    </SafeAreaView>
  );
};

export default HomeScreen;

const styles = StyleSheet.create({
  headerbackground1: {
    position: "absolute",
    top: 0,
    width: "100%",
    height: 350,
  },
  headerbackground2: {
    position: "absolute",
    top: 0,
    width: "100%",
    height: 300,
  },
  barCard: {
    alignSelf: "stretch",
    alignItems: "center",
    height: 100,
    flex: 1,
    flexDirection: "row",
    justifyContent: "space-around",
    backgroundColor: "#F9F9F9",
    borderWidth: 0,
    borderRadius: 20,
    shadowColor: "#000",
    shadowOffset: { width: 0, height: 5 },
    shadowOpacity: 0.34,
    shadowRadius: 6.27,
    elevation: 10,
  },
  barCard2: {
    //alignSelf: "stretch",
    //alignItems: "center",
    height: 50,
    flex: 1,
    //flexDirection :"row",
    //justifyContent: "space-around",
    // backgroundColor: "#F9F9F9",
    //borderWidth: 0,
    // borderRadius: 20,
    //shadowColor: "#000",
    // shadowOffset: { width: 0, height: 5,},
    // shadowOpacity: 0.34,
    // shadowRadius: 6.27,
    // elevation: 10,
  },
  barIcon: {
    flex: 1,
    alignSelf: "auto",
    height: 80,
    alignItems: "center",
  },
  myInfoIcon: {
    width: 80,
    height: 80,
    alignItems: "center",
  },
  verti: {
    flexDirection: "column",
    justifyContent: "center",
  },
  card2: {
    width: 200,
    padding: 15,
    height: 80,
    flexDirection: "row",
    borderRadius: 15,
    alignItems: "center",
    margin: 10,

    shadowColor: "#000",
    shadowOffset: { width: 0, height: 5 },
    shadowOpacity: 0.34,
    shadowRadius: 6.27,
    elevation: 5,
  },
  settingSection: {
    padding: 10,
    backgroundColor: "white",
    flexDirection: "row",
    alignItems: "center",
  },
  wordA: {
    color: "black",
    fontSize: 16,
    position: "absolute",
    left: "17%",
  },
  wordB: {
    fontSize: 12,
    color: "gray",
    position: "absolute",
    right: "15%",
  },
  iconA: {
    margin: 10,
  },
  iconB: {
    position: "absolute",
    right: 15,
    margin: 10,
  },
  line: {
    width: "100%",
    height: 1,
    backgroundColor: "lightgray",
    marginLeft: "15%",
  },
  logout: {
    marginHorizontal: 30,
    marginVertical: 20,
    borderWidth: 2,
    borderColor: "red",
    borderRadius: 50,
    padding: 10,
    alignItems: "center",
  },
  version: {
    color: "blue",
    fontSize: 12,
    textAlign: "center",
    position: "absolute",
    margin: 5,
    bottom: 0,
  },
});
