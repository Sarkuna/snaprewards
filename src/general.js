import 'react-native-gesture-handler';

import React from 'react';
import gS from './globalStyle';
import { SafeAreaView, Text, StyleSheet, View, Dimensions, Clipboard, ToastAndroid, Pressable} from 'react-native';
import * as Device from 'expo-device';
import { FlatList, TouchableOpacity } from 'react-native-gesture-handler';
import Constants from 'expo-constants';


export default function App({ navigation }) {

  console.log("General Page");  

  const screenWidth = Math.round(Dimensions.get('window').width);
  const screenHeight = Math.round(Dimensions.get('window').height);

  const copyToClipboard = () => {
    Clipboard.setString(
      "\nUniversal Unique ID: " + Constants.installationId +
      "\nActual Device: " + Device.isDevice.toString().toString() +
      "\nDevice Type: " + Device.DeviceType.PHONE +
      "\nDevice Brand: " + Device.brand +
      "\nDevice Modal: " + Device.modelName +
      "\nDevice Year: " + Device.deviceYearClass +
      "\nDevice Type: " + Device.osName +
      "\nOS Version: " + Device.osVersion +
      "\nProcessor Type: " + Device.supportedCpuArchitectures +
      "\nTotal Ram: " + Device.totalMemory +
      "\nScreen Width: " + screenWidth +
      "\nScreen Height: " + screenHeight);
      ToastAndroid.show("Copied to Clipboard!", ToastAndroid.SHORT);
  }

  return (
    <SafeAreaView style={gS.container}>
      
      <View style={[styles.box, {backgroundColor:"#FEE"}]}>
        <View>
          <Text style={styles.word}>UUID: </Text>
        </View>
        <View>
          <Text style={styles.word}>{Constants.installationId}</Text>
        </View>
      </View>

      <View style={[styles.box, {backgroundColor:"#FEE"}]}>
        <View>
          <Text style={styles.word}>Actual Device: </Text>
          <Text style={styles.word}>Device Type:</Text>
          <Text style={styles.word}>Device Brand:</Text>
          <Text style={styles.word}>Device Modal:</Text>
          <Text style={styles.word}>Device Year:</Text>
          <Text style={styles.word}>OS Type:</Text>
          <Text style={styles.word}>OS Version:</Text>
          <Text style={styles.word}>Processor Type: </Text>
          <Text style={styles.word}>Total Memory:</Text>
          <Text style={styles.word}>Screen Width:</Text>
          <Text style={styles.word}>Screen Height:</Text>
        </View>
        <View>
          <Text style={styles.word}>{Device.isDevice.toString()}</Text>
          <Text style={styles.word}>{Device.DeviceType.PHONE}</Text>
          <Text style={styles.word}>{Device.brand}</Text>
          <Text style={styles.word}>{Device.modelName}</Text>
          <Text style={styles.word}>{Device.deviceYearClass}</Text>
          <Text style={styles.word}>{Device.osName}</Text>
          <Text style={styles.word}>{Device.osVersion}</Text>
          <Text style={styles.word}>{Device.supportedCpuArchitectures}</Text>
          <Text style={styles.word}>{Device.totalMemory}</Text>
          <Text style={styles.word}>{screenWidth}</Text>
          <Text style={styles.word}>{screenHeight}</Text>
        </View>
      </View>

      <TouchableOpacity onPress={() => copyToClipboard()}>
        <View style={[styles.box, {backgroundColor: "lightblue"}]}>
          <Text>Copy to Clipboard</Text>
        </View>
      </TouchableOpacity>

    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  word:{
    color: "red",
    fontSize: 16,
  },
  box:{
    flexDirection: "row",
    padding: 15, 
    borderRadius: 15,
    marginBottom: 15,
  }
})