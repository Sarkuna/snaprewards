import React, { useState, useEffect } from "react";

import gS from "../globalStyle";
import { TouchableWithoutFeedback } from "react-native-gesture-handler";
import { HelperText } from "react-native-paper";
import {
  Alert,
  Text,
  View,
  TextInput,
  SafeAreaView,
  ScrollView,
  Switch,
  StyleSheet,
  Picker,
} from "react-native";
import * as SecureStore from "expo-secure-store";
import { useSelector } from "react-redux";
import axios from "axios";
import Colors from "../../constants/Colors";

const FiltersSwitch = (props) => {
  return (
    <View style={styles.filterContainer}>
      <Text style={styles.title}>{props.label}</Text>
      <Switch
        trackColor={{ true: Colors.primary }}
        thumbColor={Platform.OS === "android" ? Colors.primary : ""}
        value={props.state}
        onValueChange={props.onChange}
      />
    </View>
  );
};

const addAddress = (props) => {
  //const myToken = useSelector((state) => state.auth.access_token);
  const regions = global.url + "/country/region";
  const [region, setRegion] = useState("");
  let itIsDefault;
  const myToken = useSelector((state) => state.auth.access_token);
  console.log("changePassword");

  const [titleIsValid, setTitleIsValid] = useState(false);
  const [LastNameIsValid, setLastNameIsValid] = useState(false);
  const [address1IsValid, setAddress1IsValid] = useState(false);
  const [postcodeIsValid, setPostcodeIsValid] = useState(false);
  const [cityIsValid, setCityIsValid] = useState(false);
  const [regionIsValid, setRegionIsValid] = useState(false);
  const [countryIsValid, setCountryIsValid] = useState(false);

  const [firstNameValidSubmit, setFirstNameValidSubmit] = useState(false);
  const [error, setError] = useState();

  const [company, setCompany] = useState("");
  const [firstName, setFirstName] = useState("");
  const [lastName, setLastName] = useState("");
  const [address1, setAddress1] = useState("");
  const [address2, setAddress2] = useState("");
  const [postcode, setPostcode] = useState("");
  const [city, setCity] = useState("");

  const [region0, setRegion0] = useState("");
  const [region1, setRegion1] = useState("");
  const [region2, setRegion2] = useState("");
  const [region3, setRegion3] = useState("");
  const [region4, setRegion4] = useState("");
  const [region5, setRegion5] = useState("");
  const [region6, setRegion6] = useState("");
  const [region7, setRegion7] = useState("");
  const [region8, setRegion8] = useState("");
  const [region9, setRegion9] = useState("");
  const [region10, setRegion10] = useState("");
  const [region11, setRegion11] = useState("");
  const [region12, setRegion12] = useState("");
  const [region13, setRegion13] = useState("");

  const [regionValue0, setRegionValue0] = useState("");
  const [regionValue1, setRegionValue1] = useState("");
  const [regionValue2, setRegionValue2] = useState("");
  const [regionValue3, setRegionValue3] = useState("");
  const [regionValue4, setRegionValue4] = useState("");
  const [regionValue5, setRegionValue5] = useState("");
  const [regionValue6, setRegionValue6] = useState("");
  const [regionValue7, setRegionValue7] = useState("");
  const [regionValue8, setRegionValue8] = useState("");
  const [regionValue9, setRegionValue9] = useState("");
  const [regionValue10, setRegionValue10] = useState("");
  const [regionValue11, setRegionValue11] = useState("");
  const [regionValue12, setRegionValue12] = useState("");
  const [regionValue13, setRegionValue13] = useState("");

  const [country0, setCountry0] = useState("");
  const [country1, setCountry1] = useState("");
  const [country0Id, setCountry0Id] = useState("");
  const [country1Id, setCountry1Id] = useState("");

  const [selectedRegion, setSelectedRegion] = useState("");
  const [selectedCountry, setSelectedCountry] = useState("");
  const [selectedDefault, setSelectedDefault] = useState("");

  const [isVeganFree, setIsVeganFree] = useState(false);
  //console.log(selectedDefault);

  const titleChangeHandler = (text) => {
    if (text.trim().length === 0) {
      setTitleIsValid(false);
    } else {
      setTitleIsValid(true);
    }
    setFirstName(text);
  };

  const changeLastName = (text) => {
    if (text.trim().length === 0) {
      setLastNameIsValid(false);
    } else {
      setLastNameIsValid(true);
    }
    setLastName(text);
  };

  const changeAddress1 = (text) => {
    if (text.trim().length === 0) {
      setAddress1IsValid(false);
    } else {
      setAddress1IsValid(true);
    }
    setAddress1(text);
  };
  const changePostscode = (text) => {
    if (text.trim().length === 0) {
      setPostcodeIsValid(false);
    } else {
      setPostcodeIsValid(true);
    }
    setPostcode(text);
  };

  const changeCity = (text) => {
    if (text.trim().length === 0) {
      setCityIsValid(false);
    } else {
      setCityIsValid(true);
    }
    setCity(text);
  };

  const changeRegion = (itemValue) => {
    if (selectedRegion === 0 || selectedRegion === "0") {
      setRegionIsValid(false);
    } else {
      setRegionIsValid(true);
    }
    setSelectedRegion(itemValue);
  };

  const changeCountry = (itemValue) => {
    if (selectedCountry === 0 || selectedCountry === "0") {
      setCountryIsValid(false);
    } else {
      setCountryIsValid(true);
    }
    setSelectedCountry(itemValue);
  };
  console.log(selectedCountry);
  // if (selectedRegion === "0") {
  //   setRegionIsValid(false);
  // } else {
  //   setRegionIsValid(true);
  // }

  // const titleChangeHandler = (text) => {
  //   if (text.trim().length === 0) {
  //     setTitleIsValid(false);
  //   } else {
  //     setTitleIsValid(true);
  //   }
  //   setFirstName(text);
  // };

  if (selectedDefault === true) {
    itIsDefault = 1;
  } else {
    itIsDefault = 0;
  }
  let token;
  //console.log(itIsDefault);

  useEffect(() => {
    if (error) {
      // Alert.alert("Oops!", error, [{ text: "Okay" }]);
      setFirstNameValidSubmit(false);
    } else {
      setFirstNameValidSubmit(true);
    }
  }, [error]);

  // axios
  //   .get(regions)
  //   .then(function (response) {
  //     //console.log(response);
  //     setRegion(response.data);
  //   })
  //   .catch(function (error) {
  //     console.log(error);
  //   });

  // console.log(region);

  const read = async () => {
    try {
      token = await SecureStore.getItemAsync("token");
    } catch (e) {
      console.log(e);
    }
  };

  read();

  const submitUrl =
    global.url + "/address-book/new-address?access-token=" + myToken;

  const regionUrl = global.url + "/country/region";

  const countryUrl = global.url + "/country/country";

  axios
    .get(regionUrl)
    .then(function (response) {
      //setAddresslistView(response.data.address_lists[0].Postcode + "");
      // console.log(response.data);
      setRegion0(response.data[0].state_name);
      setRegion1(response.data[1].state_name);
      setRegion2(response.data[2].state_name);
      setRegion3(response.data[3].state_name);
      setRegion4(response.data[4].state_name);
      setRegion5(response.data[5].state_name);
      setRegion6(response.data[6].state_name);
      setRegion7(response.data[7].state_name);
      setRegion8(response.data[8].state_name);
      setRegion9(response.data[9].state_name);
      setRegion10(response.data[10].state_name);
      setRegion11(response.data[11].state_name);
      setRegion12(response.data[12].state_name);
      setRegion13(response.data[13].state_name);

      setRegionValue0(response.data[0].states_id);
      setRegionValue1(response.data[1].states_id);
      setRegionValue2(response.data[2].states_id);
      setRegionValue3(response.data[3].states_id);
      setRegionValue4(response.data[4].states_id);
      setRegionValue5(response.data[5].states_id);
      setRegionValue6(response.data[6].states_id);
      setRegionValue7(response.data[7].states_id);
      setRegionValue8(response.data[8].states_id);
      setRegionValue9(response.data[9].states_id);
      setRegionValue10(response.data[10].states_id);
      setRegionValue11(response.data[11].states_id);
      setRegionValue12(response.data[12].states_id);
      setRegionValue13(response.data[13].states_id);

      //setFirstName(response.data.address_lists[0].Company);
      //setAddresslistView(response.data.address_lists[0].Postcode);
    })
    .catch(function (error) {
      console.log(error);
    });

  axios
    .get(countryUrl)
    .then(function (response) {
      //setAddresslistView(response.data.address_lists[0].Postcode + "");
      //console.log(response.data);
      setCountry0(response.data[0].name);
      setCountry1(response.data[1].name);
      setCountry0Id(response.data[0].country_id);
      setCountry1Id(response.data[1].country_id);

      //setFirstName(response.data.address_lists[0].Company);
      //setAddresslistView(response.data.address_lists[0].Postcode);
    })
    .catch(function (error) {
      console.log(error);
    });

  const changeCompany = (text) => {
    setCompany(text);
  };
  const changeFirstName = (text) => {
    setFirstName(text);
  };
  // const changeLastName = (text) => {
  //   setLastName(text);
  // };
  // const changeAddress1 = (text) => {
  //   setAddress1(text);
  // };
  const changeAddress2 = (text) => {
    setAddress2(text);
  };
  // const changePostscode = (text) => {
  //   setPostcode(text);
  // };
  // const changeCity = (text) => {
  //   setCity(text);
  // };

  // console.log(pass1);
  // console.log(pass2);

  function handler() {
    if (
      !titleIsValid ||
      !LastNameIsValid ||
      !address1IsValid ||
      !postcodeIsValid ||
      !cityIsValid ||
      !regionIsValid ||
      !countryIsValid
    ) {
      setFirstNameValidSubmit(false);
    } else {
      props.navigation.navigate("AddressBook"),
        axios
          .post(submitUrl, {
            company: company,
            first_name: firstName,
            last_name: lastName,
            address_1: address1,
            address_2: address2,
            postcode: postcode,
            city: city,
            region: selectedRegion,
            country: selectedCountry,
            default_address: itIsDefault,
            // id: addressId,
          })
          .then(function (response) {
            console.log(response.data);
            if (response.data.message === true) {
              alert("Succesfully edited");
            } else {
              alert("Somthing went wrong");
            }
          })
          .catch(function (error) {
            console.log(error);
            setError(error.message);
          });
    }
  }
  return (
    <SafeAreaView style={gS.container}>
      <ScrollView style={gS.scroll}>
        <View style={gS.section2}>
          <View style={styles.containerForm}>
            <View style={gS.section2}>
              <View style={styles.formControl}>
                <View style={styles.form}>
                  <Text style={styles.label}>Company</Text>
                  <TextInput
                    style={styles.input}
                    id="title"
                    errorText="Please enter a valid title"
                    keyboardType="default"
                    autoCapitalize="sentences"
                    autoCorrect
                    returnKeyType="next"
                    onChangeText={changeCompany}
                    required
                    value={company}
                  />
                  <Text style={styles.label}>First Name</Text>
                  <TextInput
                    style={styles.input}
                    id="title"
                    errorText="Please enter a valid title"
                    keyboardType="default"
                    autoCapitalize="sentences"
                    autoCorrect
                    returnKeyType="next"
                    onChangeText={titleChangeHandler}
                    required
                    value={firstName}
                  />
                  {!firstNameValidSubmit && !titleIsValid && (
                    <HelperText type="error">
                      First name cannot be blank.
                    </HelperText>
                  )}
                  <Text style={styles.label}>Last Name</Text>
                  <TextInput
                    id="imageUrl"
                    //  label="Image Url"
                    errorText="Please enter a valid image Url"
                    keyboardType="default"
                    returnKeyType="next"
                    onChangeText={changeLastName}
                    required
                    style={styles.input}
                    value={lastName}
                  />
                  {!firstNameValidSubmit && !LastNameIsValid && (
                    <HelperText type="error">
                      Last name cannot be blank.
                    </HelperText>
                  )}
                  <Text style={styles.label}>Address 1</Text>
                  <TextInput
                    id="imageUrl"
                    //  label="Image Url"
                    errorText="Please enter a valid image Url"
                    keyboardType="default"
                    returnKeyType="next"
                    onChangeText={changeAddress1}
                    required
                    style={styles.input}
                    // style={styles.input}
                    // id="price"
                    // label="Price"
                    // errorText="Please enter a valid Price"
                    // keyboardType="decimal-pad"
                    // onInputChange={changeAddress1}
                    // returnKeyType="next"
                    // required
                    // min={0.1}
                    value={address1}
                  />
                  {!firstNameValidSubmit && !address1IsValid && (
                    <HelperText type="error">
                      Address1 cannot be blank.
                    </HelperText>
                  )}

                  <Text style={styles.label}>Address 2</Text>
                  <TextInput
                    id="imageUrl"
                    //  label="Image Url"
                    errorText="Please enter a valid image Url"
                    keyboardType="default"
                    returnKeyType="next"
                    onChangeText={changeAddress2}
                    required
                    style={styles.input}
                    // initialValue={editedProduct ? editedProduct.description : ""}
                    // initiallyValid={!!editedProduct}
                    // minLength={5}
                    value={address2}
                  />
                  <Text style={styles.label}>Postcode</Text>
                  <TextInput
                    style={styles.input}
                    id="title"
                    errorText="Please enter a valid title"
                    keyboardType="default"
                    autoCapitalize="sentences"
                    autoCorrect
                    returnKeyType="next"
                    onChangeText={changePostscode}
                    required
                    value={postcode}
                  />
                  {!firstNameValidSubmit && !postcodeIsValid && (
                    <HelperText type="error">
                      Postcode cannot be blank.
                    </HelperText>
                  )}
                  <Text style={styles.label}>City</Text>
                  <TextInput
                    style={styles.input}
                    id="title"
                    errorText="Please enter a valid title"
                    keyboardType="default"
                    autoCapitalize="sentences"
                    autoCorrect
                    returnKeyType="next"
                    onChangeText={changeCity}
                    required
                    value={city}
                  />
                  {!firstNameValidSubmit && !cityIsValid && (
                    <HelperText type="error">City cannot be blank.</HelperText>
                  )}
                  <Text style={styles.label}>State</Text>
                  {/* <View style={styles.sectionRC}> */}
                  <View
                    style={{
                      color: "#F00",
                      // borderBottomWidt: 10,
                      borderColor: "#ccc",
                      borderBottomWidth: 1,
                    }}
                  >
                    <Picker
                      selectedValue={selectedRegion}
                      onValueChange={changeRegion}
                    >
                      <Picker.Item
                        style={{ color: "#F00" }}
                        label="- State -"
                        value="0"
                      />
                      <Picker.Item
                        label={region0}
                        value={regionValue0}
                      ></Picker.Item>
                      <Picker.Item
                        label={region1}
                        value={regionValue1}
                      ></Picker.Item>
                      <Picker.Item
                        label={region2}
                        value={regionValue2}
                      ></Picker.Item>
                      <Picker.Item
                        label={region3}
                        value={regionValue3}
                      ></Picker.Item>
                      <Picker.Item label={region4} value={regionValue4} />
                      <Picker.Item
                        label={region5}
                        value={regionValue5}
                      ></Picker.Item>
                      <Picker.Item
                        label={region6}
                        value={regionValue6}
                      ></Picker.Item>
                      <Picker.Item
                        label={region7}
                        value={regionValue7}
                      ></Picker.Item>
                      <Picker.Item label={region8} value={regionValue8} />
                      <Picker.Item
                        label={region9}
                        value={regionValue9}
                      ></Picker.Item>
                      <Picker.Item
                        label={region10}
                        value={regionValue10}
                      ></Picker.Item>
                      <Picker.Item
                        label={region11}
                        value={regionValue11}
                      ></Picker.Item>
                      <Picker.Item label={region12} value={regionValue12} />
                      <Picker.Item
                        label={region13}
                        value={regionValue13}
                      ></Picker.Item>
                    </Picker>
                  </View>
                  {!firstNameValidSubmit && !regionIsValid && (
                    <HelperText type="error">
                      Region cannot be blank.
                    </HelperText>
                  )}
                  {/* </View> */}
                  {/* <View style={gS.section}> */}
                  <Text style={styles.label}>Country</Text>
                  <View
                    style={{
                      color: "#F00",
                      // borderBottomWidt: 10,
                      borderColor: "#ccc",
                      borderBottomWidth: 1,
                    }}
                  >
                    <Picker
                      selectedValue={selectedCountry}
                      onValueChange={changeCountry}
                    >
                      <Picker.Item
                        label="- Country -"
                        value="0"
                        style={{ color: "#F00" }}
                      />
                      <Picker.Item
                        label={country0}
                        value={country0Id}
                      ></Picker.Item>
                      <Picker.Item
                        label={country1}
                        value={country1Id}
                      ></Picker.Item>
                    </Picker>
                  </View>
                  {!firstNameValidSubmit && !countryIsValid && (
                    <HelperText type="error">
                      Country cannot be blank.
                    </HelperText>
                  )}
                  {/* </View> */}
                </View>
              </View>
            </View>
          </View>
          {/* <View style={styles.containerFormDefault}>
            <FiltersSwitch
              label="Set as default address"
              state={selectedDefault}
              onChange={(newValue) => setSelectedDefault(newValue)}
            />
          </View> */}

          {/* <View style={styles.containerFormDefault2}>
            <Text style={styles.DeleteAddText}>Delete Address</Text>
          </View> */}
          <TouchableWithoutFeedback onPress={handler}>
            <View style={gS.blueBtn}>
              <Text style={gS.blueBtnWord}>Submit</Text>
            </View>
          </TouchableWithoutFeedback>
        </View>
      </ScrollView>
    </SafeAreaView>
  );
};

export default addAddress;

const styles = StyleSheet.create({
  containerForm: {
    shadowColor: "#f9f5f5",
    shadowOpacity: 0.26,
    shadowOffset: { width: 0, height: 2 },
    shadowRadius: 8,
    elevation: 1,
    borderRadius: 5,
    borderLeftColor: "#f9f5f5",
    borderStartColor: "#f9f5f5",
    borderColor: "#f9f5f5",
    height: "90%",
    width: "100%",
    // margin: 5,
    backgroundColor: "#FFFFFF",
  },
  containerFormDefault: {
    marginTop: 10,
    shadowColor: "#f9f5f5",
    shadowOpacity: 0.26,
    shadowOffset: { width: 0, height: 2 },
    shadowRadius: 8,
    elevation: 1,
    borderRadius: 5,
    borderLeftColor: "#f9f5f5",
    borderStartColor: "#f9f5f5",
    borderColor: "#f9f5f5",
    height: "10%",
    width: "100%",

    backgroundColor: "#FFFFFF",
  },
  containerFormDefault2: {
    marginTop: 10,

    shadowColor: "#f9f5f5",
    shadowOpacity: 0.26,
    shadowOffset: { width: 0, height: 2 },
    shadowRadius: 8,
    elevation: 1,
    borderRadius: 5,
    // borderLeftColor: "#f9f5f5",
    // borderStartColor: "#f9f5f5",
    borderColor: "#f9f5f5",
    height: "10%",
    width: "100%",
    // margin: 5,
    backgroundColor: "#FFFFFF",
  },
  filterContainer: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    marginVertical: 5,
    width: "80%",
  },
  title: {
    fontFamily: "open-sans-bold",
    fontSize: 17,
    margin: 20,
    textAlign: "center",
    justifyContent: "center",
  },
  DeleteAddText: {
    fontFamily: "open-sans-bold",
    fontSize: 17,
    margin: 20,
    color: "#a90308",
    // textAlign: "center",
  },
  form: {
    margin: 10,
  },
  formControl: {
    width: "100%",
  },
  label: {
    fontFamily: "open-sans-bold",
    marginVertical: 8,
  },
  input: {
    paddingHorizontal: 2,
    paddingVertical: 2,
    borderBottomColor: "#ccc",
    borderBottomWidth: 1,
    fontSize: 18,
  },
  sectionRC: {
    flex: 1,
    margin: 10,
    height: 100,
    marginTop: 30,
  },
  formRC: {
    borderWidth: 1,
    borderColor: "gray",
    borderRadius: 5,
    backgroundColor: "#EFEFEF",
    marginTop: -40,
  },
});
