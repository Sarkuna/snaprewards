import React, { useState } from "react";
import axios from "axios";
import gS from "../globalStyle";
import { TouchableWithoutFeedback } from "react-native-gesture-handler";
import {
  Alert,
  Text,
  View,
  TextInput,
  SafeAreaView,
  ScrollView,
} from "react-native";
import * as SecureStore from "expo-secure-store";
import { useSelector } from "react-redux";
import { StyleSheet } from "react-native";

function ChangePassword({ navigation }) {
  const myToken = useSelector((state) => state.auth.access_token);
  console.log("changePassword");

  const [pass1, setPass1] = useState("");
  const [pass2, setPass2] = useState("");
  let token;

  const read = async () => {
    try {
      token = await SecureStore.getItemAsync("token");
    } catch (e) {
      console.log(e);
    }
  };

  read();

  const url =
    "http://api.businessboosters.com.my/profile/change-password?access-token=" +
    myToken;

  const changeHandler1 = (text) => {
    setPass1(text);
  };
  const changeHandler2 = (text) => {
    setPass2(text);
  };
  console.log(pass1);
  console.log(pass2);
  const handler = () => {
    Alert.alert(
      "Confirm to change password?",
      "Confirm to change password?",
      [
        {
          text: "Cancel",
          onPress: () => console.log("Cancel Pressed"),
          style: "cancel",
        },
        {
          text: "Confirm",
          onPress: () => {
            navigation.goBack(),
              axios
                .post(url, {
                  new_password: pass1,
                  retype_password: pass2,
                })
                .then(function (response) {
                  console.log(response.data);
                  if (response.data.message == "Yes") {
                    alert("Password Changed");
                  } else {
                    alert("Password Not matched");
                  }
                })
                .catch(function (error) {
                  console.log(error);
                });
          },
        },
      ],
      { cancelable: false }
    );
  };

  return (
    <SafeAreaView style={gS.container}>
      <ScrollView style={gS.scroll}>
        <View style={gS.section2}>
          <View style={gS.section2}>
            <Text style={styles.label}>New Password</Text>
            {/* <TextInput
              style={gS.form}
              secureTextEntry={true}
              value={pass1}
              onChangeText={changeHandler1}
            /> */}
            <TextInput
              style={styles.input}
              //id="title"
              //errorText="Please enter a valid title"
              //keyboardType="default"
              //autoCapitalize="sentences"
              //autoCorrect
              secureTextEntry={true}
              returnKeyType="next"
              onChangeText={changeHandler1}
              //required
              value={pass1}
            />
          </View>

          <View style={gS.section2}>
            <Text style={styles.label}>Retype Password</Text>
            {/* <TextInput
              style={gS.form}
              secureTextEntry={true}
              value={pass2}
              onChangeText={changeHandler2}
            /> */}
            <TextInput
              style={styles.input}
              //id="title"
              //errorText="Please enter a valid title"
              //keyboardType="default"
              //autoCapitalize="sentences"
              //autoCorrect
              secureTextEntry={true}
              returnKeyType="next"
              onChangeText={changeHandler2}
              //required
              value={pass2}
            />
          </View>

          <TouchableWithoutFeedback onPress={() => handler()}>
            <View style={gS.blueBtn}>
              <Text style={gS.blueBtnWord}>Change Password</Text>
            </View>
          </TouchableWithoutFeedback>
        </View>
      </ScrollView>
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  input: {
    paddingHorizontal: 2,
    paddingVertical: 2,
    borderBottomColor: "#ccc",
    borderBottomWidth: 1,
    fontSize: 18,
  },
  label: {
    fontFamily: "open-sans-bold",
    marginVertical: 8,
  },
});
export default ChangePassword;
