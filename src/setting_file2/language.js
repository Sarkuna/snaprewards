import React, { useState } from 'react';
import gS from "../globalStyle";
import { Icon } from 'react-native-elements'
import { Text,  View, SafeAreaView, ScrollView, StyleSheet, } from 'react-native';
import { TouchableOpacity } from 'react-native-gesture-handler';

function Lang() {

  console.log("Language Page");

  //Radio Button

  const [size1, setSize1] = useState(30);
  const [size2, setSize2] = useState(0);
  
  const use11 = () => {
    setSize1(30);
    setSize2(0);
    console.log("Engligh selected");
  }
  const use22 = () => {
    setSize1(0);
    setSize2(30);
    console.log("Bahasa selected");
  }

  return (
    <SafeAreaView style={gS.container}>
      <ScrollView style={gS.scroll}>

        <View style={styles.card}>

          <TouchableOpacity onPress={ use11 }>
            <View style={{flexDirection:"row", alignItems:"center"}}>
              <Text style={styles.word}>English</Text>
              <View style={styles.icon}><Icon name="check" type='font-awesome' color='#66F' size={size1}/></View>
            </View>
          </TouchableOpacity>

          <View style={styles.line}></View>

          <TouchableOpacity onPress={ use22 }>
            <View style={{flexDirection:"row", alignItems:"center"}}>
              <Text style={styles.word}>Bahasa Melayu</Text>
              <View style={styles.icon}><Icon name="check" type='font-awesome' color='#66F' size={size2}/></View>
            </View>
          </TouchableOpacity>

        </View>
      </ScrollView>
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({

  card:{
    marginTop: "20%",
    margin: 15,
    padding: 20,
    borderRadius: 15,
    backgroundColor: "white",
    width: "90%",

    shadowColor: "#000",
    shadowOffset: { width: 0, height: 1, },
    shadowOpacity: 0.34,
    shadowRadius: 6.27,
    elevation: 5,
  },
  line:{
    width: "100%",
    height: 1,
    backgroundColor: "gray",
    marginVertical: 15,
  },
  word:{
    fontSize: 20,
  },
  icon:{
    position: "absolute",
    right: 5,
  }
});

export default Lang;