import "react-native-gesture-handler";

import React from "react";
import {
  StyleSheet,
  Text,
  View,
  SafeAreaView,
  ScrollView,
  Linking,
} from "react-native";
import { Icon } from "react-native-elements";

import gS from "../globalStyle";
import { TouchableOpacity } from "react-native-gesture-handler";

export default function App({ navigation }) {
  console.log("Setting Launched");

  const directToAboutUs = "https://www.kansaimalaysia.com/about.html";

  return (
    <SafeAreaView style={gS.container}>
      <ScrollView style={gS.scroll}>
        {/* 
        <TouchableOpacity>
          <View style={styles.settingSection}>
            <View style={styles.iconA}>
              <Icon name='lock' type='font-awesome' color='#000'/>
            </View>
            <View style={styles.iconB}>
              <Icon name='angle-right' type='font-awesome' color='#66F'/>
            </View>
            <Text style={styles.wordA}>Change 6-Digit PIN</Text>
          </View>
        </TouchableOpacity>

        <View style={styles.line}></View> */}

        <TouchableOpacity onPress={() => navigation.navigate("ChangePassword")}>
          <View style={styles.settingSection}>
            <View style={styles.iconA}>
              <Icon name="lock" type="font-awesome" color="#000" />
            </View>
            {/* <View style={styles.iconB}>
              <Icon name="angle-right" type="font-awesome" color="#66F" />
            </View> */}
            <Text style={styles.wordA}>Change Password</Text>
          </View>
        </TouchableOpacity>

        <View style={styles.line}></View>

        <TouchableOpacity onPress={() => navigation.navigate("AboutApp")}>
          <View style={styles.settingSection}>
            <View style={styles.iconA}>
              <Icon name="info" type="font-awesome" color="#000" />
            </View>
            {/* <View style={styles.iconB}>
              <Icon name="angle-right" type="font-awesome" color="#66F" />
            </View> */}
            <Text style={styles.wordA}>About App</Text>
          </View>
        </TouchableOpacity>

        <View style={styles.line}></View>

        <TouchableOpacity onPress={() => navigation.navigate("Lang")}>
          <View style={styles.settingSection}>
            <View style={styles.iconA}>
              <Icon name="globe" type="font-awesome" color="#000" />
            </View>
            {/* <View style={styles.iconB}>
              <Icon name="angle-right" type="font-awesome" color="#66F" />
            </View> */}
            <Text style={styles.wordA}>Language</Text>
          </View>
        </TouchableOpacity>

        <View style={styles.line}></View>
      </ScrollView>
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  line: {
    width: "100%",
    height: 1,
    backgroundColor: "lightgray",
    marginLeft: "15%",
  },
  settingSection: {
    padding: 10,
    backgroundColor: "white",
    flexDirection: "row",
    alignItems: "center",
  },
  wordA: {
    color: "black",
    fontSize: 16,
    position: "absolute",
    left: "15%",
  },
  iconA: {
    margin: 10,
  },
  iconB: {
    position: "absolute",
    right: 15,
    margin: 10,
  },
});
