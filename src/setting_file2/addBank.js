import React, { useState, useEffect } from "react";
import axios from "axios";
import gS from "../globalStyle";
import { TouchableWithoutFeedback } from "react-native-gesture-handler";
import {
  Alert,
  Text,
  View,
  TextInput,
  SafeAreaView,
  ScrollView,
  Switch,
  StyleSheet,
  Picker,
} from "react-native";
import { HelperText } from "react-native-paper";
import * as SecureStore from "expo-secure-store";
import { useSelector } from "react-redux";
import Colors from "../../constants/Colors";
import { TouchableOpacity } from "react-native";

const FiltersSwitch = (props) => {
  return (
    <View style={styles.filterContainer}>
      <Text style={styles.title}>{props.label}</Text>
      <Switch
        trackColor={{ true: Colors.primary }}
        thumbColor={Platform.OS === "android" ? Colors.primary : ""}
        value={props.state}
        onValueChange={props.onChange}
      />
    </View>
  );
};

const addBank = (props) => {
  const myToken = useSelector((state) => state.auth.access_token);
  console.log("changePassword");

  const [bankNameIsValid, setBankNameIsValid] = useState(false);
  const [accountNameIsValid, setAccountNameIsValid] = useState(false);
  const [accountNumberIsValid, setAccountNumberIsValid] = useState(false);
  const [nricPassportIsValid, setNricPassportIsValid] = useState(false);
  const [submitValidation, setSubmitValidation] = useState(false);
  const [error, setError] = useState();

  const [accountName, setAccountName] = useState("");
  const [accountNumber, setAccountNumber] = useState("");
  const [nricPassport, setNricPassport] = useState("");

  const [region0, setRegion0] = useState("");
  const [region1, setRegion1] = useState("");
  const [region2, setRegion2] = useState("");
  const [region3, setRegion3] = useState("");
  const [region4, setRegion4] = useState("");
  const [region5, setRegion5] = useState("");
  const [region6, setRegion6] = useState("");
  const [region7, setRegion7] = useState("");
  const [region8, setRegion8] = useState("");
  const [region9, setRegion9] = useState("");
  const [region10, setRegion10] = useState("");
  const [region11, setRegion11] = useState("");
  const [region12, setRegion12] = useState("");
  const [region13, setRegion13] = useState("");
  const [region14, setRegion14] = useState("");
  const [region15, setRegion15] = useState("");
  const [region16, setRegion16] = useState("");
  const [region17, setRegion17] = useState("");
  const [region18, setRegion18] = useState("");
  const [region19, setRegion19] = useState("");
  const [region20, setRegion20] = useState("");
  const [region21, setRegion21] = useState("");
  const [region22, setRegion22] = useState("");
  const [region23, setRegion23] = useState("");
  const [region24, setRegion24] = useState("");
  const [region25, setRegion25] = useState("");
  const [region26, setRegion26] = useState("");
  const [region27, setRegion27] = useState("");

  const [regionValue0, setRegionValue0] = useState("");
  const [regionValue1, setRegionValue1] = useState("");
  const [regionValue2, setRegionValue2] = useState("");
  const [regionValue3, setRegionValue3] = useState("");
  const [regionValue4, setRegionValue4] = useState("");
  const [regionValue5, setRegionValue5] = useState("");
  const [regionValue6, setRegionValue6] = useState("");
  const [regionValue7, setRegionValue7] = useState("");
  const [regionValue8, setRegionValue8] = useState("");
  const [regionValue9, setRegionValue9] = useState("");
  const [regionValue10, setRegionValue10] = useState("");
  const [regionValue11, setRegionValue11] = useState("");
  const [regionValue12, setRegionValue12] = useState("");
  const [regionValue13, setRegionValue13] = useState("");
  const [regionValue14, setRegionValue14] = useState("");
  const [regionValue15, setRegionValue15] = useState("");
  const [regionValue16, setRegionValue16] = useState("");
  const [regionValue17, setRegionValue17] = useState("");
  const [regionValue18, setRegionValue18] = useState("");
  const [regionValue19, setRegionValue19] = useState("");
  const [regionValue20, setRegionValue20] = useState("");
  const [regionValue21, setRegionValue21] = useState("");
  const [regionValue22, setRegionValue22] = useState("");
  const [regionValue23, setRegionValue23] = useState("");
  const [regionValue24, setRegionValue24] = useState("");
  const [regionValue25, setRegionValue25] = useState("");
  const [regionValue26, setRegionValue26] = useState("");
  const [regionValue27, setRegionValue27] = useState("");

  const [selectedRegion, setSelectedRegion] = useState("");

  const [isVeganFree, setIsVeganFree] = useState(false);

  let token;

  useEffect(() => {
    if (error) {
      // Alert.alert("Oops!", error, [{ text: "Okay" }]);
      setSubmitValidation(false);
    } else {
      setSubmitValidation(true);
    }
  }, [error]);

  const read = async () => {
    try {
      token = await SecureStore.getItemAsync("token");
    } catch (e) {
      console.log(e);
    }
  };

  read();

  const bankListUrl = global.url + "/country/bank";
  const submitUrl = global.url + "/banks/new-bank?access-token=" + myToken;

  axios
    .get(bankListUrl)
    .then(function (response) {
      setRegion0(response.data[0].bank_name);
      setRegion1(response.data[1].bank_name);
      setRegion2(response.data[2].bank_name);
      setRegion3(response.data[3].bank_name);
      setRegion4(response.data[4].bank_name);
      setRegion5(response.data[5].bank_name);
      setRegion6(response.data[6].bank_name);
      setRegion7(response.data[7].bank_name);
      setRegion8(response.data[8].bank_name);
      setRegion9(response.data[9].bank_name);
      setRegion10(response.data[10].bank_name);
      setRegion11(response.data[11].bank_name);
      setRegion12(response.data[12].bank_name);
      setRegion13(response.data[13].bank_name);
      setRegion14(response.data[14].bank_name);
      setRegion15(response.data[15].bank_name);
      setRegion16(response.data[16].bank_name);
      setRegion17(response.data[17].bank_name);
      setRegion18(response.data[18].bank_name);
      setRegion19(response.data[19].bank_name);
      setRegion20(response.data[20].bank_name);
      setRegion21(response.data[21].bank_name);
      setRegion22(response.data[22].bank_name);
      setRegion23(response.data[23].bank_name);
      setRegion24(response.data[24].bank_name);
      setRegion25(response.data[25].bank_name);
      setRegion26(response.data[26].bank_name);
      setRegion27(response.data[27].bank_name);

      setRegionValue0(response.data[0].id);
      setRegionValue1(response.data[1].id);
      setRegionValue2(response.data[2].id);
      setRegionValue3(response.data[3].id);
      setRegionValue4(response.data[4].id);
      setRegionValue5(response.data[5].id);
      setRegionValue6(response.data[6].id);
      setRegionValue7(response.data[7].id);
      setRegionValue8(response.data[8].id);
      setRegionValue9(response.data[9].id);
      setRegionValue10(response.data[10].id);
      setRegionValue11(response.data[11].id);
      setRegionValue12(response.data[12].id);
      setRegionValue13(response.data[13].id);

      setRegionValue14(response.data[14].id);
      setRegionValue15(response.data[15].id);
      setRegionValue16(response.data[16].id);
      setRegionValue17(response.data[17].id);
      setRegionValue18(response.data[18].id);
      setRegionValue19(response.data[19].id);
      setRegionValue20(response.data[20].id);
      setRegionValue21(response.data[21].id);
      setRegionValue22(response.data[22].id);
      setRegionValue23(response.data[23].id);
      setRegionValue24(response.data[24].id);
      setRegionValue25(response.data[25].id);
      setRegionValue26(response.data[26].id);
      setRegionValue27(response.data[27].id);

      console.log(response.data);
      //setFirstName(response.data.address_lists[0].Company);
      //setAddresslistView(response.data.address_lists[0].Postcode);
    })
    .catch(function (error) {
      console.log(error);
    });

  // const changeAccountName = (text) => {
  //   setAccountName(text);
  // };
  // const changeAccountNumber = (text) => {
  //   setAccountNumber(text);
  // };
  // const changeNricPassport = (text) => {
  //   setNricPassport(text);
  // };

  const changeBankName = (itemValue) => {
    if (selectedRegion === 0 || selectedRegion === "0") {
      setBankNameIsValid(false);
    } else {
      setBankNameIsValid(true);
    }
    setSelectedRegion(itemValue);
  };

  const changeAccountName = (text) => {
    if (text.trim().length === 0) {
      setAccountNameIsValid(false);
    } else {
      setAccountNameIsValid(true);
    }
    setAccountName(text);
  };

  const changeAccountNumber = (text) => {
    if (text.trim().length === 0) {
      setAccountNumberIsValid(false);
    } else {
      setAccountNumberIsValid(true);
    }
    setAccountNumber(text);
  };

  const changeNricPassport = (text) => {
    if (text.trim().length === 0) {
      setNricPassportIsValid(false);
    } else {
      setNricPassportIsValid(true);
    }
    setNricPassport(text);
  };

  // console.log(pass1);
  // console.log(pass2);
  // const handler = () => {
  //   Alert.alert(
  //     "Confirm to edit address?",
  //     "",
  //     [
  //       {
  //         text: "Cancel",
  //         onPress: () => console.log("Cancel Pressed"),
  //         style: "cancel",
  //       },
  //       {
  //         text: "Confirm",
  //         onPress: () => {
  //           props.navigation.goBack(),
  //             axios
  //               .post(submitUrl, {
  //                 bank_name_id: selectedRegion,
  //                 account_name: accountName,
  //                 account_number: accountNumber,
  //                 nric_passport: nricPassport,
  //               })
  //               .then(function (response) {
  //                 console.log(response.data);
  //                 if (response.data.message === true) {
  //                   alert("Succesfully added");
  //                 } else {
  //                   alert("Somthing went wrong");
  //                 }
  //               })
  //               .catch(function (error) {
  //                 console.log(error);
  //               });
  //         },
  //       },
  //     ],
  //     { cancelable: false }
  //   );
  // };

  function handler() {
    if (
      !bankNameIsValid ||
      !accountNumberIsValid ||
      !accountNameIsValid ||
      !nricPassportIsValid
    ) {
      setSubmitValidation(false);
    } else {
      props.navigation.goBack(),
        axios
          .post(submitUrl, {
            bank_name_id: selectedRegion,
            account_name: accountName,
            account_number: accountNumber,
            nric_passport: nricPassport,
          })
          .then(function (response) {
            console.log(response.data);
            if (response.data.message === true) {
              alert("Succesfully added");
            } else {
              alert("Somthing went wrong");
            }
          })
          .catch(function (error) {
            console.log(error);
          });
    }
  }

  const handlerAddressDelete = () => {
    Alert.alert(
      "Are you sure to delete address?",
      "",
      [
        {
          text: "Cancel",
          onPress: () => console.log("Cancel Pressed"),
          style: "cancel",
        },
        {
          text: "Confirm",
          // onPress: () => {
          //   navigation.goBack(),
          //     axios
          //       .post(url, {
          //         //new_password: pass1,
          //         retype_password: pass2,
          //       })
          //       .then(function (response) {
          //         console.log(response.data);
          //         if (response.data.message == "Yes") {
          //           alert("Password Changed");
          //         } else {
          //           alert("Password Not matched");
          //         }
          //       })
          //       .catch(function (error) {
          //         console.log(error);
          //       });
          // },
        },
      ],
      { cancelable: false }
    );
  };

  return (
    <SafeAreaView style={gS.container}>
      <ScrollView style={gS.scroll}>
        <View style={gS.section2}>
          <View style={styles.containerForm}>
            <View style={gS.section2}>
              <View style={styles.formControl}>
                <View style={styles.form}>
                  <Text style={styles.label}>Bank Name</Text>

                  <View
                    style={{
                      color: "#F00",
                      // borderBottomWidt: 10,
                      borderColor: "#ccc",
                      borderBottomWidth: 1,
                    }}
                  >
                    <Picker
                      selectedValue={selectedRegion}
                      onValueChange={changeBankName}
                    >
                      <Picker.Item
                        style={{ color: "#F00" }}
                        label="- Bank Name -"
                        value="0"
                      />
                      <Picker.Item
                        label={region0}
                        value={regionValue0}
                      ></Picker.Item>
                      <Picker.Item
                        label={region1}
                        value={regionValue1}
                      ></Picker.Item>
                      <Picker.Item
                        label={region2}
                        value={regionValue2}
                      ></Picker.Item>
                      <Picker.Item
                        label={region3}
                        value={regionValue3}
                      ></Picker.Item>
                      <Picker.Item label={region4} value={regionValue4} />
                      <Picker.Item
                        label={region5}
                        value={regionValue5}
                      ></Picker.Item>
                      <Picker.Item
                        label={region6}
                        value={regionValue6}
                      ></Picker.Item>
                      <Picker.Item
                        label={region7}
                        value={regionValue7}
                      ></Picker.Item>
                      <Picker.Item label={region8} value={regionValue8} />
                      <Picker.Item
                        label={region9}
                        value={regionValue9}
                      ></Picker.Item>
                      <Picker.Item
                        label={region10}
                        value={regionValue10}
                      ></Picker.Item>
                      <Picker.Item
                        label={region11}
                        value={regionValue11}
                      ></Picker.Item>
                      <Picker.Item label={region12} value={regionValue12} />
                      <Picker.Item
                        label={region13}
                        value={regionValue13}
                      ></Picker.Item>
                      <Picker.Item
                        label={region14}
                        value={regionValue14}
                      ></Picker.Item>
                      <Picker.Item
                        label={region15}
                        value={regionValue15}
                      ></Picker.Item>
                      <Picker.Item
                        label={region16}
                        value={regionValue16}
                      ></Picker.Item>
                      <Picker.Item
                        label={region17}
                        value={regionValue17}
                      ></Picker.Item>
                      <Picker.Item
                        label={region18}
                        value={regionValue18}
                      ></Picker.Item>
                      <Picker.Item
                        label={region19}
                        value={regionValue19}
                      ></Picker.Item>
                      <Picker.Item
                        label={region20}
                        value={regionValue20}
                      ></Picker.Item>
                      <Picker.Item
                        label={region21}
                        value={regionValue21}
                      ></Picker.Item>
                      <Picker.Item
                        label={region22}
                        value={regionValue22}
                      ></Picker.Item>
                      <Picker.Item
                        label={region23}
                        value={regionValue23}
                      ></Picker.Item>
                      <Picker.Item
                        label={region24}
                        value={regionValue24}
                      ></Picker.Item>
                      <Picker.Item
                        label={region25}
                        value={regionValue25}
                      ></Picker.Item>
                      <Picker.Item
                        label={region26}
                        value={regionValue26}
                      ></Picker.Item>
                      <Picker.Item
                        label={region27}
                        value={regionValue27}
                      ></Picker.Item>
                    </Picker>
                  </View>
                  {!submitValidation && !bankNameIsValid && (
                    <HelperText type="error">
                      Bank name cannot be blank.
                    </HelperText>
                  )}

                  <Text style={styles.label}>Account Name</Text>
                  <TextInput
                    style={styles.input}
                    id="title"
                    errorText="Please enter a valid title"
                    keyboardType="default"
                    autoCapitalize="sentences"
                    autoCorrect
                    returnKeyType="next"
                    onChangeText={changeAccountName}
                    required
                    value={accountName}
                  />
                  {!submitValidation && !accountNameIsValid && (
                    <HelperText type="error">
                      Account name cannot be blank.
                    </HelperText>
                  )}
                  <Text style={styles.label}>Account Number</Text>
                  <TextInput
                    id="imageUrl"
                    //  label="Image Url"
                    errorText="Please enter a valid image Url"
                    keyboardType="default"
                    returnKeyType="next"
                    onChangeText={changeAccountNumber}
                    required
                    style={styles.input}
                    value={accountNumber}
                  />
                  {!submitValidation && !accountNumberIsValid && (
                    <HelperText type="error">
                      Account number cannot be blank.
                    </HelperText>
                  )}
                  <Text style={styles.label}>NRIC/Passport</Text>
                  <TextInput
                    id="imageUrl"
                    //  label="Image Url"
                    errorText="Please enter a valid image Url"
                    keyboardType="default"
                    returnKeyType="next"
                    onChangeText={changeNricPassport}
                    required
                    style={styles.input}
                    // style={styles.input}
                    // id="price"
                    // label="Price"
                    // errorText="Please enter a valid Price"
                    // keyboardType="decimal-pad"
                    // onInputChange={changeAddress1}
                    // returnKeyType="next"
                    // required
                    // min={0.1}
                    value={nricPassport}
                  />
                  {!submitValidation && !nricPassportIsValid && (
                    <HelperText type="error">
                      NRIC/Passport cannot be blank.
                    </HelperText>
                  )}
                </View>
              </View>
            </View>
          </View>
          {/* <View style={styles.containerFormDefault}>
            <FiltersSwitch
              label="Set as default address"
              state={isVeganFree}
              onChange={(newValue) => setIsVeganFree(newValue)}
            />
          </View> */}

          {/* <View style={styles.containerFormDefault2}>
            <TouchableOpacity onPress={() => handlerAddressDelete()}>
              <Text style={styles.DeleteAddText}>Delete</Text>
            </TouchableOpacity>
          </View> */}
          <TouchableWithoutFeedback onPress={handler}>
            <View style={gS.blueBtn}>
              <Text style={gS.blueBtnWord}>Submit</Text>
            </View>
          </TouchableWithoutFeedback>
        </View>
      </ScrollView>
    </SafeAreaView>
  );
};

export default addBank;

const styles = StyleSheet.create({
  containerForm: {
    shadowColor: "#f9f5f5",
    shadowOpacity: 0.26,
    shadowOffset: { width: 0, height: 2 },
    shadowRadius: 8,
    elevation: 1,
    borderRadius: 5,
    borderLeftColor: "#f9f5f5",
    borderStartColor: "#f9f5f5",
    borderColor: "#f9f5f5",
    height: "85%",
    width: "100%",
    // margin: 5,
    backgroundColor: "#FFFFFF",
  },
  containerFormDefault: {
    marginTop: 10,
    shadowColor: "#f9f5f5",
    shadowOpacity: 0.26,
    shadowOffset: { width: 0, height: 2 },
    shadowRadius: 8,
    elevation: 1,
    borderRadius: 5,
    borderLeftColor: "#f9f5f5",
    borderStartColor: "#f9f5f5",
    borderColor: "#f9f5f5",
    height: "10%",
    width: "100%",

    backgroundColor: "#FFFFFF",
  },
  containerFormDefault2: {
    marginTop: 10,

    shadowColor: "#f9f5f5",
    shadowOpacity: 0.26,
    shadowOffset: { width: 0, height: 2 },
    shadowRadius: 8,
    elevation: 1,
    borderRadius: 5,
    // borderLeftColor: "#f9f5f5",
    // borderStartColor: "#f9f5f5",
    borderColor: "#f9f5f5",
    height: "11%",
    width: "100%",
    // margin: 5,
    backgroundColor: "#FFFFFF",
  },
  filterContainer: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    marginVertical: 10,
    width: "80%",
  },
  title: {
    fontFamily: "open-sans-bold",
    fontSize: 17,
    margin: 20,
    textAlign: "center",
    justifyContent: "center",
  },
  DeleteAddText: {
    fontFamily: "open-sans-bold",
    fontSize: 17,
    margin: 15,
    color: "#a90308",
    // textAlign: "center",
  },
  form: {
    margin: 10,
  },
  formControl: {
    width: "100%",
  },
  label: {
    fontFamily: "open-sans-bold",
    marginVertical: 8,
  },
  input: {
    paddingHorizontal: 2,
    paddingVertical: 2,
    borderBottomColor: "#ccc",
    borderBottomWidth: 1,
    fontSize: 18,
  },
});
