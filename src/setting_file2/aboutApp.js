import "react-native-gesture-handler";
import React, { useState } from "react";
import gS from "../globalStyle";
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  Picker,
  SafeAreaView,
  ScrollView,
  Image,
  ToastAndroid,
  Alert,
  CheckBox,
} from "react-native";
import { Icon } from "react-native-elements";
import { LinearGradient } from "expo-linear-gradient";

export default function App({ navigation }) {
  console.log("Scan Page");

  const [modalVisible, setModalVisible] = useState(false);
  const [isSelected, setSelection] = useState(false);

  const [name, setName] = useState("John Lim");
  const [phone, setPhone] = useState("+6012-345 6789");
  const [addr1, setAddr1] = useState("63 JLN Rasha Jaya Rasha");
  const [addr2, setAddr2] = useState("Taman Universiti 1");
  const [city, setCity] = useState("Seremban");
  const [postcode, setPostcode] = useState("54200");
  const [state, setState] = useState("Sembilan");
  const [country, setCountry] = useState("Malaysia");

  const deleteCheck = () =>
    Alert.alert(
      "Confirm to Delete?",
      "This Action Cannot be Undone.",
      [
        {
          text: "No",
          onPress: () => console.log("Cancel Pressed"),
          style: "cancel",
        },
        {
          text: "Yes",
          onPress: () =>
            ToastAndroid.show("Address Deleted", ToastAndroid.SHORT),
        },
      ],
      { cancelable: false }
    );

  return (
    <SafeAreaView style={gS.container}>
      <ScrollView style={gS.scroll}>
        <View style={styles.main}>
          <Text style={styles.mainText}>Kansai Paint</Text>
          <Text style={styles.lightText}>Version 3.17</Text>
        </View>
        <View style={styles.imageContainer}>
          <Image
            style={styles.image}
            source={{
              uri: "https://www.kansai.com/about-us/images/index_img08.jpg",
            }}
          />
        </View>
        <View>
          <Text style={styles.poweredText}>Powered by BusinessBoost</Text>
          <Text style={styles.poweredText}>
            Email : Support@BusinessBoost.com
          </Text>
          <Text style={styles.poweredText}>
            Phone : +6011-18889597 Mobile : +6011-18889597
          </Text>

          <View>
            <Text></Text>
          </View>
        </View>
      </ScrollView>
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  main: {
    flex: 1,
    flexDirection: "column",
    marginTop: 100,
    alignItems: "center",
    justifyContent: "space-between",
  },
  lightText: {
    fontFamily: "open-sans-bold",
    fontSize: 16,
    color: "#888",
  },
  mainText: {
    fontFamily: "open-sans-bold",
    fontSize: 20,
  },
  image: {
    height: "70%",
    width: "100%",
    resizeMode: "contain",
  },
  container2: {
    //padding: 1,

    // borderRadius: 10,
    // elevation: 3,
    backgroundColor: "#ffffff",
  },
  imageContainer: {
    marginTop: 10,
    height: 120,
  },
  poweredText: {
    textAlign: "center",
    color: "#000080",
    fontSize: 11.5,
  },
});
