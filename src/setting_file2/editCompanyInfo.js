import React, { useState, useEffect } from "react";
import axios from "axios";
import gS from "../globalStyle";
import { TouchableWithoutFeedback } from "react-native-gesture-handler";
import {
  Alert,
  Text,
  View,
  TextInput,
  SafeAreaView,
  ScrollView,
  Switch,
  StyleSheet,
  Picker,
} from "react-native";
import * as SecureStore from "expo-secure-store";
import { useSelector } from "react-redux";
import { HelperText } from "react-native-paper";
import Colors from "../../constants/Colors";
import { TouchableOpacity } from "react-native";

const editCompany = (props) => {
  const myToken = useSelector((state) => state.auth.access_token);
  console.log("editCompanyInfo");
  let regValNum;
  if (props.route.params.frmRegType === "SSM") {
    regValNum = 1;
  } else {
    regValNum = 2;
  }
  const companyId = props.route.params.comId;
  const comName = props.route.params.comName;
  const frmComAddress = props.route.params.frmComAddress;
  const frmTelNum = props.route.params.frmTelNum;
  const frmfax = props.route.params.frmfax;
  const frmRegNum = props.route.params.frmRegNum;
  const frmRegType = props.route.params.frmRegType;

  console.log(frmRegType);
  console.log(frmRegType);
  console.log(frmRegType);
  console.log(frmRegType);
  const [regVal, setRegVal] = useState("");

  const submitUrl =
    global.url + "/profile/update-company?access-token=" + myToken;

  // console.log(frmRegTypeValue);
  // console.log(frmRegTypeValue);
  // console.log(frmRegTypeValue);
  // console.log(frmRegTypeValue);

  const [companyName, setCompanyName] = useState(comName);
  const [addressAll, setAddressAll] = useState(frmComAddress);
  const [tellNum, setTelNum] = useState(frmTelNum);
  const [faxNum, setFaxNum] = useState(frmfax);
  const [regNum, setRegNum] = useState(frmRegNum);
  const [regType, setRegType] = useState(regValNum);
  // const [regVal1, setRegVal1] = useState("2");
  // const [regVal2, setRegVal2] = useState("3");

  const [companyNameIsValid, setCompanyNameIsValid] = useState(true);
  const [registrationNumberIsValid, setRegistrationNumberIsValid] =
    useState(true);
  const [submitValidation, setSubmitValidation] = useState(false);
  const [error, setError] = useState();

  // console.log(regType);
  // console.log(regType);
  // console.log(regType);
  // console.log(regType);

  useEffect(() => {
    if (error) {
      // Alert.alert("Oops!", error, [{ text: "Okay" }]);
      setSubmitValidation(false);
    } else {
      setSubmitValidation(true);
    }

    if (frmRegType === "SSM") {
      setRegVal(1);
    } else {
      setRegVal(2);
    }
  }, [error, regVal]);

  // const handler = () => {
  //   Alert.alert(
  //     "Confirm to edit ?",
  //     "",
  //     [
  //       {
  //         text: "Cancel",
  //         onPress: () => console.log("Cancel Pressed"),
  //         style: "cancel",
  //       },
  //       {
  //         text: "Confirm",
  //         onPress: () => {
  //           props.navigation.goBack(),
  //             axios
  //               .put(submitUrl, {
  //                 id: companyId,
  //                 company_name: companyName,
  //                 contact_person: "Rayid",
  //                 contact_no: "07772123455",
  //                 mailing_address: "shilgate@hotmail.com",
  //                 tel: tellNum,
  //                 fax: faxNum,
  //                 registration_type: regType,
  //                 registration_number: regNum,
  //               })
  //               .then(function (response) {
  //                 console.log(response);
  //                 if (response.data.message === "Success") {
  //                   alert("Succesfully edited");
  //                 } else {
  //                   alert("Somthing went wrong");
  //                 }
  //               })
  //               .catch(function (error) {
  //                 console.log(error);
  //               });
  //         },
  //       },
  //     ],
  //     { cancelable: false }
  //   );
  // };
  // const changeCompanyName = (text) => {
  //   setCompanyName(text);
  // };
  const changeAddress = (text) => {
    setAddressAll(text);
  };
  const changeTelNum = (text) => {
    setTelNum(text);
  };
  const changeFaxNum = (text) => {
    setFaxNum(text);
  };
  // const changeRegNum = (text) => {
  //   setRegNum(text);
  // };

  const changeCompanyName = (text) => {
    if (text.trim().length === 0) {
      setCompanyNameIsValid(false);
    } else {
      setCompanyNameIsValid(true);
    }
    setCompanyName(text);
  };

  const changeRegNum = (text) => {
    if (text.trim().length === 0) {
      setRegistrationNumberIsValid(false);
    } else {
      setRegistrationNumberIsValid(true);
    }
    setRegNum(text);
  };

  function handler() {
    if (!companyNameIsValid || !registrationNumberIsValid) {
      setSubmitValidation(false);
      console.log("not Edit");
      console.log(companyNameIsValid);
      console.log(registrationNumberIsValid);
    } else {
      props.navigation.goBack(),
        axios
          .put(submitUrl, {
            id: companyId,
            company_name: companyName,
            // contact_person: "Rayid",
            //contact_no: "07772123455",
            mailing_address: addressAll,
            tel: tellNum,
            fax: faxNum,
            registration_type: regType,
            registration_number: regNum,
          })
          .then(function (response) {
            console.log(response);
            if (response.data.message === "Success") {
              alert("Succesfully edited");
            } else {
              alert("Somthing went wrong");
            }
          })
          .catch(function (error) {
            console.log(error);
            setError(error.message);
          });
    }
  }

  return (
    <SafeAreaView style={gS.container}>
      <ScrollView style={gS.scroll}>
        <View style={gS.section2}>
          <View style={styles.containerForm}>
            <View style={gS.section2}>
              <View style={styles.formControl}>
                <View style={styles.form}>
                  <Text style={styles.label}>Company Name</Text>
                  <TextInput
                    style={styles.input}
                    id="title"
                    errorText="Please enter a valid title"
                    keyboardType="default"
                    autoCapitalize="sentences"
                    autoCorrect
                    returnKeyType="next"
                    onChangeText={changeCompanyName}
                    required
                    value={companyName}
                  />
                  {!submitValidation &&
                    !companyNameIsValid &&
                    companyName === "" && (
                      <HelperText type="error">
                        Company name cannot be blank.
                      </HelperText>
                    )}
                  <View>
                    <Text style={styles.label}>Registration type</Text>
                    <View
                      style={{
                        color: "#F00",
                        // borderBottomWidt: 10,
                        borderColor: "#ccc",
                        borderBottomWidth: 1,
                      }}
                    >
                      <Picker
                        selectedValue={regType}
                        onValueChange={(itemValue, itemIndex) =>
                          setRegType(itemValue)
                        }
                      >
                        {/* <Picker.Item
                          style={{ color: "#F00", borderBottomWidt: 10 }}
                          label={regType}
                          value={1}
                        ></Picker.Item> */}
                        {/* {frmRegType === "SSM" ? (
                          <Picker.Item label="SSM" value={1}></Picker.Item>
                        ) : (
                          <Picker.Item label="CIDB" value={2}></Picker.Item>
                        )} */}
                        <Picker.Item
                          label="SSM"
                          value={1}
                          // selectedValue={"SSM"}
                        ></Picker.Item>
                        <Picker.Item label="CIDB" value={2}></Picker.Item>
                      </Picker>
                    </View>
                  </View>
                  <Text style={styles.label}>Registration Number</Text>
                  <TextInput
                    style={styles.input}
                    id="title"
                    errorText="Please enter a valid title"
                    keyboardType="default"
                    autoCapitalize="sentences"
                    autoCorrect
                    returnKeyType="next"
                    onChangeText={changeRegNum}
                    required
                    value={regNum}
                  />
                  {!submitValidation &&
                    !registrationNumberIsValid &&
                    regNum === "" && (
                      <HelperText type="error">
                        Registration number cannot be blank.
                      </HelperText>
                    )}
                  <Text style={styles.label}>Company Address</Text>
                  <TextInput
                    style={styles.input}
                    id="title"
                    errorText="Please enter a valid title"
                    keyboardType="default"
                    autoCapitalize="sentences"
                    autoCorrect
                    returnKeyType="next"
                    onChangeText={changeAddress}
                    required
                    value={addressAll}
                  />
                  <Text style={styles.label}>Tel </Text>
                  <TextInput
                    style={styles.input}
                    id="title"
                    errorText="Please enter a valid title"
                    keyboardType="default"
                    autoCapitalize="sentences"
                    autoCorrect
                    returnKeyType="next"
                    onChangeText={changeTelNum}
                    required
                    value={tellNum}
                  />
                  <Text style={styles.label}>Fax</Text>
                  <TextInput
                    style={styles.input}
                    id="title"
                    errorText="Please enter a valid title"
                    keyboardType="default"
                    autoCapitalize="sentences"
                    autoCorrect
                    returnKeyType="next"
                    onChangeText={changeFaxNum}
                    required
                    value={faxNum}
                  />
                </View>
              </View>
            </View>
          </View>
          {/* <View style={styles.containerFormDefault}>
            <FiltersSwitch
              label="Set as default address"
              state={isVeganFree}
              onChange={(newValue) => setIsVeganFree(newValue)}
            />
          </View> */}

          {/* <View style={styles.containerFormDefault2}>
            <TouchableOpacity onPress={() => handlerAddressDelete()}>
              <Text style={styles.DeleteAddText}>Delete</Text>
            </TouchableOpacity>
          </View> */}

          <TouchableWithoutFeedback onPress={handler}>
            <View style={gS.blueBtn}>
              <Text style={gS.blueBtnWord}>Submit</Text>
            </View>
          </TouchableWithoutFeedback>
        </View>
      </ScrollView>
    </SafeAreaView>
  );
};

export default editCompany;

const styles = StyleSheet.create({
  containerForm: {
    shadowColor: "#f9f5f5",
    shadowOpacity: 0.26,
    shadowOffset: { width: 0, height: 2 },
    shadowRadius: 8,
    elevation: 1,
    borderRadius: 5,
    borderLeftColor: "#f9f5f5",
    borderStartColor: "#f9f5f5",
    borderColor: "#f9f5f5",
    height: "90%",
    width: "100%",
    // margin: 5,
    backgroundColor: "#FFFFFF",
  },
  containerFormDefault: {
    marginTop: 10,
    shadowColor: "#f9f5f5",
    shadowOpacity: 0.26,
    shadowOffset: { width: 0, height: 2 },
    shadowRadius: 8,
    elevation: 1,
    borderRadius: 5,
    borderLeftColor: "#f9f5f5",
    borderStartColor: "#f9f5f5",
    borderColor: "#f9f5f5",
    height: "15%",
    width: "100%",

    backgroundColor: "#FFFFFF",
  },
  containerFormDefault2: {
    marginTop: 10,

    shadowColor: "#f9f5f5",
    shadowOpacity: 0.26,
    shadowOffset: { width: 0, height: 2 },
    shadowRadius: 8,
    elevation: 1,
    borderRadius: 5,
    // borderLeftColor: "#f9f5f5",
    // borderStartColor: "#f9f5f5",
    borderColor: "#f9f5f5",
    height: "15%",
    width: "100%",
    // margin: 5,
    backgroundColor: "#FFFFFF",
  },
  filterContainer: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    marginVertical: 10,
    width: "80%",
  },
  title: {
    fontFamily: "open-sans-bold",
    fontSize: 17,
    margin: 20,
    textAlign: "center",
    justifyContent: "center",
  },
  DeleteAddText: {
    fontFamily: "open-sans-bold",
    fontSize: 17,
    margin: 20,
    color: "#a90308",
    // textAlign: "center",
  },
  form: {
    margin: 10,
  },
  formControl: {
    width: "100%",
  },
  label: {
    fontFamily: "open-sans-bold",
    marginVertical: 8,
  },
  input: {
    paddingHorizontal: 2,
    paddingVertical: 2,
    borderBottomColor: "#ccc",
    borderBottomWidth: 1,
    fontSize: 18,
  },
});
