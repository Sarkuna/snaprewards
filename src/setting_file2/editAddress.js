import React, { useState, useEffect } from "react";
import axios from "axios";
import gS from "../globalStyle";
import { TouchableWithoutFeedback } from "react-native-gesture-handler";
import {
  Alert,
  Text,
  View,
  TextInput,
  SafeAreaView,
  ScrollView,
  Switch,
  StyleSheet,
  Picker,
  FlatList,
} from "react-native";
import { HelperText } from "react-native-paper";
import * as SecureStore from "expo-secure-store";
import { useSelector, useDispatch } from "react-redux";
import Colors from "../../constants/Colors";
import { TouchableOpacity } from "react-native";
import * as productsActions from "../../store/actions/categories";
import EditAddress from "../../models/editAddressBook";

const FiltersSwitch = (props) => {
  return (
    <View style={styles.filterContainer}>
      <Text style={styles.title}>{props.label}</Text>
      <Switch
        trackColor={{ true: Colors.primary }}
        thumbColor={Platform.OS === "android" ? Colors.primary : ""}
        value={props.state}
        onValueChange={props.onChange}
      />
    </View>
  );
};

//function addAddress({ navigation }) {

const addAddress = (props) => {
  const addressId = props.route.params.id;
  const addressName = props.route.params.name;
  const addressPostcode = props.route.params.postcode;
  const addressCountry = props.route.params.country;
  const address = props.route.params.address;
  const default_address = props.route.params.default_address;
  const statename = props.route.params.statename;
  const stateid = props.route.params.stateid;

  let countryID;
  if (props.route.params.country === "Malaysia") {
    countryID = 129;
  } else {
    countryID = 188;
  }

  // let stateID;
  // if (props.route.params.country === "Malaysia") {
  //   countryID = 129;
  // } else {
  //   countryID = 188;
  // }

  console.log(stateid);
  console.log(stateid);
  console.log(stateid);

  var string = "text to split";
  string = addressName.split(" ");
  var nameArray = new Array();
  for (var i = 0; i < string.length; i++) {
    nameArray.push(string[i]);
    if (i != string.length - 1) {
      nameArray.push(" ");
    }
  }
  // console.log(stateFrm);
  // console.log(addressId);
  // console.log(address);
  // console.log(addressName);
  // console.log(addressPostcode);

  //console.log(default_address);

  const myToken = useSelector((state) => state.auth.access_token);
  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(productsActions.addressDetails(myToken));
    if (error) {
      // Alert.alert("Oops!", error, [{ text: "Okay" }]);
      setFirstNameValidSubmit(false);
    } else {
      setFirstNameValidSubmit(true);
    }
    if (
      firstName === "" ||
      lastName === "" ||
      address1 === "" ||
      postcode === "" ||
      lastName === ""
    ) {
      setBeforeEdit(false);
    } else {
      setBeforeEdit(true);
    }
  }, [dispatch, error, firstName]);

  const Categories = useSelector((state) => state.address.availableAddress);
  //console.log(Categories);
  const [addresslistView, setAddresslistView] = useState([]);

  console.log("Edit Address");

  let token;
  const addressView =
    global.url +
    `/address-book/view-address?access-token=${myToken}&id=${addressId}`;

  const regionUrl = global.url + "/country/region";

  const countryUrl = global.url + "/country/country";

  const submitUrl =
    global.url + "/address-book/edit-address?access-token=" + myToken;

  console.log(submitUrl);
  //console.log(addressView);

  const [titleIsValid, setTitleIsValid] = useState(false);
  const [LastNameIsValid, setLastNameIsValid] = useState(false);
  const [address1IsValid, setAddress1IsValid] = useState(false);
  const [postcodeIsValid, setPostcodeIsValid] = useState(false);
  const [cityIsValid, setCityIsValid] = useState(false);
  const [regionIsValid, setRegionIsValid] = useState(false);
  const [countryIsValid, setCountryIsValid] = useState(false);

  const [firstNameValidSubmit, setFirstNameValidSubmit] = useState(false);
  const [beforeEdit, setBeforeEdit] = useState(false);
  const [error, setError] = useState();

  const [defaultAdd, setDefaultAdd] = useState(default_address);
  const [company, setCompany] = useState("");
  const [editCompany, setEditCompany] = useState(" ");
  const [firstName, setFirstName] = useState("");
  const [editFirstName, setEditFirstName] = useState(" ");
  const [lastName, setLastName] = useState("");
  const [editLastName, setEditLastName] = useState(" ");
  const [address1, setAddress1] = useState("");
  const [editAddress1, setEditAddress1] = useState(" ");
  const [address2, setAddress2] = useState("");
  const [editAddress2, setEditAddress2] = useState(" ");
  const [postcode, setPostcode] = useState("");
  const [editPostcode, setEditPostcode] = useState(" ");
  const [city, setCity] = useState("");
  const [editCity, setEditCity] = useState(" ");
  const [state, setState] = useState("");
  const [country, setCountry] = useState(" ");
  const [selectedRegion, setSelectedRegion] = useState(stateid);
  const [selectedCountry, setSelectedCountry] = useState(countryID);

  // const [titleIsValid, setTitleIsValid] = useState(false);

  const [region0, setRegion0] = useState("");
  const [region1, setRegion1] = useState("");
  const [region2, setRegion2] = useState("");
  const [region3, setRegion3] = useState("");
  const [region4, setRegion4] = useState("");
  const [region5, setRegion5] = useState("");
  const [region6, setRegion6] = useState("");
  const [region7, setRegion7] = useState("");
  const [region8, setRegion8] = useState("");
  const [region9, setRegion9] = useState("");
  const [region10, setRegion10] = useState("");
  const [region11, setRegion11] = useState("");
  const [region12, setRegion12] = useState("");
  const [region13, setRegion13] = useState("");

  const [regionValue0, setRegionValue0] = useState("");
  const [regionValue1, setRegionValue1] = useState("");
  const [regionValue2, setRegionValue2] = useState("");
  const [regionValue3, setRegionValue3] = useState("");
  const [regionValue4, setRegionValue4] = useState("");
  const [regionValue5, setRegionValue5] = useState("");
  const [regionValue6, setRegionValue6] = useState("");
  const [regionValue7, setRegionValue7] = useState("");
  const [regionValue8, setRegionValue8] = useState("");
  const [regionValue9, setRegionValue9] = useState("");
  const [regionValue10, setRegionValue10] = useState("");
  const [regionValue11, setRegionValue11] = useState("");
  const [regionValue12, setRegionValue12] = useState("");
  const [regionValue13, setRegionValue13] = useState("");

  const [country0, setCountry0] = useState("");
  const [country1, setCountry1] = useState("");
  const [country0Id, setCountry0Id] = useState("");
  const [country1Id, setCountry1Id] = useState("");

  const [selectedDefault, setSelectedDefault] = useState("");
  const [data, setData] = useState([]);
  const [dataState, setDataState] = useState([]);

  // console.log(selectedDefault);

  let itIsDefault;

  if (selectedDefault === true) {
    itIsDefault = 1;
  } else {
    itIsDefault = 0;
  }

  const countries = data;
  const states = dataState;
  console.log("countries");
  console.log(countries);

  axios
    .get(addressView)
    .then(function (response) {
      //setAddresslistView(response.data.address_lists[0].Postcode + "");
      setCompany(response.data.address_lists[0].company);
      setFirstName(response.data.address_lists[0].firsr_name);
      setLastName(response.data.address_lists[0].last_name);
      setAddress1(response.data.address_lists[0].address_1);
      setAddress2(response.data.address_lists[0].address_2);
      setPostcode(response.data.address_lists[0].postcode);
      setCity(response.data.address_lists[0].city);
      setState(response.data.address_lists[0].state);
      setCountry(response.data.address_lists[0].country);

      // console.log(response.data.address_lists);
      //setFirstName(response.data.address_lists[0].Company);
      //setAddresslistView(response.data.address_lists[0].Postcode);
    })
    .catch(function (error) {
      console.log(error);
    });

  axios
    .get(regionUrl)
    .then(function (response) {
      //setAddresslistView(response.data.address_lists[0].Postcode + "");
      //console.log(response.data);
      setRegion0(response.data[0].state_name);
      setRegion1(response.data[1].state_name);
      setRegion2(response.data[2].state_name);
      setRegion3(response.data[3].state_name);
      setRegion4(response.data[4].state_name);
      setRegion5(response.data[5].state_name);
      setRegion6(response.data[6].state_name);
      setRegion7(response.data[7].state_name);
      setRegion8(response.data[8].state_name);
      setRegion9(response.data[9].state_name);
      setRegion10(response.data[10].state_name);
      setRegion11(response.data[11].state_name);
      setRegion12(response.data[12].state_name);
      setRegion13(response.data[13].state_name);

      setRegionValue0(response.data[0].states_id);
      setRegionValue1(response.data[1].states_id);
      setRegionValue2(response.data[2].states_id);
      setRegionValue3(response.data[3].states_id);
      setRegionValue4(response.data[4].states_id);
      setRegionValue5(response.data[5].states_id);
      setRegionValue6(response.data[6].states_id);
      setRegionValue7(response.data[7].states_id);
      setRegionValue8(response.data[8].states_id);
      setRegionValue9(response.data[9].states_id);
      setRegionValue10(response.data[10].states_id);
      setRegionValue11(response.data[11].states_id);
      setRegionValue12(response.data[12].states_id);
      setRegionValue13(response.data[13].states_id);

      //setFirstName(response.data.address_lists[0].Company);
      //setAddresslistView(response.data.address_lists[0].Postcode);
    })
    .catch(function (error) {
      console.log(error);
    });

  useEffect(() => {
    fetch("http://api.businessboosters.com.my/country/country")
      .then((response) => response.json())
      .then((json) => setData(json))

      .catch((error) => console.error(error));
    // .finally(() => setLoading(false));
    fetch(regionUrl)
      .then((response) => response.json())
      .then((json) => setDataState(json))

      .catch((error) => console.error(error));
  }, []);
  console.log(" Data List");
  console.log(dataState);
  console.log(" Data List End");

  // const fetchAPI = () => {
  //   const result = axios
  axios
    .get(countryUrl)
    .then((response) => {
      const data1 = JSON.parse(JSON.stringify(response));
      //setState(data)
      // console.log("Test Object");
      // console.log(data1);
    })
    .catch((error) => {
      console.error(error);
    });
  //   return result;
  // };

  axios
    .get(countryUrl)
    .then(function (response) {
      //setAddresslistView(response.data.address_lists[0].Postcode + "");
      // countries = { country_id: "4", name: "American Samoa" };
      // countries = response.data;
      // countries = [{ country_id: "1", name: "Malaysia" }];

      // console.log(response.data);
      setCountry0(response.data[0].name);
      setCountry1(response.data[1].name);
      setCountry0Id(response.data[0].country_id);
      setCountry1Id(response.data[1].country_id);

      //setFirstName(response.data.address_lists[0].Company);
      //setAddresslistView(response.data.address_lists[0].Postcode);
    })
    .catch(function (error) {
      console.log(error);
    });

  // console.log();

  const read = async () => {
    try {
      token = await SecureStore.getItemAsync("token");
    } catch (e) {
      console.log(e);
    }
  };

  read();

  const url =
    "http://api.businessboosters.com.my/profile/change-password?access-token=" +
    myToken;

  const changeCompany = (text) => {
    //setCompany(text);
    // setCompany(text);
    setEditCompany(text);
  };
  // const changeFirstName = (text) => {
  //   setEditFirstName(text);

  // };
  // const changeLastName = (text) => {
  //   setEditLastName(text);
  // };
  // const changeAddress1 = (text) => {
  //   setEditAddress1(text);
  // };
  const changeAddress2 = (text) => {
    setEditAddress2(text);
  };
  // const changePostscode = (text) => {
  //   setEditPostcode(text);
  // };
  // const changeCity = (text) => {
  //   setEditCity(text);
  // };

  const titleChangeHandler = (text) => {
    if (text.trim().length === 0) {
      setTitleIsValid(false);
    } else {
      setTitleIsValid(true);
    }
    setEditFirstName(text);
  };

  const changeLastName = (text) => {
    if (text.trim().length === 0) {
      setLastNameIsValid(false);
    } else {
      setLastNameIsValid(true);
    }
    setEditLastName(text);
  };

  const changeAddress1 = (text) => {
    if (text.trim().length === 0) {
      setAddress1IsValid(false);
    } else {
      setAddress1IsValid(true);
    }
    setEditAddress1(text);
  };
  const changePostscode = (text) => {
    if (text.trim().length === 0) {
      setPostcodeIsValid(false);
    } else {
      setPostcodeIsValid(true);
    }
    setEditPostcode(text);
  };

  const changeCity = (text) => {
    if (text.trim().length === 0) {
      setCityIsValid(false);
    } else {
      setCityIsValid(true);
    }
    setEditCity(text);
  };

  const changeRegion = (itemValue) => {
    if (selectedRegion === 0 || selectedRegion === "0") {
      setRegionIsValid(false);
    } else {
      setRegionIsValid(true);
    }
    setSelectedRegion(itemValue);
  };

  const changeCountry = (itemValue) => {
    if (selectedCountry === 0 || selectedCountry === "0") {
      setCountryIsValid(false);
    } else {
      setCountryIsValid(true);
    }
    setSelectedCountry(itemValue);
  };
  // const changePostscode = (text) => {
  //   setEditPostcode(text);
  // };
  // const changeCity = (text) => {
  //   setEditCity(text);
  // };
  // console.log(pass1);
  // console.log(pass2);
  // console.log(editCompany);
  // console.log(editFirstName);
  // console.log(editLastName);
  // console.log(editAddress1);
  // console.log(editAddress2);
  // console.log(editPostcode);
  // console.log(editCity);
  // console.log(selectedRegion);
  // console.log(selectedCountry);
  // console.log(selectedRegion);
  // console.log(addressId);

  // const handler = () => {
  //   Alert.alert(
  //     "Are you confirm to edit ?",
  //     "",
  //     [
  //       {
  //         text: "Cancel",
  //         onPress: () => console.log("Cancel Pressed"),
  //         style: "cancel",
  //       },
  //       {
  //         text: "Confirm",
  //         onPress: () => {
  //           props.navigation.navigate("AddressBook"),
  //             axios
  //               .post(submitUrl, {
  //                 company: editCompany,
  //                 first_name: editFirstName,
  //                 last_name: editLastName,
  //                 address_1: editAddress1,
  //                 address_2: editAddress2,
  //                 postcode: editPostcode,
  //                 city: editCity,
  //                 region: selectedRegion,
  //                 country: selectedCountry,
  //                 default_address: itIsDefault,
  //                 id: addressId,
  //               })
  //               .then(function (response) {
  //                 console.log(response.data);
  //                 if (response.data.message === true) {
  //                   alert("Succesfully edited");
  //                 } else {
  //                   alert("Somthing went wrong");
  //                 }
  //               })
  //               .catch(function (error) {
  //                 console.log(error);
  //               });
  //         },
  //       },
  //     ],
  //     { cancelable: false }
  //   );
  // };

  function handler() {
    if (
      editFirstName === "" ||
      editLastName === "" ||
      editAddress1 === "" ||
      editPostcode === "" ||
      editCity === "" ||
      selectedRegion === "" ||
      // !regionIsValid ||
      selectedCountry === ""
    ) {
      setFirstNameValidSubmit(false);
      console.log("Not Submited");
    } else {
      console.log("suuubmit");
      props.navigation.navigate("AddressBook");
      if (
        editFirstName != " " &&
        editLastName != " " &&
        editAddress1 != " " &&
        editPostcode != " " &&
        editCity != " "
      ) {
        console.log("OK Firstname");
        axios
          .post(submitUrl, {
            company: editCompany,
            first_name: editFirstName,
            last_name: editLastName,
            address_1: editAddress1,
            address_2: editAddress2,
            postcode: editPostcode,
            city: editCity,
            region: selectedRegion,
            country: selectedCountry,
            default_address: itIsDefault,
            id: addressId,
          })
          .then(function (response) {
            console.log(response.data);
            if (response.data.message === true) {
              alert("Succesfully edited");
            } else {
              alert("Somthing went wrong");
            }
          })
          .catch(function (error) {
            console.log(error);
            setError(error.message);
          });
      } else if (
        editFirstName != " " &&
        editLastName != " " &&
        editAddress1 != " " &&
        editPostcode != " " &&
        editCity === " "
      ) {
        console.log("OK LastNmae");
        axios
          .post(submitUrl, {
            company: editCompany,
            first_name: editFirstName,
            last_name: editLastName,
            address_1: editAddress1,
            address_2: editAddress2,
            postcode: editPostcode,
            city: city,
            region: selectedRegion,
            country: selectedCountry,
            default_address: itIsDefault,
            id: addressId,
          })
          .then(function (response) {
            console.log(response.data);
            if (response.data.message === true) {
              alert("Succesfully edited");
            } else {
              alert("Somthing went wrong");
            }
          })
          .catch(function (error) {
            console.log(error);
            setError(error.message);
          });
      } else if (
        editFirstName != " " &&
        editLastName != " " &&
        editAddress1 != " " &&
        editPostcode === " " &&
        editCity != " "
      ) {
        axios
          .post(submitUrl, {
            company: editCompany,
            first_name: editFirstName,
            last_name: editLastName,
            address_1: editAddress1,
            address_2: editAddress2,
            postcode: postcode,
            city: editCity,
            region: selectedRegion,
            country: selectedCountry,
            default_address: itIsDefault,
            id: addressId,
          })
          .then(function (response) {
            console.log(response.data);
            if (response.data.message === true) {
              alert("Succesfully edited");
            } else {
              alert("Somthing went wrong");
            }
          })
          .catch(function (error) {
            console.log(error);
            setError(error.message);
          });
      } else if (
        editFirstName != " " &&
        editLastName != " " &&
        editAddress1 != " " &&
        editPostcode === " " &&
        editCity === " "
      ) {
        axios
          .post(submitUrl, {
            company: editCompany,
            first_name: editFirstName,
            last_name: editLastName,
            address_1: editAddress1,
            address_2: editAddress2,
            postcode: postcode,
            city: city,
            region: selectedRegion,
            country: selectedCountry,
            default_address: itIsDefault,
            id: addressId,
          })
          .then(function (response) {
            console.log(response.data);
            if (response.data.message === true) {
              alert("Succesfully edited");
            } else {
              alert("Somthing went wrong");
            }
          })
          .catch(function (error) {
            console.log(error);
            setError(error.message);
          });
      } else if (
        editFirstName != " " &&
        editLastName != " " &&
        editAddress1 === " " &&
        editPostcode != " " &&
        editCity != " "
      ) {
        axios
          .post(submitUrl, {
            company: editCompany,
            first_name: editFirstName,
            last_name: editLastName,
            address_1: address1,
            address_2: editAddress2,
            postcode: editPostcode,
            city: editCity,
            region: selectedRegion,
            country: selectedCountry,
            default_address: itIsDefault,
            id: addressId,
          })
          .then(function (response) {
            console.log(response.data);
            if (response.data.message === true) {
              alert("Succesfully edited");
            } else {
              alert("Somthing went wrong");
            }
          })
          .catch(function (error) {
            console.log(error);
            setError(error.message);
          });
      } else if (
        editFirstName === " " &&
        editLastName === " " &&
        editAddress1 != " " &&
        editPostcode === " " &&
        editCity != " "
      ) {
        console.log("OK FirstName and LastName");
        axios
          .post(submitUrl, {
            company: editCompany,
            first_name: firstName,
            last_name: lastName,
            address_1: editAddress1,
            address_2: editAddress2,
            postcode: postcode,
            city: editCity,
            region: selectedRegion,
            country: selectedCountry,
            default_address: itIsDefault,
            id: addressId,
          })
          .then(function (response) {
            console.log(response.data);
            if (response.data.message === true) {
              alert("Succesfully edited");
            } else {
              alert("Somthing went wrong");
            }
          })
          .catch(function (error) {
            console.log(error);
            setError(error.message);
          });
      } else if (
        editFirstName != " " &&
        editLastName != " " &&
        editAddress1 === " " &&
        editPostcode === " " &&
        editCity != " "
      ) {
        axios
          .post(submitUrl, {
            company: editCompany,
            first_name: editFirstName,
            last_name: editLastName,
            address_1: address1,
            address_2: editAddress2,
            postcode: postcode,
            city: editCity,
            region: selectedRegion,
            country: selectedCountry,
            default_address: itIsDefault,
            id: addressId,
          })
          .then(function (response) {
            console.log(response.data);
            if (response.data.message === true) {
              alert("Succesfully edited");
            } else {
              alert("Somthing went wrong");
            }
          })
          .catch(function (error) {
            console.log(error);
            setError(error.message);
          });
      } else if (
        editFirstName != " " &&
        editLastName != " " &&
        editAddress1 === " " &&
        editPostcode === " " &&
        editCity === " "
      ) {
        axios
          .post(submitUrl, {
            company: editCompany,
            first_name: editFirstName,
            last_name: editLastName,
            address_1: address1,
            address_2: editAddress2,
            postcode: postcode,
            city: city,
            region: selectedRegion,
            country: selectedCountry,
            default_address: itIsDefault,
            id: addressId,
          })
          .then(function (response) {
            console.log(response.data);
            if (response.data.message === true) {
              alert("Succesfully edited");
            } else {
              alert("Somthing went wrong");
            }
          })
          .catch(function (error) {
            console.log(error);
            setError(error.message);
          });
      } else if (
        editFirstName != " " &&
        editLastName === " " &&
        editAddress1 != " " &&
        editPostcode != " " &&
        editCity != " "
      ) {
        axios
          .post(submitUrl, {
            company: editCompany,
            first_name: editFirstName,
            last_name: lastName,
            address_1: editAddress1,
            address_2: editAddress2,
            postcode: editPostcode,
            city: editCity,
            region: selectedRegion,
            country: selectedCountry,
            default_address: itIsDefault,
            id: addressId,
          })
          .then(function (response) {
            console.log(response.data);
            if (response.data.message === true) {
              alert("Succesfully edited");
            } else {
              alert("Somthing went wrong");
            }
          })
          .catch(function (error) {
            console.log(error);
            setError(error.message);
          });
      } else if (
        editFirstName != " " &&
        editLastName === " " &&
        editAddress1 != " " &&
        editPostcode != " " &&
        editCity === " "
      ) {
        axios
          .post(submitUrl, {
            company: editCompany,
            first_name: editFirstName,
            last_name: lastName,
            address_1: editAddress1,
            address_2: editAddress2,
            postcode: editPostcode,
            city: city,
            region: selectedRegion,
            country: selectedCountry,
            default_address: itIsDefault,
            id: addressId,
          })
          .then(function (response) {
            console.log(response.data);
            if (response.data.message === true) {
              alert("Succesfully edited");
            } else {
              alert("Somthing went wrong");
            }
          })
          .catch(function (error) {
            console.log(error);
            setError(error.message);
          });
      } else if (
        editFirstName != " " &&
        editLastName === " " &&
        editAddress1 != " " &&
        editPostcode === " " &&
        editCity != " "
      ) {
        axios
          .post(submitUrl, {
            company: editCompany,
            first_name: editFirstName,
            last_name: lastName,
            address_1: editAddress1,
            address_2: editAddress2,
            postcode: postcode,
            city: editCity,
            region: selectedRegion,
            country: selectedCountry,
            default_address: itIsDefault,
            id: addressId,
          })
          .then(function (response) {
            console.log(response.data);
            if (response.data.message === true) {
              alert("Succesfully edited");
            } else {
              alert("Somthing went wrong");
            }
          })
          .catch(function (error) {
            console.log(error);
            setError(error.message);
          });
      } else if (
        editFirstName != " " &&
        editLastName === " " &&
        editAddress1 != " " &&
        editPostcode === " " &&
        editCity === " "
      ) {
        axios
          .post(submitUrl, {
            company: editCompany,
            first_name: editFirstName,
            last_name: lastName,
            address_1: editAddress1,
            address_2: editAddress2,
            postcode: postcode,
            city: city,
            region: selectedRegion,
            country: selectedCountry,
            default_address: itIsDefault,
            id: addressId,
          })
          .then(function (response) {
            console.log(response.data);
            if (response.data.message === true) {
              alert("Succesfully edited");
            } else {
              alert("Somthing went wrong");
            }
          })
          .catch(function (error) {
            console.log(error);
            setError(error.message);
          });
      } else if (
        editFirstName != " " &&
        editLastName === " " &&
        editAddress1 === " " &&
        editPostcode != " " &&
        editCity != " "
      ) {
        axios
          .post(submitUrl, {
            company: editCompany,
            first_name: editFirstName,
            last_name: lastName,
            address_1: address1,
            address_2: editAddress2,
            postcode: editPostcode,
            city: editCity,
            region: selectedRegion,
            country: selectedCountry,
            default_address: itIsDefault,
            id: addressId,
          })
          .then(function (response) {
            console.log(response.data);
            if (response.data.message === true) {
              alert("Succesfully edited");
            } else {
              alert("Somthing went wrong");
            }
          })
          .catch(function (error) {
            console.log(error);
            setError(error.message);
          });
      } else if (
        editFirstName != " " &&
        editLastName === " " &&
        editAddress1 === " " &&
        editPostcode != " " &&
        editCity === " "
      ) {
        axios
          .post(submitUrl, {
            company: editCompany,
            first_name: editFirstName,
            last_name: lastName,
            address_1: address1,
            address_2: editAddress2,
            postcode: editPostcode,
            city: city,
            region: selectedRegion,
            country: selectedCountry,
            default_address: itIsDefault,
            id: addressId,
          })
          .then(function (response) {
            console.log(response.data);
            if (response.data.message === true) {
              alert("Succesfully edited");
            } else {
              alert("Somthing went wrong");
            }
          })
          .catch(function (error) {
            console.log(error);
            setError(error.message);
          });
      } else if (
        editFirstName != " " &&
        editLastName === " " &&
        editAddress1 === " " &&
        editPostcode === " " &&
        editCity != " "
      ) {
        axios
          .post(submitUrl, {
            company: editCompany,
            first_name: editFirstName,
            last_name: lastName,
            address_1: address1,
            address_2: editAddress2,
            postcode: postcode,
            city: editCity,
            region: selectedRegion,
            country: selectedCountry,
            default_address: itIsDefault,
            id: addressId,
          })
          .then(function (response) {
            console.log(response.data);
            if (response.data.message === true) {
              alert("Succesfully edited");
            } else {
              alert("Somthing went wrong");
            }
          })
          .catch(function (error) {
            console.log(error);
            setError(error.message);
          });
      } else if (
        editFirstName != " " &&
        editLastName === " " &&
        editAddress1 === " " &&
        editPostcode === " " &&
        editCity === " "
      ) {
        axios
          .post(submitUrl, {
            company: editCompany,
            first_name: editFirstName,
            last_name: lastName,
            address_1: address1,
            address_2: editAddress2,
            postcode: postcode,
            city: city,
            region: selectedRegion,
            country: selectedCountry,
            default_address: itIsDefault,
            id: addressId,
          })
          .then(function (response) {
            console.log(response.data);
            if (response.data.message === true) {
              alert("Succesfully edited");
            } else {
              alert("Somthing went wrong");
            }
          })
          .catch(function (error) {
            console.log(error);
            setError(error.message);
          });
      } else if (
        editFirstName === " " &&
        editLastName != " " &&
        editAddress1 != " " &&
        editPostcode != " " &&
        editCity != " "
      ) {
        axios
          .post(submitUrl, {
            company: editCompany,
            first_name: firstName,
            last_name: editLastName,
            address_1: editAddress1,
            address_2: editAddress2,
            postcode: editPostcode,
            city: editCity,
            region: selectedRegion,
            country: selectedCountry,
            default_address: itIsDefault,
            id: addressId,
          })
          .then(function (response) {
            console.log(response.data);
            if (response.data.message === true) {
              alert("Succesfully edited");
            } else {
              alert("Somthing went wrong");
            }
          })
          .catch(function (error) {
            console.log(error);
            setError(error.message);
          });
      } else if (
        editFirstName === " " &&
        editLastName != " " &&
        editAddress1 != " " &&
        editPostcode != " " &&
        editCity === " "
      ) {
        axios
          .post(submitUrl, {
            company: editCompany,
            first_name: firstName,
            last_name: editLastName,
            address_1: editAddress1,
            address_2: editAddress2,
            postcode: editPostcode,
            city: city,
            region: selectedRegion,
            country: selectedCountry,
            default_address: itIsDefault,
            id: addressId,
          })
          .then(function (response) {
            console.log(response.data);
            if (response.data.message === true) {
              alert("Succesfully edited");
            } else {
              alert("Somthing went wrong");
            }
          })
          .catch(function (error) {
            console.log(error);
            setError(error.message);
          });
      } else if (
        editFirstName === " " &&
        editLastName != " " &&
        editAddress1 != " " &&
        editPostcode === " " &&
        editCity != " "
      ) {
        axios
          .post(submitUrl, {
            company: editCompany,
            first_name: firstName,
            last_name: editLastName,
            address_1: editAddress1,
            address_2: editAddress2,
            postcode: postcode,
            city: editCity,
            region: selectedRegion,
            country: selectedCountry,
            default_address: itIsDefault,
            id: addressId,
          })
          .then(function (response) {
            console.log(response.data);
            if (response.data.message === true) {
              alert("Succesfully edited");
            } else {
              alert("Somthing went wrong");
            }
          })
          .catch(function (error) {
            console.log(error);
            setError(error.message);
          });
      } else if (
        editFirstName === " " &&
        editLastName != " " &&
        editAddress1 != " " &&
        editPostcode === " " &&
        editCity === " "
      ) {
        axios
          .post(submitUrl, {
            company: editCompany,
            first_name: firstName,
            last_name: editLastName,
            address_1: editAddress1,
            address_2: editAddress2,
            postcode: postcode,
            city: city,
            region: selectedRegion,
            country: selectedCountry,
            default_address: itIsDefault,
            id: addressId,
          })
          .then(function (response) {
            console.log(response.data);
            if (response.data.message === true) {
              alert("Succesfully edited");
            } else {
              alert("Somthing went wrong");
            }
          })
          .catch(function (error) {
            console.log(error);
            setError(error.message);
          });
      } else if (
        editFirstName === " " &&
        editLastName != " " &&
        editAddress1 === " " &&
        editPostcode != " " &&
        editCity != " "
      ) {
        axios
          .post(submitUrl, {
            company: editCompany,
            first_name: firstName,
            last_name: editLastName,
            address_1: address1,
            address_2: editAddress2,
            postcode: editPostcode,
            city: editCity,
            region: selectedRegion,
            country: selectedCountry,
            default_address: itIsDefault,
            id: addressId,
          })
          .then(function (response) {
            console.log(response.data);
            if (response.data.message === true) {
              alert("Succesfully edited");
            } else {
              alert("Somthing went wrong");
            }
          })
          .catch(function (error) {
            console.log(error);
            setError(error.message);
          });
      } else if (
        editFirstName === " " &&
        editLastName != " " &&
        editAddress1 === " " &&
        editPostcode != " " &&
        editCity === " "
      ) {
        axios
          .post(submitUrl, {
            company: editCompany,
            first_name: firstName,
            last_name: editLastName,
            address_1: address1,
            address_2: editAddress2,
            postcode: editPostcode,
            city: city,
            region: selectedRegion,
            country: selectedCountry,
            default_address: itIsDefault,
            id: addressId,
          })
          .then(function (response) {
            console.log(response.data);
            if (response.data.message === true) {
              alert("Succesfully edited");
            } else {
              alert("Somthing went wrong");
            }
          })
          .catch(function (error) {
            console.log(error);
            setError(error.message);
          });
      } else if (
        editFirstName === " " &&
        editLastName != " " &&
        editAddress1 === " " &&
        editPostcode === " " &&
        editCity != " "
      ) {
        axios
          .post(submitUrl, {
            company: editCompany,
            first_name: firstName,
            last_name: editLastName,
            address_1: address1,
            address_2: editAddress2,
            postcode: postcode,
            city: editCity,
            region: selectedRegion,
            country: selectedCountry,
            default_address: itIsDefault,
            id: addressId,
          })
          .then(function (response) {
            console.log(response.data);
            if (response.data.message === true) {
              alert("Succesfully edited");
            } else {
              alert("Somthing went wrong");
            }
          })
          .catch(function (error) {
            console.log(error);
            setError(error.message);
          });
      } else if (
        editFirstName === " " &&
        editLastName != " " &&
        editAddress1 === " " &&
        editPostcode === " " &&
        editCity === " "
      ) {
        axios
          .post(submitUrl, {
            company: editCompany,
            first_name: firstName,
            last_name: editLastName,
            address_1: address1,
            address_2: editAddress2,
            postcode: postcode,
            city: city,
            region: selectedRegion,
            country: selectedCountry,
            default_address: itIsDefault,
            id: addressId,
          })
          .then(function (response) {
            console.log(response.data);
            if (response.data.message === true) {
              alert("Succesfully edited");
            } else {
              alert("Somthing went wrong");
            }
          })
          .catch(function (error) {
            console.log(error);
            setError(error.message);
          });
      } else if (
        editFirstName === " " &&
        editLastName === " " &&
        editAddress1 != " " &&
        editPostcode != " " &&
        editCity != " "
      ) {
        axios
          .post(submitUrl, {
            company: editCompany,
            first_name: firstName,
            last_name: lastName,
            address_1: editAddress1,
            address_2: editAddress2,
            postcode: editPostcode,
            city: editCity,
            region: selectedRegion,
            country: selectedCountry,
            default_address: itIsDefault,
            id: addressId,
          })
          .then(function (response) {
            console.log(response.data);
            if (response.data.message === true) {
              alert("Succesfully edited");
            } else {
              alert("Somthing went wrong");
            }
          })
          .catch(function (error) {
            console.log(error);
            setError(error.message);
          });
      } else if (
        editFirstName === " " &&
        editLastName === " " &&
        editAddress1 != " " &&
        editPostcode != " " &&
        editCity === " "
      ) {
        axios
          .post(submitUrl, {
            company: editCompany,
            first_name: firstName,
            last_name: lastName,
            address_1: editAddress1,
            address_2: editAddress2,
            postcode: editPostcode,
            city: city,
            region: selectedRegion,
            country: selectedCountry,
            default_address: itIsDefault,
            id: addressId,
          })
          .then(function (response) {
            console.log(response.data);
            if (response.data.message === true) {
              alert("Succesfully edited");
            } else {
              alert("Somthing went wrong");
            }
          })
          .catch(function (error) {
            console.log(error);
            setError(error.message);
          });
      } else if (
        editFirstName === " " &&
        editLastName === " " &&
        editAddress1 != " " &&
        editPostcode === " " &&
        editCity != " "
      ) {
        axios
          .post(submitUrl, {
            company: editCompany,
            first_name: firstName,
            last_name: lastName,
            address_1: editAddress1,
            address_2: editAddress2,
            postcode: postcode,
            city: editCity,
            region: selectedRegion,
            country: selectedCountry,
            default_address: itIsDefault,
            id: addressId,
          })
          .then(function (response) {
            console.log(response.data);
            if (response.data.message === true) {
              alert("Succesfully edited");
            } else {
              alert("Somthing went wrong");
            }
          })
          .catch(function (error) {
            console.log(error);
            setError(error.message);
          });
      } else if (
        editFirstName === " " &&
        editLastName === " " &&
        editAddress1 != " " &&
        editPostcode === " " &&
        editCity === " "
      ) {
        axios
          .post(submitUrl, {
            company: editCompany,
            first_name: firstName,
            last_name: lastName,
            address_1: editAddress1,
            address_2: editAddress2,
            postcode: postcode,
            city: city,
            region: selectedRegion,
            country: selectedCountry,
            default_address: itIsDefault,
            id: addressId,
          })
          .then(function (response) {
            console.log(response.data);
            if (response.data.message === true) {
              alert("Succesfully edited");
            } else {
              alert("Somthing went wrong");
            }
          })
          .catch(function (error) {
            console.log(error);
            setError(error.message);
          });
      } else if (
        editFirstName === " " &&
        editLastName === " " &&
        editAddress1 === " " &&
        editPostcode != " " &&
        editCity != " "
      ) {
        axios
          .post(submitUrl, {
            company: editCompany,
            first_name: firstName,
            last_name: lastName,
            address_1: address1,
            address_2: editAddress2,
            postcode: editPostcode,
            city: editCity,
            region: selectedRegion,
            country: selectedCountry,
            default_address: itIsDefault,
            id: addressId,
          })
          .then(function (response) {
            console.log(response.data);
            if (response.data.message === true) {
              alert("Succesfully edited");
            } else {
              alert("Somthing went wrong");
            }
          })
          .catch(function (error) {
            console.log(error);
            setError(error.message);
          });
      } else if (
        editFirstName === " " &&
        editLastName === " " &&
        editAddress1 === " " &&
        editPostcode != " " &&
        editCity === " "
      ) {
        axios
          .post(submitUrl, {
            company: editCompany,
            first_name: firstName,
            last_name: lastName,
            address_1: address1,
            address_2: editAddress2,
            postcode: editPostcode,
            city: city,
            region: selectedRegion,
            country: selectedCountry,
            default_address: itIsDefault,
            id: addressId,
          })
          .then(function (response) {
            console.log(response.data);
            if (response.data.message === true) {
              alert("Succesfully edited");
            } else {
              alert("Somthing went wrong");
            }
          })
          .catch(function (error) {
            console.log(error);
            setError(error.message);
          });
      } else if (
        editFirstName === " " &&
        editLastName === " " &&
        editAddress1 === " " &&
        editPostcode === " " &&
        editCity != " "
      ) {
        axios
          .post(submitUrl, {
            company: editCompany,
            first_name: firstName,
            last_name: lastName,
            address_1: address1,
            address_2: editAddress2,
            postcode: postcode,
            city: editCity,
            region: selectedRegion,
            country: selectedCountry,
            default_address: itIsDefault,
            id: addressId,
          })
          .then(function (response) {
            console.log(response.data);
            if (response.data.message === true) {
              alert("Succesfully edited");
            } else {
              alert("Somthing went wrong");
            }
          })
          .catch(function (error) {
            console.log(error);
            setError(error.message);
          });
      } else if (
        editFirstName === " " &&
        editLastName === " " &&
        editAddress1 === " " &&
        editPostcode === " " &&
        editCity === " "
      ) {
        axios
          .post(submitUrl, {
            company: editCompany,
            first_name: firstName,
            last_name: lastName,
            address_1: address1,
            address_2: editAddress2,
            postcode: postcode,
            city: city,
            region: selectedRegion,
            country: selectedCountry,
            default_address: itIsDefault,
            id: addressId,
          })
          .then(function (response) {
            console.log(response.data);
            if (response.data.message === true) {
              alert("Succesfully edited");
            } else {
              alert("Somthing went wrong");
            }
          })
          .catch(function (error) {
            console.log(error);
            setError(error.message);
          });
      } else {
        axios
          .post(submitUrl, {
            company: editCompany,
            first_name: editFirstName,
            last_name: editLastName,
            address_1: editAddress1,
            address_2: editAddress2,
            postcode: editPostcode,
            city: editCity,
            region: selectedRegion,
            country: selectedCountry,
            default_address: itIsDefault,
            id: addressId,
          })
          .then(function (response) {
            console.log(response.data);
            if (response.data.message === true) {
              alert("Succesfully edited");
            } else {
              alert("Somthing went wrong");
            }
          })
          .catch(function (error) {
            console.log(error);
            setError(error.message);
          });
      }
    }
  }
  console.log(editFirstName);
  return (
    <SafeAreaView style={gS.container}>
      <ScrollView style={gS.scroll}>
        <View style={gS.section2}>
          <View style={styles.containerForm}>
            <View style={gS.section2}>
              <View style={styles.formControl}>
                <View style={styles.form}>
                  <Text style={styles.label}>Company</Text>
                  {editCompany === " " ? (
                    <TextInput
                      style={styles.input}
                      id="title"
                      errorText="Please enter a valid title"
                      keyboardType="default"
                      autoCapitalize="sentences"
                      autoCorrect
                      returnKeyType="next"
                      onChangeText={changeCompany}
                      required
                      value={company}
                    />
                  ) : (
                    <TextInput
                      style={styles.input}
                      id="title"
                      errorText="Please enter a valid title"
                      keyboardType="default"
                      autoCapitalize="sentences"
                      autoCorrect
                      returnKeyType="next"
                      onChangeText={changeCompany}
                      required
                      value={editCompany}
                    />
                  )}

                  <Text style={styles.label}>First Name</Text>
                  {editFirstName === " " ? (
                    <TextInput
                      style={styles.input}
                      id="title"
                      errorText="Please enter a valid title"
                      keyboardType="default"
                      autoCapitalize="sentences"
                      autoCorrect
                      returnKeyType="next"
                      onChangeText={titleChangeHandler}
                      required
                      value={firstName}
                    />
                  ) : (
                    <TextInput
                      style={styles.input}
                      id="title"
                      errorText="Please enter a valid title"
                      keyboardType="default"
                      autoCapitalize="sentences"
                      autoCorrect
                      returnKeyType="next"
                      onChangeText={titleChangeHandler}
                      required
                      value={editFirstName}
                    />
                  )}
                  {!firstNameValidSubmit &&
                    !titleIsValid &&
                    editFirstName === "" && (
                      <HelperText type="error">
                        First name cannot be blank.
                      </HelperText>
                    )}

                  <Text style={styles.label}>Last Name</Text>
                  {editLastName === " " ? (
                    <TextInput
                      id="imageUrl"
                      //  label="Image Url"
                      errorText="Please enter a valid image Url"
                      keyboardType="default"
                      returnKeyType="next"
                      onChangeText={changeLastName}
                      required
                      style={styles.input}
                      value={lastName}
                    />
                  ) : (
                    <TextInput
                      id="imageUrl"
                      //  label="Image Url"
                      errorText="Please enter a valid image Url"
                      keyboardType="default"
                      returnKeyType="next"
                      onChangeText={changeLastName}
                      required
                      style={styles.input}
                      value={editLastName}
                    />
                  )}
                  {!firstNameValidSubmit &&
                    !LastNameIsValid &&
                    editLastName === "" && (
                      <HelperText type="error">
                        Last name cannot be blank.
                      </HelperText>
                    )}
                  <Text style={styles.label}>Address 1</Text>
                  {editAddress1 === " " ? (
                    <TextInput
                      id="imageUrl"
                      //  label="Image Url"
                      errorText="Please enter a valid image Url"
                      keyboardType="default"
                      returnKeyType="next"
                      onChangeText={changeAddress1}
                      required
                      style={styles.input}
                      // style={styles.input}
                      // id="price"
                      // label="Price"
                      // errorText="Please enter a valid Price"
                      // keyboardType="decimal-pad"
                      // onInputChange={changeAddress1}
                      // returnKeyType="next"
                      // required
                      // min={0.1}
                      value={address1}
                    />
                  ) : (
                    <TextInput
                      id="imageUrl"
                      //  label="Image Url"
                      errorText="Please enter a valid image Url"
                      keyboardType="default"
                      returnKeyType="next"
                      onChangeText={changeAddress1}
                      required
                      style={styles.input}
                      // style={styles.input}
                      // id="price"
                      // label="Price"
                      // errorText="Please enter a valid Price"
                      // keyboardType="decimal-pad"
                      // onInputChange={changeAddress1}
                      // returnKeyType="next"
                      // required
                      // min={0.1}
                      value={editAddress1}
                    />
                  )}
                  {!firstNameValidSubmit &&
                    !address1IsValid &&
                    editAddress1 === "" && (
                      <HelperText type="error">
                        Address1 cannot be blank.
                      </HelperText>
                    )}
                  <Text style={styles.label}>Address 2</Text>
                  {editAddress2 === " " ? (
                    <TextInput
                      id="imageUrl"
                      //  label="Image Url"
                      errorText="Please enter a valid image Url"
                      keyboardType="default"
                      returnKeyType="next"
                      onChangeText={changeAddress2}
                      required
                      style={styles.input}
                      // initialValue={editedProduct ? editedProduct.description : ""}
                      // initiallyValid={!!editedProduct}
                      // minLength={5}
                      value={address2}
                    />
                  ) : (
                    <TextInput
                      id="imageUrl"
                      //  label="Image Url"
                      errorText="Please enter a valid image Url"
                      keyboardType="default"
                      returnKeyType="next"
                      onChangeText={changeAddress2}
                      required
                      style={styles.input}
                      // initialValue={editedProduct ? editedProduct.description : ""}
                      // initiallyValid={!!editedProduct}
                      // minLength={5}
                      value={editAddress2}
                    />
                  )}

                  <Text style={styles.label}>Postcode</Text>
                  {editPostcode === " " ? (
                    <TextInput
                      style={styles.input}
                      id="title"
                      errorText="Please enter a valid title"
                      keyboardType="default"
                      autoCapitalize="sentences"
                      autoCorrect
                      returnKeyType="next"
                      onChangeText={changePostscode}
                      required
                      value={postcode}
                    />
                  ) : (
                    <TextInput
                      style={styles.input}
                      id="title"
                      errorText="Please enter a valid title"
                      keyboardType="default"
                      autoCapitalize="sentences"
                      autoCorrect
                      returnKeyType="next"
                      onChangeText={changePostscode}
                      required
                      value={editPostcode}
                    />
                  )}
                  {!firstNameValidSubmit &&
                    !postcodeIsValid &&
                    editPostcode === "" && (
                      <HelperText type="error">
                        Postcode cannot be blank.
                      </HelperText>
                    )}
                  <Text style={styles.label}>City</Text>
                  {editCity === " " ? (
                    <TextInput
                      style={styles.input}
                      id="title"
                      errorText="Please enter a valid title"
                      keyboardType="default"
                      autoCapitalize="sentences"
                      autoCorrect
                      returnKeyType="next"
                      onChangeText={changeCity}
                      required
                      value={city}
                    />
                  ) : (
                    <TextInput
                      style={styles.input}
                      id="title"
                      errorText="Please enter a valid title"
                      keyboardType="default"
                      autoCapitalize="sentences"
                      autoCorrect
                      returnKeyType="next"
                      onChangeText={changeCity}
                      required
                      value={editCity}
                    />
                  )}
                  {!firstNameValidSubmit && !cityIsValid && editCity === "" && (
                    <HelperText type="error">City cannot be blank.</HelperText>
                  )}
                  {/* <View style={styles.sectionRC}> */}
                  <Text style={styles.label}>State</Text>
                  <View
                    style={{
                      color: "#F00",
                      // borderBottomWidt: 10,
                      borderColor: "#ccc",
                      borderBottomWidth: 1,
                    }}
                  >
                    <Picker
                      //  mode="dropdown"
                      // iosIcon={<Icon name="arrow-down" color={"#007aff"} />}
                      // style={{ height: 50, width: 150 }}
                      selectedValue={selectedRegion}
                      // placeholder="Select your country"
                      // placeholderStyle={{ color: "#007aff" }}
                      //placeholderIconColor="#007aff"
                      onValueChange={changeRegion}
                    >
                      {Object(states).map((c) => {
                        return (
                          <Picker.Item
                            label={c.state_name}
                            value={c.states_id}
                            style={{ color: "#F00" }}
                          />
                        );
                      })}
                    </Picker>
                  </View>
                  {!firstNameValidSubmit &&
                    !regionIsValid &&
                    selectedRegion === "" && (
                      <HelperText type="error">
                        Region cannot be blank.
                      </HelperText>
                    )}
                  {/* </View> */}
                  {/* <View style={gS.section}> */}
                  <Text style={styles.label}>Country</Text>
                  <View
                    style={{
                      color: "#F00",
                      // borderBottomWidt: 10,
                      borderColor: "#ccc",
                      borderBottomWidth: 1,
                    }}
                  >
                    <Picker
                      //  mode="dropdown"
                      // iosIcon={<Icon name="arrow-down" color={"#007aff"} />}
                      // style={{ height: 50, width: 150 }}
                      selectedValue={selectedCountry}
                      // placeholder="Select your country"
                      // placeholderStyle={{ color: "#007aff" }}
                      //placeholderIconColor="#007aff"
                      onValueChange={changeCountry}
                    >
                      {Object(countries).map((c) => {
                        return (
                          <Picker.Item
                            label={c.name}
                            value={c.country_id}
                            style={{ color: "#F00" }}
                          />
                        );
                      })}
                    </Picker>
                    {/* </View> */}
                    {!firstNameValidSubmit &&
                      !countryIsValid &&
                      selectedCountry === "" && (
                        <HelperText type="error">
                          Country cannot be blank.
                        </HelperText>
                      )}
                  </View>
                </View>
              </View>
            </View>
          </View>
          {/* <View style={styles.containerFormDefault}>
            <FiltersSwitch
              label="Set as default address"
              state={selectedDefault}
              onChange={(newValue) => setSelectedDefault(newValue)}
            />
          </View> */}

          {/* <View style={styles.containerFormDefault2}>
            <TouchableOpacity onPress={() => handlerAddressDelete()}>
              <Text style={styles.DeleteAddText}>Delete Address</Text>
            </TouchableOpacity>
          </View> */}
          <TouchableWithoutFeedback onPress={handler}>
            <View style={gS.blueBtn}>
              <Text style={gS.blueBtnWord}>Submit</Text>
            </View>
          </TouchableWithoutFeedback>
        </View>
      </ScrollView>
    </SafeAreaView>
  );
};

export default addAddress;

const styles = StyleSheet.create({
  containerForm: {
    shadowColor: "#f9f5f5",
    shadowOpacity: 0.26,
    shadowOffset: { width: 0, height: 2 },
    shadowRadius: 8,
    elevation: 1,
    borderRadius: 5,
    borderLeftColor: "#f9f5f5",
    borderStartColor: "#f9f5f5",
    borderColor: "#f9f5f5",
    height: "90%",
    width: "100%",
    // margin: 5,
    backgroundColor: "#FFFFFF",
  },
  containerFormDefault: {
    marginTop: 10,
    shadowColor: "#f9f5f5",
    shadowOpacity: 0.26,
    shadowOffset: { width: 0, height: 2 },
    shadowRadius: 8,
    elevation: 1,
    borderRadius: 5,
    borderLeftColor: "#f9f5f5",
    borderStartColor: "#f9f5f5",
    borderColor: "#f9f5f5",
    height: "10%",
    width: "100%",

    backgroundColor: "#FFFFFF",
  },
  containerFormDefault2: {
    marginTop: 10,

    shadowColor: "#f9f5f5",
    shadowOpacity: 0.26,
    shadowOffset: { width: 0, height: 2 },
    shadowRadius: 8,
    elevation: 1,
    borderRadius: 5,
    // borderLeftColor: "#f9f5f5",
    // borderStartColor: "#f9f5f5",
    borderColor: "#f9f5f5",
    height: "10%",
    width: "100%",
    // margin: 5,
    backgroundColor: "#FFFFFF",
  },
  filterContainer: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    marginVertical: 10,
    width: "80%",
  },
  title: {
    fontFamily: "open-sans-bold",
    fontSize: 17,
    margin: 20,
    textAlign: "center",
    justifyContent: "center",
  },
  DeleteAddText: {
    fontFamily: "open-sans-bold",
    fontSize: 17,
    margin: 20,
    color: "#a90308",
    // textAlign: "center",
  },
  form: {
    margin: 10,
  },
  formControl: {
    width: "100%",
  },
  label: {
    fontFamily: "open-sans-bold",
    marginVertical: 8,
  },
  input: {
    paddingHorizontal: 2,
    paddingVertical: 2,
    borderBottomColor: "#ccc",
    borderBottomWidth: 1,
    fontSize: 18,
  },
  sectionRC: {
    flex: 1,
    margin: 10,
    height: 100,
    marginTop: 30,
  },
  formRC: {
    borderWidth: 1,
    borderColor: "gray",
    borderRadius: 5,
    backgroundColor: "#EFEFEF",
    marginTop: -40,
  },
});
