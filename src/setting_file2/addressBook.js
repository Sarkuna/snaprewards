import "react-native-gesture-handler";
import React, { useState, useEffect } from "react";
import gS from "../globalStyle";
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  Picker,
  SafeAreaView,
  ScrollView,
  Modal,
  ToastAndroid,
  Alert,
  CheckBox,
  FlatList,
  RefreshControl,
} from "react-native";
import { Icon } from "react-native-elements";
import { LinearGradient } from "expo-linear-gradient";
import axios from "axios";
import { useSelector, useDispatch } from "react-redux";
import AddressBookUI from "../../components/UI/AddressBookUI";

import * as productsActions from "../../store/actions/categories";
import categories from "../../store/reducers/categories";
import { useIsFocused } from "@react-navigation/native";

const addressBook = (props) => {
  console.log("Screen Address Book");

  const [modalVisible, setModalVisible] = useState(false);
  const [isSelected, setSelection] = useState(false);

  const [name, setName] = useState("John Lim");
  const [phone, setPhone] = useState("+6012-345 6789");
  const [addr1, setAddr1] = useState("63 JLN Rasha Jaya Rasha");
  const [addr2, setAddr2] = useState("Taman Universiti 1");
  const [city, setCity] = useState("Seremban");
  const [postcode, setPostcode] = useState("54200");
  const [state, setState] = useState("Sembilan");
  const [country, setCountry] = useState("Malaysia");

  const myToken = useSelector((state) => state.auth.access_token);
  console.log(myToken);
  const dispatch = useDispatch();
  const isFocused = useIsFocused();

  React.useEffect(() => {
    //const unsubscribe = props.navigation.addListener("focus", () => {
    // Alert.alert('Refreshed');

    dispatch(productsActions.addressDetails(myToken));
    //dispatch(productsActions.pendingDetails(myToken));
    // });
    // return unsubscribe;
  }, [isFocused, dispatch]);

  // useEffect(() => {
  //   dispatch(productsActions.addressDetails(myToken));
  //   //dispatch(productsActions.pendingDetails(myToken));
  // }, [dispatch]);
  //console.log("GenePending Detail Page");

  //const myToken = useSelector(state => state.auth.access_token)
  const Categories = useSelector((state) => state.address.availableAddress);
  console.log(Categories);

  console.log(global.url);
  console.log(myToken);
  const addressBookList =
    global.url + "/address-book/index?access-token=" + myToken;

  const deleteCheck = () =>
    Alert.alert(
      "Confirm to Delete?",
      "This Action Cannot be Undone.",
      [
        {
          text: "No",
          onPress: () => console.log("Cancel Pressed"),
          style: "cancel",
        },
        {
          text: "Yes",
          onPress: () =>
            ToastAndroid.show("Address Deleted", ToastAndroid.SHORT),
        },
      ],
      { cancelable: false }
    );

  axios
    .get(addressBookList)
    .then(function (response) {
      // console.log(response.data.address_lists);
      // setPendingCount(response.data.totalpoint.length);
    })
    .catch(function (error) {
      console.log(error);
    });
  const renderGrideItem = (itemData) => {
    console.log(itemData.item.default_Address);

    return (
      <AddressBookUI
        name={itemData.item.name}
        address={itemData.item.address}
        Postcode={itemData.item.postcode}
        Country={itemData.item.country}
        default_address={itemData.item.default_address}
        // color={itemData.item.color}
        // pts={itemData.item.pts}

        defaultBtn={() => {
          Alert.alert(
            "Are you sure to change as default address?",
            "",
            [
              {
                text: "Cancel",
                onPress: () => console.log("Cancel Pressed"),
                style: "cancel",
              },
              {
                text: "Confirm",
                onPress: () => {
                  props.navigation.goBack(),
                    axios
                      .get(
                        global.url +
                          `/address-book/default-address?access-token=${myToken}&id=${itemData.item.id}`
                      )
                      .then(function (response) {
                        console.log(response.data);
                        // if (response.data.message === true) {
                        //   alert("Succesfully edited");
                        // } else {
                        //   alert("Somthing went wrong");
                        // }
                      })
                      .catch(function (error) {
                        console.log(error);
                      });
                },
              },
            ],
            { cancelable: false }
          );
        }}
        onEdit={() => {
          props.navigation.navigate({
            name: "EditAddress",
            params: {
              id: itemData.item.id,
              name: itemData.item.name,
              address: itemData.item.address,
              postcode: itemData.item.postcode,
              country: itemData.item.country,
              statename: itemData.item.statename,
              stateid: itemData.item.stateid,
              default_address: itemData.item.default_address,
              //pageName: "Redeem Gallery",
            },
          });
        }}
      />
    );
  };
  //console.log(id);
  const defaultHandler = () => {
    Alert.alert(
      "Are you sure to delete address?",
      "",
      [
        {
          text: "Cancel",
          onPress: () => console.log("Cancel Pressed"),
          style: "cancel",
        },
        {
          text: "Confirm",
          onPress: () => {
            props.navigation.goBack(),
              axios
                .get(defaultUrl, {})
                .then(function (response) {
                  console.log(response.data);
                  if (response.data.message === true) {
                    alert("Succesfully edited");
                  } else {
                    alert("Somthing went wrong");
                  }
                })
                .catch(function (error) {
                  console.log(error);
                });
          },
        },
      ],
      { cancelable: false }
    );
  };

  return (
    <SafeAreaView style={gS.container}>
      <ScrollView style={gS.scroll}>
        <View style={{ margin: 25, justifyContent: "center" }}>
          <TouchableOpacity
            activeOpacity={0.92}
            // onPress={() => {
            //   props.navigation.navigate({
            //     name: "DetailsScreen",
            //     params: {
            //       pageId: 1,
            //       pageName: "Redeem Gallery",
            //     },
            //   });
            // }}
          >
            <LinearGradient
              // Background Linear Gradient
              colors={["rgba(0,0,0,0.8)", "transparent"]}
              style={styles.background}
            />
            <LinearGradient
              // Button Linear Gradient

              // tvParallaxTiltAngle={-176}
              colors={["#E6E6FA", "#E6E6FA"]}
              start={{ x: 0, y: 0.5 }}
              end={{ x: 1, y: 0.5 }}
              style={{
                borderColor: "#66F",
                borderStyle: "dashed",
                borderWidth: 2,
                borderRadius: 1,
              }}
            >
              <TouchableOpacity
                onPress={() => {
                  props.navigation.navigate({
                    name: "AddAddress",
                    // params: {
                    //   categoryId: itemData.item.id,
                    // },
                  });
                }}
              >
                <View style={{ height: 80 }}>
                  {/* <Icon raised name="gift" type="font-awesome" color="#66F" /> */}

                  <Text
                    style={{
                      fontSize: 18,
                      color: "#66F",
                      marginTop: 25,
                      textAlign: "center",
                    }}
                  >
                    + Add Address
                  </Text>
                  {/* <Text style={{ fontSize: 8, color: "#ffffff" }}>
                    Redeem Your Point
                  </Text> */}
                </View>
              </TouchableOpacity>
            </LinearGradient>
          </TouchableOpacity>
        </View>
        {/* 
        <TouchableOpacity>
          <View style={styles.settingSection}>
            <View style={styles.iconAlign}>
              <View style={styles.iconA}>
                <Icon name="map-marker" type="font-awesome" color="#285aed" />
              </View>
              <View style={styles.addressMain}>
                <Text style={styles.name}>I M Rayid</Text>
                <Text style={styles.phone}>0777123456</Text>
                <View style={styles.addressDefaultAlign}>
                  <LinearGradient
                    // Background Linear Gradient
                    colors={["rgba(0,0,0,0.8)", "transparent"]}
                    style={styles.background}
                  />
                  <LinearGradient
                    // Button Linear Gradient

                    // tvParallaxTiltAngle={-176}
                    colors={["#3eb1f0", "#346cdd"]}
                    start={{ x: 0, y: 0.5 }}
                    end={{ x: 1, y: 0.5 }}
                    style={styles.button}
                  >
                    <View style={{ alignItems: "center", marginTop: 1 }}>
                      <Text style={{ fontSize: 12, color: "#ffffff" }}>
                        Default
                      </Text>
                    </View>
                  </LinearGradient>

                  <Text style={styles.address}>
                    Cecilia Chapman 711-2880 Nulla St. Mankato Mississippi 96522
                    (257) 563-7401
                  </Text>
                </View>
              </View>
              <View style={styles.iconB}>
                <Icon
                  name="pencil-square-o"
                  type="font-awesome"
                  color="#66F"
                  onPress={() => navigation.navigate("EditAddress")}
                />
              </View>
            </View>
          </View>
        </TouchableOpacity>

        <View style={styles.line}></View>

        <TouchableOpacity>
          <View style={styles.settingSection}>
            <View style={styles.iconAlign}>
              <View style={styles.iconA}>
                <Icon name="map-marker" type="font-awesome" color="#285aed" />
              </View>
              <View style={styles.addressMain}>
                <Text style={styles.name}>I M Rayid</Text>
                <Text style={styles.phone}>0777123456</Text>

                <Text style={styles.address}>
                  Celeste Slater 606-3727 Ullamcorper. Street Roseville NH 11523
                  (786) 713-8616
                </Text>
              </View>
              <View style={styles.iconB}>
                <Icon
                  name="pencil-square-o"
                  type="font-awesome"
                  color="#66F"
                  onPress={() => navigation.navigate("EditAddress")}
                />
              </View>
            </View>
          </View>
        </TouchableOpacity>

        <View style={styles.line}></View>

        <TouchableOpacity>
          <View style={styles.settingSection}>
            <View style={styles.iconAlign}>
              <View style={styles.iconA}>
                <Icon name="map-marker" type="font-awesome" color="#285aed" />
              </View>
              <View style={styles.addressMain}>
                <Text style={styles.name}>I M Rayid</Text>
                <Text style={styles.phone}>0777123456</Text>

                <Text style={styles.address}>
                  Iris Watson P.O. Box 283 8562 Fusce Rd. Frederick Nebraska
                  20620 (372) 587-2335
                </Text>
              </View>
              <View style={styles.iconB}>
                <Icon
                  name="pencil-square-o"
                  type="font-awesome"
                  color="#66F"
                  onPress={() => navigation.navigate("EditAddress")}
                />
              </View>
            </View>
          </View>
        </TouchableOpacity>

        <View style={styles.line}></View> */}

        <FlatList
          //horizontal={true}
          showsHorizontalScrollIndicator={false}
          keyExtractor={(item, index) => item.id}
          data={Categories}
          renderItem={renderGrideItem}
          // sliderWidth={200}
          // itemWidth={200}
          //numColumns={2}
        />
      </ScrollView>
    </SafeAreaView>
  );
};

export default addressBook;

const styles = StyleSheet.create({
  btn: {
    padding: 15,
    marginHorizontal: 15,
    height: 50,
    borderRadius: 15,
    justifyContent: "center",
    marginVertical: 20,
  },
  postcode: {
    color: "black",
    fontSize: 20,
    fontWeight: "bold",
  },
  card: {
    margin: 20,
    padding: 20,
    backgroundColor: "#DDF",
    borderRadius: 10,

    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.58,
    shadowRadius: 5.0,

    elevation: 5,
  },
  up: {
    padding: 20,
    paddingBottom: 40,
    position: "absolute",
    bottom: 0,
    width: "96%",
    height: "80%",
    marginLeft: "2%",
    marginRight: "1%",
    borderTopLeftRadius: 30,
    borderTopRightRadius: 30,
    backgroundColor: "#fff",
  },
  settingSection: {
    padding: 10,
    backgroundColor: "white",
    flexDirection: "row",
    alignItems: "flex-start",
  },
  wordA: {
    color: "black",
    fontSize: 16,
    position: "absolute",
    left: "17%",
  },
  addressMain: {
    flexDirection: "column",
    flex: 1,
  },
  name: {
    color: "black",
    fontSize: 16,
    left: "5%",
    //  marginTop: 5,
    fontFamily: "open-sans",
  },
  phone: {
    color: "black",
    fontSize: 16,
    left: "5%",
    marginTop: 10,
    fontFamily: "open-sans",
  },
  address: {
    color: "black",
    fontSize: 13,
    left: "5%",
    marginTop: 10,
    marginRight: 15,
    fontFamily: "open-sans",
  },
  // iconA: {
  //   margin: 5,
  // },
  // iconB: {
  //   position: "absolute",
  //   right: 2,
  //   margin: 12,
  // },
  iconAlign: {
    flex: 1,
    flexDirection: "row",
    justifyContent: "space-between",
    // alignItems: "center",
    // padding: 10,
    // marginLeft: 10,
    // marginRight: 10,
    margin: 10,
  },
  line: {
    width: "100%",
    height: 1,
    backgroundColor: "lightgray",
    marginLeft: "5%",
  },
  addressDefaultAlign: {
    flex: 1,
    flexDirection: "row",
    justifyContent: "space-between",
    // alignItems: "center",
    // padding: 10,
    // marginLeft: 10,
    // marginRight: 10,
    margin: 10,
  },
  button: {
    //padding: 15,
    //width: "100%",
    //flex: 1,
    height: 20,
    marginTop: 10,
    width: 50,
    left: "15%",
    marginRight: 10,
    //flexDirection: "row",
    borderRadius: 8,
    // alignItems: "center",
    //margin: 10,

    // shadowColor: "#000",
    // shadowOffset: { width: 0, height: 5 },
    // shadowOpacity: 0.34,
    // shadowRadius: 6.27,
    // elevation: 5,
  },
});
