import "react-native-gesture-handler";
import React, { useState, useEffect } from "react";
import gS from "../globalStyle";
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  Picker,
  SafeAreaView,
  ScrollView,
  Modal,
  ToastAndroid,
  Alert,
  CheckBox,
  FlatList,
} from "react-native";
import { Icon } from "react-native-elements";
import { LinearGradient } from "expo-linear-gradient";
import { useSelector, useDispatch } from "react-redux";
import BankUI from "../../components/UI/BankUI";
import { useIsFocused } from "@react-navigation/native";

import * as productsActions from "../../store/actions/categories";

export default function App({ navigation }) {
  const [showDetails, setshowDetails] = useState(false);
  const [showDetails2, setshowDetails2] = useState(false);
  console.log("Scan Page");

  const [modalVisible, setModalVisible] = useState(false);
  const [isSelected, setSelection] = useState(false);

  const [name, setName] = useState("John Lim");
  const [phone, setPhone] = useState("+6012-345 6789");
  const [addr1, setAddr1] = useState("63 JLN Rasha Jaya Rasha");
  const [addr2, setAddr2] = useState("Taman Universiti 1");
  const [city, setCity] = useState("Seremban");
  const [postcode, setPostcode] = useState("54200");
  const [state, setState] = useState("Sembilan");
  const [country, setCountry] = useState("Malaysia");

  const myToken = useSelector((state) => state.auth.access_token);
  console.log(myToken);
  const dispatch = useDispatch();
  const isFocused = useIsFocused(); // Referesh the screen
  useEffect(() => {
    dispatch(productsActions.bankDetails(myToken));
    //dispatch(productsActions.pendingDetails(myToken));
  }, [isFocused, dispatch]);

  const Categories = useSelector((state) => state.bank.availableBanks);
  console.log(Categories);
  const [idConfirm, setIdConfirm] = useState(false);
  // if (Categories[0].id === 5) {
  //   setIdConfirm(true);
  // }
  const renderGrideItem = (itemData) => {
    return (
      <BankUI
        bank_name={itemData.item.bank_name}
        account_name={itemData.item.account_name}
        account_number={itemData.item.account_number}
        nric_passport={itemData.item.nric_passport}
        verify={itemData.item.verify}
        id={itemData.item.id}
        // color={itemData.item.color}
        // pts={itemData.item.pts}

        onAngle={() => {
          setshowDetails((prevState) => !prevState);
        }}
        arrowControl={showDetails ? "angle-up" : "angle-down"}
        viewControl={showDetails}
        onEdit={() => {
          navigation.navigate({
            name: "EditBank",
            params: {
              id: itemData.item.id,
              bank_name: itemData.item.bank_name,
              account_name: itemData.item.account_name,
              account_number: itemData.item.account_number,
              nric_passport: itemData.item.nric_passport,
              verify: itemData.item.verify,
            },
          });
        }}
      />
    );
  };
  const deleteCheck = () =>
    Alert.alert(
      "Confirm to Delete?",
      "This Action Cannot be Undone.",
      [
        {
          text: "No",
          onPress: () => console.log("Cancel Pressed"),
          style: "cancel",
        },
        {
          text: "Yes",
          onPress: () =>
            ToastAndroid.show("Address Deleted", ToastAndroid.SHORT),
        },
      ],
      { cancelable: false }
    );

  return (
    <SafeAreaView style={gS.container}>
      <ScrollView style={gS.scroll}>
        <View style={{ margin: 25, justifyContent: "center" }}>
          <TouchableOpacity
            activeOpacity={0.92}
            // onPress={() => {
            //   props.navigation.navigate({
            //     name: "DetailsScreen",
            //     params: {
            //       pageId: 1,
            //       pageName: "Redeem Gallery",
            //     },
            //   });
            // }}
          >
            <LinearGradient
              // Background Linear Gradient
              colors={["rgba(0,0,0,0.8)", "transparent"]}
              style={styles.background}
            />
            <LinearGradient
              // Button Linear Gradient

              // tvParallaxTiltAngle={-176}
              colors={["#E6E6FA", "#E6E6FA"]}
              start={{ x: 0, y: 0.5 }}
              end={{ x: 1, y: 0.5 }}
              style={{
                borderColor: "#66F",
                borderStyle: "dashed",
                borderWidth: 2,
                borderRadius: 1,
              }}
            >
              <TouchableOpacity
                onPress={() => {
                  navigation.navigate({
                    name: "AddBank",
                    // params: {
                    //   categoryId: itemData.item.id,
                    // },
                  });
                }}
              >
                <View style={{ height: 80 }}>
                  {/* <Icon raised name="gift" type="font-awesome" color="#66F" /> */}

                  <Text
                    style={{
                      fontSize: 18,
                      color: "#66F",
                      marginTop: 25,
                      textAlign: "center",
                    }}
                  >
                    + Add New
                  </Text>
                  {/* <Text style={{ fontSize: 8, color: "#ffffff" }}>
                    Redeem Your Point
                  </Text> */}
                </View>
              </TouchableOpacity>
            </LinearGradient>
          </TouchableOpacity>
        </View>

        <View style={styles.line}></View>

        <View style={styles.line}></View>

        <FlatList
          //horizontal={true}
          showsHorizontalScrollIndicator={false}
          keyExtractor={(item, index) => item.id}
          data={Categories}
          renderItem={renderGrideItem}
          // sliderWidth={200}
          // itemWidth={200}
          //numColumns={2}
        />
      </ScrollView>
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  btn: {
    padding: 15,
    marginHorizontal: 15,
    height: 50,
    borderRadius: 15,
    justifyContent: "center",
    marginVertical: 20,
  },
  postcode: {
    color: "black",
    fontSize: 20,
    fontWeight: "bold",
  },
  card: {
    margin: 20,
    padding: 20,
    backgroundColor: "#DDF",
    borderRadius: 10,

    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.58,
    shadowRadius: 5.0,

    elevation: 5,
  },
  up: {
    padding: 20,
    paddingBottom: 40,
    position: "absolute",
    bottom: 0,
    width: "96%",
    height: "80%",
    marginLeft: "2%",
    marginRight: "1%",
    borderTopLeftRadius: 30,
    borderTopRightRadius: 30,
    backgroundColor: "#fff",
  },
  settingSection: {
    padding: 10,
    backgroundColor: "white",
    flexDirection: "row",
    alignItems: "flex-start",
  },
  wordA: {
    color: "black",
    fontSize: 16,
    position: "absolute",
    left: "17%",
  },
  addressMain: {
    flexDirection: "column",
    flex: 1,
  },
  name: {
    color: "black",
    fontSize: 16,
    marginLeft: 10,
    // left: "5%",
    //  marginTop: 5,
    fontFamily: "open-sans",
  },
  phone: {
    color: "black",
    fontSize: 16,
    left: "5%",
    marginTop: 10,
    fontFamily: "open-sans",
  },
  address: {
    color: "black",
    fontSize: 13,
    left: "5%",
    marginTop: 10,
    marginRight: 15,
    fontFamily: "open-sans",
  },
  // iconA: {
  //   margin: 5,
  // },
  // iconB: {
  //   position: "absolute",
  //   right: 2,
  //   margin: 12,
  // },
  iconAlign: {
    flex: 1,
    flexDirection: "row",
    justifyContent: "space-between",
    // alignItems: "center",
    // padding: 10,
    // marginLeft: 10,
    // marginRight: 10,
  },
  alignFirstColoumn: {
    flexDirection: "column",
    flex: 1,
  },
  line: {
    width: "100%",
    height: 1,
    backgroundColor: "lightgray",
    marginLeft: "5%",
  },
  defaultAlign: {
    flex: 1,
    flexDirection: "row",
    justifyContent: "flex-start",
    // alignItems: "center",
    // padding: 10,
    // marginLeft: 10,
    // marginRight: 10,
  },
  button: {
    //padding: 15,
    //width: "100%",
    //flex: 1,
    height: 20,
    marginTop: 10,
    width: 50,
    marginLeft: 10,
    // marginRight: 10,
    //flexDirection: "row",
    borderRadius: 8,
    // alignItems: "center",
    //margin: 10,

    // shadowColor: "#000",
    // shadowOffset: { width: 0, height: 5 },
    // shadowOpacity: 0.34,
    // shadowRadius: 6.27,
    // elevation: 5,
  },
  dateTime: {
    fontFamily: "open-sans",
    fontSize: 17,
    fontWeight: "500",
    textAlign: "left",
    marginLeft: 10,
  },
  receiptDate: {
    fontFamily: "open-sans",
    fontSize: 17,
    fontWeight: "500",
    textAlign: "left",
    marginLeft: 10,
    marginTop: 5,
  },
  statusVerification: {
    fontFamily: "open-sans",
    fontSize: 17,
    fontWeight: "500",
    textAlign: "left",
    marginLeft: 10,
    marginTop: 5,
  },
});

// <TouchableOpacity>
//           <View style={styles.settingSection}>
//             <View style={styles.iconAlign}>
//               <View style={styles.alignFirstColoumn}>
//                 <Text style={styles.name}>Bank Name : Commercial Bank</Text>
//                 <Text style={styles.name}>Account Name : J I M Rayid</Text>

//                 <LinearGradient
//                   // Background Linear Gradient
//                   colors={["rgba(0,0,0,0.8)", "transparent"]}
//                   style={styles.background}
//                 />
//                 {/* <LinearGradient
//                   // Button Linear Gradient

//                   // tvParallaxTiltAngle={-176}
//                   colors={["#3eb1f0", "#346cdd"]}
//                   start={{ x: 0, y: 0.5 }}
//                   end={{ x: 1, y: 0.5 }}
//                   style={styles.button}
//                 > */}
//                 {/* <View style={{ alignItems: "center", marginTop: 1 }}>
//                     <Text style={{ fontSize: 12, color: "#ffffff" }}>
//                       Default
//                     </Text>
//                   </View> */}
//                 {/* </LinearGradient> */}
//                 {showDetails2 && (
//                   <View
//                     style={{
//                       top: 8,
//                       flex: 1,
//                       flexDirection: "column",
//                       justifyContent: "space-between",
//                     }}
//                   >
//                     <Text style={styles.dateTime}>
//                       Account Number : 1000346332344
//                     </Text>
//                     <Text style={styles.receiptDate}>
//                       NRIC/Passport : 143269
//                     </Text>
//                     <View
//                       style={{
//                         flex: 1,
//                         flexDirection: "row",
//                         //  justifyContent: "flex-end",
//                         marginBottom: 10,
//                       }}
//                     >
//                       <Text style={styles.statusVerification}>
//                         Verify : Pending
//                       </Text>
//                       <View
//                         style={{
//                           flex: 1,
//                           marginTop: 5,
//                           //left: 5,
//                           //marginLeft: 120,
//                           flexDirection: "row",
//                           justifyContent: "flex-end",
//                           marginRight: -50,
//                         }}
//                       >
//                         {/* <View style={{ marginLeft: 100 }}>
//                           <Icon
//                             name="trash-o"
//                             type="font-awesome"
//                             color="#66F"
//                           />
//                         </View> */}
//                         <View style={{ marginLeft: 20 }}>
//                           <Icon
//                             onPress={() => navigation.navigate("EditBank")}
//                             name="pencil-square-o"
//                             type="font-awesome"
//                             color="#66F"
//                           />
//                         </View>
//                       </View>
//                     </View>
//                   </View>
//                 )}
//               </View>
//               <Icon
//                 raised
//                 type="fontisto"
//                 color="#03145c"
//                 size={20}
//                 name={showDetails2 ? "angle-up" : "angle-down"}
//                 onPress={() => {
//                   setshowDetails2((prevState) => !prevState);
//                 }}
//               />
//             </View>
//           </View>
//         </TouchableOpacity>
