import "react-native-gesture-handler";
import React, { useState, useEffect } from "react";
import gS from "../globalStyle";
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  SafeAreaView,
  ScrollView,
} from "react-native";
import { Icon } from "react-native-elements";

import axios from "axios";
import { useSelector, useDispatch } from "react-redux";
import { useIsFocused } from "@react-navigation/native";
import CompanyInfoUI from "../../components/UI/CompanyInfoUI";

const companyInfo = (props) => {
  console.log("Screen company Info");
  const isFocused = useIsFocused(); // Referesh the screen
  const [companyName, setcompanyName] = useState("");
  const [regType, setRegType] = useState("");
  const [regNumber, setRegNumber] = useState("");
  const [contactPerson, setContactPerson] = useState("");
  const [contactNo, setContactNo] = useState("");
  const [mailAddress, setMailAddress] = useState("");
  const [tel, setTel] = useState("");
  const [fax, setFax] = useState("");
  const [comId, setComId] = useState("");

  const myToken = useSelector((state) => state.auth.access_token);
  console.log(myToken);

  const companyInfoUrl =
    global.url + "/profile/company?access-token=" + myToken;
  useEffect(() => {
    axios
      .get(companyInfoUrl)
      .then(function (response) {
        // console.log(response);
        setcompanyName(response.data.company_name);
        setRegType(response.data.registration_type);
        setRegNumber(response.data.registration_number);
        setContactPerson(response.data.contact_person);
        setContactNo(response.data.contact_no);
        setMailAddress(response.data.mailing_address);
        setTel(response.data.tel);
        setFax(response.data.fax);
        setComId(response.data.id);
      })
      .catch(function (error) {
        console.log(error);
      });
  }, [isFocused]);

  console.log(regType);
  console.log(regType);
  console.log(regType);
  console.log(regType);

  // onEdit={() => {
  //   props.navigation.navigate({
  //     name: "EditAddress",
  //     params: {
  //       id: itemData.item.id,
  //       name: itemData.item.name,
  //       address: itemData.item.address,
  //       postcode: itemData.item.postcode,
  //       country: itemData.item.country,
  //       default_address: itemData.item.default_address,

  //console.log(id);

  return (
    <SafeAreaView style={gS.container}>
      <ScrollView style={gS.scroll}>
        <View>
          <View style={styles.line}></View>
          <TouchableOpacity onLongPress={props.defaultBtn}>
            <View style={styles.settingSection}>
              <View style={styles.iconAlign}>
                {/* <View style={styles.iconA}>
              <Icon name="map-marker" type="font-awesome" color="#285aed" />
            </View> */}
                <View style={styles.addressMain}>
                  <Text style={styles.addressName}>Company Name</Text>

                  <Text style={styles.address}>{companyName}</Text>

                  <View style={{ marginTop: 18 }}>
                    <Text style={styles.addressName}>
                      Registration Type & Number
                    </Text>
                    <Text style={styles.address}>{regType}</Text>
                    <Text style={styles.address}>{regNumber}</Text>
                  </View>
                  {/* <View style={{ marginTop: 18 }}>
                    <Text style={styles.addressName}>Contact Person</Text>
                    <Text style={styles.address}>{contactPerson}</Text>
                    <Text style={styles.address}>{contactNo}</Text>
                  </View> */}
                  <View style={{ marginTop: 18 }}>
                    <Text style={styles.addressName}>Address</Text>
                    <View style={{ marginTop: 2 }}>
                      <Text style={styles.address}>{mailAddress}</Text>
                      <Text style={styles.address}>{props.address2}</Text>
                    </View>
                  </View>
                  <View
                    style={{
                      marginTop: 18,
                      flex: 1,
                      flexDirection: "row",
                      alignItems: "space-between",
                      left: "2%",
                    }}
                  >
                    <Text style={styles.addressName}>Tel :{"  "} </Text>
                    <View>
                      <Text style={styles.tel_fax}>{tel}</Text>
                    </View>
                  </View>
                  <View
                    style={{
                      marginTop: 18,
                      flex: 1,
                      flexDirection: "row",
                      alignItems: "space-between",
                      left: "2%",
                    }}
                  >
                    <Text style={styles.addressName}>Fax :{"  "}</Text>
                    <View>
                      <Text style={styles.tel_fax}>{fax}</Text>
                    </View>
                  </View>
                </View>
                <View style={styles.iconB}>
                  <Icon
                    name="pencil-square-o"
                    type="font-awesome"
                    color="#66F"
                    onPress={() =>
                      props.navigation.navigate({
                        name: "EditCompanyInfo",
                        params: {
                          comId: comId,
                          comName: companyName,
                          frmComAddress: mailAddress,
                          frmTelNum: tel,
                          frmfax: fax,
                          frmRegNum: regNumber,
                          frmRegType: regType,
                        },
                      })
                    }
                  />
                </View>
              </View>
            </View>
          </TouchableOpacity>
          <View style={styles.line}></View>
        </View>
      </ScrollView>
    </SafeAreaView>
  );
};

export default companyInfo;

const styles = StyleSheet.create({
  settingSection: {
    padding: 10,
    backgroundColor: "white",
    flexDirection: "row",
    alignItems: "flex-start",
  },
  line: {
    width: "100%",
    height: 1,
    backgroundColor: "lightgray",
    //marginLeft: "2%",
  },
  iconAlign: {
    flex: 1,
    flexDirection: "row",
    justifyContent: "space-between",
    // alignItems: "center",
    // padding: 10,
    // marginLeft: 10,
    // marginRight: 10,
    margin: 10,
  },
  addressMain: {
    flexDirection: "column",
    flex: 1,
  },
  name: {
    color: "black",
    fontSize: 16,
    left: "2%",
    //  marginTop: 5,
    fontFamily: "open-sans",
  },
  phone: {
    color: "black",
    fontSize: 16,
    left: "2%",
    marginTop: 10,
    fontFamily: "open-sans",
  },
  address: {
    color: "black",
    fontSize: 16,
    left: "2%",
    marginTop: 10,
    marginRight: 15,
    fontFamily: "open-sans",
  },
  addressRow: {
    color: "#606060",
    fontSize: 16,
    left: "2%",
    //marginTop: 10,
    marginRight: 15,
    fontFamily: "open-sans",
  },
  addressDefaultAlign: {
    flex: 1,
    flexDirection: "row",
    justifyContent: "flex-start",
    // alignItems: "center",
    // padding: 10,
    // marginLeft: 10,
    // marginRight: 10,
    margin: 10,
  },
  button: {
    //padding: 15,
    //width: "100%",
    //flex: 1,
    height: 20,
    marginTop: 10,
    width: 50,
    left: "12%",
    marginRight: 10,
    //flexDirection: "row",
    borderRadius: 8,
    // alignItems: "center",
    //margin: 10,

    // shadowColor: "#000",
    // shadowOffset: { width: 0, height: 5 },
    // shadowOpacity: 0.34,
    // shadowRadius: 6.27,
    // elevation: 5,
  },
  addressName: {
    color: "#606060",
    fontSize: 17,

    left: "2%",
    //  marginTop: 5,
    //width: 190,
    fontFamily: "open-sans-bold",
  },
  tel_fax: {
    color: "black",
    fontSize: 16,
    left: "2%",
    //marginTop: 10,
    marginRight: 15,
    fontFamily: "open-sans",
  },
});
