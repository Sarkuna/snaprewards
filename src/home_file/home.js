import "react-native-gesture-handler";

import React, { useState, useEffect } from "react";
import { Icon, Badge } from "react-native-elements";
import gS from "../globalStyle";
import {
  StyleSheet,
  Text,
  View,
  ImageBackground,
  ScrollView,
  Linking,
  ToastAndroid,
  Alert,
  FlatList,
  BackHandler,
  TouchableNativeFeedback,
  Platform,
  Image,
} from "react-native";
import {
  TouchableOpacity,
  TouchableWithoutFeedback,
} from "react-native-gesture-handler";
import { useRoute, useFocusEffect } from "@react-navigation/native";
import { useSelector } from "react-redux";
import axios from "axios";
import { useDispatch } from "react-redux";
import { CATEGORIES, HIGHLIGHTS, PROMOTIONS } from "../../data/dummy-data";
//import CategoryGridTile from "../../components/CategoryGridTile";
import * as productsActions from "../../store/actions/categories";
import * as productsActions2 from "../../store/actions/receipt";
import CategryGridTile from "../../components/UI/CategoryGridTile2";
import CategryGridTile3 from "../../components/UI/CategoryGridTile3";
import CategryGridTile4 from "../../components/UI/CategoryGridTile4";
import SafeAreaView from "react-native-safe-area-view";
import { LinearGradient } from "expo-linear-gradient";

const HomeScreen = (props) => {
  var commaNumber = require("comma-number");

  const renderGrideItem = (itemData) => {
    return (
      <CategryGridTile
        title={itemData.item.title}
        item={itemData.item.item}
        color={itemData.item.color}
        pts={itemData.item.pts}
        onSelect={() => {
          props.navigation.navigate({
            name: "DetailsScreen",
            params: {
              pageId: 1,
              pageName: "Redeem Gallery",
            },
          });
        }}
      />
    );
  };
  const renderGrideItem3 = (itemData) => {
    return (
      <CategryGridTile3
        title={itemData.item.title}
        item={itemData.item.item}
        color={itemData.item.color}
        pts={itemData.item.pts}
        onSelect={() => {
          //console.log(itemData.item.id);
          props.navigation.navigate({
            name: "Highlight",
            params: {
              pageId: itemData.item.id,
              imageUri: itemData.item.color,
              // related_id: itemData.item.related_id,
            },
          });
        }}
      />
    );
  };

  const renderGrideItem4 = (itemData) => {
    return (
      <CategryGridTile4
        title={itemData.item.title}
        item={itemData.item.item}
        color={itemData.item.color}
        pts={itemData.item.pts}
        onSelect={() => {
          //console.log(itemData.item.id);
          props.navigation.navigate({
            name: "Promotion",
            params: {
              pageId: itemData.item.id,
              imageUri: itemData.item.color,
              // related_id: itemData.item.related_id,
            },
          });
        }}
      />
    );
  };

  useFocusEffect(() => {
    const backAction = () => {
      Alert.alert("Are you sure you want to exit?", [
        {
          text: "No",
          onPress: () => null,
          style: "cancel",
        },
        { text: "Yes", onPress: () => BackHandler.exitApp() },
      ]);
      return true;

      //   const backAction = () => {
      //     props.navigation.goBack();
      //     return true;

      // }
    };

    const backHandler = BackHandler.addEventListener(
      "hardwareBackPress",
      backAction
    );

    return () => backHandler.remove();
  }, []);

  const myToken = useSelector((state) => state.auth.access_token);
  const Type = useSelector((state) => state.auth.Type);

  const myName = useSelector((state) => "Test"); //before state.auth.userinfo.

  const pendingValue = "";
  const processingValue = "";
  global.url + "/profile/total-available-point?access-token=" + myToken;

  const acc1Points = "http://biz.businessboosters.com.my/api/auth/dashboard";

  axios({
    url: acc1Points,
    method: "GET",
    headers: {
      Accept: "application/json",
      Authorization: `Bearer ${myToken}`,
    },
  })
    .then(function (response) {
      setPts(response.data.user_points_balance);
      setExp1(response.data.expiring_on);
      setNameUser(response.data.name);
    })
    .catch(function (error) {
      console.log(error.message);
    });

  const acc2Points = global.url + "/test " + myToken;

  const expiringacc1 =
    global.url + "/profile/point-expiring?access-token=" + myToken;
  const expiringacc2 =
    global.url + "/profile/point-expiring-account-two?access-token=" + myToken;

  const [PendingCount, setPendingCount] = useState(0);
  const [ProcessingCount, setProcessingCount] = useState(0);
  const [headerShown, setHeaderShown] = useState(false);

  const pendingList =
    "http://api.businessboosters.com.my/receipt/receipt-list-pending?access-token=" +
    myToken;
  const processingList =
    "http://api.businessboosters.com.my/receipt/receipt-list-processing?access-token=" +
    myToken;

  axios
    .get(pendingList)
    .then(function (response) {
      //console.log(response.data.totalpoint.length);
      setPendingCount(response.data.totalpoint.length);
    })
    .catch(function (error) {
      console.log(error);
    });

  axios
    .get(processingList)
    .then(function (response) {
      //console.log(response.data.totalpoint.length);
      setProcessingCount(response.data.totalpoint.length);
    })
    .catch(function (error) {
      console.log(error);
    });

  console.log(global.url);
  const [pts, setPts] = useState(""); //Points display MV Rewards
  const [pts2, setPts2] = useState("");
  const [points, setPoints] = useState(pts);
  const [exp1, setExp1] = useState("");
  const [exp2, setExp2] = useState("");
  const [nameUser, setNameUser] = useState("");
  const [expPoints, setExpPoints] = useState("");
  const [acc, setAcc] = useState("1");
  const [grd, setGrd] = useState("");
  const [onOffAcc2, setOnOffAcc2] = useState(30);
  const [isSignup, setIsSignup] = useState(false);
  const [error, setError] = useState();
  const [pending, setPending] = useState(pendingValue);
  const [processing, setProcessing] = useState(processingValue);

  const dispatch = useDispatch();
  // console.log(Categories);

  const directToRewardGallery =
    global.kansai + "/site/app-login?token=" + myToken;
  const directToOrdersSummary = global.kansai + "/order-history";
  const url1 = global.kansai + "/my-reward-points";
  const url2 = global.kansai + "/order-history";
  const url3 = global.kansai + "/my-transactions";
  const url4 = global.kansai + "/receipts-history";
  const url5 = global.kansai + "/flash-deal-history";
  const url6 = global.kansai + "/point-system";
  const url7 = global.kansai + "/product-info";
  const url8 = global.kansai + "/latest-promotion";
  console.log(global.kansai);

  axios
    .get(acc2Points)
    .then(function (response) {
      // console.log(response.data);
      setPts2(response.data.account2point + "");
    })
    .catch(function (error) {
      console.log(error);
    });

  // axios
  //   .get(expiringacc1)
  //   .then(function (response) {
  //     //console.log(response.data);
  //     setExp1(
  //       response.data.message + ": " + response.data.total_expiry_account_one
  //     );
  //   })
  //   .catch(function (error) {
  //     console.log(error);
  //   });

  axios
    .get(expiringacc2)
    .then(function (response) {
      //console.log(response.data);
      setExp2(
        response.data.message + ": " + response.data.total_expiry_account_two
      );
    })
    .catch(function (error) {
      console.log(error);
    });

  function switchAcc() {
    if (Type === 1) {
      if (acc === "1") {
        setPoints(pts);
        setExpPoints(exp1);
        setAcc("2");
        ToastAndroid.show("Account 2", ToastAndroid.SHORT);
      } else {
        setPoints(pts2);
        setExpPoints(exp2);
        setAcc("1");
        ToastAndroid.show("Account 1", ToastAndroid.SHORT);
      }
    }
  }

  console.log(points);
  function getGrd() {
    const date = new Date();
    const hour = date.getHours();
    return hour < 12 ? "Good Morning" : "Good Afternoon";
  }

  const Categories = useSelector((state) => state.myPage.availableCategories);

  React.useEffect(() => {
    const unsubscribe = props.navigation.addListener("focus", () => {
      // Alert.alert('Refreshed');

      dispatch(productsActions.fetchProducts(myToken));
      //dispatch(productsActions.pendingDetails(myToken));
    });
    return unsubscribe;
  }, [props.navigation, dispatch]);

  let points1 = commaNumber(points);
  let pts1 = commaNumber(pts);

  if (points === "") {
    points1 = pts2;
    points1 = commaNumber(points1);
  }
  let expPoints1 = expPoints;
  let exp3 = exp2;

  if (expPoints === "") {
    expPoints1 = exp2;
  }

  return (
    <SafeAreaView style={gS.container}>
      <ScrollView style={gS.scroll} showsVerticalScrollIndicator={false}>
        {Type == 5 ? (
          <ImageBackground
            source={require("C:/React_Native/snaprewards/assets/headerBackground.jpg")}
            style={styles.headerbackground1}
          />
        ) : (
          <ImageBackground
            source={require("C:/React_Native/snaprewards/assets/headerBackground.jpg")}
            style={styles.headerbackground2}
          />
        )}

        <View style={{ position: "absolute", right: 20, top: 50 }}>
          <Icon
            reverse
            name="user-circle-o"
            type="font-awesome"
            color="#f0b867"
            onPress={() =>
              props.navigation.navigate("SettingHome", {
                screen: "SettingHome",
              })
            }
          />
        </View>
        {/*
        <View style={{ position: "absolute", right: 20, top: 55 }}>
          {Type === 1 ? (
            <Icon
              name="random"
              type="font-awesome"
              color="#16B6E0"
              size={onOffAcc2}
              onPress={() => switchAcc()}
            />
          ) : (
            <Icon />
          )}
          {Type === 1 ? (
            <Text
              style={{
                textAlign: "center",
                color: "white",
                fontSize: onOffAcc2 - 15,
              }}
            >
              Acc {acc}
            </Text>
          ) : (
            <Text />
          )}
        </View>*/}
        <View style={{ alignItems: "center" }}>
          {/* <Text style={{ fontSize: 10, color: "white", marginTop: 70 }}>
            {getGrd()}
            {grd}, {"\n"}
            {nameUser}.
          </Text> */}

          {
            //points MV Rewards display here
            <View>
              <Text style={{ fontSize: 22, color: "white", marginTop: 60 }}>
                {getGrd()}
                {grd}, {"\n"}
                {nameUser}
                {"\n"}
                {pts1}
                {" pts"}
              </Text>
              <Text style={{ fontSize: 12, color: "white", marginBottom: 10 }}>
                {" Expiring on : "}
                {exp1}
              </Text>
            </View>
          }
        </View>

        {Type == 5 ? (
          <View style={gS.section}>
            <View style={styles.barCard}>
              <View style={styles.barIcon}>
                <Icon
                  reverse
                  name="qrcode"
                  type="font-awesome"
                  color="#24F3D2"
                  onPress={() => props.navigation.navigate("Scan")}
                />

                <Text>Upload</Text>
              </View>

              <View style={styles.barIcon}>
                <Icon
                  reverse
                  name="clock-o"
                  type="font-awesome"
                  color="#23EED3"
                  onPress={() => props.navigation.navigate("Pending")}
                />
                <Badge
                  status="warning"
                  value={PendingCount}
                  containerStyle={{
                    position: "absolute",
                    top: 5,
                    right: "20%",
                  }}
                />
                <Text>Pending</Text>
              </View>

              <View style={styles.barIcon}>
                <Icon
                  reverse
                  name="cogs"
                  type="font-awesome"
                  color="#1DD7D8"
                  onPress={() => props.navigation.navigate("Processing")}
                />
                <Badge
                  status="primary"
                  value={ProcessingCount}
                  containerStyle={{
                    position: "absolute",
                    top: 5,
                    right: "20%",
                  }}
                />
                <Text>Processing</Text>
              </View>
              <View style={styles.barIcon}>
                <Icon
                  reverse
                  name="check"
                  type="font-awesome"
                  color="#1DD7D8"
                  onPress={() => props.navigation.navigate("Approved")}
                />
                <Text>Approved</Text>
              </View>
            </View>
          </View>
        ) : (
          <View style={gS.section}>
            <View style={styles.barCard}>
              <View style={styles.barIcon}>
                <Icon
                  reverse
                  name="upload"
                  type="font-awesome"
                  color="#24F3D2"
                  onPress={() => props.navigation.navigate("Scan")}
                />

                <Text>Upload Receipt</Text>
              </View>

              <View style={styles.barIcon}>
                <Icon
                  reverse
                  name="clock-o"
                  type="font-awesome"
                  color="#23EED3"
                  onPress={() => props.navigation.navigate("Pending")}
                />
                <Badge
                  status="warning"
                  value={PendingCount}
                  containerStyle={{
                    position: "absolute",
                    top: 5,
                    right: "20%",
                  }}
                />
                <Text>Pending</Text>
              </View>

              <View style={styles.barIcon}>
                <Icon
                  reverse
                  name="cogs"
                  type="font-awesome"
                  color="#1DD7D8"
                  onPress={() => props.navigation.navigate("Processing")}
                />
                <Badge
                  status="primary"
                  value={ProcessingCount}
                  containerStyle={{
                    position: "absolute",
                    top: 5,
                    right: "20%",
                  }}
                />
                <Text>Processing</Text>
              </View>
              <View style={styles.barIcon}>
                <Icon
                  reverse
                  name="check"
                  type="font-awesome"
                  color="#1DD7D8"
                  onPress={() => props.navigation.navigate("Approved")}
                />
                <Text>Approved</Text>
              </View>
            </View>
          </View>
        )}

        <View style={styles.containerMain}>
          <View style={styles.containerHeader}>
            <Text style={styles.title}>Features</Text>
          </View>
          <View style={styles.container1}>
            <View style={styles.imageContainer}>
              <View style={styles.container2}>
                <TouchableOpacity
                  onPress={() => {
                    props.navigation.navigate({
                      name: "DetailsScreen",
                      params: {
                        pageId: 1,
                        pageName: "Member List",
                      },
                    });
                  }}
                >
                  <Icon
                    reverse
                    name="users"
                    type="font-awesome"
                    color="#7367F0"
                  />
                  <Text style={styles.name1}>Member List</Text>
                </TouchableOpacity>
              </View>
            </View>

            <View style={styles.imageContainer}>
              <View style={styles.container2}>
                <TouchableOpacity
                  onPress={() => {
                    props.navigation.navigate({
                      name: "DetailsScreen",
                      params: {
                        pageId: 1,
                        pageName: "Redeem Gallery",
                      },
                    });
                  }}
                >
                  <Icon
                    reverse
                    name="gift"
                    type="font-awesome"
                    color="#7367F0"
                  />
                  <Text style={styles.name1}>Reward Gallery</Text>
                </TouchableOpacity>
              </View>
            </View>
            <View style={styles.imageContainer}>
              <View style={styles.container2}>
                <TouchableOpacity
                  onPress={() => {
                    props.navigation.navigate({
                      name: "DetailsScreen",
                      params: {
                        pageId: 1,
                        pageName: "Events",
                      },
                    });
                  }}
                >
                  <Icon
                    reverse
                    name="calendar"
                    type="font-awesome"
                    color="#7367F0"
                  />
                  <Text style={styles.name1}>Events</Text>
                </TouchableOpacity>
              </View>
            </View>
          </View>

          <View style={styles.container1}>
            <View style={styles.imageContainer}>
              <View style={styles.container2}>
                <TouchableOpacity
                  onPress={() => {
                    props.navigation.navigate({
                      name: "DetailsScreen",
                      params: {
                        pageId: 4,
                        pageName: "Refer A Friends",
                      },
                    });
                  }}
                >
                  <Icon
                    reverse
                    name="thumbs-up"
                    type="font-awesome"
                    color="#7367F0"
                  />
                  <Text style={styles.name1}>Refer A Friends</Text>
                </TouchableOpacity>
              </View>
            </View>
            <View style={styles.imageContainer}>
              <View style={styles.container2}>
                <TouchableOpacity
                  onPress={() => {
                    props.navigation.navigate({
                      name: "DetailsScreen",
                      params: {
                        pageId: 1,
                        pageName: "Latest Promotion",
                      },
                    });
                  }}
                >
                  <Icon
                    reverse
                    name="percent"
                    type="font-awesome"
                    color="#7367F0"
                  />
                  <Text style={styles.name1}>Latest Promotion</Text>
                </TouchableOpacity>
              </View>
            </View>
            <View style={styles.imageContainer}>
              <View style={styles.container2}>
                <TouchableOpacity
                  onPress={() => {
                    props.navigation.navigate({
                      name: "DetailsScreen",
                      params: {
                        pageId: 4,
                        pageName: "Referral History",
                      },
                    });
                  }}
                >
                  <Icon
                    reverse
                    name="history"
                    type="font-awesome"
                    color="#7367F0"
                  />

                  <Text style={styles.name1}>Referral History</Text>
                </TouchableOpacity>
              </View>
            </View>
          </View>
          <View style={styles.container1}>
            <View style={styles.imageContainer}>
              <View style={styles.container2}>
                <TouchableOpacity
                  onPress={() => {
                    props.navigation.navigate({
                      name: "DetailsScreen",
                      params: {
                        pageId: 4,
                        pageName: "Points History",
                      },
                    });
                  }}
                >
                  <Icon
                    reverse
                    name="list"
                    type="font-awesome"
                    color="#7367F0"
                  />
                  <Text style={styles.name1}>Points History</Text>
                </TouchableOpacity>
              </View>
            </View>
            <View style={styles.imageContainer}>
              <View style={styles.container2}>
                <TouchableOpacity
                  onPress={() => {
                    props.navigation.navigate({
                      name: "DetailsScreen",
                      params: {
                        pageId: 1,
                        pageName: "Customer Profile",
                      },
                    });
                  }}
                >
                  <Icon
                    reverse
                    name="user"
                    type="font-awesome"
                    color="#7367F0"
                  />
                  <Text style={styles.name1}>Customer Profile</Text>
                </TouchableOpacity>
              </View>
            </View>
            <View style={styles.imageContainer}>
              <View style={styles.container2}>
                <TouchableOpacity
                  onPress={() => {
                    props.navigation.navigate({
                      name: "DetailsScreen",
                      params: {
                        pageId: 4,
                        pageName: "Latest Info",
                      },
                    });
                  }}
                >
                  <Icon
                    reverse
                    name="info"
                    type="font-awesome"
                    color="#7367F0"
                  />

                  <Text style={styles.name1}>Latest Info</Text>
                </TouchableOpacity>
              </View>
            </View>
          </View>
        </View>

        <View style={gS.section2}></View>
        {/*<View>
          <Text
            style={{
              fontSize: 20,
              fontFamily: "open-sans",
              marginLeft: 15,
              marginTop: 8,
            }}
          >
            Highlights
          </Text>
        </View>*
        <FlatList
          horizontal={true}
          showsHorizontalScrollIndicator={false}
          keyExtractor={(item, index) => item.id.toString()}
          data={HIGHLIGHTS}
          renderItem={renderGrideItem3}
          sliderWidth={200}
          itemWidth={200}
          // numColumns={2}
        />*/}
        <View>
          <Text
            style={{
              fontSize: 20,
              fontFamily: "open-sans",
              marginLeft: 15,
              marginTop: 8,
            }}
          >
            Promotions
          </Text>
        </View>
        <FlatList
          horizontal={true}
          showsHorizontalScrollIndicator={false}
          keyExtractor={(item, index) => item.id.toString()}
          data={PROMOTIONS}
          renderItem={renderGrideItem4}
          sliderWidth={200}
          itemWidth={200}
          // numColumns={2}
        />

        {/* CODE FOR CATEGORIES
        <View style={styles.containerMain}>
          <View style={styles.containerHeader}>
            <Text style={styles.title}>Categories</Text>
            <Text style={styles.title2}>More</Text>
          </View>
          <View style={styles.container1}>
            <View style={styles.imageContainer}>
              <View style={styles.container2}>
                <TouchableOpacity
                  onPress={() => {
                    props.navigation.navigate({
                      name: "DetailsScreen",
                      params: {
                        pageId: 1,
                        pageName: "Redeem Gallery",
                      },
                    });
                  }}
                >
                  <Image
                    style={styles.image}
                    source={{
                      uri: "http://madmin.businessboosters.com.my/upload/product_cover/thumbnail/MCH0029.png",
                    }}
                  />
                  <Text style={styles.name1}>Mobiles & Tablets</Text>
                  <Text style={styles.nameItems}>3 Items</Text>
                </TouchableOpacity>
              </View>
            </View>

            <View style={styles.imageContainer}>
              <View style={styles.container2}>
                <TouchableOpacity
                  onPress={() => {
                    props.navigation.navigate({
                      name: "DetailsScreen",
                      params: {
                        pageId: 1,
                        pageName: "Redeem Gallery",
                      },
                    });
                  }}
                >
                  <Image
                    style={styles.image}
                    source={{
                      uri: "http://madmin.businessboosters.com.my/upload/product_cover/thumbnail/MCH0124.jpg",
                    }}
                  />
                  <Text style={styles.name1}>Accesories</Text>
                  <Text style={styles.nameItems}>5 Items</Text>
                </TouchableOpacity>
              </View>
            </View>
            <View style={styles.imageContainer}>
              <View style={styles.container2}>
                <TouchableOpacity
                  onPress={() => {
                    props.navigation.navigate({
                      name: "DetailsScreen",
                      params: {
                        pageId: 1,
                        pageName: "Redeem Gallery",
                      },
                    });
                  }}
                >
                  <Image
                    style={styles.image}
                    source={{
                      uri: "data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAoHCBUWFRUWFhUVGRgYGhkeGBkZHBkaHRwWGhwaGhwZHhgdJC4lHB4sHxweJjgmLDExNTU1GiQ7QDs0Py40NTEBDAwMEA8QHhISHzQrIyw0NDExNjU0NDU1NDQxNzQ0NDY0NDE0OD00ND80NDQ0MTc0NDQ0NDQ9NDQ0NDQ0NDQ4NP/AABEIAKsBJwMBIgACEQEDEQH/xAAcAAEAAgMBAQEAAAAAAAAAAAAABQYDBAcBAgj/xABJEAACAQIDBAYHBAcGAwkAAAABAgADEQQSIQUGMUETIlFhcZEHMnKBobHBFEJS0SMzYoKSssIWQ1PS4fAVc/EkJTREY4OEovL/xAAZAQEBAQEBAQAAAAAAAAAAAAAAAQIDBAX/xAApEQEAAgEEAQIEBwAAAAAAAAAAAQIRAwQSITETQVFxgZEFIjJhobHw/9oADAMBAAIRAxEAPwDs0REBERAREQEREBERAREQERI53cs1nKgGwACnhx4iBIxIpGqG/wClOn7KD6GbeCqEqc2pBIJ7eY+BgbUREBERAREQEREBERAREQEREBERAREQEREBERAREQEREBERAREQESuV9tVTUZEFNQrlbsGe9ja9gy28NZEYve2ujlQaJA5mm4PvHSwL1ErdXbdVFViqOCAbAMnxu8sFJ8yqeFwDbxF4GSR/3m9o/EAyQkefWb2v6RAx0PvTawQ6p72b52+k16Q9bxmzgvV/eb+YwNmIiAiIgIiICIiAiRWP3gwlB8lbE0Kb2Byu6KbHgbMeGh8pqNvlgNLYzCn/AN6n+cCwRIP+1eBtf7ZhLWv+up8O3jMR302eOONw3uqKfrAsMSvUt89ns6omLos7sqqqtclmNlGnaTLDAREQEREBERAREQEREBERAREQERPkmBTCf+0VP+a3zlc3g2aXxFdsmIAfKCVemo6gABQZ9L9/wmDa+93Q4ysgo5itVtS+Xn2ZTK3tXetemqn7Mo69QsoqN1ivWZr5dCTc6TVZrn8zepo6taxbGMuobaNkXS3VGmmnlpLdRFlXwHynIsRvqtRkpfZygyqAQ+YAAdhUeE66h0HgJjMT4W+jfTxyjGWWR59Zva/pE3c00r9ZvaPylc3lP73jNjBer72/mM16Z9bxmbBnq27Gb+YmBtxPjNGaB9xPjNPGqAakgeMDJE1zik/EJ59sT8Q+MDZia32xPxD4z37Wn4hAoXpS3eqYoURTTMQxJYZAwABGXMxBy9a9geIvOYY/cTEUVzulaxNuqgqG9ifVQk2048J+gsW6sVIIOh4Htt+U18XRylGBPWIVhfQg8DbkbniPfytUcVXY69EV6DEZzQRcww7+sHdi17esVKjt0ExYT0f16ihlSqFN7ZhTptobaq5DDhzE70PUmhhKd1qOxJJZgBc2ARiosOA7Sed/CwyoOxN1GpYSsjpatTzNRYFS6uFZ0YFSbNdtCNbWnT9lVxUoUnVgwZEOa973Ua3mjhcMT0rBhq4sCPwogtfjxBkZunZMRi6SiyWpVQvJXqmrmsOV8gJA53PMxJC3RESKREQEREBERAREQEREBERA+WMwu8ytNSq0Dgm9P/j8R/zm+kqG1nPTVgCQC73F9ONp0LeLYWJbGVnWi5DVHYWyklSFsbA3kJj90azVWYYeqQzHUA2LHU/GYicTL62vX1dOsRMfd9UVviaY7h8p+gOlnFMFsLENigRSayEZrlAQPAm86/nlr7uW+mJ4xEtzpZrI/EnmzfOwkftrEMlCqykhghII4gzjtPatd1qs1aqSOFnccWF9AQOZibYefR2ttWs2iY+DufSWuOZOg+vhMmHqWB9o/OcGwGPrFXIqVb9UAK9QXJNgLK2pnQNyquMUuuKWqqhVyBxqXJOYjmdLRExK6m0nTiZm0L70sdLI37V2K3wE9+09qn4TTy4SPSzn/pG3ybB18OigMpQu44HVrLryHVbkZcFxKnS+vYdJwz0u4rPj3H+GlNPJRU+dQwOqneamFw2eiytiFuoV8xFk6RgbqOWnjK+npPwpUMMPirE2BzUfW7OM1tvkLjtk0+zpFt7SJTHynMMG1so6oFxxHPJe59/O3v5Sjr9f0jYZFLNh8SLGxF6N/H1tf9Z94H0h4apUWkKGJVnV2UsadrKrPrlJPBZzHG1lYE3Qi1uXI68OJv8ASfW7BH2hXzAhMPWYm1rH7PUuuvGxI7uwSzGGYnLtuH2wj06bohAqIHUlrmxUMARl42PwMlDWz0KTftJ8GAPylD3Sq58FgW7mQ/urVT5qJcdmvfDqPw1AP/sD9ZFTQPUmnhj+jbvd/i5m39yaVD9WO9z8zA2cO9qbn2z5FvykXu2g+145u7DJ/Cjt/XN/N+jPeD5t/wBZH7oXL49z9/FWX2Uo0lHxvAtMTwT2RSIiAiIgIiICIiAiIgImHEHqmRNaqiKzu+VVF2ZjYAdpJgb1fHKrFcrEi3BSRqAeXjI47UQtca24DhqNDe/O+kq+K9KWBpdUdLUIJ6wU29zNrK2PSFhS7PkqqGZjbLf1jmIv4k+cuJFxxeBpszVb1VdySbNoL9nV04SNxFGggGevUp2N7sQo82W0xH0j4F6LrnqI2U2D02sTy1W48yJT9o4ykxL03LggN1/WAPeNOPLlM8Yb9S0e62U8dgkrmsazvdRcDNbxJVbGSlXfXDMxIL31uAt727uN5yPE7Tpvo4ce6/1tIx6GGuSr1R4C30muOGbWtbzLsW1N7cNUo1EQvmYWsVNgdL3M5kcMzI1MMgZ3UAs2VQCy6s5sABxM0aeLoply5rgC5AF78z1iNe+15LYLFK5BA6x5asbdvZfzkmsZy6017UpNIxiXS9y9yFwiB84qVGF89zax4ZBqFFvvcTc8OEtiYdhwVD4k/KQe4WPR6T0lFhSK6XuOuCSb8iSDcdtzzlkeqo7R8ZHLOWvicQyJmYIF0GgJ49wmHA40PmCBTYAm4bgb/i8JnrPTa2Zjp7XyGhmPD9Guoc352z2Pul6wneX3VoFuKL7j9DK7vDuXh8ULtSAqLYrUS2fTk34xysb9xEtK1VPM+Uyqw5D3yK4tvzVZNq4BmBCDoiDY2INVs1j7NvCVTF0snSKMl0xTJqxDaMRlIB0Tvl89MeJIpYJzq6VqhB7gENr+IHlKftHZuFxdV6tDFKjVGLmnUspDt1mAuRzJ4XlRG4q4OL0pDKVy5XJC8v0djZgb9/DTtkjs/qnEEhRlwD+ozMM1QIlzc6Mc+o01Hdab+P2FmpZOhw1K2W9YVAWOUa6XAN/fIjEfZsNh69NK/TVq4RSVHVVFcOddRrYDifCUXbcOv/3fQ/YrovuauoPwcy84OrkpVBZjaohsoubEjW3Phy1nMvR5Wvgq6ZrEVcynjY2pkG3it5Pf8XrHMq1lGdgNVXOzKb2VV4+AEQkr1/x+lcoSb2uDZtedstrg90JirYfPYjKrtY2+7mI4TnPTurg/ajcqG6TK3Vct6hObR9baczae4veDEKhQV0ZFuHWy5sp4qQ2ovflrNcZTK5pUxFYvTw4pqlEqj1a5eoTUyKxCItrgBxdiw1uLaSxbC2aMPSyBy7Fnd3NgXqO2ZmsNAL6AcgAJEboEGiz83qO7G97u1iT/AKctBLPTmWmYT2eCeyBERAREQEREBERAREQNXH1AqEk24ed5DYXHpVYot72vw4ppY69t5DelylWbBp0C1WdaysTSDllXI6knJra7CQvohbEOMTUxDOTmREDizCwJbiAfvL5QLs2xKBWzUkPiqmYqm7WGYW6FAP2Rb4DSTVUGxsQDbS/C/K/dCDhe3fNZRwPe3d8UMUaS6I7oKZNzbOBlzd18wuL+qZAY7BVcLUKOClxc5esroeDAcG7r8xynZt8NmU6mJw+fQMj5Wva1allqU/EWLi3fKP6SMKb4Z+TLl/ga/wDX8JcqgtobkYhAHz4dlaxBGddDqD6sjRutWJ/uR73/AMs6M4qfZsMGVf1VIXJN75Ba4tNKhTY3uoHgb/QSCsbP3IquwBq0k7wpb52k9tPcxcLhKtc1XqugWyWCIbsqkkXYmwJPEcJYtlpZhJnbGBNfDVqQ4uhA9riPiJEUL0e7cqpiKaCsnR1yb00W5uiM2rOLiw427ROqVcanNgPHT48JzT0a7KQKKrr+lQuo1PVDesCPxaW7tZc8SuZwvaQPPSZluI6bz4lDwdP4hPkYlBxZB+8JX6tSnxLuo70X6PMaVqZ1DuQOxF/zyZOK3UcYl7Z1v3a/KMftEopCjW3E8B7ufvlF3j222DyZFV2LMBmuBZedhz1Gl5B1N+MU/wDdJbtX/W8sZk6hm9LjkU9npbOWWoxzZiSxFMA6EXPGUPZGHVqyJUoHKzKuhdbZmUE3N+AvLm29eJYgmkDYWW5Y2HYOwT5/tPU50Kf+/dLhGntXd3BUadR1atUZAvVZejHWZVvmDNwzXtaUbpB+BfNv806P/ahudCl/v3T3+04PHD0fh+UqJf0dbuU8RgqZfOmarUcFDa9sqC+YNe2U27LntkntTdTBoBarULq5IHSICGPrZiEJXS/LjKwm9NtBRpjwNvkIO30NycNSJPE6E/KTtU6mDpCmU6eqFt6nTqFKtfMulO/E+8Zuye7K3dwD51eoyM7CwNZCXvYCxZQS17iw7pBLtiieOGpeS/lMo2nRuCMOoIIIKhLgjgQStwfCXlKcYdL2LshMK5RGqEPa+ds2qg2twA0+Q7JZKc4/T3kF9VrHv6Q/SS2D3pbpKSKzC7oCGcsbZgDxPDX4yTK4dQE9iIQiIgIiICIiAiIgIiIHkjcYt3vc6KOHiTwklKrtfba0cQ6sCAUU5rG2vfw5S1iZnpJmI8pKsWynW/dp5a6TNh0IHH5StY3eeiF/WKM3MMLjvHHWZsFvTRZSQ6G3fr5Tp6dsZwnKM4yxb1JZ8G+Y3XEoOXB0qUz/ADiUHelzisZhsKhv0YsxHJ3sW/hUKfOS2/e82amOjDB0dGRiptnRgw46HhwkPuzQFHB4jFrUV8SQy6MGamzMAxbsc3J1+pmbRMeViYmFl23Tw6AIXRTpZS5BsOFgWvI/DYVV1F9e1mb5kzmO02RmLMGZixzMWN+NtLjXt49nC9hMbm7VelWSiWLUahAW/Jm0W3Zc6EdpkHTcElmEsFDhIXDrYySfFpTXM7oi9rkKPjINdAA7WAF2J955z5c/pE9pfmJEbN28lfGVKSAlVQOrHQsQwVrA626y9/HSSGJqZXBPIg+RvMy3XwgNoU3Wo6i+jNpcjgTa3dMWHRy6rc6sNLtfjw8Jc8Tilc9ViR+yUv7wwM8w+IVD1msO1sg8goEmDkoHpHW9ZV/C1T+cD6Ru7tVEwyIVUlQBci59UEeHGeb5sHr5hwIYj3uxlZ2axyHuy/yJOO5py04+b2bHj6s8oz0s2J2ulWkabO9Nr6smaxsTpdSDY9kz7Kq0Qiqa1V2F7sa1dL6m2nAWGnulVUEmbeCNpz9a1Yemdpp2t7wtwqUe2of/AJNT8pixmRkdUZ0ciyv07tlPbYjWQ6MTMzZhwDeRnOd3b4Q1X8O0595/hrLg8SHUnG2AOq3Zha1uDE9ss9sFU/X0kqlfU6zrlBtf1eN7D+GV00n/AAmY2Rx91vIzF9e94xExHydq/h+lWc9z/v2e724bBBaRw9EI+c3Id2uuVjwOnEDX85H4Zg74eiqoHZbDM2VdMxzMwF7sRYAd2usw7SvdL39Y/wArzZ2VUyVg5QVA1N6ZQkD10ZFsx0WxYe4T27bPp9zl8ze0rTU41+DUp1FdGYLlZSCVLAjKfVKk2J1HDiO/jJXZ7ZcQrd6NwAuLKePE28te28isNTKI5LXZxlNmsQq2Oo5g38OrNtW69/8A0kPDjdKdtb68D2fOdpeR+i4nk9hCIiAiIgIiICIiAiIgJVNp4jJjGurkdGhuq57auNVXrcuQMtc1cVgqdS2dFYjgSNR4MNR7oFW2vi0dFyM6MGBJKOhtY6HPTOl7cuUy7Ixyoln6RnudVp1H05DMlNRb3e8yabYtLtrDwrVv88xf8HpjnVPtVapHkXtN8+sJjvLn28mEfE4mmio66lgXABOVWI6t8wF7XJAteclxWFr4auwqoyNdhrexzA8DwYaifp2lgUS+RFW/GwAJ8TzmPF7Mp1RlqIjg8mAPzmcq/MOIqAtdWXiSQe0/7/6wmKKFWVtUIZSOTA5gfOd7xXo32e//AJcL7BZfkZqD0V4D8FTwzv8AnGUw4vW3pxrXBxNUeycvxW00sPVqVKqkmpUa/PM7fUzv+G9Guz0/uA3tFm+Zk7gNhYeiLU6KJ7KgSKom5m7FQVPtFVSnUKKh9Y5ipLEcvVFpb8Rs9zwZWHY6g+RGsm+ijooFVfZDf4SHwdh8LQmx2/wl97t+UtXRR0ULmXJ988KUqoCFH6MGy+0/mZSsFVy28E/kX8p0D0juBi6aczRU+7PUEh9lbFQpRe7XCq2lhxS1r9moPumNaJmuIh32t60vytOEIuRj1qi+A0Hmfylm2ImCCqzYhFcOLqxAFgeBzDUHmdeOmtyMKbuUy5Z9VLX00IGpPcxv2yXo7t7P5msD43/pngvFq+Ymfo+hbcVnxMfTy2n2hhBmXpMM6HVmVwjg/hQKtiPa/wBZq0tq4ei91Kulh/eIWzHkeqbgKbcRqDy1n0+7GzeJq1V8f/zPU3U2cRda789bjkbW9WcuXX6Z+zFb6WZzOc+Whi9p0yTkZBmOoKUzlBvcqwOtu8cQJj6VALtUQnQBVK8LXzE248b98kH3SwP+O3kT8ppYjdLBkWFZj+4/1YSRXl1ifs7V3NK9xb+0Jt3EKwpqpBIYk2INhkYakDTjNWvhrKpDrZuJuAFN/VJOn+zJnbWy1SiMjMQrL1SoQAHqXFmOY9bn5zX2bgsO5D1cQtIrayvSdwy/es6nqXbibXsFn0trE0pjGO/d8/e6ldTU5VnMYRLoQuuunHt79NJlyDNfh+gw2v7jX+QnlWkEaqqsjICpUoGCFjfMQrgFCRa4tyU87zapUGcIqi7NTpKB2nLb6zvZ5YfoKnwHgJ9zHR9VfAfKZJEIiICIiAiIgIiICIiAiIgeGfJWfcQMRSeZJmiBhyRkmaIGHJGSZogYckZJmiBhyRkmaIHMPSjuZisXVoV8MVzIhRlJKn1iykaEHib+AnL6G0sZSrthXd8yMUKqV0K8hmGvCfp+Q67v4ZaxxC4eiKxJJqZRnzEWJzciRzHbA5Nh9j7RdQwGJAP4hQB8mW8+jsfaQ54j+HDn+mduAnsuRw8bO2iPvYj+CmD5qomKl9tNwOmYjjmoknzK3ndItGfYcPLY4fcf34d/pPk4vGDig99GqP6p3HIOwTzo1/CPIRkcHxuLxDoUdUAJW9kcHqsG5sRy7JoPn6ubUJ6twdBppw4aT9DdCv4R5CedAn4V8hGR+cqjm1rrbXS9gO3QC06Z6O9g3CYh14IoQEc7WLW8Pn3ySO4dM4s4lqmZS7N0WQAXPItfhfulwRABYAAdgie1ZIiJEIiICIiAiIgIiICIiAiIgIiICIiAiIgIiICIiAiIgIiICIiAiIgIiICIiAiIgIiICIiAiIgIiICIiAiIgIiICIiAiIgIiICIiAiIgIiICIiAiIgIiICIiAiIgIiICIiAiIgIiICIiB//2Q==",
                    }}
                  />
                  <Text style={styles.name1}>Home Appliences</Text>
                  <Text style={styles.nameItems}>7 Items</Text>
                </TouchableOpacity>
              </View>
            </View>
          </View>

          <View style={styles.container1}>
            <View style={styles.imageContainer}>
              <View style={styles.container2}>
                <TouchableOpacity
                  onPress={() => {
                    props.navigation.navigate({
                      name: "DetailsScreen",
                      params: {
                        pageId: 1,
                        pageName: "Redeem Gallery",
                      },
                    });
                  }}
                >
                  <Image
                    style={styles.image}
                    source={{
                      uri: "data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAoHCBYWFRgWFhYYGBgaHBgYHBoaHBwaGhgcHRocHCEaHBocIS4lHB8rIRkYJjgmKy8xNTU1GiQ7QDszPy40NTEBDAwMEA8QGhESGjUhGCM0NDQ0MTExMTQxNDExMTQ0NDQ/MTQ0MTExMTExNDQ0MTQ0MTc0NDQ0NDQ/MTE0PzExMf/AABEIANwA5QMBIgACEQEDEQH/xAAcAAEAAgMBAQEAAAAAAAAAAAAABQYDBAcIAgH/xABIEAACAQIDBAUJAwoEBQUAAAABAgADEQQSIQUxQVEGImFxgQcTMpGhscHR8EJSchQVNGJzgpKy0uFDU8LxIyQzY7NUg6Kjw//EABcBAQEBAQAAAAAAAAAAAAAAAAABAgP/xAAcEQEBAQACAwEAAAAAAAAAAAAAARECIRIxUUH/2gAMAwEAAhEDEQA/AOzREQEREBERAREQEREBERAREiNq9I8LhjavXRGtfKTdrc8ouYEvEplTylYAei9R/wANNz8Jgbyn4MfYxJ/9o/ExgvUShjyp4H7S4he+n8jNzCeUnZrkL+UZCfvo6L4uVyjxMC4RMNGsrqGVgykXDKQQRzBGhEzQEREBERAREQEREBERAREQEREBERAREQEREBERAqXlA6QNhMMShtUe6qdCVA9JwDvIuAO1gTcCce2DSLO1RyS7Em56zXPEs1yT2y4eUPGriMd+TEdXDpYm+96gVzpyC5B4nslW2N1fODirZRbx+UsG5WrI5IJY2YqTnIFwATuIHG016hpffH8Z/qmiiBkKlgpLsdTbS48RuMl9jV8NSQrVRnYNmBFQAA2tc34201vKIqvTQg2Obucn4yDxaI6h1uL7r799vhJuqi5nZbDNwz3t3C9h4SN2fhgaD5h1lzW36G7QLL5L+kNTD1hTLE0HYB1J0QscoqJyIYjMOIJO8Tvs86YDDJ5jziAK9NTmt9pSut+3W9+yeihJR+xESBERAREQEREBERAREQEREBMNWui+kyr3kD3yO6T45qGFrVVYKyIxDEXCn71uNt9uyctwuz6tXLVOJRyTmzjru246te3+0DsVLEI/osrfhYH3TPOIY3bdWnmuURFvZgwDX0sxJ3jUbufZLHsDp89TDXCKWXMod31YBiAcgXrG1uIuZcHTJr4rEpTUu7KijezEKB4mcrxG38Sxua7j8Jyj1LaaK7SevVKV65BAAQu5VQrDrabma43NvFtdLRg6U3S7CdW1XMHNlKq7Bj2EC3+x5SQo7XoNe1RdN9zlt65x7G4GtQyulV3ZmutrLTLbrhFIO7w75HY3pTXQOtZkUCwNNOo9S4vcsA19N+4dbfpGJXx0v2qE2ni3TK12QAg9X/pIN436gyP2VjURHzXuzA6AngZC1qV2Rib50DkDmFt6tL+Bkvg8KWyIouzEKBxJY2A9ZhWnXD5jlcEdqEH1CYwtT74/hMtFboxiFZUKAuzBAquhOazGxAOmisb7tN8+U6NVnKhAj52ZFKVEZS6jMUzZrBra2O8XIvKKyBU++P4TNrD1MtN1YlmYG1lNt3zMnKXRnEMypkUMzMiqzorFktmXKzA3Fxp2iaeM2Y9LJnUKXXMozKWy8yoN18QLwNjomwNalTYXWoyIR+IgfGehp5noYs0XSoti1N1cA7iVIYA9mlp1DCeU4uob8kte3+MP6BIOkROfHykcPyb/AO0f0T9Tykp9rDkD9Wore8CMHQIlSwPT/BubMz0z+uun8SkgeNpaabhgGBBBAII1BB1BB4iQZIiICIiAiIgIiICR+1ccKKBiAbsii5Ci7GwuT9d2+SE18VhkqIUdQynQg7jAonS7HNVp5HTqrUKt6OUkJusM19GOubha15zSv0ap3LI70r7wp6vztL/5RcH5jzPmma7tUOVm6gyqm4W09vHnKDUx9ddCmYdwPuPwmh9r0KAHnKjO6gXNyBpzPG0lKLIgCKAoG5Ru3X+cq7VXJVSK7KCLUy75Li1hu3C3E6SWqYh1Gd0QG1rB2ZbHgdB7LwJ4YVymcL1bFr6eiNb91pUekFTLWW+ilRY9oOo9gmlXxzsSp84UO9FrMU7rZbga7pl2ntEVFAenbWwPW395WBq4moCnVvvO7lfs+vbIytUXIB9q5J0+Pr9ffJPZOxWxDmnTYg5XezMFBCi5APE9k0BhVB1zdxgbWGqGoaS7sq5fC5HuEsOHfIysADlIaxFwSDexHEabpWauKyEFLoQLaHWfB2tW/wA1/XIOiv0rxPUsVGV849N9crLazuwVSGYZVsNZgo7fdMqpTpIisz5AHKl2QoWN3LE5SQBewvulAO1q3+Y/rmVcdiTqHqHtAY/CLZFktXbEbarP5rOQ/mSShN829TZmBBa2RRe97DfNbaeOavUeqwUO5u2W4F924k8pTm2nXGhqOO8/OfP50rf5j+uJUxPY5OqT9fWpkhsorkF3ZNF9EX4d4lTXadQkB3Zl4gnh/bfJFdqlLLkvYDW+/TfulE83Pf3zEUHIeqRI26fuf/L+0HbRP2B4mNEvO1dFttUKlCjTSqpqLTpqyXs4IRQeqbE940nAKWPdjbQd3950vYOxGxNO6UUylaIWs7G6NTKhwgVgwIKvrbfbWSjq0T8An7IEREBERAREQEREDmPldq2qYTsFc/8AiHuv65TqaZiBmAvxY2G6++W7yxDr4Q9lf30pRUfS3q7uX1zlgzdIMU6otLziuLC2SxsFuMtwASbW8DIOjilOZXcFR22HeL6zc2mhK3X0l153HKV9sUgVhkIa47bW5E7r685RmOLVaqlCbXA14jW/vE/duYnNVVBuW1/xH+1vWZoUh1s5FlBvY67twvxmEOWfMd5a58TAkaNdk1Rip3XBI90+C0EzBVfgN8lWTXw6XJtcmfJpgekbnkPrSbuGRmARdBxbn3fP4aSzbH2FSFmZGe3O4Xdfsvu7ZMt9rbJ1FZwyt9lB4i/xt7JJricSi2AUD9jRPtKToOFQILIiJ+FSwtzBGUE+M1toM2U9bX8BA3jm191z4Rk+HnZ+uZ4nGOfTVT+6q+xcs0CQeFvd9euW/aCA73F+RW2t91yeVjeQWIwN7WAN72y79Dbdv4cRLkNv7UaBz9czFyba3sLDumJkImbD0y1wouQCfULmTc9mb69pjYSUMtVqjUxVApikKoY0xdjndlUHMyqFsh0OYmxtG36tBqgOHVFp20ChlNybtmDajUmwBICgCaOzcYKZYsquCpAVkRxntZScwJUC5PV1NgN25j6lJinmkKACzXJOZuerGaZZsGesJ6E8nmDalgaYewLGpUABv1XdnW/aVINu2ee8AOsJ6Z6NfomH/Y0v5BIJSIiQIiICIiAiIgIiIHM/LCP0U/tx6/NfKUFKYKE8tbcb9h5zoPldH6L31vck5nUxL0yQNQfXLBqfnPq5rXFyNbX0vv17DI3GVEJvkIP0PgZsP5q56hF+R+uZmOoaVvQa/Mn+0CMquW7hrYbp84b0h4+6ZKoHAWnzQ9IePuMo22mBFubmfdVuEz4LCM/YOfykWVt4I2IlmwOKIC6FsoAGbgANw7JE4XDqm4a8z3yRptp4fCVEq203tpYfX9pqYnHPZhm3gg2vqBc2381Ewl9/j/r+UxYgHXQ8eB/XgRuLpk363Ph+L+n2yLr4dxqp17DY6X/pMlsQ2/x/1zVqnf8Avf8A6QIbE1XYgPe40138vhP3BV8jq9r5Te3Ps+HjJh0DGxFxf/WB85H18CQMy6iwJHEXJHju9slmrLl2Me1KaLVfIQUJzKR91tR3crdkwJPxhcT9SJ0W7dSGBPWE9N9HR/yuH/Y0v5BPMeA9IT07sD9Fw/7Kl/41iokYiJAiIgIiICIiAiIgc68rK6YU/rVB61X5Tn+0cHcX+ufLunQvK16GGP8A3H/lHylKxhBp8Nw5D65eJlgrJ2cSCR28uAvznziNjsihjx3eq8nMGO4Hwsb6j4z42ogVNCdAbXJKjW9hyB09QgUrELYzHS9IfXCZcRvM+EHWEDcwOENRz91d5+A7TrJwKAAALADdM2FwopoF472Pbx+XhIvE4vMbLovv/tKN5KgJsNfdv5yc2bg1b0tezhulcwRtaWHCY3KNFv3/AF2GBZaWHQDRR7/f4zQx6i24eqaj7WfhYfR590j8Zj3N+tz4fj+QgRu0lFzoJBVnI3H61+Z9cmMUSb68+XDP/SPXIzE4ZtbWO/s+9/TAxUsfqMw4g3H4gd3rkhRcEaG/ofzGQdVCp1BE/cPWZDcdmnA2N9YG/j8MBd1G4m4+M0q9MK7KNwZgO65tJZa4dSR23HKR+ON3vzGvfc3gZcD6Qnp/Yf6NQ/ZU/wCRZ5gwO8T1Dsb9Ho/s6f8AIJKN2IiQIiICIiAiIgIiIHPPK4P+Fhj/AN1h60PynO8VWIWw7Ozn8p0jyur/AMtRPKuPbTecsxb6f7SwftOufdfUbhblr9WmDH1rp7t3st3bzrMQb6FvoTDiW04ey8oiKgmxsmjmrIDuBufAX99p+ebvNrZXUct91WPukG/trFa5BuHpdp5TTw+EJ1Og9pmfD4cuS7aj+Y/KfeMxapv1NtAN/wDaUZ0QKNB9WkpgcK9Q2RC3sG88TpxlUTHMzda/Yq/XtMseA29VQWQonchqv7SqL6zAs1HojXYXZkTsuWPsFuc0to9HHQaup37gbfXWMjanSGo3pYiu2/QVkpjvyUEzD+KRtfF5yQDUY6f4mJY687mBgxxKE3137u3N/VNBsWpvc237/wB7+oT9xKXv1nFrbyTa/PPrI6tRPAg+yBMABrbiCe/7SzUrbO0unIad99x8PbI1XZDpdT7PkZK4DaIJAfqm668NL+rfA06DlT7CJ94kaiSWKwoYAj0rE9+u4zTxtDKqHmoPu0PjfwtA/MHvE9RbK/6FL9mn8onl/AoWdVUFmYhVA3sToABxJJtPUmDQqiKd6qqnvAAko2IiJAiIgIiICIiAiIgUvyp4ctgcw/w6lNz3ElP9YnHsU2n12T0RtTArXpPSf0XUqbbxfcR2g2PhOFdJej2JwpIqU2ZBe1VFLIRzJHodzW4798sFfz/X18ZjrPf/AHPxMx/lSH7S+sfKfDYlTYAgk6ADUnsEoyYVCQ1hJr8wVBh0rkZUqVBSUne3VZ2cD7oyWvxMsXQHoNVrMKuJR6dAWOVwUerbhlOqpzJtcbt9xZ/Krjlo0aKBRoSyqBb0QEVVA3A5zp2SDlu08VksiC720HBRbefrtkH5k3uxvzPE919w7TJAUrBnc3ueu333+4v6o9vda2uqs7AAXJNgogYkW27T69Zm/gnCauiuNLBr2Guthu1Fx2aHhYyVDZioLvZn9g7uffNTH1AouZjz+DEMcy5QoUZRYWA3Wca3Bueu2vbPz851MxfMcxtc9wtu3buyRVbEkgkacrb+8k8JpNUb759ZllFjG0342a7KxBCkErltfT9Ve/xn5jMWrpbzaK1xd10J9K9+JJuNSfsiV9MW68b98kcLilfTc3Ln3TUGJ6XL+3qms1O3y+R+ElXo+Bms662IsffAy4DFkWVtVNwp5dhk70e2WmLxNHDVWZEbOMyWzZspYC5BGpXiOJldRBrpcfaXiR95f1hv/teWLoTUyY/Chm/xEAbcHVrqp15kgW4EW3iUdm6OdBMHg2z00Zqg3VKjZ2XnlFgq94AMtURMhERAREQEREBERAREQEREDWfA021amhPaoPwn7SwqJ6KIv4VA9wmxEBOMeVmqXx1OmD6NJLcgzPUu37qAnxBnZ5598pOKJx+KN9QadJey9NL+4j96WCsYuuHPV0ROqg7OLHtJ1klsfKilj6R9g3290g6zZQB4eEz4DFEg9/wEzy76E7icXoSZXMZXzt2an1azZx1UhGPd7xImnU6xvxB94PzmfHAq3sNJ8nDsEFTL1CxW53ZgN2+97G8tWHfDtRsbBrSsVQFbMp1BOgG4c77pnhyvK2WZi2NSAbajnNjFVENiq5TbXW9zzmuN3iPjOsRZMDUzoDxGh758bQp9QsN66/P2TH0eFw/7vxm9j1/4b/hb3QIyk9wGXQj3zdwFTLWouugWolRf1bOpdPWAw8ecisE1mI5/CZ6rkXtpazDs4H67JR6yia2Aq56aMd7IretQfjNmQIiICIiAiIgIiICIiAiIgIiICebvKAx/OGIB/wA4/wAq29wnpGeffKzgvN7QqNwdadYd1ght4o0sFH2gfR75iwFbK1uB982Nopdb8pGnnGCw1EzKV5giV9gVNjoRJfZ2LDdU+l75lxuzs/WXRvY3f2yCCznnPwtMlWiymzKQe34c5jBgfoE+wt+6KaFjYXJ7NTLNsXYJJDVBYbwvE98loz7CwJWncixY38OE/durlp24sfYNT8B4y0UaAAudAJUduYoVXsvoLx5ns7Jny7wQVEWdfrlNnEj+UxgsOz1DlBJHVAGtyBc+rSZ/yVncUwDmd0pAcQzNlt33InQem9g3/JqF9/mqV+/IskJioUgqqo3KAo7gLTLIEREBERAREQEREBERAREQEREBOc+V/YfncOmJUXagSHtvNJ9GP7pCnsGadGmKrTDAqwBDAgg6ggixBHEWgeWcNhWfMgsWUa6gacCL7+G6Q1ekVJBFu/3HtEvXTro2+z8SGTMaT5jTb9XjTY/eUG3apHbK5tBhXJbKqk20RQqggW0HA85oQgEkcJtUro4uOfGRzoVNjPoEGQWehjabi2ZT2G3uM2EwlI65E9QlR83Mio3M+syC7UVRNwRfUJ+ttukm45zyXX27pSxRJ33PebzPSsN5AksE/i9qVK2nopyHxkfiny2RBd2sFA4X4mab7Q+zTF2Ol7e4cZMbB2V6VRz+J7+iDvUGxIJG9rbgeF7548Rs7OopQpO7G5AyJYkFnNyX6pDWuLXFtBbXdLF5MNhNWxi1XBKYcecYnjVcdRb8bL1uyy85XsBhWxNVEprnGbKi2yB235mAJyqAAWYfZW++1+9dGtiphKC0lOZrl3e1i7tqzHlyA4AAcJ0ExERIEREBERAREQEREBERAREQEREBERAjdt7IpYqi1GsuZG9akbmU8CJ546WdGK+z62VgWpsepUAIVxy/VccV94npma2NwdOshp1UV0YWKsAQfAwPLByOLEazWqbLP2T4Gdf6R+SNSS+DqBePmqlyvcri5HcQe+UTH9HMVhjavQqIB9u2dO/Ot1HiRKKr+b6o3LfuIn0MPX4JU8FY+4Sx4fCZ/QdD4/ImbQ2NV4FfW/wWBVDg653ow77D3z6TZTH0mA9pljq7OdfSZR4/O0wYbBvVbJRV6rXtampe3eVBC95MDRo0EpjTfzO8zPg8O9d1pojOzGyoguz/ACA4k6DeZddheSzEVSHxLDDpxW4qVSPAlV4akntE6rsDo3h8GuWhTCk+k561R/xOdbdgsBwECO6F9ElwSZmIeuwszD0UXfkS+uW+pJ1Y6ncALXESBERAREQEREBERAREQEREBERAREQEREBERAREQIzG7BwtXWrhqLnm9NGPrIvNM9DsB/6WkO5be6T8QIKh0TwKHMuEw+bmaasR3EgkSZp0wosoAA4AWA8BMkQEREBERAREQEREBERAREQP/9k=",
                    }}
                  />
                  <Text style={styles.name1}>Cameras</Text>
                  <Text style={styles.nameItems}>4 Items</Text>
                </TouchableOpacity>
              </View>
            </View>
            <View style={styles.imageContainer}>
              <View style={styles.container2}>
                <TouchableOpacity
                  onPress={() => {
                    props.navigation.navigate({
                      name: "DetailsScreen",
                      params: {
                        pageId: 1,
                        pageName: "Redeem Gallery",
                      },
                    });
                  }}
                >
                  <Image
                    style={styles.image}
                    source={{
                      uri: "data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAoHCBcIBxgXCBUWGRUaHCQeHBoaGB8hJRkhJSUjHhwkJB0lJTAlIyExKCUeLUYnOC8xNTU1JCQ+QDs0PzA0NTQBDAwMEA8QHxISHzQoJCs9NDY0NjQ0NDQ0NDQ0NDQ0PT80NDc0NjQ0PTQ0NDQ0MTY0NDQ2NDQ0NzY0NTQ7NjQ2Pf/AABEIAOEA4QMBIgACEQEDEQH/xAAbAAEAAgMBAQAAAAAAAAAAAAAABQYBBAcDAv/EADwQAAIBAwMDAgQEBAMHBQAAAAECAAMRIQQFEgYxQSJREzJhcRRCgZEHFVKxcqHBIyQzYtHw8RclNaLh/8QAGAEBAQEBAQAAAAAAAAAAAAAAAAIBAwT/xAAlEQEBAAICAgEDBQEAAAAAAAAAAQIRAyESMUEiUWETFHGBoQT/2gAMAwEAAhEDEQA/AOzREQERMQOTdf74+t31dPpFqD4TY48l5MR4yLkHjY+Bc+ZfulNY2t2SmdYytVUcXKsD6h727NaxIlX3nQ6duqmq6jkQpHK92BYLey+B2A/eWXpDk+zh6qhS7M4AFsE+k/taXZNIxx1bd+0/ERIWREQEREBERAREQEREBERAREQI3fNU2h2mo+nsXVTxBIALflGfrOf9E9UPR3lqO7vVZqr2X4nL0t2sAflubiwx2t2MufV6/wDsrMUD8GV7EXsAcn9AT+l5XNs2ygeqUraZ8MSwC/1cT6Wv4yw/QS5JpGWNys1dadBmZiZkLIiICIiAiIgYlN6z6xPT44aemGcgAEnClr9x3NgL/WXKVbrjp4b5oFKG1SmeSm9gQcMD4OO31/WbPaM5bOrpW+mKtTdtIaOoTi7uatR2PzBsKwXupwFC+wYy8dPkjSMvhHKL9hbEoG1aN9HRqhHPEqpLubswQn4bKQRgHnOh7HpDo9vVar8yc3tbvmXkqJKIic2kREBERAREQEREBERAREQERECL35+Oht4dgpzbBvcX+vb9ZTOpaj7Nt3w6KMaqVFqoyZwuC7juVC3Ujx6TLvvOlbV7ey0GCtggkXtY3lE3XT1NVRpXZlJDEVF+YF7CoWvccfkxOmCanOk+sk3/AIpUQpVswbI4lltcDzkEkY7Ay3SodCdNHZdK76rNWo1/HpUYX7EjvLfIvsw3rtmIiYoiIgIiIGJF9QawaLanZyBi2e0lJQv4s13obLSOnI4mqAy2+b0sQL+Bj9byuPHyykZldTbx2ugKewLWquWWzqFsPQocgKthci18e5Ev1CoKtANSN1IBH2nMejNQ+toLS1I4o7clAYclbBawHyqQvId83HedB2gGkjIwsFN1x4bP97zcmY3cScREhRERAREQEREBERAREQEREBERA86rBEJc2AFyfpKPulAVun2q6V7JxCkFQeSM9mUE/KCLfW8s2+v/ALuqAE8zYjPyjLdvfA/WUjrHVVNBo2o6dS6B+T5F2OXVbYBH5icYX3te8fScrqL3suqGr25GS3bwcD6SRlB/hTqn1e11jXsFFSyrY3XAJBPkZH+cv0zkx8crK3G7m2YiJLSIiAiIgYkR1Jsi7/tZpVyRkMCD2Ydr+4+kmJiJdDku4dM1djphdNWVK1U8aboCAjAM3G5+UN2v2/eRnS3UOr0GuNPUVKxfkOdOonJSQfkD91Y3J9rC86B1qFL0RqwTSZ/Va97gHh28cip98SK0mpGo1gJ4tUTBo1WQVA4wptyDMpXIvk/pO0yvhqSdueXXcdBU8hMyC2PfhudZqddCldVDMhBxfxcgZGLj6j3k7OVll1Vy7ZiImNIiICIiAiIgIiICIiBiYOJmQm+b8u1VFQAvWcEogv6rfYHJ8DubH2MSW3UZbpy3q/qnVajXBNPVdGDnilNO2bBWY92wDb6+ZvaDYdRv6FdbUV69OwqM5NiSAwQkDPEeRjP0kzuN9NqfisnBn7U0CF2c49Kk8hgkk2xJLohVGrrfhQTTBB5Ne/Mgc8nuLhp3t+nVk6RJv2mOlNhHT+1CmG5MSWY+OR7282k5EThbvurk0zERDSIiAiIgIiIGprqA1OmKuAcYuOx8H7zknVNGlt2td3p8zVAZbgekiwdSTkG/H9J2WUvqTZl0pet81LDMgW5FmBYqB+YryHbt7y8LPVcubinJjqqr0VrfxOrWpXPwyi8Q5dmFzcBRfAWxtfxYTonTm7/zXRnn86Eqwta9iQD9jaVPUImk171dE7/ANldaaiyXtwPAi3E3sQe+fMl1pPtDc9DYBslfyP7Z/KxwAQbd7iXlrL0rCeM0uETU0OrGt04ZQQexU91PkGbc4rIiICIiAiIgIiICImnrdwp6Ff8AeHAPhe5P2HcwPHedzXa9JzcEkkKqgXLEmwsBk95z7rzVHb9d8Rbs7LxutQrwP9JA7Yv9fUZaKtRt5ri62CG6j+g5B5MD6iM3ANhjJkTSK67VINUzfhw9uLooFRhe4tbCLa9hi+TkidcPp9oznlNKjsFCluOrR6dPgad3Y4N/AUNlieV8e32nX9r0o0miUU1CkgE2HcnuT7mVXp/Yk1Wr+LTxQDMVXjbndvQT9gCB9GxLwZOVnqI4eOcc1H1ERIdiIiAiIgIiICIiAmtq6S1qBFYXAz9QRkEEZBmzMEXGYHFtDv7vu7vWIFOox9LjkByA4XH5gR+U4vbyAZcdDqPwOmPwyPg8iHV7si2Njk+pbix8jMi9w6Ac7tz0xDUuRZVJC8T3swt6hfsR4sCJN6DQDS6KsmqIZmcuR4XkCthfxgTruaceKZyXz9m07n+G1ycwoV1PPibhOOFNwL9rDP0ltpVVqoDSYEHsQb3lK3amdBURtMoKsFupYqGUkLZmAJC+oXt7ZvOf7bvFfaN6f8XUc6hWIJ5WUsMFGT5eJsQLD2IN5s4/OWz4XcpLp3mJG7Nui7npwyji1sr7e9vcXmxX1Jo6lVI9LXu1+xxYW+uZx0ttRI/dtxXbNEXre4UCxySbDt48/a851ufXFTQbiqanU+oOb/Doq1NqdrqcguM2BPL3j8RNykdViQPTvUKbxowanFao+dAwPE4zf2NwRN/cEq1gF0jBQb828geAv3Pc9wL2sSCN02WWbjaauqNZ2UH2JE+1YOPQQR9DIHS7Wj6p1bkQoy1wAxPcWAviwySZ5b1Qp6DTszO/Igmyhb5NyQQARnF/2jQ39Vry9Rl0xA4/MxNgM2OTgec+4IlUqkHdHZLO1Nbq/wApcn0seROQDyz9ZQt13h9XuAOmdkqd7g8lXvZVS1iAO4IJuSTL104W3imr6yw5AlQFvwXsLXzbBNjfvO1wuOMt+UTKW6SVfU/iNFxplUoghQEYgOSQti9rnzge2TKjuu/PS16fhQppU3HJUFuZXly/wKBjiMEsSbk3lw1GiGp22lT07cWDK49jwC4Nu1y1pDaboN33YvWISkzcnUEEHJNkFsXxdj4vYeZPSeWZ2aw9ugaGktPTD4IsG9WTcknNyfebU+VHEWHYT6nJ2IiICIiAiIgIiICIiAiIgYlS65RdFtjV+LFh6SATm97EgeQbGW2aO76Bd0216VQ2DqQD7Hwf0M3G6u2ZTcc3PVb7poRT3ekqf01UuwRh8oZc+lrWuDi8h9RpW1erV6lNuYCqpCXDMRfNjxYX7nFwbixnxuPSuvp1zTp0GKA44fK9u129j+hE6HoaYTaaVPXKRURVV3WxU8c9/wA32tedPU28+Eyzx+vq/h4bWBodEi6WzNTAUujCzkC5NmzbuO98SWqatd10JFZGVgcC4OQLjscg+0jHGn01XjvAp02LEqQ3FgCbgk+ftm0+a19BuyjTZLix9VuBXzny39rTNbehq7xqKqPTXUNVUIwe9NAxUWKkhmHqvewFja5vfEpHUaHbdTwItRZrkn1FKlgeDvnHkDt6v26nW1TceOvTmucEWYAeb9if+7yu63Y119ErQdXpcSq03Xiy3YF2veztbF/GJfFyXjy8pNueeO0V/CvbRrdPXq6wW4twVgbcTkvb8tvlwQQZZ9RqC546OoHQGzMwZVFxgBrkE9z2tKzp6D6LbhR25WFEMWSmbAPZv9oz1DkqAQbnF7AX7SN1W9Pt+iD0kNQBuLPchAbeggW+bxyz5HmZld25W908scZJ6dL0Wsp6DQf7IFmOSFtkkXxmwEid1tr9I66o8DUxzZlst8jAzxtYd7i/vNLY667zUR3UKSikHt8QtjsO3H+15IgUNTWK7P8ADqODdrtdmF7tY9h9vMnS3PhtrUqzNwfkbo9qdhcC5AueKpewHkAEk3krpOpn2vSsu30ldjh3ckLci7Kii1wt7cr5t7y46oD+W1U0YZndWCMbBVLj/wCp+lrznW39La56wpvQYKTnn8q37kNft+5P0lS7lcM5lhPo7rofQ9Jdbt61yp5XKgEmwtbkQPctyz9pbpH7Ltw2rbEpIb8Rk27nuT+8kJyyu7t3xmp2zERMUREQEREBERAREQEREBERAREQMSq9UFto0lSvpFBYAkem/qOLke69wZap51aYrUitUAgggg+QcETcbqsrhVHbzr0+Kapq1mYlkXug7LdmyHLZta1v0ls2GjU1OtVaxDOgYuDk4shut8n2tgi03D/Dv8NWb+XVgqMQQGUkpY3tcMOQtjIv7kyY0ewfyrX0jpQW+bk7EenBCqB4XOB/yzvllj3Zd7csMMsfd2gt46oK6J0JASmxpue1R8WUoCCON8E97gjFpXNPv1TU6haGnR6buw4WYAOflvY/IL+QTck47TU3qrwQfFq82R3RiU/4bA8yrNb0qSTbucX8zyq7o+/19KKSj4yPZWVeOB24gXsv1PlSY1q6ifPK5a109963F9MyJSVFQsnMK5cOEJL0y1vSS2Sls8R7T76g6g/nVbhUplKFJQVpXW9Vm9KZXAsTfj4sPeN93FaX/HQtW5HnSwUd7+h17eoj2OLnHmSnR+2Uhq01OoQOha/JioFM34K5UknkG9JBNx3tiZlq3f8AjOTjmXVuvy+Tpn2bTJRLWqrTQ2AINzcDiPe5+wF7yEXSfhdCleo4pVSCUKuTydTZ1Y91e/cdhi06H1FtTavcCavoVmVQ4sSwKn09xi4yPPKaP/puNRUX8dVXiCSwpoVL3ti5YhRbFwL/AFE2ZY9W3Ss8bb0lulKjb1oadfWKA1s+m3qU25AeS3k/tLbPLT0V01BVogBVAUAeAMAT1nDK7rtGYiJjSIiAiIgIiICIiAiIgIiICIiAiIgIiICRu73+EoAupb1e1vr9JJT4IuPVEFK3jZqe87kG0/KmxA5sLetfJK5U2AAv3mjotnTZ95+IjMxf0sXW3BmvwsBhe3b81x7Sx9Rbpp9qpBNRUWmX/Kq3Zhn8o8f9DK/V3BNxoO23kFPSisxsTUW7EcRfk/n6CdpllrXekam9qxv2pXUb+zV1VCgCqrH5mJILBfyk475zifOl1QPxKVFkOeZUjkA5XiCtsFyeIsTg2PcWNn3/AKSp7iy1EN6rXVu7CpcBgG8ixU5HgT52HpNdufm4XiuQqCwdr4ckm7KCR9r/AElbl1d/0837a/q3k3V409ZNRQAqMrMtuQNrqw9wcggzfkPQ26jqTzK3PYgm4vfP3/1kvOFexmIiYEREBERAREQEREBERAREQEREBERAREQEREBPDV1TR0rsguVUkD3IFxPeeboHQhxcEWI94HENz286vfKgqFuTkfEJYFgT3UleynuBftYfSb+zBNLT1L0kbgWVaYTsxK2AYd+PpIuJMbl0on44jRoWCgBGpv8AD4ryJdXIw/8Ai7+J66fZvwxHxGT4SK5TLXu5vZ39wBiejfzt4seLknL5W9fZN09Uz1FDXZSqrjvysT6SMfKWx7Xja9yWnq2QIQo5Ke/Efm9IPckePeRW49SUtk21wypUdmFkpjiAoA9R8gA3yM3zNXaOrE1tKzgBqllQCzL37NixJzgjNsZnL4/l6/KdLqKJqi+3VApsMMt7DvYrjGZKjtmVYVODCpQupBsQSfJsDyOeBOLH5Ta0stGqK1IMvYi8mxsesRExpERAREQEREBERAREQEREBERAREQEREBERATX1NNqtK1JuN+5tfH7ibE0dStSo1qXED3JOff/AMefebBFa100tIrS9TObH3qMMhcWsts4x+pMhd3C0NOhLKQKi82vxDt45L7KbfeS+t0q0LkEM4B9TWB9zwPZbeF+9795oEc9HRpMGZWubsouFsWLA+WP7DMtij9e6xq1OyujMeStUT0qSwBIA8YUgG5vn6yn7FV/DaxEpjkrtm+QG/KwAyGUjv7Toero/i9trLXRynAU0VFuKgVrjmLXDDBVha2bSO2LpxdLriNs+J8YsDT1Fwopgg/FXgwsWC8h/wBLSpnP07hZ393Cy+l3FRq+106qkBnQXzcE3HKy2tY2v9Me0tehQ09Iobvb/wDZE7pRXS7Qg0tgFwpsCbFT/mfeTw7ZnK+neMxETGkREBERAREQEREBERAREQEREBERAREQEREBNfVVvgU7+TgT3le3dmr7mtMAsOIsA1snlyubew7zZNjVetd2UXYq/EuRYoGHLnxPzYPeamqrcatUuqlkUKiZuQfzBb/1Wwe1p76KrdGaoFFOmC3FSTdh6QCT3va+JRurd+OlqIm31WNVuTt6OJpFsMva5JPv2se86a1LajLLXa8UaLU1A0+FKKbgglWuALnx3wPtPejUGq07U7AG4NvzI9/SwHdgTkn6mVb+HnUq63TCluFuYLKQcArjli3zXIPfsD7Sz6rQLpNSoqKxySrBuJK4FgfLDvbz+uMb7StXV0qGmRa55sLEKoJJbxYfe/eSwNxI/RbfSo2aiCTbDMSSB9L9pIyKoiImBERAREQEREBERAREQEREBERAREQEREBERAxI7WaI1NatSiRcAhgfIsbW+uT+8kpiJdCk72SmwhELgvVRCaYBaw9R4g4vgyl7Wf5luWrbX353+GGZQG42yWHYECw+95f+q9OdTo2TRBgysKhKHNx7Hxj/ACM5Y9Kvo9WX0Sq61FLMhb1IVwSwNitgR/i8XneS5Y9fd4f+3jz5OOzDq/DS2nWnSdQcaNuKsxuWC8u35j9Da3mdu0h/mO1WGG7obEcSOwznHY+4vOS7btrV91pc144T1PYgKfUSxFwrNbC/N3nWNdWWhS+PTNl7t/ytbB/Xtbzj3mZTxkl9u/Bj446bexVeeksfHi97fT97yVkN02jDbg1bu2f9T9s3kzOV9u5ERMCIiAiIgIiICIiAiIgIiICIiAiIgIiICIiAiIgQHxVGkqNVsGY8f9T/AHOfp9JobptVCoqNqqIdnDXY3J44Iuw/ST9bQLV1IZiwt2C4FybscZuex+n6z53XSrqKQNUX4m+PAIsZUy7ZppPs1HR0gNLSRVbBUDF7Ya3lrYv3zPU7ap0jkciGU+k5z3H3IIxNnWVgdCChGSAL+9//ADNrTg/BHLBt29pm6NbZ6RpbcgdeOL8f6b+JvzEzMrSIiAiIgIiICIiAiIgIiICIiAiIgIiICIiAiIgIiICYPaIgRSf/AB9P/EP7mS0RNrIRETGkREBERAREQEREBERAREQP/9k=",
                    }}
                  />
                  <Text style={styles.name1}>Jeweleries</Text>
                  <Text style={styles.nameItems}>3 Items</Text>
                </TouchableOpacity>
              </View>
            </View>
            <View style={styles.imageContainer}>
              <View style={styles.container2}>
                <TouchableOpacity
                  onPress={() => {
                    props.navigation.navigate({
                      name: "DetailsScreen",
                      params: {
                        pageId: 1,
                        pageName: "Redeem Gallery",
                      },
                    });
                  }}
                >
                  <Image
                    style={styles.image}
                    source={{
                      uri: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRlxGSGFkUGIKPvdIDTlTDDWVGPJKWID5v0hA&usqp=CAU",
                    }}
                  />
                  <Text style={styles.name1}>Health & Beauty</Text>
                  <Text style={styles.nameItems}>8 Items</Text>
                </TouchableOpacity>
              </View>
            </View>
          </View>
        </View>*/}

        {/* PRODUCT LIST
        <View>
          <Text
            style={{
              fontSize: 20,
              fontFamily: "open-sans",
              marginLeft: 15,
              marginTop: 8,
            }}
          >
            Products
          </Text>
        </View>
        <FlatList
          //horizontal={true}
          showsHorizontalScrollIndicator={false}
          keyExtractor={(item, index) => item.id.toString()}
          data={CATEGORIES}
          renderItem={renderGrideItem}
          // sliderWidth={200}
          // itemWidth={200}
          numColumns={2}
        />*/}
      </ScrollView>
    </SafeAreaView>
  );
};

export default HomeScreen;

const styles = StyleSheet.create({
  headerbackground1: {
    position: "absolute",
    top: 0,
    width: "100%",
    height: 200,
  },
  headerbackground2: {
    position: "absolute",
    top: 0,
    width: "100%",
    height: 200,
  },
  barCard: {
    alignSelf: "flex-start",
    alignItems: "center",
    // height: 80,

    flex: 1,
    flexDirection: "row",
    //justifyContent: "space-around",
    backgroundColor: "#ffffff",
    borderWidth: 0,
    borderRadius: 10,
    shadowColor: "#000",
    shadowOffset: { width: 0, height: 5 },
    shadowOpacity: 0.34,
    shadowRadius: 6.27,
    elevation: 10,
  },
  barCard2: {
    //alignSelf: "stretch",
    //alignItems: "center",
    // height: 50,
    flex: 1,
    //flexDirection :"row",
    //justifyContent: "space-around",
    // backgroundColor: "#F9F9F9",
    //borderWidth: 0,
    // borderRadius: 20,
    //shadowColor: "#000",
    // shadowOffset: { width: 0, height: 5,},
    // shadowOpacity: 0.34,
    // shadowRadius: 6.27,
    // elevation: 10,
  },
  barIcon: {
    flex: 1,
    alignSelf: "auto",
    //height: 50,
    alignItems: "center",
  },
  myInfoIcon: {
    width: 80,
    height: 80,
    alignItems: "center",
  },
  verti: {
    flexDirection: "column",
    justifyContent: "center",
  },
  card2: {
    //width: 200,
    //padding: 15,
    flex: 1,
    flexDirection: "row",
    // borderRadius: 15,
    alignItems: "center",
    //margin: 5,

    // shadowColor: "#000",
    // shadowOffset: { width: 0, height: 5 },
    // shadowOpacity: 0.34,
    // shadowRadius: 6.27,
    // elevation: 5,
  },
  containerMain: {
    flex: 1,
    margin: 6,
    //height: 350,
    borderRadius: 10,
    backgroundColor: "#f9f5f5",
    //elevation: 3,
    overflow:
      Platform.OS === "android" && Platform.Version >= 21
        ? "hidden"
        : "visible",
  },
  title: {
    fontSize: 20,
    fontFamily: "open-sans",
    //margin: 10,
  },
  title2: {
    fontSize: 15,
    fontFamily: "open-sans",
    color: "#7ecefc",
  },
  containerHeader: {
    flex: 1,
    flexDirection: "row",
    justifyContent: "space-between",
    // alignItems: "center",
    // padding: 10,
    // marginLeft: 10,
    // marginRight: 10,
    margin: 10,
    //backgroundColor: "#ebf8fc",
  },
  container1: {
    flexDirection: "row",
    justifyContent: "space-between",
    // padding: 10,

    //backgroundColor: "#ebf8fc",
  },
  container2: {
    flex: 1,
    flexDirection: "column",
    justifyContent: "space-around",
    alignItems: "center",
    //padding: 1,

    // borderRadius: 10,
    // elevation: 3,
    backgroundColor: "#ffffff",
  },
  imageContainer: {
    flex: 1,
    margin: 5,
    height: 120,
    borderRadius: 10,
    // elevation: 3,
    overflow: "hidden",
    //   Platform.OS === "android" && Platform.Version >= 21
    //     ? "hidden"
    //     : "visible",
    elevation: 0.5,
  },

  image: {
    height: "70%",
    width: "100%",
    resizeMode: "contain",
  },
  name1: {
    fontFamily: "open-sans",
    //justifyContent: "center",
    textAlign: "center",
    fontSize: 12,
    // justifyContent: "space-evenly",
  },
  button: {
    //padding: 15,
    //width: "100%",
    flex: 1,
    height: 65,
    //flexDirection: "row",
    borderRadius: 10,
    // alignItems: "center",
    //margin: 10,

    // shadowColor: "#000",
    // shadowOffset: { width: 0, height: 5 },
    // shadowOpacity: 0.34,
    // shadowRadius: 6.27,
    // elevation: 5,
  },
  btnContainer: {
    flex: 1,
    // margin: 10,
    flexDirection: "column",
    justifyContent: "space-between",
    alignItems: "center",
    //  padding: 1,
  },
  nameItems: {
    fontFamily: "open-sans",
    //justifyContent: "center",
    textAlign: "center",
    fontSize: 11,
    color: "#878686",
  },
});
