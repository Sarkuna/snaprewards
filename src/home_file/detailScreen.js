import "react-native-gesture-handler";
import React, { useState } from "react";
import gS from "../globalStyle";
import { useSelector } from "react-redux";
import {
  StyleSheet,
  Text,
  View,
  TextInput,
  SafeAreaView,
  ScrollView,
  Alert,
  TouchableOpacity,
} from "react-native";
import axios from "axios";
import { WebView } from "react-native-webview";
const DetailsScreen = (props) => {
  //const [details, setDetails] = useState("");
  const myToken = useSelector((state) => state.auth.access_token);

  //    const pageId = props.route.params.pageId
  //    const pageName = props.route.params.pageName
  //    const related_id = props.route.params.related_id

  //const { pageId,pageName} = route.params;

  // const pageDetails = ((global.url + "/pages/view-page?access-token="+myToken).concat("&related_id="+related_id))

  const pageId = props.route.params.pageId;
  const directToRewardGallery =
    global.kansai + "/site/app-login?token=" + myToken;
  let link = "";
  console.log(pageId);
  if (pageId === 1) {
    link = directToRewardGallery;
  } else if (pageId === 2) {
    link =
      "https://th.bing.com/th/id/OIP.E8P8Qx0s-I9vOavlo7XhTwHaEL?pid=ImgDet&rs=1";
  } else if (pageId === 3) {
    link = "https://vispace.net/wp-content/uploads/2020/05/10a.jpg";
  } else if (pageId === 4) {
    link = "https://vispace.net/wp-content/uploads/2020/05/10a.jpg";
  } else if (pageId === 5) {
    link = "https://vispace.net/wp-content/uploads/2020/05/10a.jpg";
  } else if (pageId === 6) {
    link = "https://vispace.net/wp-content/uploads/2020/05/10a.jpg";
  } else if (pageId === 7) {
    link = "https://vispace.net/wp-content/uploads/2020/05/10a.jpg";
  } else if (pageId === 8) {
    link = "https://vispace.net/wp-content/uploads/2020/05/10a.jpg";
  } else if (pageId === 9) {
    link = "https://vispace.net/wp-content/uploads/2020/05/10a.jpg";
  } else {
    link = "https://vispace.net/wp-content/uploads/2020/05/10a.jpg";
  }

  return <WebView source={{ uri: link }} />;
};

export const screenOptions1 = (navData) => {
  const pageName = navData.route.params.pageName;
  console.log(pageName);
  return {
    headerTitle: pageName,
    headerStyle: { backgroundColor: "#7367F0" },
    headerTintColor: "white",
    //backgroundColor: "white",
  };
};

const styles = StyleSheet.create({
  btn: {
    height: 50,
    backgroundColor: "white",
    fontSize: 16,
    margin: 20,
    borderColor: "gray",
    borderWidth: 1,
    borderRadius: 50,
    justifyContent: "center",
  },
  btnWord: {
    fontSize: 16,
    color: "#000",
    textAlign: "center",
  },

  card2: {
    flex: 1,
    width: 190,
    height: 200,
    backgroundColor: "#F9F9F9",
    justifyContent: "center",
    alignItems: "center",
    borderWidth: 0,
    borderRadius: 15,
    margin: 10,
    shadowColor: "#000",
    shadowOffset: { width: 0, height: 5 },
    shadowOpacity: 0.34,
    shadowRadius: 6.27,
    elevation: 10,
  },
});

export default DetailsScreen;
