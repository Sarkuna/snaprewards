import "react-native-gesture-handler";
import React, { useState } from "react";
import gS from "../globalStyle";
import { useSelector } from "react-redux";
import {
  StyleSheet,
  Text,
  View,
  TextInput,
  SafeAreaView,
  ScrollView,
  Alert,
  TouchableOpacity,
} from "react-native";
import axios from "axios";
import { WebView } from "react-native-webview";
const MyPageScreen = (props) => {
  const pageId = props.route.params.pageId;
  // const [details, setDetails] = useState("");
  // const myToken = useSelector((state) => state.auth.access_token);

  const imageUri = props.route.params.imageUri;
  const related_id = props.route.params.related_id;

  //  setDetails(pageId);

  //const { pageId,pageName} = route.params;

  // const pageDetails = (
  //   global.url +
  //   "/pages/view-page?access-token=" +
  //   myToken
  // ).concat("&related_id=" + related_id);

  // axios
  //   .get(pageDetails)
  //   .then(function (response) {
  //     //console.log(response.data.page_description);
  //     setDetails(response.data.page_description + "");
  //   })
  //   .catch(function (error) {
  //     console.log(error);
  //   });

  return <WebView source={{ uri: imageUri }} />;
  // return (
  //   <View>
  //     <Text>{pageId}</Text>
  //   </View>
  // );
};

export const screenOptionsPromotion = (navData) => {
  //const pageName = navData.route.params.pageName
  //console.log(pageName)
  return {
    //  headerTitle : pageName,
    headerStyle: { backgroundColor: "#7367F0" },
    headerTintColor: "white",
  };
};

const styles = StyleSheet.create({
  btn: {
    height: 50,
    backgroundColor: "white",
    fontSize: 16,
    margin: 20,
    borderColor: "gray",
    borderWidth: 1,
    borderRadius: 50,
    justifyContent: "center",
  },
  btnWord: {
    fontSize: 16,
    color: "#000",
    textAlign: "center",
  },

  card2: {
    flex: 1,
    width: 190,
    height: 200,
    backgroundColor: "#F9F9F9",
    justifyContent: "center",
    alignItems: "center",
    borderWidth: 0,
    borderRadius: 15,
    margin: 10,
    shadowColor: "#000",
    shadowOffset: { width: 0, height: 5 },
    shadowOpacity: 0.34,
    shadowRadius: 6.27,
    elevation: 10,
  },
});

export default MyPageScreen;
