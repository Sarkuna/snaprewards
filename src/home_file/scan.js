import "react-native-gesture-handler";
import { Icon } from "react-native-elements";
import React, { useState, useEffect } from "react";
import gS from "../globalStyle";
import {
  StyleSheet,
  Text,
  View,
  TextInput,
  SafeAreaView,
  ScrollView,
  Alert,
  TouchableOpacity,
  Image,
  Picker,
  RefreshControl,
  ActivityIndicator,
} from "react-native";
import DateTimePickerModal from "react-native-modal-datetime-picker";
import { TouchableWithoutFeedback } from "react-native-gesture-handler";
import * as imagePicker from "expo-image-picker";
import * as Permissions from "expo-permissions";
import { CAMERA_ROLL } from "expo-permissions";
import { useSelector } from "react-redux";
import axios from "axios";
import Colors from "../../constants/Colors";
const Scan = (props) => {
  console.log(" Scan Page");
  const MY_TOKEN = useSelector((state) => state.auth.access_token);

  const [date_of_receipt, setDate_of_receipt] = useState(
    "     mm/dd/yyyy"
  );
  const [invoice_no, setInvoice_no] = useState("");
  const [member_cat, setMemberCat] = useState("");
  const [member_company, setMemberCompany] = useState("");
  const [isDatePickerVisible, setDatePickerVisibility] = useState(false);
  const [pickedImage, setPickedImage] = useState("");
  const [pickedAll, setPickedAll] = useState();
  const [pickedAllImages, setPickedAllImages] = useState([]);
  const [pickedImageUri, setPickedImageUri] = useState([]);
  const [pickedImagesGallery1, setPickedImagesGallery1] = useState();
  const [pickedImagesGallery2, setPickedImagesGallery2] = useState();
  const [pickedImagesGallery3, setPickedImagesGallery3] = useState();
  const [pickedImagesGallery4, setPickedImagesGallery4] = useState();
  const [pickedImagesGallery5, setPickedImagesGallery5] = useState();
  const [error, setError] = useState();
  const [loading, setLoading] = useState(false);
  const showDatePicker = () => {
    setDatePickerVisibility(true);
  };
  const hideDatePicker = () => {
    setDatePickerVisibility(false);
  };
  const handleConfirm = (date) => {
    setDate_of_receipt(date);
    console.log("A date has been picked: ", date_of_receipt);
    hideDatePicker();
  };

  const receiptHandler = (text) => {
    setInvoice_no(text);
  };

  const categorytHandler = (text) => {
    setInvoice_no(text);
  };

  const membertHandler = (text) => {
    setInvoice_no(text);
  };

  const verifyPermissions = async () => {
    const result = await Permissions.askAsync(CAMERA_ROLL);
    if (result.status != "granted") {
      Alert.alert(
        "Insufficient Permission",
        "You need to grant camera permission to use this app",
        [{ text: "Okay" }]
      );
      return true;
    }
    return true;
  };

  const verifyPermissions2 = async () => {
    const result2 = await Permissions.askAsync(CAMERA_ROLL);
    if (result2.status != "granted") {
      Alert.alert(
        "Insufficient Permission",
        "You need to grant Gallery permission to use this app",
        [{ text: "Okay" }]
      );
      return true;
    }
    return true;
  };

  const takeImageHandler = async () => {
    const hasPermissions = await verifyPermissions();
    if (!hasPermissions) {
      return;
    }
    const data = await imagePicker.launchCameraAsync({
      allowsEditing: true,
      quality: 0.5,
    });
    if (!data.cancelled) {
      let localUri = data.uri;
      let filename = localUri.split("/").pop();
      let match = /\.(\w+)$/.exec(filename);
      let type = match ? `image/${match[1]}` : `image`;

      let newfile = {
        uri: data.uri,
        type: type,
        name: filename,
      };
      console.log(newfile);
      setPickedImage(data.uri);
      setPickedAll(newfile);
      // console.log(pickedImage);
      // submitHandler(pickedAll)
      if (!pickedImage && pickedAllImages.length === 0) {
        setPickedAllImages([newfile]);
        setPickedImageUri([newfile.uri]);
      } else {
        setPickedAllImages([...pickedAllImages, newfile]);
        setPickedImageUri([...pickedImageUri, newfile.uri]);
      }
    }
  };

  console.log(pickedImageUri);
  console.log(pickedAllImages);
  const takeImageHandler2 = async () => {
    const hasPermissions = await verifyPermissions2();
    if (!hasPermissions) {
      return;
    }
    const data = await imagePicker.launchImageLibraryAsync({
      allowsEditing: true,
      //allowsMultipleSelection: true
    });
    console.log(data);
    // console.log(data.uri);

    if (data.cancelled === false) {
      let localUri = data.uri;
      let filename = localUri.split("/").pop();
      let match = /\.(\w+)$/.exec(filename);
      let type = match ? `image/${match[1]}` : `image`;
      let newfile = {
        uri: data.uri,
        type: type,
        name: filename,
      };
      setPickedImage(data.uri);
      setPickedAll(newfile);
      // console.log(pickedImage)
      // submitHandler(pickedAll)
      // console.log(newfile)
      if (!pickedImage && pickedAllImages.length === 0) {
        setPickedAllImages([newfile]);
        setPickedImageUri([newfile.uri]);
      } else {
        setPickedAllImages([...pickedAllImages, newfile]);
        setPickedImageUri([...pickedImageUri, newfile.uri]);
      }
    }
  };

  // const imageHandler =  () => {
  //   console.log(pickedImage)
  // console.log(pickedAll)
  // }

  //  console.log(pickedImage)
  //  console.log(pickedAll)

  //  setPickedAllImages([
  //   ...pickedAllImages,
  //   pickedAll
  // ])
  // setPickedImageUri([
  //   ...pickedImageUri,
  //   pickedImage
  // ])

  const Sumbit = () => {
    // setLoading(true);
    submitHandler(pickedAllImages);
  };

  useEffect(() => {
    if (error) {
      Alert.alert("An Error Occured", error, [{ text: "Okay" }]);
    }
  }, [error]);

  //function submitHandler(newfile) {
  const submitHandler = (newfile) => {
    var newfile = newfile.splice(0, 5);
    console.log(newfile);
    // create formData object
    const recieptDate = date_of_receipt;
    const recieptDate1 = recieptDate.toString();
    const recieptDate2 = recieptDate1.substring(4, 24);
    const formData = new FormData();

    newfile.forEach((file) => {
      console.log(file);
      formData.append("upfile[]", file);
    });
    formData.append("invoice_no", invoice_no);
    formData.append("date_of_receipt", recieptDate2);
    axios({
      url: `http://api.businessboosters.com.my/receipt/upload-receipt?access-token=${MY_TOKEN}`,
      method: "POST",
      data: formData,
      headers: {
        Accept: "application/json",
        "Content-Type": "multipart/form-data",
        Authorization: "Basic YnJva2VyOmJyb2tlcl8xMjM=",
      },
    })
      .then(function (response) {
        //console.log("response :", response);
        console.log(response.data);

        if (response.data.message === true) {
          Alert.alert(" Upload Successufull", "", [{ text: "Okay" }]);
          props.navigation.navigate("Home", {
            screen: "Home",
          });
        } else {
          setLoading(false);
          Alert.alert(
            "Required",
            "You must need valid receipt number and image for a successful upload",
            [{ text: "Okay" }]
          );
        }
      })
      .catch(function (error) {
        console.log("error from image :");
      });
  };
  //  console.log(pickedAllImages[1].uri)
  let uri1;
  let uri2;
  let uri3;
  let uri4;
  let uri5;
  if (pickedAllImages.length > 0) {
    uri1 = pickedAllImages[0].uri;
  } else {
    uri1 = pickedImageUri[0];
  }
  if (pickedAllImages.length > 1) {
    uri2 = pickedAllImages[1].uri;
  } else {
    uri2 = pickedImageUri[1];
  }
  if (pickedAllImages.length > 2) {
    uri3 = pickedAllImages[2].uri;
  } else {
    uri3 = pickedImageUri[2];
  }
  if (pickedAllImages.length > 3) {
    uri4 = pickedAllImages[3].uri;
  } else {
    uri4 = pickedImageUri[3];
  }
  if (pickedAllImages.length > 4) {
    uri5 = pickedAllImages[4].uri;
  } else {
    uri5 = pickedImageUri[4];
  }

  const reduceArray1 = (text) => {
    const pickedAllImages1 = pickedAllImages.splice(0, 1);
    uri1 = uri2;
    setPickedImagesGallery1(text);
  };
  const reduceArray2 = (text) => {
    const pickedAllImages1 = pickedAllImages.splice(1, 1);
    uri2 = uri3;
    setPickedImagesGallery2(text);
  };
  const reduceArray3 = (text) => {
    const pickedAllImages1 = pickedAllImages.splice(2, 1);
    uri3 = uri4;
    setPickedImagesGallery3(text);
  };
  const reduceArray4 = (text) => {
    const pickedAllImages1 = pickedAllImages.splice(3, 1);
    uri4 = uri5;
    setPickedImagesGallery4(text);
  };
  const reduceArray5 = (text) => {
    const pickedAllImages1 = pickedAllImages.splice(4, 1);
    uri5 = "";
    setPickedImagesGallery5(text);
  };
  return (
    <SafeAreaView style={gS.container}>
      <ScrollView style={gS.scroll}>
        <Text
          style={{
            color: "red",
            margin: 20,
            backgroundColor: "#FDD",
            padding: 10,
            borderRadius: 10,
          }}
        >
          Dear valued customers, in line with CMCO, we are expecting delivery
          delays during this period. Your patience and understanding are highly
          appreciated. Stay safe! Thank you!
        </Text>

        <View style={gS.sectionScan}>
          {/*Member Category*/}
          <View style={gS.sectionScan}>
            <Text
              style={{
                fontSize: 18,
                fontFamily: "open-sans",
              }}
            >
              Member Category
            </Text>
            <View
                    style={{
                      color: "#F00",
                      // borderBottomWidt: 10,
                      borderColor: "#ccc",
                      borderBottomWidth: 1,
                    }}
                  >
            <Picker
              selectedValue={member_cat}
              style={styles.input}              
              onValueChange={(itemValue, itemIndex) => setMemberCat(itemValue)}
            >
               <Picker.Item label="- Select Member Category -" value="0" style={{ color: "#F00" }}
                      />
              <Picker.Item label="Caterer" value="caterer" />
              <Picker.Item label="Printing Service" value="printing" />
            </Picker>
            </View>
          </View>

          {/*Member Company Name*/}
          <View style={gS.sectionScan}>
            <Text
              style={{
                fontSize: 18,
                fontFamily: "open-sans",
                // marginLeft: 5,
                // marginTop: 8,
              }}
            >
              Member Company Name
            </Text>
            <View
                    style={{
                      color: "#F00",
                      // borderBottomWidt: 10,
                      borderColor: "#ccc",
                      borderBottomWidth: 1,
                    }}
                  >
            <Picker
              selectedValue={member_company}
              style={styles.input}              
              onValueChange={(itemValue, itemIndex) => setMemberCompany(itemValue)}
            >
               <Picker.Item label="- Select Company Name -" value="0" style={{ color: "#F00" }}/>
              <Picker.Item label="Business Booster Sdn Bhd" value="Business Booster" />
              <Picker.Item label="ABC Pvt Ltd" value="ABC Pvt Ltd" />
              <Picker.Item label="Maxis Sdn Bhd" value="Maxis Sdn Bhd" />
            </Picker>
            </View>
          </View>

          {/*Receipt Number*/}
          <View style={gS.sectionScan}>
            <Text
              style={{
                fontSize: 18,
                fontFamily: "open-sans",
                // marginLeft: 5,
                // marginTop: 8,
              }}
            >
              Receipt Number
            </Text>
            <TextInput
              style={styles.input}
              value={invoice_no}
              onChangeText={receiptHandler}
              placeholder={"ER525938"}
            />
          </View>
          {/* <View style={gS.sectionScan}>
            <Text
              style={{
                fontSize: 18,
                fontFamily: "open-sans",
                marginLeft: 5,
                // marginTop: 8,
              }}
            >
              Receipt Number
            </Text>
            <TextInput
              style={styles.input}
              value={invoice_no}
              onChangeText={receiptHandler}
              placeholder={"eg. ABC12345678"}
            />
          </View> */}

          {/*Receipt Date*/}
          <View style={gS.sectionScan}>
            <Text
              style={{
                fontSize: 18,
                fontFamily: "open-sans",
                // marginLeft: 5,
                // marginTop: 3,

              }}
            >
              Receipt Date
            </Text>

            <TouchableWithoutFeedback onPress={showDatePicker}>
              {date_of_receipt === "     mm/dd/yyyy" ? (
                <Text
                  // style={{
                  //   color: "#999",
                  // }}
                  style={styles.input2}
                >
                  {date_of_receipt.toString().substring(4, 15)}
                </Text>
              ) : (
                <Text
                  // style={{
                  //   color: "#999",
                  // }}
                  style={styles.input2True}
                >
                  {date_of_receipt.toString().substring(4, 15)}
                </Text>
              )}

              <DateTimePickerModal
                isVisible={isDatePickerVisible}
                mode="date"
                onConfirm={handleConfirm}
                onCancel={hideDatePicker}
              />
            </TouchableWithoutFeedback>
          </View>

          <View style={gS.sectionScan}>
            <Text
              style={{
                fontSize: 18,
                fontFamily: "open-sans",
                // marginLeft: 5,
                // marginTop: 3,
              }}
            >
              Upload Receipt
            </Text>

            <View style={gS.hori}>
              <View style={styles.card2}>
                <Icon
                  raised
                  name="camera"
                  type="font-awesome"
                  color="#7367F0"
                  size={40}
                  onPress={takeImageHandler}
                />
                <Text>Take Photo</Text>
              </View>

              <View style={styles.card2}>
                <Icon
                  raised
                  name="image"
                  type="font-awesome"
                  color="#7367F0"
                  size={40}
                  onPress={takeImageHandler2}
                />
                <Text>Upload from Gallery</Text>
                {pickedAllImages.length === 6 &&
                  Alert.alert(
                    "Muximum exceeded",
                    "You already selected five images",
                    [{ text: "Okay" }]
                  )}
              </View>
            </View>
          </View>
          <Text style={styles.notice}>
            You may upload up to 5 receipt photos.
          </Text>
          <Text></Text>
          {pickedAllImages.length <= 0 ? (
            <View style={gS.section}>
              <TouchableOpacity onPress={Sumbit}>
                <View style={gS.blueBtn}>
                  <Text style={gS.blueBtnWord}> Submit </Text>
                </View>
              </TouchableOpacity>
            </View>
          ) : (
            <Text></Text>
          )}

          <View style={gS.hori}>
            <View style={styles.imagePicker}>
              <View style={styles.imagePreview}>
                {pickedImage && pickedAllImages.length > 0 ? (
                  <Image style={styles.image} source={{ uri: uri1 }} />
                ) : (
                  <Image />
                )}
                {pickedImage && pickedAllImages.length > 0 ? (
                  <Icon
                    raised
                    name="close-a"
                    type="fontisto"
                    color="#DC143C"
                    size={17}
                    onPress={reduceArray1}
                    value={pickedImagesGallery1}
                  />
                ) : (
                  <Text />
                )}
              </View>
            </View>
            <View style={styles.imagePicker}>
              <View style={styles.imagePreview}>
                {pickedImage && pickedAllImages.length > 1 ? (
                  <Image style={styles.image} source={{ uri: uri2 }} />
                ) : (
                  <Image />
                )}
                {pickedImage && pickedAllImages.length > 1 ? (
                  <Icon
                    raised
                    name="close-a"
                    type="fontisto"
                    color="#DC143C"
                    size={17}
                    onPress={reduceArray2}
                    value={pickedImagesGallery2}
                  />
                ) : (
                  <Text />
                )}
              </View>
            </View>

            <View style={styles.imagePicker}>
              <View style={styles.imagePreview}>
                {pickedImage && pickedAllImages.length > 2 ? (
                  <Image style={styles.image} source={{ uri: uri3 }} />
                ) : (
                  <Image />
                )}
                {pickedImage && pickedAllImages.length > 2 ? (
                  <Icon
                    raised
                    name="close-a"
                    type="fontisto"
                    color="#DC143C"
                    size={17}
                    onPress={reduceArray3}
                    value={pickedImagesGallery3}
                  />
                ) : (
                  <Text />
                )}
              </View>
            </View>
          </View>
          <View style={gS.hori}>
            <View style={styles.imagePicker}>
              <View style={styles.imagePreview}>
                {pickedImage && pickedAllImages.length > 3 ? (
                  <Image style={styles.image} source={{ uri: uri4 }} />
                ) : (
                  <Image />
                )}
                {pickedImage && pickedAllImages.length > 3 ? (
                  <Icon
                    raised
                    name="close-a"
                    type="fontisto"
                    color="#DC143C"
                    size={17}
                    onPress={reduceArray4}
                    value={pickedImagesGallery4}
                  />
                ) : (
                  <Text />
                )}
              </View>
            </View>
            <View style={styles.imagePicker}>
              <View style={styles.imagePreview}>
                {pickedImage && pickedAllImages.length > 4 ? (
                  <Image style={styles.image} source={{ uri: uri5 }} />
                ) : (
                  <Image />
                )}
                {pickedImage && pickedAllImages.length > 4 ? (
                  <Icon
                    raised
                    name="close-a"
                    type="fontisto"
                    color="#DC143C"
                    size={17}
                    onPress={reduceArray5}
                    value={pickedImagesGallery5}
                  />
                ) : (
                  <Text />
                )}
              </View>
            </View>
          </View>
          {pickedAllImages.length > 0 && !loading ? (
            <View style={gS.section}>
              <TouchableOpacity onPress={Sumbit}>
                <View style={gS.blueBtn}>
                  <Text style={gS.blueBtnWord}> Submit </Text>
                </View>
              </TouchableOpacity>
            </View>
          ) : (
            <Text></Text>
          )}
        </View>
      </ScrollView>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  btn: {
    height: 50,
    backgroundColor: "white",
    fontSize: 16,
    margin: 20,
    borderColor: "gray",
    borderWidth: 1,
    borderRadius: 50,
    justifyContent: "center",
  },
  btnWord: {
    fontSize: 16,
    color: "#000",
    textAlign: "center",
  },

  card2: {
    flex: 1,
    width: 190,
    height: 150,
    backgroundColor: "#F9F9F9",
    justifyContent: "center",
    alignItems: "center",
    borderWidth: 0,
    borderRadius: 15,
    margin: 10,
    shadowColor: "#000",
    shadowOffset: { width: 0, height: 5 },
    shadowOpacity: 0.34,
    shadowRadius: 6.27,
    elevation: 10,
  },
  imagePicker: {
    alignItems: "stretch",
    marginBottom: 15,
  },
  imagePreview: {
    flex: 1,
    width: 100,
    height: 80,
    // backgroundColor: "#F9F9F9",
    justifyContent: "center",
    alignItems: "center",
    //borderWidth: 0,
    //borderRadius: 15,
    margin: 10,
    // shadowColor: "#000",
    // shadowOffset: { width: 0, height: 5, },
    // shadowOpacity: 0.34,
    // shadowRadius: 6.27,
    // elevation: 10,
  },
  image: {
    width: "100%",
    height: "100%",
  },
  centered: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
  },
  spinnerTextStyle: {
    color: "#FFF",
  },
  input: {
    paddingHorizontal: 2,
    paddingVertical: 5,
    borderBottomColor: "#ccc",
    borderBottomWidth: 1,
    fontSize: 16,
  },
  input2: {
    paddingHorizontal: 2,
    paddingVertical: 5,
    borderBottomColor: "#ccc",
    borderBottomWidth: 1,
    color: "#999",
    marginTop: 7,
    fontSize: 16,
  },
  input2True: {
    paddingHorizontal: 2,
    paddingVertical: 5,
    borderBottomColor: "#ccc",
    borderBottomWidth: 1,
    color: "#000000",
    marginTop: 7,
  },
});

export default Scan;
