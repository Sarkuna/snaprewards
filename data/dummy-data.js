import Category from "../models/category copy";
import Promotion from "../models/promotion";
import Highlight from "../models/highlight";
import Meal from "../models/meal";

// export const PROMOTIONS = [
//   new Promotion(
//     "p1",
//     "http://kansaiadmin.businessboosters.com.my/upload/product_cover/thumbnail/SPR0032.jpg"
//   ),
//   new Promotion("p2", "highlight2.jpeg"),
//   new Promotion("p3", "highlight3.jpeg"),
//   new Promotion("p4", "highlight4.jpeg"),
//   new Promotion("p5", "highlight5.jpeg"),
// ];

export const HIGHLIGHTS = [
  new Highlight(
    "h0",
    "Belkin 8 Way Surge+2s",
    "https://i.ibb.co/d5MhwkY/highlight1.jpg",
    "",
    ""
  ),
  new Highlight(
    "h1",
    "Camelbak Crux 2L",
    "https://i.ibb.co/k1Lf4cs/highlight2.jpg",
    "SPR 0060",
    "2200 - Pts"
  ),
  new Highlight(
    "h2",
    "Camelbak Bike Hydro black best qualaty",
    "https://i.ibb.co/RvH7XwN/highlight3.jpg",
    "SPR 0057",
    "2200 - Pts"
  ),
  new Highlight(
    "h3",
    "Breville Soft Top",
    "https://i.ibb.co/sR2fSKG/highlight4.jpg",
    "SPR 0053",
    "2200 - Pts"
  ),
  new Highlight(
    "h4",
    "Camelbak Bike Classic",
    "https://i.ibb.co/PxhKtP3/highlight5.jpg",
    "SPR 0056",
    "1320 - Pts"
  ),
];

export const PROMOTIONS = [
  new Promotion(
    "p0",
    "Belkin 8 Way Surge+2s",
    "https://i.ibb.co/hXwTzbx/promo1.jpg",
    "",
    ""
  ),
  new Promotion(
    "p1",
    "Camelbak Crux 2L",
    "https://i.ibb.co/cNmJ5fW/promo2.jpg",
    "SPR 0060",
    "2200 - Pts"
  ),
  new Promotion(
    "p2",
    "Camelbak Bike Hydro black best qualaty",
    "https://i.ibb.co/S0s9wjZ/promo3.jpg",
    "SPR 0057",
    "2200 - Pts"
  ),
  new Promotion(
    "p3",
    "Breville Soft Top",
    "https://i.ibb.co/rMZxS1H/promo4.jpg",
    "SPR 0053",
    "2200 - Pts"
  ),
  new Promotion(
    "p4",
    "Camelbak Bike Classic",
    "https://i.ibb.co/LCpkgfj/promo5.jpg",
    "SPR 0056",
    "1320 - Pts"
  ),
];

export const CATEGORIES = [
  new Category(
    "c1",
    "Belkin 8 Way Surge+2sssss",
    "http://kansaiadmin.businessboosters.com.my/upload/product_cover/thumbnail/SPR0032.jpg",
    "SPR 0032",
    "2200 - Pts"
  ),
  new Category(
    "c1",
    "Camelbak Crux 2L",
    "http://kansaiadmin.businessboosters.com.my/upload/product_cover/thumbnail/SPR0060.jpg",
    "SPR 0060",
    "2200 - Pts"
  ),
  new Category(
    "c2",
    "Camelbak Bike Hydro black best qualaty",
    "http://kansaiadmin.businessboosters.com.my/upload/product_cover/thumbnail/SPR0057.jpg",
    "SPR 0057",
    "2200 - Pts"
  ),
  new Category(
    "c3",
    "Breville Soft Top",
    "http://kansaiadmin.businessboosters.com.my/upload/product_cover/thumbnail/SPR0053.jpg",
    "SPR 0053",
    "2200 - Pts"
  ),
  new Category(
    "c4",
    "Camelbak Bike Classic",
    "http://kansaiadmin.businessboosters.com.my/upload/product_cover/thumbnail/SPR0056.jpg",
    "SPR 0056",
    "1320 - Pts"
  ),
  new Category(
    "c5",
    "Camelbak Crux 3L",
    "http://kansaiadmin.businessboosters.com.my/upload/product_cover/thumbnail/SPR0061.jpg",
    "SPR 0061",
    "2200 - Pts"
  ),
  new Category(
    "c6",
    "Camelbak Crux Clean",
    "http://kansaiadmin.businessboosters.com.my/upload/product_cover/thumbnail/SPR0062.jpg",
    "SPR 0062",
    "2200 - Pts"
  ),
  new Category(
    "c7",
    "Bosch ART 23 SL Elec",
    "http://kansaiadmin.businessboosters.com.my/upload/product_cover/thumbnail/SPR0044.jpg",
    "SPR 0044",
    "2200 - Pts"
  ),
  new Category(
    "c8",
    "Camelbak Delaney",
    "http://kansaiadmin.businessboosters.com.my/upload/product_cover/thumbnail/SPR0063.jpg",
    "SPR 0032",
    "2200 - Pts"
  ),
  new Category(
    "c9",
    "Bosch GBL 800 E",
    "http://kansaiadmin.businessboosters.com.my/upload/product_cover/thumbnail/SPR0045.jpg",
    "SSPR 0045",
    "2200 - Pts"
  ),
  new Category(
    "c10",
    "Camelbak Crux 1.5L",
    "http://kansaiadmin.businessboosters.com.my/upload/product_cover/thumbnail/SPR0059.jpg",
    "SPR 0059",
    "2200 - Pts"
  ),
  new Category(
    "c11",
    "Camelbak Bike Classic",
    "http://kansaiadmin.businessboosters.com.my/upload/product_cover/thumbnail/SPR0054.jpg",
    "SPR 0054",
    "2200 - Pts"
  ),
];

export const MEALS = [
  new Meal(
    "m1",
    ["c2"],
    "Toast Hawaii",
    "affordable",
    "simple",
    "https://www.blanchemacdonald.com/wp-content/uploads/2018/01/hair-salon-vancouver-student-salon.jpg",
    10,
    [
      "1 Slice White Bread",
      "1 Slice Ham",
      "1 Slice Pineapple",
      "1-2 Slices of Cheese",
      "Butter",
    ],
    [
      "Butter one side of the white bread",
      "Layer Ham, the pineapple and cheese on the white bread",
      "Bake the toast for round about 10 minutes in the oven at 200 c",
    ],
    false,
    false,
    false,
    false
  ),
  new Meal(
    "m2",
    ["c2"],
    "La Passion",
    "affordable",
    "simple",
    "https://pbs.twimg.com/media/CYj4cXxWsAECyjN.png",
    10,
    [
      "1 Slice White Bread",
      "1 Slice Ham",
      "1 Slice Pineapple",
      "1-2 Slices of Cheese",
      "Butter",
    ],
    [
      "Butter one side of the white bread",
      "Layer Ham, the pineapple and cheese on the white bread",
      "Bake the toast for round about 10 minutes in the oven at 200 c",
    ],
    false,
    false,
    false,
    false
  ),
  new Meal(
    "m3",
    ["c2"],
    "La Passion3",
    "affordable",
    "simple",
    "https://pbs.twimg.com/media/CYj4cXxWsAECyjN.png",
    10,
    [
      "1 Slice White Bread",
      "1 Slice Ham",
      "1 Slice Pineapple",
      "1-2 Slices of Cheese",
      "Butter",
    ],
    [
      "Butter one side of the white bread",
      "Layer Ham, the pineapple and cheese on the white bread",
      "Bake the toast for round about 10 minutes in the oven at 200 c",
    ],
    false,
    false,
    false,
    false
  ),
  new Meal(
    "m4",
    ["c2"],
    "La Passion4",
    "affordable",
    "simple",
    "https://pbs.twimg.com/media/CYj4cXxWsAECyjN.png",
    10,
    [
      "1 Slice White Bread",
      "1 Slice Ham",
      "1 Slice Pineapple",
      "1-2 Slices of Cheese",
      "Butter",
    ],
    [
      "Butter one side of the white bread",
      "Layer Ham, the pineapple and cheese on the white bread",
      "Bake the toast for round about 10 minutes in the oven at 200 c",
    ],
    false,
    false,
    false,
    false
  ),
  new Meal(
    "m5",
    ["c2"],
    "La Passion5",
    "affordable",
    "simple",
    "https://pbs.twimg.com/media/CYj4cXxWsAECyjN.png",
    10,
    [
      "1 Slice White Bread",
      "1 Slice Ham",
      "1 Slice Pineapple",
      "1-2 Slices of Cheese",
      "Butter",
    ],
    [
      "Butter one side of the white bread",
      "Layer Ham, the pineapple and cheese on the white bread",
      "Bake the toast for round about 10 minutes in the oven at 200 c",
    ],
    false,
    false,
    false,
    false
  ),
  new Meal(
    "m6",
    ["c1", "c2"],
    "La Passion4",
    "affordable",
    "simple",
    "https://pbs.twimg.com/media/CYj4cXxWsAECyjN.png",
    10,
    [
      "1 Slice White Bread",
      "1 Slice Ham",
      "1 Slice Pineapple",
      "1-2 Slices of Cheese",
      "Butter",
    ],
    [
      "Butter one side of the white bread",
      "Layer Ham, the pineapple and cheese on the white bread",
      "Bake the toast for round about 10 minutes in the oven at 200 c",
    ],
    false,
    false,
    false,
    false
  ),
];
