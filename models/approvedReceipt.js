class Approved {
  constructor(
    vip_receipt_id,
    created_datetime,
    date_of_receipt,
    invoice_no,
    reseller_name,
    totalItem,
    totalpoint,
    resellername
  ) {
    this.vip_receipt_id = vip_receipt_id;
    this.created_datetime = created_datetime;
    this.date_of_receipt = date_of_receipt;
    this.invoice_no = invoice_no;
    this.reseller_name = reseller_name;
    this.totalItem = totalItem;
    this.totalpoint = totalpoint;
    this.resellername = resellername;
  }
}

export default Approved;
