class Category {
  constructor(id, title, color, item, pts) {
    this.id = id;
    this.title = title;
    this.color = color;
    this.item = item;
    this.pts = pts;
  }
}

export default Category;
