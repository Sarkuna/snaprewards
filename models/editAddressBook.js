class EditAddress {
  constructor(company, country, id, postcode, state) {
    this.company = company;
    this.country = country;
    this.id = id;
    this.postcode = postcode;
    this.state = state;
  }
}
//other fields
// //resData1[key].Address 1,
//         // resData1[key].Address 2,
//         resData1[key].Company,
//         resData1[key].Country,
//         // resData1[key].Firsr Name,
//         resData1[key].Id,
//         //resData1[key].Last Name,
//         resData1[key].Postcode,
//         resData1[key].State
export default EditAddress;
