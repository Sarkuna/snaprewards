class Bank {
  constructor(
    id,
    bank_name,
    account_name,
    account_number,
    nric_passport,
    verify
  ) {
    this.id = id;
    this.bank_name = bank_name;
    this.account_name = account_name;
    this.account_number = account_number;
    this.nric_passport = nric_passport;
    this.verify = verify;
  }
}

export default Bank;
