class Address {
  constructor(
    address,
    country,
    default_address,
    id,
    name,
    postcode,
    stateid,
    statename
  ) {
    this.address = address;
    this.country = country;
    this.default_address = default_address;
    this.id = id;
    this.name = name;
    this.postcode = postcode;
    this.stateid = stateid;
    this.statename = statename;
  }
}

export default Address;
