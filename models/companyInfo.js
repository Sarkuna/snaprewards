class Company {
  constructor(
    company_name,
    contact_no,
    contact_person,
    fax,
    mailing_address,
    registration_number,
    registration_type,
    tel
  ) {
    this.company_name = company_name;
    this.contact_no = contact_no;
    this.contact_person = contact_person;
    this.fax = fax;
    this.mailing_address = mailing_address;
    this.registration_number = registration_number;
    this.registration_type = registration_type;
    this.tel = tel;
  }
}

export default Company;
