class Category {
    constructor(id ,  title, link,target,parent,sort,created_at,updated_at, icon_font,menu_id,related_id,type,depth){
        this.id = id
        this.title = title
        this.link = link
        this.target = target
        this.parent = parent
        this.sort = sort
        this.created_at = created_at
        this.updated_at = updated_at
        this.icon_font = icon_font
        this.menu_id = menu_id
        this.related_id = related_id
        this.type = type
        this.depth = depth
    }
}

export default Category;