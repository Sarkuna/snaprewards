import * as React from "react";
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";

import Login from "../src/login_file/login";
import ResetPassword from "../src/login_file/resetPassword";
import Register from "../src/login_file/register";

import HomeScreen from "../src/home_file/home";
import ScanPage from "../src/home_file/scan";

import Settings from "../src/setting_file1/setting";
import MyPages from "../src/setting_file1/myPages";
import MyApplication from "../src/setting_file1/myApplication";
import Profile from "../src/setting_file1/profile";

import EditSetting from "../src/setting_file2/editSettings";
import ChangePassword from "../src/setting_file2/changePassword";
import Lang from "../src/setting_file2/language";
import Company from "../src/setting_file2/companyInfo";
import EditCompanyInfo from "../src/setting_file2/editCompanyInfo";
import AddressBook from "../src/setting_file2/addressBook";
import AddAddress from "../src/setting_file2/addAddress";
import EditAddress from "../src/setting_file2/editAddress";
import AddBank from "../src/setting_file2/addBank";
import EditBank from "../src/setting_file2/editBank";
import Bank from "../src/setting_file2/bankinfo";
import AboutApp from "../src/setting_file2/aboutApp";

import Approved from "../src/receipt_file/approved";
import Pending from "../src/receipt_file/pending";
import Processing from "../src/receipt_file/processing";

import MyPage, { screenOptions } from "../src/home_file/myPage";
import Highlight, { screenOptionsH } from "../src/home_file/highlight";
import Promotion, { screenOptionsPromotion } from "../src/home_file/promotion";
import DetailsScreen, { screenOptions1 } from "../src/home_file/detailScreen";

import General from "../src/general";

const Stack = createStackNavigator();

global.url = "http://api.businessboosters.com.my";
global.kansai = "http://kansai.businessboosters.com.my";

export const SettingMain = () => {
  return (
    <Stack.Navigator>
      <Stack.Screen
        name="EditSetting"
        component={EditSetting}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name="ChangePassword"
        component={ChangePassword}
        options={{ headerShown: false }}
      />
    </Stack.Navigator>
  );
};

export const SettingHome = () => {
  return (
    <Stack.Navigator>
      <Stack.Screen
        name="Setting"
        component={Settings}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name="MyPages"
        component={MyPages}
        options={{ headerShown: true }}
        options={{
          title: "My Pages",
          headerStyle: { backgroundColor: "#7367F0" },
          headerTintColor: "white",
        }}
      />
      <Stack.Screen
        name="AboutApp"
        component={AboutApp}
        options={{ headerShown: true }}
        options={{
          title: "About App",
          headerStyle: { backgroundColor: "#7367F0" },
          headerTintColor: "white",
        }}
      />

      <Stack.Screen
        name="MyApplication"
        component={MyApplication}
        options={{ headerShown: true }}
        options={{
          title: "My Application",
          headerStyle: { backgroundColor: "#7367F0" },
          headerTintColor: "white",
        }}
      />

      <Stack.Screen
        name="SettingMain"
        component={SettingMain}
        options={{
          headerShown: true,
          title: "Settings",
          headerStyle: { backgroundColor: "#7367F0" },
          headerTintColor: "white",
        }}
      />

      <Stack.Screen
        name="Profile"
        component={Profile}
        options={{ headerShown: true }}
        options={{
          title: "Edit Profile",
          headerStyle: { backgroundColor: "#7367F0" },
          headerTintColor: "white",
        }}
      />
      <Stack.Screen
        name="Lang"
        component={Lang}
        options={{ headerShown: true }}
        options={{
          title: "Language",
          headerStyle: { backgroundColor: "#7367F0" },
          headerTintColor: "white",
        }}
      />

      <Stack.Screen
        name="Bank"
        component={Bank}
        options={{ headerShown: true }}
        options={{
          title: "Bank Info",
          headerStyle: { backgroundColor: "#7367F0" },
          headerTintColor: "white",
        }}
      />

      <Stack.Screen
        name="Company"
        component={Company}
        options={{ headerShown: true }}
        options={{
          title: "Company Info",
          headerStyle: { backgroundColor: "#7367F0" },
          headerTintColor: "white",
        }}
      />
      <Stack.Screen
        name="EditCompanyInfo"
        component={EditCompanyInfo}
        options={{ headerShown: true }}
        options={{
          title: "Edit",
          headerStyle: { backgroundColor: "#7367F0" },
          headerTintColor: "white",
        }}
      />
      <Stack.Screen
        name="AddressBook"
        component={AddressBook}
        options={{ headerShown: true }}
        options={{
          title: "Address Book",
          headerStyle: { backgroundColor: "#7367F0" },
          headerTintColor: "white",
        }}
      />
      <Stack.Screen
        name="AddAddress"
        component={AddAddress}
        options={{ headerShown: true }}
        options={{
          title: "Add Address",
          headerStyle: { backgroundColor: "#7367F0" },
          headerTintColor: "white",
        }}
      />
      <Stack.Screen
        name="EditAddress"
        component={EditAddress}
        options={{ headerShown: true }}
        options={{
          title: "Edit Address",
          headerStyle: { backgroundColor: "#7367F0" },
          headerTintColor: "white",
        }}
      />
      <Stack.Screen
        name="AddBank"
        component={AddBank}
        options={{ headerShown: true }}
        options={{
          title: "Add Bank",
          headerStyle: { backgroundColor: "#7367F0" },
          headerTintColor: "white",
        }}
      />
      <Stack.Screen
        name="EditBank"
        component={EditBank}
        options={{ headerShown: true }}
        options={{
          title: "Edit Bank",
          headerStyle: { backgroundColor: "#7367F0" },
          headerTintColor: "white",
        }}
      />
    </Stack.Navigator>
  );
};

export const HomeMain = () => {
  return (
    <Stack.Navigator>
      <Stack.Screen
        name="Login"
        component={Login}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name="Home"
        component={HomeScreen}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name="SettingHome"
        component={SettingHome}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name="Scan"
        component={ScanPage}
        options={{ headerShown: true }}
        options={{
          title: "Upload Receipt",
          headerStyle: { backgroundColor: "#7367F0" },
          headerTintColor: "white",
        }}
      />

      <Stack.Screen
        name="General"
        component={General}
        options={{ headerShown: true }}
        options={{
          title: "General Page",
          headerStyle: { backgroundColor: "#7367F0" },
          headerTintColor: "white",
        }}
      />

      <Stack.Screen
        name="Approved"
        component={Approved}
        options={{ headerShown: true }}
        options={{
          title: "Approved Receipt",
          headerStyle: { backgroundColor: "#7367F0" },
          headerTintColor: "white",
        }}
      />
      <Stack.Screen
        name="Pending"
        component={Pending}
        options={{ headerShown: true }}
        options={{
          title: "Pending Receipt",
          headerStyle: { backgroundColor: "#7367F0" },
          headerTintColor: "white",
        }}
      />
      <Stack.Screen
        name="Processing"
        component={Processing}
        options={{ headerShown: true }}
        options={{
          title: "Processing Receipt",
          headerStyle: { backgroundColor: "#7367F0" },
          headerTintColor: "white",
        }}
      />
      <Stack.Screen name="MyPage" component={MyPage} options={screenOptions} />
      <Stack.Screen
        name="Highlight"
        component={Highlight}
        options={screenOptionsH}
      />
      <Stack.Screen
        name="Promotion"
        component={Promotion}
        options={screenOptionsPromotion}
      />
      <Stack.Screen
        name="DetailsScreen"
        component={DetailsScreen}
        options={screenOptions1}
      />
      <Stack.Screen
        name="ResetPassword"
        component={ResetPassword}
        options={{ headerShown: true }}
        options={{
          title: "Forgot Password",
          headerStyle: { backgroundColor: "#7367F0" },
          headerTintColor: "white",
        }}
      />
      <Stack.Screen
        name="Register"
        component={Register}
        options={{ headerShown: true }}
        options={{
          title: "Sign Up",
          headerStyle: { backgroundColor: "#7367F0" },
          headerTintColor: "white",
        }}
      />
      <Stack.Screen
        name="HomeMain"
        component={HomeMain}
        options={{ headerShown: false }}
      />
    </Stack.Navigator>
  );
};

const AppNavigator = (props) => {
  console.log("Application Start!!!");

  return (
    <NavigationContainer>
      <HomeMain />
    </NavigationContainer>
  );
};

export default AppNavigator;
