import React from "react";
import gS from "../src/globalStyle";
import {
  TouchableOpacity,
  View,
  Text,
  StyleSheet,
  Platform,
  TouchableNativeFeedback,
  Image,
  ScrollView,
} from "react-native";
import SafeAreaView from "react-native-safe-area-view";
import { Icon, Badge } from "react-native-elements";

//import ENV from "../apiEnv";

const CategoryGridTile = (props) => {
  let TouchableCmp = TouchableNativeFeedback;

  if (Platform.OS === "android" && Platform.Version >= 21) {
    TouchableCmp = TouchableNativeFeedback;
  }

  return (
    // <View>
    //   <View style={styles.gridItem}>
    //     <View style={styles.touchable}>
    //       <TouchableCmp onPress={props.onSelect}>
    //         <View>
    //           <View>
    //             <View style={styles.imageContainer}>
    //               <Image style={styles.image} source={{ uri: props.image }} />
    //             </View>
    //           </View>
    //         </View>
    //       </TouchableCmp>
    //     </View>
    //   </View>
    //   <View>
    //     <Text style={styles.title}>{props.title}</Text>
    //   </View>
    // </View>

    // <SafeAreaView style={gS.container}>
    //   <ScrollView style={gS.scroll}>
    // {/* <Text style={gS.sectionTitle}>My Applications</Text> */}
    <View>
      <TouchableOpacity onPress={props.onSelect}>
        <View style={styles.settingSection}>
          {/* <Image
            style={
              (styles.image, { position: "absolute", right: 15, margin: 10 })
            }
            source={{ uri: props.image }}
          /> */}
          <View style={styles.iconB}>
            <Icon name="long-arrow-right" type="font-awesome" color="black" />
            {/* <Image
              style={styles.image}
              source={{
                uri: "http://kansaiadmin.businessboosters.com.my/upload/product_cover/thumbnail/SPR0032.jpg",
              }}
            /> */}
          </View>
          <Text style={styles.wordA}>{props.title}</Text>
        </View>
      </TouchableOpacity>

      <View style={styles.line}></View>
    </View>
    //   </ScrollView>
    // </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  gridItem: {
    shadowColor: "black",
    shadowOpacity: 0.26,
    shadowOffset: { width: 0, height: 2 },
    shadowRadius: 8,
    elevation: 5,
    borderRadius: 10,
    height: 60,
    width: 60,
    margin: 20,
    backgroundColor: "white",
  },
  touchable: {
    borderRadius: 10,
    overflow: "hidden",
  },
  image: {
    flex: 1,
    // height: "20%",
    // width: "20%",
    resizeMode: "contain",
  },
  imageContainer: {
    height: "100%",
    width: "100%",
    borderTopLeftRadius: 10,
    borderTopRightRadius: 10,
    overflow: "hidden",
  },
  title: {
    //fontFamily: "open-sans-bold",
    fontSize: 14,
    fontWeight: "900",
    textAlign: "center",
  },
  line: {
    width: "100%",
    height: 1,
    backgroundColor: "lightgray",
    marginLeft: "15%",
  },
  settingSection: {
    padding: 10,
    backgroundColor: "white",
    flexDirection: "row",
    alignItems: "center",
  },
  iconB: {
    margin: 10,
  },
  wordA: {
    color: "black",
    fontSize: 16,
    position: "absolute",
    left: "15%",
  },
});

export default CategoryGridTile;
