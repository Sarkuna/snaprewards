import React from "react";
import {
  TouchableOpacity,
  View,
  Text,
  StyleSheet,
  Platform,
  TouchableNativeFeedback,
  Image,
} from "react-native";
import { LinearGradient } from "expo-linear-gradient";
import { Icon } from "react-native-elements";
const CompanyInfoDetail = (props) => {
  let TouchableCmp = TouchableNativeFeedback;

  if (Platform.OS === "android" && Platform.Version >= 21) {
    TouchableCmp = TouchableNativeFeedback;
  }
  // onLongPress={defaultBtn}
  return (
    <View>
      <View style={styles.line}></View>
      <TouchableOpacity onLongPress={props.defaultBtn}>
        <View style={styles.settingSection}>
          <View style={styles.iconAlign}>
            {/* <View style={styles.iconA}>
              <Icon name="map-marker" type="font-awesome" color="#285aed" />
            </View> */}
            <View style={styles.addressMain}>
              <Text style={styles.addressName}>Company Name</Text>

              <Text style={styles.address}>{props.name}</Text>

              <View style={{ marginTop: 18 }}>
                <Text style={styles.addressName}>
                  Registration Type & Number
                </Text>
                <Text style={styles.address}>{props.regType}</Text>
                <Text style={styles.address}>{props.regNum}</Text>
              </View>
              <View style={{ marginTop: 18 }}>
                <Text style={styles.addressName}>Contact Person</Text>
                <Text style={styles.address}>{props.personName}</Text>
                <Text style={styles.address}>{props.personNumber}</Text>
              </View>
              <View style={{ marginTop: 18 }}>
                <Text style={styles.addressName}>Address</Text>
                <View style={{ marginTop: 2 }}>
                  <Text style={styles.address}>{props.address1}</Text>
                  <Text style={styles.address}>{props.address2}</Text>
                </View>
              </View>
              <View
                style={{
                  marginTop: 18,
                  flex: 1,
                  flexDirection: "row",
                  marginLeft: "2%",
                }}
              >
                <Text style={styles.addressName}>Tel: </Text>
                <Text style={styles.addressRow}>{props.tel}</Text>
              </View>
              <View
                style={{
                  marginTop: 18,
                  flex: 1,
                  flexDirection: "row",
                  marginLeft: "2%",
                }}
              >
                <Text style={styles.addressName}>Fax: </Text>
                <Text style={styles.addressRow}>{props.fax}</Text>
              </View>

              {/* {props.default_address === 1 ? (
                  <LinearGradient
                    // Button Linear Gradient

                    // tvParallaxTiltAngle={-176}
                    colors={["#3eb1f0", "#346cdd"]}
                    start={{ x: 0, y: 0.5 }}
                    end={{ x: 1, y: 0.5 }}
                    style={styles.button}
                  >
                    <View style={{ alignItems: "center", marginTop: 1 }}>
                      <Text style={{ fontSize: 12, color: "#ffffff" }}>
                        Default
                      </Text>
                    </View>
                  </LinearGradient>
                ) : (
                  <View></View>
                )} */}

              {/* <Text style={styles.address}>
                  Cecilia Chapman 711-2880 Nulla St. Mankato Mississippi 96522
                  (257) 563-7401
                </Text> */}
            </View>
            <View style={styles.iconB}>
              <Icon
                name="pencil-square-o"
                type="font-awesome"
                color="#66F"
                onPress={props.onEdit}
                // onPress={() => navigation.navigate("EditAddress")}
              />
            </View>
          </View>
        </View>
      </TouchableOpacity>
      <View style={styles.line}></View>
    </View>
  );
};

export default CompanyInfoDetail;

const styles = StyleSheet.create({
  settingSection: {
    padding: 10,
    backgroundColor: "white",
    flexDirection: "row",
    alignItems: "flex-start",
  },
  line: {
    width: "100%",
    height: 1,
    backgroundColor: "lightgray",
    //marginLeft: "2%",
  },
  iconAlign: {
    flex: 1,
    flexDirection: "row",
    justifyContent: "space-between",
    // alignItems: "center",
    // padding: 10,
    // marginLeft: 10,
    // marginRight: 10,
    margin: 10,
  },
  addressMain: {
    flexDirection: "column",
    flex: 1,
  },
  name: {
    color: "black",
    fontSize: 16,
    left: "2%",
    //  marginTop: 5,
    fontFamily: "open-sans",
  },
  phone: {
    color: "black",
    fontSize: 16,
    left: "2%",
    marginTop: 10,
    fontFamily: "open-sans",
  },
  address: {
    color: "#606060",
    fontSize: 16,
    left: "2%",
    marginTop: 10,
    marginRight: 15,
    fontFamily: "open-sans",
  },
  addressRow: {
    color: "#606060",
    fontSize: 16,
    left: "2%",
    //marginTop: 10,
    marginRight: 15,
    fontFamily: "open-sans",
  },
  addressDefaultAlign: {
    flex: 1,
    flexDirection: "row",
    justifyContent: "flex-start",
    // alignItems: "center",
    // padding: 10,
    // marginLeft: 10,
    // marginRight: 10,
    margin: 10,
  },
  button: {
    //padding: 15,
    //width: "100%",
    //flex: 1,
    height: 20,
    marginTop: 10,
    width: 50,
    left: "12%",
    marginRight: 10,
    //flexDirection: "row",
    borderRadius: 8,
    // alignItems: "center",
    //margin: 10,

    // shadowColor: "#000",
    // shadowOffset: { width: 0, height: 5 },
    // shadowOpacity: 0.34,
    // shadowRadius: 6.27,
    // elevation: 5,
  },
  addressName: {
    color: "#606060",
    fontSize: 17,

    left: "2%",
    //  marginTop: 5,
    //width: 190,
    fontFamily: "open-sans-bold",
  },
});
