import React from "react";
import {
  TouchableOpacity,
  View,
  Text,
  StyleSheet,
  Platform,
  TouchableNativeFeedback,
  Image,
} from "react-native";

const CategoryGridTile = (props) => {
  let TouchableCmp = TouchableNativeFeedback;

  if (Platform.OS === "android" && Platform.Version >= 21) {
    TouchableCmp = TouchableNativeFeedback;
  }

  return (
    <View style={styles.gridItem}>
      <TouchableCmp style={{ flex: 1 }} onPress={props.onSelect}>
        <View style={{ ...styles.container }}>
          <Image
            style={styles.image}
            source={{
              uri: props.color,
            }}
          />
          <View
            style={{ flexDirection: "column", justifyContent: "space-between" }}
          >
            <Text style={styles.title1} numberOfLines={2}>
              {props.item}
            </Text>
            <Text style={styles.title2} numberOfLines={2}>
              {props.pts}
            </Text>
          </View>

          <Text style={styles.title} numberOfLines={2}>
            {props.title}
          </Text>
        </View>
      </TouchableCmp>
    </View>
  );
};

const styles = StyleSheet.create({
  gridItem: {
    flex: 1,
    flexDirection: "row",
    margin: 10,
    height: 300,
    borderRadius: 10,
    // borderColor: "#f54278",
    // borderColor: "black",
    elevation: 0.5,
    overflow: "hidden",
    //   Platform.OS === "android" && Platform.Version >= 21
    //     ? "hidden"
    //     : "visible",
    backgroundColor: "#ffffff",
  },
  container: {
    flex: 1,
    //borderRadius: 15,
    //shadowColor: "black",
    //shadowOpacity: 0.26,
    // shadowOffset: { width: 0, height: 2 },
    // shadowRadius: 10,
    // elevation: 0.5,
    // borderColor: "black",
    padding: 25,
    justifyContent: "center",
    alignItems: "center",
  },
  title: {
    fontFamily: "open-sans",
    fontSize: 18,
    textAlign: "center",
    color: "#706f6f",
    padding: 10,
  },
  title1: {
    /* fontFamily: 'open-sans-bold', */
    fontSize: 13,
    textAlign: "center",
    color: "#706f6f",
  },
  title2: {
    fontSize: 14,
    textAlign: "center",
    color: "black",
  },
  image: {
    height: "80%",
    width: "100%",
    resizeMode: "contain",
  },
});

export default CategoryGridTile;
