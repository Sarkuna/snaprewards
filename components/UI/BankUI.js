import React, { useState } from "react";
import {
  TouchableOpacity,
  View,
  Text,
  StyleSheet,
  Platform,
  TouchableNativeFeedback,
  Image,
} from "react-native";
import { LinearGradient } from "expo-linear-gradient";
import { Icon } from "react-native-elements";
const AddressBookDetail = (props) => {
  let TouchableCmp = TouchableNativeFeedback;

  if (Platform.OS === "android" && Platform.Version >= 21) {
    TouchableCmp = TouchableNativeFeedback;
  }
  const [showDetails, setshowDetails] = useState(false);
  return (
    <View>
      <View style={styles.line}></View>
      <TouchableOpacity>
        <View style={styles.settingSection}>
          <View style={styles.iconAlign}>
            <View style={styles.alignFirstColoumn}>
              <View style={{ flex: 1, flexDirection: "row" }}>
                <Text style={styles.name}>Bank Name {"      "}:</Text>
                <Text numberOfLines={1} style={styles.bankName}>
                  {props.bank_name}
                </Text>
              </View>
              <Text style={styles.name}>
                Account Name : {"  "}
                {props.account_name}
              </Text>

              <LinearGradient
                // Background Linear Gradient
                colors={["rgba(0,0,0,0.8)", "transparent"]}
                style={styles.background}
              />
              {/* <LinearGradient
                // Button Linear Gradient

                // tvParallaxTiltAngle={-176}
                colors={["#3eb1f0", "#346cdd"]}
                start={{ x: 0, y: 0.5 }}
                end={{ x: 1, y: 0.5 }}
                style={styles.button}
              >
                <View style={{ alignItems: "center", marginTop: 1 }}>
                  <Text style={{ fontSize: 12, color: "#ffffff" }}>
                    Default
                  </Text>
                </View>
              </LinearGradient> */}
              {showDetails && (
                <View
                  style={{
                    top: 8,
                    flex: 1,
                    flexDirection: "column",
                    justifyContent: "space-between",
                  }}
                >
                  <Text style={styles.dateTime}>
                    Account Number : {props.account_number}
                  </Text>
                  <Text style={styles.receiptDate}>
                    NRIC/Passport : {props.nric_passport}
                  </Text>
                  <View
                    style={{
                      flex: 1,
                      flexDirection: "row",
                      //  justifyContent: "flex-end",
                      marginBottom: 10,
                    }}
                  >
                    <Text style={styles.statusVerification}>
                      Verify : {props.verify}
                    </Text>
                    <View
                      style={{
                        flex: 1,
                        marginTop: 5,
                        //left: 5,
                        //marginLeft: 120,
                        flexDirection: "row",
                        justifyContent: "flex-end",
                        marginRight: -50,
                      }}
                    >
                      {/* <View style={{ marginLeft: 100 }}>
                          <Icon
                            name="trash-o"
                            type="font-awesome"
                            color="#66F"
                          />
                        </View> */}
                      <View style={{ marginLeft: 20 }}>
                        <Icon
                          onPress={props.onEdit}
                          name="pencil-square-o"
                          type="font-awesome"
                          color="#66F"
                        />
                      </View>
                    </View>
                  </View>
                </View>
              )}
            </View>
            <Icon
              raised
              type="fontisto"
              color="#03145c"
              size={20}
              name={showDetails ? "angle-up" : "angle-down"}
              onPress={() => {
                setshowDetails((prevState) => !prevState);
              }}
            />
          </View>
        </View>
      </TouchableOpacity>
      <View style={styles.line}></View>
    </View>
  );
};

const styles = StyleSheet.create({
  settingSection: {
    padding: 10,
    backgroundColor: "white",
    flexDirection: "row",
    alignItems: "flex-start",
  },
  line: {
    width: "100%",
    height: 1,
    backgroundColor: "lightgray",
    marginLeft: "5%",
  },
  iconAlign: {
    flex: 1,
    flexDirection: "row",
    justifyContent: "space-between",
    // alignItems: "center",
    // padding: 10,
    // marginLeft: 10,
    // marginRight: 10,
  },
  addressMain: {
    flexDirection: "column",
    flex: 1,
  },
  name: {
    color: "#606060",
    fontSize: 15,
    marginLeft: 10,
    // left: "5%",
    //  marginTop: 5,

    fontFamily: "open-sans-bold",
  },
  bankName: {
    color: "#606060",
    fontSize: 15,
    marginLeft: 10,
    // left: "5%",
    //  marginTop: 5,
    width: 190,
    fontFamily: "open-sans-bold",
  },
  phone: {
    color: "black",
    fontSize: 16,
    left: "5%",
    marginTop: 10,
    fontFamily: "open-sans",
  },
  address: {
    color: "black",
    fontSize: 14,
    left: "5%",
    marginTop: 10,
    marginRight: 15,
    fontFamily: "open-sans",
  },
  addressDefaultAlign: {
    flex: 1,
    flexDirection: "row",
    justifyContent: "flex-start",
    // alignItems: "center",
    // padding: 10,
    // marginLeft: 10,
    // marginRight: 10,
    margin: 10,
  },
  button: {
    //padding: 15,
    //width: "100%",
    //flex: 1,
    height: 20,
    marginTop: 10,
    width: 50,
    marginLeft: 10,
    // marginRight: 10,
    //flexDirection: "row",
    borderRadius: 8,
    // alignItems: "center",
    //margin: 10,

    // shadowColor: "#000",
    // shadowOffset: { width: 0, height: 5 },
    // shadowOpacity: 0.34,
    // shadowRadius: 6.27,
    // elevation: 5,
  },
  alignFirstColoumn: {
    flexDirection: "column",
    flex: 1,
  },
  dateTime: {
    fontFamily: "open-sans",
    fontSize: 17,
    fontWeight: "500",
    textAlign: "left",
    marginLeft: 10,
  },
  receiptDate: {
    fontFamily: "open-sans",
    fontSize: 17,
    fontWeight: "500",
    textAlign: "left",
    marginLeft: 10,
    marginTop: 5,
  },
  statusVerification: {
    fontFamily: "open-sans",
    fontSize: 17,
    fontWeight: "500",
    textAlign: "left",
    marginLeft: 10,
    marginTop: 5,
  },
});

export default AddressBookDetail;
