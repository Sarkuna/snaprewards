import React, { useState } from "react";
import { View, Text, Button, StyleSheet } from "react-native";
import { Icon } from "react-native-elements";
import CartItem from "./CartItem";
import Colors from "../../constants/Colors";
import Card from "../UI/Card";

const OrderItem = (props) => {
  const [showDetails, setshowDetails] = useState(false);

  return (
    <Card style={styles.orderItem}>
      <View style={styles.summary}>
        {/* <Text style={styles.date}>Added Date : {props.created_datetime}</Text> */}

        {/* <Text style={styles.date}>Distributor : {props.reseller_name}</Text> */}
      </View>
      {/* <Button
        color="#10298f"
        title={showDetails ? "Hide Details" : "Show Details"}
        onPress={() => {
          setshowDetails((prevState) => !prevState);
        }}
      /> */}

      <View style={styles.detailItems}>
        <View style={styles.rowItem}>
          <Text style={styles.date}>Added Date{"    "} : </Text>
          <Text style={styles.mainText}> {props.created_datetime}</Text>
        </View>
        <View style={styles.rowItem}>
          <Text style={styles.date}>Invoice No{"      "} : </Text>
          <Text style={styles.mainText}> {props.items}</Text>
        </View>
        <View style={styles.rowItem}>
          <Text style={styles.date}>Receipt Date{"  "} : </Text>
          <Text style={styles.mainText}> {props.date_of_receipt}</Text>
        </View>
        {/* <Text style={styles.mainText}>Invoice No : {props.items}</Text>
        <Text style={styles.mainText}>
          Receipt Date : {props.date_of_receipt}
        </Text> */}
        {/* invoice_no={cartItem.invoice_no}
              reseller_name={cartItem.reseller_name} */}
      </View>
    </Card>
  );
};

const styles = StyleSheet.create({
  orderItem: {
    margin: 3,
    padding: 5,
    alignItems: "center",
  },
  rowItem: {
    flex: 1,

    flexDirection: "row",
    //justifyContent: "space-between",
  },
  summary: {
    flexDirection: "column",
    justifyContent: "space-between",
    alignItems: "center",
    width: "100%",
  },
  amount: {
    fontFamily: "open-sans-bold",
    fontSize: 16,
  },
  date: {
    fontFamily: "open-sans-bold",
    fontSize: 16,
    color: "#888",
  },
  detailItems: {
    width: "100%",
  },
  mainText: {
    fontFamily: "open-sans-bold",
    fontSize: 16,
  },
  mainText: {
    fontFamily: "open-sans-bold",
    fontSize: 16,
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    width: "100%",
  },
  icon: {
    flexDirection: "column",
    alignItems: "stretch",
    padding: 5,
  },
});

export default OrderItem;
