import React from "react";
import {
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  Platform,
} from "react-native";

const CartItem = (props) => {
  return (
    <View style={styles.CartItems}>
      <Text style={styles.itemData}>
        <Text style={styles.quantity}>{props.invoice_no}</Text>
        <Text style={styles.mainText}>{props.reseller_name}</Text>
      </Text>
      <View style={styles.itemData}></View>
    </View>
  );
};

const styles = StyleSheet.create({
  CartItems: {
    padding: 10,
    backgroundColor: "white",
    flexDirection: "row",
    justifyContent: "space-between",
    marginHorizontal: 20,
  },
  itemData: {
    flexDirection: "row",
    alignItems: "center",
  },
  quantity: {
    fontFamily: "open-sans",
    color: "#888",
    fontSize: 16,
  },
  mainText: {
    fontFamily: "open-sans-bold",
    fontSize: 16,
  },
  deleteButton: {
    marginLeft: 20,
  },
});

export default CartItem;
