import React, { useState } from "react";
import { View, Text, Button, StyleSheet } from "react-native";
import { Icon } from "react-native-elements";
import CartItem from "../../components/shop/CartItem";
import Colors from "../../constants/Colors";
import Card from "../UI/Card";

const BankDetails = (props) => {
  const [showDetails, setshowDetails] = useState(false);

  return (
    <Card style={styles.orderItem}>
      <View style={styles.summary2}>
        <View style={styles.summary}>
          <Text style={styles.date}>Added Date : {props.created_datetime}</Text>
          <Text style={styles.date}>Reseler Name : {props.resellername}</Text>
        </View>
        <Icon
          raised
          type="fontisto"
          color="#03145c"
          size={20}
          name={showDetails ? "angle-up" : "angle-down"}
          onPress={() => {
            setshowDetails((prevState) => !prevState);
          }}
        />
      </View>
      {/* <Button
        color="#10298f"
        title={showDetails ? "Hide Details" : "Show Details"}
        onPress={() => {
          setshowDetails((prevState) => !prevState);
        }}
      /> */}

      {showDetails && (
        <View style={styles.detailItems}>
          <Text style={styles.mainText}>Invoice No : {props.items}</Text>
          <Text style={styles.mainText}>
            Receipt Date : {props.date_of_receipt}
          </Text>
          <Text style={styles.mainText}>Total Items : {props.totalItem}</Text>
          <Text style={styles.mainText}>Total Points : {props.totalPoint}</Text>
          {/* invoice_no={cartItem.invoice_no}
              reseller_name={cartItem.reseller_name} */}
        </View>
      )}
    </Card>
  );
};

const styles = StyleSheet.create({
  orderItem: {
    margin: 20,
    padding: 10,
    alignItems: "center",
  },
  summary: {
    flexDirection: "column",
    justifyContent: "space-evenly",
    alignItems: "center",
    width: "100%",
  },
  summary2: {
    flexDirection: "row",
    justifyContent: "flex-end",
    //alignItems: "center",
    //width: "100%",
  },
  amount: {
    fontFamily: "open-sans-bold",
    fontSize: 16,
  },
  date: {
    fontFamily: "open-sans-bold",
    fontSize: 16,
    color: "#888",
  },
  detailItems: {
    width: "100%",
  },
  mainText: {
    fontFamily: "open-sans-bold",
    fontSize: 16,
  },
  mainText: {
    fontFamily: "open-sans-bold",
    fontSize: 16,
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    width: "100%",
  },
  icon: {
    flexDirection: "column",
    alignItems: "stretch",
    padding: 5,
  },
});

export default BankDetails;
