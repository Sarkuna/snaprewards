import React from "react";
import {
  TouchableOpacity,
  View,
  Text,
  StyleSheet,
  Platform,
  TouchableNativeFeedback,
  Image,
} from "react-native";

//import ENV from "../apiEnv";

const PendingDetail = (props) => {
  let TouchableCmp = TouchableNativeFeedback;

  if (Platform.OS === "android" && Platform.Version >= 21) {
    TouchableCmp = TouchableNativeFeedback;
  }

  return (
    <View style={styles.gridItem}>
      <View >
        <View style={styles.touchable}>
          <TouchableCmp onPress={props.onSelect}>
            <View>
              <View>
                
              </View>
            </View>
          </TouchableCmp>
        </View>
      </View>
      <View>
        <Text style={styles.invoiceNo}>Invoice No : {props.invoice_no}</Text>
        <Text style={styles.space}></Text>
        <Text style={styles.dateTime}>Created Date : {props.created_datetime}</Text>
        <Text style={styles.receiptDate}>Receipt Date : {props.date_of_receipt}</Text>
        <Text style={styles.name}>Reseler Name : {props.reseller_name}</Text>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  gridItem: {
    shadowColor: "#1d1b63",
    shadowOpacity: 0.26,
    shadowOffset: { width: 0, height: 2 },
    shadowRadius: 8,
    elevation: 5,
    borderRadius: 10,
    borderLeftColor: "black",
    borderStartColor: "black",
    borderColor: "#1e3545",
    height: 130,
    width: "90%",
    margin: 20,
    backgroundColor: "#a29ffc",
  },
  touchable: {
    borderRadius: 10,
    overflow: "hidden",
  },
  image: {
    height: "100%",
    width: "100%",
  },
  imageContainer: {
    height: "100%",
    width: "100%",
    borderTopLeftRadius: 10,
    borderTopRightRadius: 10,
    overflow: "hidden",
  },
  invoiceNo: {
    fontFamily: "open-sans-bold",
    fontSize: 20,
    fontWeight: "900",
    textAlign: "center",
  },
  dateTime: {
    fontFamily: "open-sans-bold",
    fontSize: 17,
    fontWeight: "500",
    textAlign: "center",
  },
  receiptDate: {
    fontFamily: "open-sans-bold",
    fontSize: 17,
    fontWeight: "500",
    textAlign: "center",
  },
  name: {
    fontFamily: "open-sans-bold",
    fontSize: 17,
    fontWeight: "500",
    textAlign: "center",
  },
  space: {
    fontFamily: "open-sans-bold",
    fontSize: 15,
    fontWeight: "400",
    textAlign: "center",
  }
});

export default PendingDetail;
